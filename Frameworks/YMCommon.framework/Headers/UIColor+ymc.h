//
//  UIColor+ym.h
//  YMCommonFrameWork
//
//  Created by liushilou on 16/12/21.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ymc)

// 颜色转换：iOS中（以#开头）十六进制的颜色转换为UIColor(RGB)
+ (UIColor *)colorWithHexString:(NSString *)color;

@end
