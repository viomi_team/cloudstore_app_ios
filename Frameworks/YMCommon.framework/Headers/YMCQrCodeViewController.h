//
//  YMQrCodeViewController.h
//  WaterPurifier
//
//  Created by liushilou on 16/11/15.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YMCQrCodeViewControllerDelegate <NSObject>
    
- (void)YMCQrCodeViewControllerScanLogin:(NSString *)clientID;
    
@end


@interface YMCQrCodeViewController : UIViewController

@property (nonatomic,assign) id<YMCQrCodeViewControllerDelegate> delegate;
    
@end
