//
//  YMOrderAPI.h
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//


#import "YMAPIManager.h"

@interface YMOrderAPI : YMCBaseAPIManager


/**
 【销售数据】数据分析接口

 @param type 日期类型
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchSalesAnalyse:(YMProfileType)type completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;


/**
 【销售数据】销售报表接口

 @param type 日期类型
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchSalesReport:(YMProfileType)type completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;


///**
// 【订单数据】
// @param beginDate 开始时间
// @param endDate 结束时间
// @param pageNum 请求页码
// @param block status:请求状态  message:错误信息  data:返回数据
// */
//- (void)fetchOrderDataWithBeginDate:(NSString *)beginDate endDate:(NSString *)endDate pageNum:(NSInteger)pageNum completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;

/**
 【订单数据】
 @param orderSource 订单来源
 @param beginDate 开始时间
 @param endDate 结束时间
 @param pageNum 请求页码
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchOrderDataWithSource:(YMOrderSource)orderSource BeginDate:(NSString *)beginDate endDate:(NSString *)endDate pageNum:(NSInteger)pageNum completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;


/**
 【获取订单详情】
 @param orderSource 订单来源
 @param orderCode 订单编号
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchOrderDetailsWithSource:(YMOrderSource)orderSource orderCode:(NSInteger)orderCode completeBlock:(void (^)(YMCRequestStatus, NSString *, NSDictionary *))block;


/**
 【门店日报】

 @param beginDate 开始时间
 @param endDate 结束时间
 @param storeName 门店名称
 @param agencyName 分销商名称
 @param pageNum 页码
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchStoreDailyDataWithBeginDate:(NSString *)beginDate endDate:(NSString *)endDate storeName:(NSString *)storeName agencyName:(NSString *)agencyName pageNum:(NSInteger)pageNum completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;


/**
 【订单概况】

 @param type 日期类型
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchOrderProfile:(YMProfileType)type completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;


/**
 【预付款订单】
 
 @param type 日期类型
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchAdPayOrders:(YMAdPayOrderType)type completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;

/**
 【预付款订单详情】
 
 @param beginDate 开始日期
 @param endDate 结束日期
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchAdPayOrdersDetails:(NSString *)beginDate endDate:(NSString *)endDate completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;

/**
 【预付款订单查询】
 
 @param data 请求参数
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchAdPayOrdersSearch:(NSDictionary *)data completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;

/**
 【经销商报表】
 
 @param type 类型
 @param date 时间
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchDealerReport:(YMAdPayOrderType)type lastDate:(NSString *)date completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;

/**
 【经销商报表详情】
 
 @param beginDate 开始时间
 @param endDate 结束时间
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchDealerReportDetails:(NSString *)beginDate endDate:(NSString *)endDate completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;

/**
 【经销商日报查询】
 
 @param data 请求参数
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchDealerReportSearch:(NSDictionary *)data completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;

/**
 【商品数据报表】
 
 @param dateType    请求类型（日、月、年）
 @param sourceType  渠道来源（云米、米家、天猫）
 */
- (void)fetchCommodityData:(YMCommodityDateType)dateType sourceType:(YMCommoditySourceType)sourceType successBlock:(void(^)(NSDictionary *data))successBlock failBlcok:(void(^)(YMCRequestStatus status,NSString *message))failBlcok;

/**
  【商品数据报表详情】
  
  @param dateType    日期类型（日、月、年）
  @param reportDate  日期
  @param sourceType  渠道来源（云米、米家、天猫）
  @param pageNum     页码
  */
- (void)fetchCommodityDetail:(YMCommodityDateType)dateType reportDate:(NSString *)reportDate sourceType:(YMCommoditySourceType)sourceType pageNum:(NSInteger)pageNum successBlock:(void(^)(NSDictionary *data))successBlock failBlcok:(void(^)(YMCRequestStatus status,NSString *message))failBlcok;

@end
