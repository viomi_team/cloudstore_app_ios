//
//  YMOrderAPI.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMOrderAPI.h"

@implementation YMOrderAPI

//【销售数据】数据分析接口
- (void)fetchSalesAnalyse:(YMProfileType)type completeBlock:(void (^)(YMCRequestStatus, NSString *, NSDictionary *))block
{
    NSString *urlString = @"";
    
    /*
     *区域管理员请求用的 url 与超级管理员不同，要分开写
     */
    if ([[YMUserManager shareinstance].roleId integerValue] == 21) {
        //城市运营的接口
//        urlString =[NSString stringWithFormat:@"%@channel/report/salesReport.json?token=%@&type=%d", YMBASEURL, [YMUserManager shareinstance].token, type];
        urlString =[NSString stringWithFormat:@"%@channel/report/salesReportNew.json?token=%@&type=%d", YMBASEURL, [YMUserManager shareinstance].token, type];
    } else {
        //超级管理员和区域管理员的接口
//        urlString = [NSString stringWithFormat:@"%@orders/analyse/channel/salesReport/eachLevel.json?token=%@&type=%d", YMBASEURL, [YMUserManager shareinstance].token, type];
        urlString = [NSString stringWithFormat:@"%@channel/report/salesReportNew/eachLevel.json?token=%@&type=%d&beginTime=&beginTime=", YMBASEURL, [YMUserManager shareinstance].token, type];
    }
    
    [self asynGetToUrl:urlString type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        block(status,message,data);
    }];
}

//【销售数据】销售报表接口
- (void)fetchSalesReport:(YMProfileType)type completeBlock:(void (^)(YMCRequestStatus, NSString *, NSDictionary *))block
{
//    NSString *urlString = [NSString stringWithFormat:@"%@channel/report/salesReport/subs.json?token=%@&type=%d", YMBASEURL, [YMUserManager shareinstance].token, type];
    
    NSString *urlString = [NSString stringWithFormat:@"%@channel/report/salesReportNew/subs.json?token=%@&type=%d", YMBASEURL, [YMUserManager shareinstance].token, type];
    
    [self asynGetToUrl:urlString type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, id data) {
        block(status,message,data);
    }];
}

/**
 【订单数据】
 @param orderSource 订单来源
 @param beginDate 开始时间
 @param endDate 结束时间
 @param pageNum 请求页码
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchOrderDataWithSource:(YMOrderSource)orderSource BeginDate:(NSString *)beginDate endDate:(NSString *)endDate pageNum:(NSInteger)pageNum completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block {
    
    NSString *urlString = @"";
    
//    if ([[YMUserManager shareinstance].roleId intValue] == 21) {  //区域管理员
//        urlString = [NSString stringWithFormat:@"%@orders/info/queryOrderProfitList.json?token=%@&beginTime=%@&dealPhase=&endTime=%@&orderCode=&pageNum=%ld&pageSize=10", YMBASEURL, [YMUserManager shareinstance].token, beginDate, endDate, (long)pageNum];
//    } else {   //非区域管理员
//        urlString = [NSString stringWithFormat:@"%@fd/order/list.json?beginTime=%@&channelId=&channelType=&endTime=%@&invoiceStatus=&needInvoice=&orderCode=&pageNum=%ld&pageSize=10&payStatus=&settleStatus=&token=%@", YMBASEURL, beginDate, endDate, (long)pageNum, [YMUserManager shareinstance].token];
//    }
    
    if(orderSource == YMOrderSourceYunmi) {
        //云分销订单
        if ([[YMUserManager shareinstance].roleId intValue] == 21) { //城市运营
            urlString = [NSString stringWithFormat:@"%@orders/info/queryOrderProfitList.json?token=%@&beginTime=%@&dealPhase=&endTime=%@&orderCode=&pageNum=%ld&pageSize=10", YMBASEURL, [YMUserManager shareinstance].token, beginDate, endDate, (long)pageNum];
        } else {   
            urlString = [NSString stringWithFormat:@"%@fd/order/list.json?beginTime=%@&channelId=&channelType=&endTime=%@&invoiceStatus=&needInvoice=&orderCode=&pageNum=%ld&pageSize=10&payStatus=&settleStatus=&token=%@", YMBASEURL, beginDate, endDate, (long)pageNum, [YMUserManager shareinstance].token];
        }
    }else {
        //第三方订单 超级管理员 仓库管理员 财务结算人员
        if([[YMUserManager shareinstance].roleId intValue] == 1 ||
           [[YMUserManager shareinstance].roleId intValue] == 40 ||
           [[YMUserManager shareinstance].roleId intValue] == 50) {
            urlString = [NSString stringWithFormat:@"%@order/third/list.json?token=%@&thirdOrderCode=&source=%d&payStatus=&orderStatus=&vmOrderCode=&sellerName=&receiverPhone=&beginTime=%@&endTime=%@&pageSize=10&currentPage=%ld",YMBASEURL,[YMUserManager shareinstance].token,orderSource,beginDate,endDate,(long)pageNum];
        }
    }
    
    [self asynGetToUrl:urlString type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, id data) {
        block(status,message,data);
    }];
}


/**
 【获取订单详情】
 @param orderSource 订单来源
 @param orderCode 订单编号
 @param block status:请求状态  message:错误信息  data:返回数据
 */- (void)fetchOrderDetailsWithSource:(YMOrderSource)orderSource orderCode:(NSInteger)orderCode completeBlock:(void (^)(YMCRequestStatus, NSString *, NSDictionary *))block
{
    NSString *urlString = @"";
    
//    if ([[YMUserManager shareinstance].roleId intValue] == 21) {
//        urlString = [NSString stringWithFormat:@"%@orders/info/queryOrderDetail/%ld?token=%@", YMBASEURL, (long)orderCode, [YMUserManager shareinstance].token];
//    } else {
//        urlString = [NSString stringWithFormat:@"%@fd/order/%ld.json?token=%@", YMBASEURL, (long)orderCode, [YMUserManager shareinstance].token];
//    }
   
    if(orderSource == YMOrderSourceYunmi) {
         //云分销订单详情
        if ([[YMUserManager shareinstance].roleId intValue] == 21) {
            urlString = [NSString stringWithFormat:@"%@orders/info/queryOrderDetail/%ld?token=%@", YMBASEURL, (long)orderCode, [YMUserManager shareinstance].token];
        } else {
            urlString = [NSString stringWithFormat:@"%@fd/order/%ld.json?token=%@", YMBASEURL, (long)orderCode, [YMUserManager shareinstance].token];
        }
    }else {
        //第三方订单 超级管理员 仓库管理员 财务结算人员
        if([[YMUserManager shareinstance].roleId intValue] == 1 ||
           [[YMUserManager shareinstance].roleId intValue] == 40 ||
           [[YMUserManager shareinstance].roleId intValue] == 50) {
            urlString = [NSString stringWithFormat:@"%@order/third/list.json?token=%@&thirdOrderCode=&source=%d&payStatus=&orderStatus=&vmOrderCode=%ld&sellerName=&receiverPhone=&beginTime=&endTime=&pageSize=10&currentPage=",YMBASEURL,[YMUserManager shareinstance].token,orderSource,(long)orderCode];
        }
    }

    [self asynGetToUrl:urlString type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, id data) {
        block(status,message,data);
    }];
}

//【门店日报】接口
- (void)fetchStoreDailyDataWithBeginDate:(NSString *)beginDate endDate:(NSString *)endDate storeName:(NSString *)storeName agencyName:(NSString *)agencyName pageNum:(NSInteger)pageNum completeBlock:(void (^)(YMCRequestStatus, NSString *, NSDictionary *))block
{
    NSString *urlString = [NSString stringWithFormat:@"%@channel/report/listChannelDailyReport.json?beginTime=%@&endTime=%@&pageNum=%ld&pageSize=12&channelName=%@&rootChannel&secondChannel=%@&token=%@", YMBASEURL, beginDate, endDate, (long)pageNum,storeName,agencyName, [YMUserManager shareinstance].token];

    [self asynGetToUrl:urlString type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, id data) {
        block(status,message,data);
    }];
}


//销售概况
- (void)fetchOrderProfile:(YMProfileType)type completeBlock:(void (^)(YMCRequestStatus, NSString *, NSDictionary *))block
{
    NSString *url = [NSString stringWithFormat:@"%@channel/report/salesReportNew/subs.json?token=%@&type=%d", YMBASEURL, [YMUserManager shareinstance].token, type];
    
    [self asynGetToUrl:url type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        block(status,message,data);
    }];
}


/**
 【预付款订单】
 
 @param type 日期类型
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchAdPayOrders:(YMAdPayOrderType)type completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block
{
    NSString *url = [NSString stringWithFormat:@"%@channel/report/queryPurchaseChart?token=%@&type=%d", YMBASEURL, [YMUserManager shareinstance].token, type];
    
    [self asynGetToUrl:url type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        block(status,message,data);
    }];
}

/**
 【预付款订单详情】
 
 @param beginDate 开始日期
 @param endDate 结束日期
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchAdPayOrdersDetails:(NSString *)beginDate endDate:(NSString *)endDate completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block
{
    NSString *url = [NSString stringWithFormat:@"%@channel/report/queryPurchaseChartDetail?token=%@&beginDate=%@&endDate=%@", YMBASEURL, [YMUserManager shareinstance].token, beginDate,endDate];
    
    [self asynGetToUrl:url type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        block(status,message,data);
    }];
}

/**
 【预付款订单查询】
 
 @param data 请求参数
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchAdPayOrdersSearch:(NSDictionary *)data completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block
{
    NSString *queryString = [data transFormToUrlString];
    NSString *url = [NSString stringWithFormat:@"%@channel/report/listPurchaseDailyReport?token=%@&%@", YMBASEURL, [YMUserManager shareinstance].token, queryString];
    
    [self asynGetToUrl:url type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        block(status,message,data);
    }];
}

/**
 【经销商报表】
 
 @param type 类型
 @param date 时间
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchDealerReport:(YMAdPayOrderType)type lastDate:(NSString *)date completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block
{
//    NSString *url = [NSString stringWithFormat:@"%@channel/report/queryChannelChart?token=%@&type=%@&lastDate=%@", YMBASEURL, [YMUserManager shareinstance].token, @(type),date];
    
    NSString *url = [NSString stringWithFormat:@"%@channel/report/queryChannelChart.json?token=%@&type=%@", YMBASEURL, [YMUserManager shareinstance].token,@(type)];
    
    [self asynGetToUrl:url type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        block(status,message,data);
    }];
}

/**
 【经销商报表详情】
 
 @param beginDate 开始时间
 @param endDate 结束时间
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchDealerReportDetails:(NSString *)beginDate endDate:(NSString *)endDate completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block
{
    NSString *url = [NSString stringWithFormat:@"%@channel/report/queryChannelChartDetail.json?token=%@&beginDate=%@&endDate=%@", YMBASEURL, [YMUserManager shareinstance].token,beginDate,endDate];
    
    [self asynGetToUrl:url type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        block(status,message,data);
    }];
}

/**
 【经销商日报查询】
 
 @param data 请求参数
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchDealerReportSearch:(NSDictionary *)data completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block
{
    NSString *queryString = [data transFormToUrlString];
    NSString *url = [NSString stringWithFormat:@"%@channel/report/listChannelDailyReport.json?token=%@&%@&type=1", YMBASEURL, [YMUserManager shareinstance].token, queryString];
    
    [self asynGetToUrl:url type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        block(status,message,data);
    }];
}

/**
 【商品数据报表】
 //http://note.youdao.com/share/?token=18BF54DEE9294C5FB5341020AF8F788C&gid=42130582#/
 @param dateType    请求类型（日、月、年）
 @param sourceType  渠道来源（云米、米家、天猫）
 */
- (void)fetchCommodityData:(YMCommodityDateType)dateType sourceType:(YMCommoditySourceType)sourceType successBlock:(void(^)(NSDictionary *data))successBlock failBlcok:(void(^)(YMCRequestStatus status,NSString *message))failBlcok
{
    NSInteger reportType = -1;
    
    switch (dateType) {
        case YMCommodityDateDay:
            reportType = 1;
            break;
        case YMCommodityDateMonth:
            reportType = 0;
            break;
        case YMCommodityDateYear:
            reportType = 6;
    }

    NSString *url = [NSString stringWithFormat:@"%@report/waresSales/query/salesDateStat.json?token=%@&reportType=%ld&sourceType=%li",YMBASEURL,[YMUserManager shareinstance].token,(long)reportType,(long)sourceType];
    
    [self asynGetToUrl:url type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, id data) {
        if(status == YMCRequestSuccess && [data isKindOfClass:[NSDictionary class]]) {
            successBlock(data);
        }else {
            failBlcok(status,message);
        }
    }];
}

/**
 【商品数据报表详情】
 //https://note.youdao.com/share/?token=BE7F1957EB3C461B82B830269A795D02&gid=42130582#/
 @param dateType    日期类型（日、月、年）
 @param reportDate  日期
 @param sourceType  渠道来源（云米、米家、天猫）
 @param pageNum     页码
 */
- (void)fetchCommodityDetail:(YMCommodityDateType)dateType reportDate:(NSString *)reportDate sourceType:(YMCommoditySourceType)sourceType pageNum:(NSInteger)pageNum successBlock:(void(^)(NSDictionary *data))successBlock failBlcok:(void(^)(YMCRequestStatus status,NSString *message))failBlcok
{
    NSInteger reportType = -1;
    
    switch (dateType) {
        case YMCommodityDateDay:
            reportType = 1;
            break;
        case YMCommodityDateMonth:
            reportType = 0;
            break;
        case YMCommodityDateYear:
            reportType = 6;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@report/waresSales/query/salesReport.json?token=%@&reportDate=%@&reportType=%ld&sourceType=%li&pageNum=%li&pageSize=10",YMBASEURL,[YMUserManager shareinstance].token,reportDate,(long)reportType,(long)sourceType,(long)pageNum];
    
    [self asynGetToUrl:url type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, id data) {
        if(status == YMCRequestSuccess && [data isKindOfClass:[NSDictionary class]]) {
            successBlock(data);
        }else {
            failBlcok(status,message);
        }
    }];
}

@end
