//
//  YMManagerAPI.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/25.
//  Copyright © 2016年 yunmi. All rights reserved.
//
#import "YMAPIManager.h"


@interface YMManagerAPI : YMCBaseAPIManager


/**
 获取【管理】根视图门店数量的数据

 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchStoreCountCompleteBlock:(void (^)(YMCRequestStatus status, NSString *message, NSDictionary *data))block;


/**
 获取【管理】根视图经销商数量的数据

 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchDealerCountCompleteBlock:(void (^)(YMCRequestStatus status, NSString *message, NSDictionary *data))block;


/**
 搜索店员的数据

 @param pageNum 页码
 @param contactName 用户名称
 @param phoneNum 手机号码,搜索时需要输入完整的11位手机号码，如13812345678（注：url 参数里 userName 表示 phoneNum）
 @param channelName 所属渠道
 @param roleId 促销员类型
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchStaffManagerSearchDataWithPageNum:(NSInteger)pageNum contactName:(NSString *)contactName phoneNum:(NSString *)phoneNum channelName:(NSString *)channelName roleId:(NSString *)roleId completeBlock:(void (^)(YMCRequestStatus status, NSString *message, NSDictionary *data))block;


/**
 获取门店管理的数据

 @param name 搜索条件（默认无）
 @param pageNum 页码
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchStoreManagerDataWithName:(NSString *)name pageNum:(NSInteger)pageNum completeBlock:(void (^)(YMCRequestStatus status, NSString *message, NSDictionary *data))block;


/**
 获取经销商管理的数据

 @param name 搜索条件（默认无）
 @param pageNum 页码
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchDealerManagerDataWithName:(NSString *)name pageNum:(NSInteger)pageNum completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;



/**
 获取门店管理和经销商管理详情页的图片 URL

 @param imageId 参数 ID
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)fetchImageUrlWithId:(NSInteger)imageId completeBlock:(void (^)(YMCRequestStatus status,NSString *message,NSDictionary *data))block;

@end
