//
//  YMManagerAPI.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/25.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMManagerAPI.h"
#import "YMUserManager.h"

@implementation YMManagerAPI

// 获取【管理】根视图门店数量的数据
- (void)fetchStoreCountCompleteBlock:(void (^)(YMCRequestStatus, NSString *, NSDictionary *))block
{
    NSString *urlString = [NSString stringWithFormat:@"%@channel/info/listSubChannels.json?token=%@&name=&pageNum=1&pageSize=2&templateId=&type=2&divisionCode=", YMBASEURL, [YMUserManager shareinstance].token];
    
    [self asynGetToUrl:urlString type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, id data) {
        block(status, message, data);
    }];
}

//获取【管理】根视图经销商数量的数据
- (void)fetchDealerCountCompleteBlock:(void (^)(YMCRequestStatus, NSString *, NSDictionary *))block
{
    NSString *urlString = [NSString stringWithFormat:@"%@channel/info/listSubChannels.json?token=%@&name=&pageNum=1&pageSize=2&templateId=&type=1&divisionCode=", YMBASEURL, [YMUserManager shareinstance].token];
    
    [self asynGetToUrl:urlString type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, id data) {
        block(status, message, data);        
    }];
}

//搜索店员的数据
- (void)fetchStaffManagerSearchDataWithPageNum:(NSInteger)pageNum contactName:(NSString *)contactName phoneNum:(NSString *)phoneNum channelName:(NSString *)channelName roleId:(NSString *)roleId completeBlock:(void (^)(YMCRequestStatus, NSString *, NSDictionary *))block
{
    NSString *urlString = [NSString stringWithFormat:@"%@channel/user/listChannelUser?token=%@&pageNum=%ld&pageSize=50&username=%@&contactName=%@&role=%@&channelName=%@&root=true&beginTime=&endTime=", YMBASEURL, [YMUserManager shareinstance].token, (long)pageNum, phoneNum, contactName, roleId, channelName];
    
    [self asynGetToUrl:urlString type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, id data) {
        block(status, message, data);
    }];
}

//获取门店管理的数据
- (void)fetchStoreManagerDataWithName:(NSString *)name pageNum:(NSInteger)pageNum completeBlock:(void (^)(YMCRequestStatus, NSString *, NSDictionary *))block
{
    NSString *urlString = [NSString stringWithFormat:@"%@channel/info/listSubChannels.json?token=%@&name=%@&pageNum=%ld&pageSize=12&templateId=&type=2", YMBASEURL, [YMUserManager shareinstance].token, name, (long)pageNum];
    
    [self asynGetToUrl:urlString type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, id data) {
        block(status, message, data);
    }];
}

//获取经销商管理的数据
- (void)fetchDealerManagerDataWithName:(NSString *)name pageNum:(NSInteger)pageNum completeBlock:(void (^)(YMCRequestStatus, NSString *, NSDictionary *))block
{
    NSString *urlString = [NSString stringWithFormat:@"%@channel/info/listSubChannels.json?token=%@&name=%@&pageNum=%ld&pageSize=50&templateId=&type=1&divisionCode=", YMBASEURL, [YMUserManager shareinstance].token, name, (long)pageNum];
    
    [self asynGetToUrl:urlString type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, id data) {
        block(status, message, data);
    }];
}

//获取门店管理和经销商管理详情页的图片 URL
- (void)fetchImageUrlWithId:(NSInteger)imageId completeBlock:(void (^)(YMCRequestStatus, NSString *, NSDictionary *))block
{
    NSString *urlString = [NSString stringWithFormat:@"%@channel/info/queryChannelAuthAttr?token=%@&channelId=%ld", YMBASEURL, [YMUserManager shareinstance].token, (long)imageId];
    
    [self asynGetToUrl:urlString type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, id data) {
        block(status, message, data);
    }];
}

@end
