//
//  YMAppUpdateAPI.h
//  yunSale
//
//  Created by liushilou on 16/11/30.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMAPIManager.h"

@interface YMAppUpdateAPI : YMCBaseAPIManager

- (void)checkVersion;

+ (NSString *)appUpdateUrl;

+ (NSString *)latestAppversion;

@end
