//
//  YMAppUpdateAPI.m
//  yunSale
//
//  Created by liushilou on 16/11/30.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMAppUpdateAPI.h"
#import "YMAppUpdateViewController.h"

@implementation YMAppUpdateAPI

- (void)checkVersion
{
    
    NSString *url = [NSString stringWithFormat:@"https://itunes.apple.com/cn/lookup?id=%@",YM_APPID];
    [self asynGetToUrl:url type:YMCRequestTypeAppstore completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        if (status == YMCRequestSuccess) {
            NSString *version = [data objectForKey:@"version"];
            NSString *trackViewUrl = [data objectForKey:@"trackViewUrl"];
            NSString *releaseNotes = [data objectForKey:@"releaseNotes"];
            
            NSNumber *lasttime = [self latestTime];
            NSString *lastversion = [YMAppUpdateAPI latestAppversion];
            NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
            if (lasttime) {
                NSInteger days = (currentTime - lasttime.integerValue)/(24 * 60 *60);
                //提示版本更新后，如果点击忽略，并且记录的版本等于最新版本时，则要过7天才给提示
#warning 上线前需要改为实际值，1为测试使用
                if (days < 7 && [version isEqualToString:lastversion]) {
                    return;
                }
            }
            
            NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
            if ([version compare:currentVersion options:NSNumericSearch] == NSOrderedDescending){
                
                [self saveLatestAppversion:version url:trackViewUrl];
                
                YMAppUpdateViewController *controller = [[YMAppUpdateViewController alloc] initWithVersion:version notes:releaseNotes url:trackViewUrl];
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:controller animated:YES completion:^{
                    
                            }];
            }
        }
    }];
}

- (void)saveLatestAppversion:(NSString *)version url:(NSString *)url
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:version forKey:@"ym_latestversion"];
    [defaults setObject:url forKey:@"ym_trackViewUrl"];
    
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    [defaults setObject:@(currentTime) forKey:@"ym_version_updatetime"];
    
    [defaults synchronize];
}

- (NSNumber *)latestTime
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *latesttime = [defaults objectForKey:@"ym_version_updatetime"];
    return latesttime;
}

+ (NSString *)latestAppversion
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *latestversion = [defaults objectForKey:@"ym_latestversion"];
    return latestversion;
}

+ (NSString *)appUpdateUrl
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *latestversion = [defaults objectForKey:@"ym_trackViewUrl"];
    return latestversion;
}






@end
