//
//  YMUserAPI.h
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMAPIManager.h"

@interface YMUserAPI : YMCBaseAPIManager


/**
 用户登陆及登陆后的一些处理

 @param userName 用户名
 @param password 密码
 @param block 请求成功的Block
 */
- (void)loginWithUserName:(NSString *)userName password:(NSString *)password completeBlock:(void (^)(YMCRequestStatus status, NSString* message, NSDictionary *data))block;


/**
 找回密码

 @param phoneString 手机号码
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)recoverPasswordWithPhoneString:(NSString *)phoneString completeBlock:(void (^)(YMCRequestStatus status, NSString* message, NSDictionary *data))block;


/**
 检查验证码

 @param captchaString 验证码
 @param phoneString 手机号码
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)sendCaptchaString:(NSString *)captchaString withPhoneString:(NSString *)phoneString completeBlock:(void (^)(YMCRequestStatus status, NSString* message, NSDictionary *data))block;


/**
 修改用户密码

 @param oldPwd 旧密码
 @param newPwd 新密码
 @param block status:请求状态  message:错误信息  data:返回数据
 */
- (void)changePasswordWitholdPassword:(NSString *)oldPwd newPassword:(NSString *)newPwd completeBlock:(void (^)(YMCRequestStatus status, NSString* message, NSDictionary *data))block;

@end
