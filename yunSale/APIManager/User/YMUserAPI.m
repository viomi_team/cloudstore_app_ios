//
//  YMUserAPI.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMUserAPI.h"
#import "YMSecurity.h"

@implementation YMUserAPI

//用户登陆及登陆后的一些处理
- (void)loginWithUserName:(NSString *)userName password:(NSString *)password completeBlock:(void (^)(YMCRequestStatus status, NSString* message, NSDictionary *data))block
{
    NSString *udid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *urlString = [NSString stringWithFormat:@"%@channel/login/1.json?account=%@&pwdMD5=%@&sourceSystem=iOS&version=%@&deviceId=%@&systemVersion=%@", YMBASEURL, userName, [YMSecurity md5:password],currentVersion,udid,systemVersion];
    [self asynGetToUrl:urlString type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, id data) {
        block(status, message, data);
    }];
}

//找回密码
- (void)recoverPasswordWithPhoneString:(NSString *)phoneString completeBlock:(void (^)(YMCRequestStatus status, NSString* message, NSDictionary *data))block
{
    NSString *urlString = [NSString stringWithFormat:@"%@channel/login/sendAuthCode?mobile=%@", YMBASEURL, phoneString];
    
    [self asynPostToUrl:urlString widthData:nil type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        block(status, message, data);
    }];
}

//检查验证码
- (void)sendCaptchaString:(NSString *)captchaString withPhoneString:(NSString *)phoneString completeBlock:(void (^)(YMCRequestStatus status, NSString* message, NSDictionary *data))block
{
    NSString *urlString = [NSString stringWithFormat:@"%@channel/login/recoverPassword?mobile=%@&authCode=%@", YMBASEURL, phoneString, captchaString];
    
    [self asynPostToUrl:urlString widthData:nil type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        block(status, message, data);
    }];
}

//修改用户密码
- (void)changePasswordWitholdPassword:(NSString *)oldPwd newPassword:(NSString *)newPwd completeBlock:(void (^)(YMCRequestStatus status, NSString* message, NSDictionary *data))block
{
    NSString *urlString = [NSString stringWithFormat:@"%@channel/user/changePassword?token=%@&oldPwd=%@&newPwd=%@", YMBASEURL, [YMUserManager shareinstance].token, [YMSecurity md5:oldPwd], [YMSecurity md5:newPwd]];
    
    [self asynPutToUrl:urlString widthData:nil type:YMCRequestTypeYunmi completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        block(status, message, data);
    }];
}

@end
