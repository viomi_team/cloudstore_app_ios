//
//  Header.h
//  yunSale
//
//  Created by ChengMinZhang on 2017/2/16.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#ifndef Header_h
#define Header_h

#import <MBProgressHUD.h>
#import <MJExtension.h>
#import <YMCommon/YMCBaseAPIManager.h>

//生产环境 商城
#define YMBASEURL YM_ENVIRONMENT==1?@"https://s.viomi.com.cn/services/":@"https://vj.viomi.com.cn/services/"

//#define YMBASEURL @"http://192.168.1.250/services/"


#ifndef YMAdPayOrderType_ENUM
#define YMAdPayOrderType_ENUM
typedef NS_ENUM(int, YMAdPayOrderType) {
    YMAdPayOrderDay = 1,
    YMAdPayOrderWeek,
    YMAdPayOrderMonth
};
#endif


#ifndef YMProfileType_ENUM
#define YMProfileType_ENUM
typedef NS_ENUM(int, YMProfileType) {
    YMProfileTypeMonth = 0,
    YMProfileTypeDay = 1,
    YMProfileTypeAll = 2,
    YMProfileTypeWeek = 3
    
};
#endif

#ifndef YMOrderSource_ENUM
#define YMOrderSource_ENUM
typedef NS_ENUM(int,YMOrderSource) {
    YMOrderSourceYunmi = 0,
    YMOrderSourceMijia = 1,
    YMOrderSourceTianmao = 2,
};
#endif

#ifndef YMCommoditySource_ENUM
#define YMCommoditySource_ENUM
typedef NS_ENUM(NSInteger, YMCommoditySourceType) {
    YMCommoditySourceViomi = 1,   //云米商品数据
    YMCommoditySourceMijia,       //米家商品数据
    YMCommoditySourceTianmao      //天猫商品数据
};
#endif

#ifndef YMCommodityDate_ENUM
#define YMCommodityDate_ENUM
typedef NS_ENUM(NSInteger, YMCommodityDateType) {
    YMCommodityDateDay = 0,    //日数据
    YMCommodityDateMonth ,      //月数据
    YMCommodityDateYear         //年数据
};
#endif

#endif /* Header_h */
