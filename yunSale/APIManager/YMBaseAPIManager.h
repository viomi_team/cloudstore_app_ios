//
//  YMBaseAPIManager.h
//  WaterPurifier
//
//  Created by 刘世楼 on 16/5/16.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import <MBProgressHUD.h>
#import <MJExtension.h>
#import "YMNetErrorView.h"



//已崩～各种返回格式～，妈蛋的。。
typedef NS_ENUM(NSInteger, YMRequestType) {
    YMRequestTypeAppstore,
    YMRequestTypeXiaomi,
    YMRequestTypeYunmi,
    YMRequestTypeReact,
    YMRequestTypeQuanxin,//权新的接口
    YMRequestTypeBaidu,
    YMRequestTypeImage,
};

#ifndef YMAdPayOrderType_ENUM
#define YMAdPayOrderType_ENUM
typedef NS_ENUM(int, YMAdPayOrderType) {
    YMAdPayOrderDay = 1,
    YMAdPayOrderWeek,
    YMAdPayOrderMonth
};
#endif


#ifndef YMProfileType_ENUM
#define YMProfileType_ENUM
typedef NS_ENUM(int, YMProfileType) {
    YMProfileTypeMonth = 0,
    YMProfileTypeDay = 1,
    YMProfileTypeAll = 2,
    YMProfileTypeWeek = 3
    
};
#endif


//生产环境 商城
#define YMBASEURL YM_ENVIRONMENT==1?@"https://s.viomi.com.cn/services/":@"https://vj.viomi.com.cn/services/"

//#define YMBASEURL @"http://192.168.1.250/services/"



//@"http://viomi-vstore-test.mi-ae.com.cn/services/"
//#define YMBASEURL @"http://192.168.1.250:8080/services/"
//#define YMBASEURL @"http://192.168.1.100:8080/services/"

//#define YMBASEURL @"https://vj.viomi.com.cn/services/"

//权新(老人关怀)
#define YMCAREOLDERURL @"http://viomi-app-api.mi-ae.net/api/care"

#define YMWEIXINACCESSTOKENURL YM_ENVIRONMENT==1?@"https://vmall.viomi.com.cn/api/app/auth":@"https://vwater-wechat.mi-ae.net/api/app/auth"

//测试环境
//#define YMBASEURL @"http://viomi-vstore-test.mi-ae.com.cn/services/"

//extern CGFloat const MBProgressMaxOffset;




@interface YMBaseAPIManager : NSObject

//+(YMBaseAPIManager*)shareinstance;

-(void)asynPostToUrl:(NSString*)url widthData:(NSDictionary*)data type:(YMRequestType)type completeBlock:(void (^)(YMRequestStatus status,NSString *message,NSDictionary *data))block;

-(void)asynGetToUrl:(NSString*)url type:(YMRequestType)type completeBlock:(void (^)(YMRequestStatus status,NSString *message,id data))block;

-(void)asynPutToUrl:(NSString*)url widthData:(NSDictionary*)data type:(YMRequestType)type completeBlock:(void (^)(YMRequestStatus status,NSString *message,NSDictionary *data))block;

//-(void)asynGetimageByurl:(NSString *)imageurl completeBlock:(void (^)(requestStatus status,UIImage *image))block;

- (void)uploadImageToUrl:(NSString *)url images:(NSArray *)images completeBlock:(void (^)(YMRequestStatus status,NSString *message,id data))block;



- (void)cancle;

@end
