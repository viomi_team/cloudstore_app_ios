//
//  YMBaseAPIManager.m
//  WaterPurifier
//
//  Created by 刘世楼 on 16/5/16.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "YMBaseAPIManager.h"
#import "YMLoginViewController.h"

@interface YMBaseAPIManager()

@property (nonatomic,strong) AFHTTPRequestOperationManager *manager;

@end


@implementation YMBaseAPIManager


-(void)asynPostToUrl:(NSString *)url widthData:(NSDictionary *)data type:(YMRequestType)type completeBlock:(void (^)(YMRequestStatus, NSString *, NSDictionary *))block
{
    //加入log
    NSLog(@"请求url：%@", url);
    NSLog(@"请求参数:%@",data);
    
    NSString *requestUrl = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self requestOperationManager:type];

    //发送请求
    [self.manager POST:requestUrl parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);

        [self handleRespondata:responseObject type:type url:url completeBlock:^(YMRequestStatus status, NSString *message, NSDictionary *data) {
            block(status,message,data);
        }];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self handleError:error completeBlock:^(YMRequestStatus status, NSString *message) {
            block(status,message,nil);
        }];
    }];

}


-(void)asynPutToUrl:(NSString*)url widthData:(NSDictionary*)data type:(YMRequestType)type completeBlock:(void (^)(YMRequestStatus status,NSString *message,NSDictionary *data))block
{
  //加入log
  NSLog(@"请求url：%@", url);
  NSLog(@"请求参数:%@",data);
  
    NSString *requestUrl = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
  [self requestOperationManager:type];
  
  //发送请求
  [self.manager PUT:requestUrl parameters:data success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
    NSLog(@"JSON: %@", responseObject);
    
    [self handleRespondata:responseObject type:type url:url completeBlock:^(YMRequestStatus status, NSString *message, NSDictionary *data) {
      block(status,message,data);
    }];
  } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
      
      [self handleError:error completeBlock:^(YMRequestStatus status, NSString *message) {
          block(status,message,nil);
      }];
      
  }];
  
}

-(void)asynGetToUrl:(NSString *)url type:(YMRequestType)type completeBlock:(void (^)(YMRequestStatus, NSString *, id))block
{
    NSLog(@"url:%@",url);
    
    NSString *requestUrl = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self requestOperationManager:type];

    [self.manager GET:requestUrl parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSLog(@"JSON: %@", responseObject);

        [self handleRespondata:responseObject type:type url:url completeBlock:^(YMRequestStatus status, NSString *message, NSDictionary *data) {
          block(status,message,data);
        }];



    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        
        [self handleError:error completeBlock:^(YMRequestStatus status, NSString *message) {
            block(status,message,nil);
        }];
        
    }];
  
}


- (void)uploadImageToUrl:(NSString *)url images:(NSArray *)images completeBlock:(void (^)(YMRequestStatus, NSString *, id))block
{
    NSLog(@"url:%@",url);
    
    NSString *requestUrl = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self requestOperationManager:YMRequestTypeImage];

    [self.manager POST:requestUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {

        for (UIImage *image in images) {
            NSInteger index = [images indexOfObject:image];
            NSString *filename = [NSString stringWithFormat:@"%@.jpg",@(index)];

            [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 0.2) name:@(index).stringValue fileName:filename mimeType:@"image/jpg"];
        }

    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *responsedatas = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];

        NSLog(@"JSON: %@", responsedatas);

        [self handleRespondata:responsedatas type:YMRequestTypeYunmi url:url completeBlock:^(YMRequestStatus status, NSString *message, NSDictionary *data) {
            block(status,message,data);
        }];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

        [self handleError:error completeBlock:^(YMRequestStatus status, NSString *message) {
            block(status,message,nil);
        }];

    }];
}




- (void)cancle
{
  if (self.manager) {
    [self.manager.operationQueue cancelAllOperations];
  }
}

- (void)handleError:(NSError *)error completeBlock:(void (^)(YMRequestStatus status,NSString *message))block
{
    NSLog(@"Error: %@", error);
    NSString *errmsg = nil;
    if (error.code == -1009) {
        errmsg = @"网络连接失败，请检查网络！";
        block(YMRequestNetError,errmsg);
    }else if (error.code == -1001) {
        errmsg = @"网络连接超时，请重试！";
        block(YMRequestNetError,errmsg);
    }else if (error.code == -999) {
//        errmsg = @"取消连接";
//        block(YMRequestCancle,errmsg);
    }else{
        errmsg = @"加载数据失败，请重试！";
        block(YMRequestError,errmsg);
    }
}

- (void)handleRespondata:(id)responseObject type:(YMRequestType)type url:(NSString *)url completeBlock:(void (^)(YMRequestStatus status,NSString *message,id data))block
{
    if (type == YMRequestTypeAppstore) {
        NSNumber *resultCount = [responseObject objectForKey:@"resultCount"];
        if (resultCount.integerValue > 0) {
            NSArray *results = [responseObject objectForKey:@"results"];
            NSDictionary *appInfo = results[0];
            block(YMRequestSuccess,@"",appInfo);
        }else{
            block(YMRequestError,@"获取app版本失败",nil);
        }
    }else if(type == YMRequestTypeXiaomi){
        NSString *code = [responseObject objectForKey:@"code"];
        NSString *msg = [responseObject objectForKey:@"description"];
        if (code.integerValue == 0) {
            NSDictionary *datas = [responseObject objectForKey:@"data"];
            block(YMRequestSuccess,nil,datas);
        }else{
            block(YMRequestError,msg,responseObject);
        }
    }else if(type == YMRequestTypeYunmi){

        NSDictionary *mobBaseRes = [responseObject objectForKey:@"mobBaseRes"];
        if (!mobBaseRes) {
            mobBaseRes = [responseObject objectForKey:@"commonGenericResultRes"];
            if (!mobBaseRes) {
                mobBaseRes = responseObject;
            }
        }
        NSNumber *code = [mobBaseRes objectForKey:@"code"];
        if (code.integerValue == 100) {
        
            block(YMRequestSuccess,@"",mobBaseRes);
      
        }else if (code.integerValue == 919){
            //token过期。。怎么处理。。
            [self tokenOuttime];
//            NSString *errmsg = [mobBaseRes objectForKey:@"desc"];
//            block(YMRequestError,errmsg,nil);
        }else if (code.integerValue == 902){
            NSString *errmsg = [mobBaseRes objectForKey:@"desc"];
            block(YMRequestPassWordError,errmsg,nil);
        }else{
            NSString *errmsg = [mobBaseRes objectForKey:@"desc"];
            block(YMRequestError,errmsg,nil);
        }
    }else if(type == YMRequestTypeReact){
        NSNumber *total = [responseObject objectForKey:@"total"];
        if (total.integerValue > 0) {
            NSArray *data = [responseObject objectForKey:@"data"];
            NSDictionary *info = [data objectAtIndex:0];
            block(YMRequestSuccess,@"",info);
        }else{
            block(YMRequestError,@"失败",nil);
        }
    }else if(type == YMRequestTypeQuanxin){
        NSNumber *code = [responseObject objectForKey:@"code"];
        if (code.integerValue == 0) {
            NSDictionary *data = [responseObject objectForKey:@"data"];
            block(YMRequestSuccess,@"",data);
        }else if (code.integerValue == 1){
            //1表示没有记录，也就是老人关怀为关闭，SB的逻辑
            if ([url isEqualToString:YMCAREOLDERURL]) {
                block(YMRequestSuccess,@"",nil);
            }else{
                NSString *errmsg = [responseObject objectForKey:@"msg"];
                block(YMRequestError,errmsg,nil);
            }
        }else{
            NSString *errmsg = [responseObject objectForKey:@"msg"];
            block(YMRequestError,errmsg,nil);
        }
    }else if(type == YMRequestTypeBaidu){
        NSString *result = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"%@",result);
        NSInteger bindex = [result rangeOfString:@"("].location;
        NSInteger eindex = [result rangeOfString:@")"].location;
        NSString *resultJsonStr = [result substringWithRange:NSMakeRange(bindex+1, eindex - bindex-1)];
        NSLog(@"resultJsonStr:%@",resultJsonStr);
        NSData *resultJsonData = [resultJsonStr dataUsingEncoding:NSUTF8StringEncoding];
        NSError *err;
        NSDictionary *resultJson = [NSJSONSerialization JSONObjectWithData:resultJsonData
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:&err];
        NSLog(@"resultJson:%@",resultJson);

        NSNumber *status = [resultJson objectForKey:@"status"];
        if (status.integerValue == 0) {
            NSDictionary *result = [resultJson objectForKey:@"result"];
            block(YMRequestSuccess,@"",result);
        }else{
            NSString *errmsg = [resultJson objectForKey:@"message"];
            block(YMRequestError,errmsg,nil);
        }
    }

}

- (void)requestOperationManager:(YMRequestType)type
{
    if (!self.manager) {
        self.manager = [AFHTTPRequestOperationManager manager];
    }
    
    if (type == YMRequestTypeBaidu || type == YMRequestTypeImage) {
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }else{
        //申明返回的结果是json类型
        self.manager.responseSerializer = [AFJSONResponseSerializer serializer];

    }
    //申明请求的数据是json类型
    
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
//    serializer.timeoutInterval = 10; //这么设置会导致：无论有没有网络，都要加载10秒

    self.manager.requestSerializer = serializer;
    // 设置超时时间
    [self.manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    self.manager.requestSerializer.timeoutInterval = 8;
    [self.manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
}

- (void)tokenOuttime
{
    //token过期，重新登录
    if ([YMUserManager shareinstance].token) {
        [[YMUserManager shareinstance] logout];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"登录已过期，请重新登录" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"重新登录" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            UITabBarController *tabbarController = (UITabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
            for (UINavigationController *controller in tabbarController.viewControllers) {
                [controller popToRootViewControllerAnimated:NO];
            }
            
            YMLoginViewController *loginController = [[YMLoginViewController alloc] init];
            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:loginController animated:YES completion:^{
                
            }];
        }]];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:^{
            
        }];
    }
}


@end
