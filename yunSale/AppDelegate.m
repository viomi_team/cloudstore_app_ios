//
//  AppDelegate.m
//  yunSale
//
//  Created by liushilou on 16/11/1.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "AppDelegate.h"
#import "YMTabBarController.h"
#import <UIImageView+WebCache.h>
#import "UMMobClick/MobClick.h"
#import <YMCommon/YMCommon.h>

//#if DEBUG
//#import <FBMemoryProfiler/FBMemoryProfiler.h>
//#import <FBRetainCycleDetector/FBRetainCycleDetector.h>
//#import "CacheCleanerPlugin.h"
//#import "RetainCycleLoggerPlugin.h"
//#endif

@interface AppDelegate ()

//#if DEBUG
//@property (nonatomic, strong) FBMemoryProfiler *memoryProfiler;
//#endif

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[YMCProjectConfig shareInstance] userDefaultNavigationBarStyle];
    
    YMTabBarController *tabBarController = [[YMTabBarController alloc] init];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = tabBarController;
    [self.window makeKeyAndVisible];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    //zcm add： 友盟统计服务
    UMConfigInstance.appKey = @"5994ff7f734be41d50000726";
    UMConfigInstance.channelId = @"App Store";
    [MobClick startWithConfigure:UMConfigInstance];//配置以上参数后调用此方法初始化SDK！
    [MobClick setEncryptEnabled:YES];
    
    //zcm add: 有些图片是以文件格式返回，这边设置为全部都接受，有些图片才能显示
    [[SDWebImageManager sharedManager].imageDownloader setValue:nil forHTTPHeaderField:@"Accept"];
    
//#if DEBUG
//    NSArray *filters = @[FBFilterBlockWithObjectIvarRelation([UIView class], @"_subviewCache"),
//                         FBFilterBlockWithObjectIvarRelation([UIPanGestureRecognizer class], @"_internalActiveTouches")];
//    
//    FBObjectGraphConfiguration *configuration =
//    [[FBObjectGraphConfiguration alloc] initWithFilterBlocks:filters
//                                         shouldInspectTimers:NO];
//    
//    self.memoryProfiler = [[FBMemoryProfiler alloc] initWithPlugins:@[[CacheCleanerPlugin new],
//                                                                 [RetainCycleLoggerPlugin new]]
//                              retainCycleDetectorConfiguration:configuration];
//    [self.memoryProfiler enable];
//#endif
    
//    // 1 注册统计服务
//    [MiStatSDK registerMiStat:@"channel"];
//    
//    // 2 打点 统计
//    [MiStatSDK setUploadPolicy:UPLOAD_POLICY_TYPE_LAUNCH interval:1];
//    
//    // 3 crash 统计
//    [MiStatCrashReporter enabledCrashReportWithMode:MI_STAT_CRASH_REPORTER_MODE_MANUAL];
//    if ([MiStatCrashReporter hasPendingCrashReport]) {
//        [MiStatCrashReporter triggerUploadCrashReport];
//    }
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - MiAAnalyticsNetworkDelegate
//- (NSString*)mistatAnalyticsNetwork:(NSString*)url
//{
//    return url;
//}


@end
