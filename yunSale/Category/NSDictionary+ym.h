//
//  NSDictionary+YM.h
//  yunSale
//
//  Created by liushilou on 16/11/1.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (YM)

- (NSString*)jsonString;

- (NSString*)requestString;

- (id)notNullObjectForKey:(NSString *)key;

@end
