//
//  NSDictionary+YM.m
//  yunSale
//
//  Created by liushilou on 16/11/1.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "NSDictionary+YM.h"

@implementation NSDictionary (YM)

- (NSString*)jsonString {
    NSError *parseError = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]; 
}

- (NSString*)requestString
{
    NSMutableArray *query = [NSMutableArray array];
    
    for (id key in self) {
        id value = [self objectForKey:key];
        [query addObject:[NSString stringWithFormat:@"%@=%@",
                          [key description],
                          [value description]]];
    }
    
    return [query componentsJoinedByString:@"&"];
}

- (id)notNullObjectForKey:(NSString *)key {
    id object = [self objectForKey:key];
    if ([object isKindOfClass:[NSNull class]]) {
        object = nil;
    }
    return object;
}


@end
