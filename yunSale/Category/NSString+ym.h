//
//  NSString+YM.h
//  yunSale
//
//  Created by liushilou on 16/11/30.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (YM)

//去掉左右两端空格和换行符
- (NSString *)noWhitespaceAndNewlineCharacterString;

//去掉左右两端空格
- (NSString *)noWhitespaceCharacterString;

 //只能包含“字母”，“数字”长度6~20
- (BOOL)isValidPassword;

//验证手机号，只验证11位数字
- (BOOL)isValidPhone;

- (CGFloat)widthWithFontsize:(CGFloat)size;

- (CGFloat)heightWithWidth:(CGFloat)width fontsize:(CGFloat)size;

//判断字符串是否为空字符串，（nil、""、"(null)"、"<null>"）都判断为空
+ (BOOL)isEmptyString:(NSString *)string;

- (BOOL)isEmpty;

//判断字符串是否存在空格
- (BOOL)isContainSpace;

@end
