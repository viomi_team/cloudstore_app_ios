//
//  NSString+YM.M
//  yunSale
//
//  Created by liushilou on 16/11/30.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "NSString+YM.h"

@implementation NSString (YM)

- (NSString *)noWhitespaceAndNewlineCharacterString {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)noWhitespaceCharacterString {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (BOOL)isValidPassword {
    //只能包含“字母”，“数字”长度6~20
    
    NSString *regex = @"^[a-zA-Z0-9]{6,20}$";
    
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    
    return [pre evaluateWithObject:self];
 
}

- (BOOL)isValidPhone {
    //不要检测的太细，不清楚哪些号码是有效的:以1开始的11位数字
    NSString *pattern = @"^1\\d{10}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:self];
    return isMatch;
}

- (CGFloat)widthWithFontsize:(CGFloat)size {
    CGFloat width = [self boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, DBL_MIN) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:YM_FONT(size)} context:nil].size.width;
    return width;
}

- (CGFloat)heightWithWidth:(CGFloat)width fontsize:(CGFloat)size {
    CGFloat height = [self boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:YM_FONT(size)} context:nil].size.height;
    return height;
}

+ (BOOL)isEmptyString:(NSString *)string {
    if (string && ![string isEqual:[NSNull null]] && ![string isEqual:@"(null)"] && ![string isEqualToString:@"<null>"] && [string length] > 0) {
        return NO;
    }
    return YES;
}

- (BOOL)isEmpty {
    if (self && ![self isEqual:[NSNull null]] && ![self isEqual:@"(null)"] && ![self isEqualToString:@"<null>"] && [self length] > 0) {
        return NO;
    }
    return YES;
}

- (BOOL)isContainSpace {
    NSRange range = [self rangeOfString:@" "];
    
    //判断字符串是否存在空格
    if (range.location != NSNotFound) {
        return NO;
    }
    return YES;
}

@end
