//
//  UIImage+YM.h
//  WaterPurifier
//
//  Created by liushilou on 16/5/19.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (YM)

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

@end
