//
//  UIImage+YM.m
//  WaterPurifier
//
//  Created by liushilou on 16/5/19.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "UIImage+YM.h"

@implementation UIImage (YM)

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
  CGRect rect = CGRectMake(0, 0, size.width, size.height);
  UIGraphicsBeginImageContext(size);
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextSetFillColorWithColor(context, [color CGColor]);
  CGContextFillRect(context, rect);
  UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return img;
}

@end
