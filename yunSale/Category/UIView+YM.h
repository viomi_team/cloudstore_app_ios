//
//  UIView+YM.h
//  yunSale
//
//  Created by 谢立颖 on 2016/12/21.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIView (YM)


/**
 绘制一个包含顶部和底部分割细线的宽、高、背景颜色分别为width、height、color的矩形视图
 
 @param width 宽
 @param height 高
 @param color 背景颜色
 @return 需要绘制的视图
 */
+ (UIView *)drawLineViewWithWidth:(CGFloat)width
                           height:(CGFloat)height
                            color:(UIColor *)color;

@end
