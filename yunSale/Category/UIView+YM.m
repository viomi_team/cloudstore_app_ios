//
//  UIView+YM.m
//  yunSale
//
//  Created by 谢立颖 on 2016/12/21.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "UIView+YM.h"

@implementation UIView (YM)

+ (UIView *)drawLineViewWithWidth:(CGFloat)width
                           height:(CGFloat)height
                            color:(UIColor *)color {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    view.backgroundColor = color;
    
    UIView *topLineView = [[UIView alloc] init];
    topLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [view addSubview:topLineView];
    [topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view);
        make.right.equalTo(view);
        make.top.equalTo(view);
        make.height.mas_equalTo(0.5);
    }];
    
    UIView *bottomLineView = [[UIView alloc] init];
    bottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [view addSubview:bottomLineView];
    [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view);
        make.right.equalTo(view);
        make.bottom.equalTo(view);
        make.height.mas_equalTo(0.5);
    }];
    
    return view;
}

@end
