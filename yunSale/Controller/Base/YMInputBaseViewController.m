//
//  YMInputBaseViewController.m
//  WaterPurifier
//
//  Created by liushilou on 16/8/4.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "YMInputBaseViewController.h"

@interface YMInputBaseViewController ()<UIScrollViewDelegate>

@property (nonatomic,assign) BOOL autoScroll;

@end

@implementation YMInputBaseViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    _parentScrollView = [[UIScrollView alloc] init];
    _parentScrollView.delegate = self;
    _parentScrollView.showsVerticalScrollIndicator = NO;
    _parentScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_parentScrollView];
    [_parentScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_offset(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickAction)];
    [_parentScrollView addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)keyboardWillShow:(NSNotification*)notification {
    /*
     Reduce the size of the text view so that it's not obscured by the keyboard.
     Animate the resize so that it's in sync with the appearance of the keyboard.
     */
    
    NSDictionary *userInfo = [notification userInfo];
    
    // Get the origin of the keyboard when it's displayed.
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    // Get the top of the keyboard as the y coordinate of its origin in self's view's coordinate system. The bottom of the text view's frame should align with the top of the keyboard's final position.
    CGRect keyboardRect = [aValue CGRectValue];
    
    //不需要支持8.0以下了
    if ([[UIDevice currentDevice] systemVersion].floatValue < 8.0) {
        //如果是IOS7、IOS6系统，转换frame
        CGRect rect = keyboardRect;
        keyboardRect.origin.x = rect.origin.y;
        keyboardRect.origin.y = rect.origin.x;
        keyboardRect.size.width = rect.size.height;
        keyboardRect.size.height = rect.size.width;
    }
    
    // Get the duration of the animation.
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    // Animate the resize of the text view's frame in sync with the keyboard's appearance.
    // [self moveInputBarWithKeyboardHeight:keyboardRect.size.height withDuration:animationDuration];
    [self moveInputBarWithKeyboardHeight:keyboardRect actionDown:NO withDuration:animationDuration];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary* userInfo = [notification userInfo];
    
    /*
     Restore the size of the text view (fill self's view).
     Animate the resize so that it's in sync with the disappearance of the keyboard.
     */
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [self moveInputBarWithKeyboardHeight:CGRectMake(0.0, 0.0, 0.0, 0.0) actionDown:YES withDuration:animationDuration];
}

-(void)moveInputBarWithKeyboardHeight:(CGRect)rect actionDown:(BOOL)down withDuration:(NSTimeInterval)animationDuration {
    @weakify(self);
    [self.parentScrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.bottom.equalTo(self.view).with.offset(-rect.size.height);
    }];
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
   
    } completion:^(BOOL finished) {

    }];
    
    self.autoScroll = YES;
    if (rect.size.height > 0) {
        [self.parentScrollView setContentOffset:CGPointMake(0, self.offset)];
    }else{
        [self.parentScrollView setContentOffset:CGPointMake(0, 0)];
    }
}

//单击 scrollView 收起键盘
- (void)clickAction {
    [self.view endEditing:YES];
}
@end
