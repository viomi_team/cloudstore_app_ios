//
//  YMScrollItemsBaseViewController.h
//  yunSale
//
//  Created by liushilou on 16/11/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

//优化方法，与轮播图一样，只创建3个controller。本项目目前都只要3个item，所以先简单做


#import <UIKit/UIKit.h>

@interface YMScrollItemsBaseViewController : UIViewController

//items 和views 的数量要一致
@property (nonatomic,copy) NSArray<NSString *> *items;

@property (nonatomic,copy) NSArray *itemContentViews;

@property (nonatomic,strong) UIScrollView *scrollView;      //把scrollView的定义移到头文件，以让子类实现appearControllerOfIndex:方法时可以更改scrollView的contentOffset（by 谢立颖 2017,1,16）

//子类实现
- (void)appearControllerOfIndex:(NSUInteger)index;

@end
