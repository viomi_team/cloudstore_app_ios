//
//  YMScrollItemsBaseViewController.m
//  yunSale
//
//  Created by liushilou on 16/11/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMScrollItemsBaseViewController.h"
#import "YMTabButton.h"

#define YM_SELECTEDVIEW_HEIGHT [YMCScreenAdapter sizeBy750:80]
#define YM_SELECTEDVIEW_SPACE [YMCScreenAdapter sizeBy750:30]
#define YM_SELECTEDVIEW_LINEHEIGHT 2


@interface YMScrollItemsBaseViewController ()<UIScrollViewDelegate>

@property (nonatomic,strong) UIView *itemsView;

//@property (nonatomic,strong) UIScrollView *scrollView;

@property (nonatomic,strong) UIView *selectedLineView;

//存储，避免每次都运算:去掉YM_SELECTEDVIEW_SPACE后的宽度
@property (nonatomic,assign) CGFloat itemsWidth;

@end

@implementation YMScrollItemsBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.itemsWidth = [UIScreen mainScreen].bounds.size.width;// - YM_SELECTEDVIEW_SPACE * 2;
    
    [self drawView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)drawView {
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.itemsView = [[UIView alloc] init];
    [self.view addSubview:self.itemsView];
    [self.itemsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(0);
        make.top.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.height.mas_equalTo(YM_SELECTEDVIEW_HEIGHT);
    }];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [self.itemsView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.itemsView).with.offset(0);
        make.bottom.equalTo(self.itemsView).with.offset(0);
        make.right.equalTo(self.itemsView).with.offset(0);
        make.height.mas_equalTo(0.5);
    }];
    
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.itemsView.mas_bottom).with.offset(0);
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.bottom.equalTo(self.view).with.offset(0);
    }];
    
    
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offset = scrollView.contentOffset.x;
    
    CGFloat x = (offset/([UIScreen mainScreen].bounds.size.width * self.items.count)) * self.itemsWidth;
    
    CGRect rect = self.selectedLineView.frame;
    rect.origin.x = x + YM_SELECTEDVIEW_SPACE;
    
    self.selectedLineView.frame = rect;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat offset = scrollView.contentOffset.x;
    NSUInteger index = round(offset/([UIScreen mainScreen].bounds.size.width));
    [self appearControllerOfIndex:index]; 
}

#pragma mark - setter
- (void)setItems:(NSArray<NSString *> *)items {
    _items = [items copy];
    
    CGFloat x = 0;
    CGFloat width = [UIScreen mainScreen].bounds.size.width/self.items.count;
    for (NSString *item in self.items) {
        YMTabButton *btn = [[YMTabButton alloc] initWithFrame:CGRectMake(x, 0, width, YM_SELECTEDVIEW_HEIGHT - YM_SELECTEDVIEW_LINEHEIGHT - 0.5) title:item];
        btn.tag = [self.items indexOfObject:item];
        [btn addTarget:self action:@selector(btnsClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.itemsView addSubview:btn];
        x += width;
    }

    self.selectedLineView = [[UIView alloc] initWithFrame:CGRectMake(YM_SELECTEDVIEW_SPACE, YM_SELECTEDVIEW_HEIGHT - YM_SELECTEDVIEW_LINEHEIGHT - 0.5, width - YM_SELECTEDVIEW_SPACE * 2, YM_SELECTEDVIEW_LINEHEIGHT)];
    self.selectedLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_THEME];
    [self.view addSubview:self.selectedLineView];
    
    self.scrollView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width * self.items.count, 0);
}

- (void)setItemContentViews:(NSArray *)itemContentViews {
    _itemContentViews = [itemContentViews copy];
    
    CGFloat x = 0;
    CGRect rectStatus = [[UIApplication sharedApplication] statusBarFrame];
    CGFloat statusheight = rectStatus.size.height;
    CGFloat navheight = self.navigationController.navigationBar.frame.size.height;
    CGFloat height = [UIScreen mainScreen].bounds.size.height - statusheight - navheight - [YMCScreenAdapter sizeBy750:80];
    
    NSLog(@"width:%f",[UIScreen mainScreen].bounds.size.width);
    
    for (UIView *view in itemContentViews) {
        view.frame = CGRectMake(x, 0, [UIScreen mainScreen].bounds.size.width, height);
        [self.scrollView addSubview:view];
        
        x += [UIScreen mainScreen].bounds.size.width;
    }
}

#pragma mark - private method
- (void)btnsClick:(UIButton *)button {
    
    [UIView animateWithDuration:0.4 animations:^{
        [self.scrollView setContentOffset:CGPointMake([UIScreen mainScreen].bounds.size.width * button.tag, 0)];
    }];
    
    CGRect rect = self.selectedLineView.frame;
    rect.origin.x = ([UIScreen mainScreen].bounds.size.width/self.items.count) * button.tag + YM_SELECTEDVIEW_SPACE;
    
    self.selectedLineView.frame = rect;
    
    [self appearControllerOfIndex:button.tag];
}

- (void)appearControllerOfIndex:(NSUInteger)index {
    //子类继承实现
}


@end
