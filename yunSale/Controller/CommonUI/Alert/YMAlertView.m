//
//  YMAlertView.m
//  yunSale
//
//  Created by liushilou on 16/11/10.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMAlertView.h"
#import <MBProgressHUD.h>


@implementation YMAlertView

+ (void)showMessage:(NSString *)message {
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertview show];
}

+ (void)showHubInView:(UIView *)view message:(NSString *)message
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = message;
    [hud hideAnimated:YES afterDelay:1]; 
}

+ (void)showNetErrorInView:(UIView *)view type:(YMRequestStatus)type message:(NSString *)message actionBlock:(void (^)())block
{
    YMNetErrorView *errorView = [[YMNetErrorView alloc] initWithActionBlock:^{
        block();
    }];
    errorView.message = message;
    errorView.erorType = type;
    [view addSubview:errorView];
    [errorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(view);
    }];
}

+ (void)removeNetErrorInView:(UIView *)view
{
    for (UIView *subview in view.subviews) {
        if ([subview isKindOfClass:[YMNetErrorView class]]) {
            NSLog(@"123456789");
            [subview removeFromSuperview];
        }
    }
}

@end
