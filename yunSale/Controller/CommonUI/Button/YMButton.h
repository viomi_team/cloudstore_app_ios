//
//  YMButton.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, YMButtonStyle) {
    YMButtonStyleNormal,   //主题背景色的圆角按钮
    YMButtonStyleWhite,    //背景色为白色的圆角的按钮
    YMButtonStyleRectangle //主题背景色的小圆角按钮，主要在搜索视图中使用
};

@interface YMButton : UIButton

- (id)initWithStyle:(YMButtonStyle)style;

@end
