//
//  YMButton.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMButton.h"
#import <YMCommon/UIImage+ymc.h>

@interface YMButton()

//用于定义按钮圆角的样式，在drawRect:方法中使用
@property (nonatomic, assign) NSInteger buttonStyle;

@end

@implementation YMButton

- (id)initWithStyle:(YMButtonStyle)type
{
    self = [super init];
    if (self) {
        [self.titleLabel setFont:YM_FONT([YMCScreenAdapter fontsizeBy750:32])];
        
        switch (type) {
            case YMButtonStyleNormal:
            {
                //普通样式
                _buttonStyle = 0;
                self.backgroundColor = [UIColor colorWithHexString:YMCOLOR_THEME];
                
                [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [self setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
                [self setTitleColor:[UIColor colorWithHexString:YMCOLOR_GRAY] forState:UIControlStateDisabled];
//                [self setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:YMCOLOR_THEME] size:CGSizeMake(self.frame.size.width, self.frame.size.height)] forState:UIControlStateNormal];
//                //颜色待修改
//                [self setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:YMCOLOR_GRAY] size:CGSizeMake(self.frame.size.width, self.frame.size.height)] forState:UIControlStateDisabled];
                
                //配置圆角
            }
                break;
                
            case YMButtonStyleWhite:
            {
                //白色背景样式
                _buttonStyle = 0;
                self.backgroundColor = [UIColor whiteColor];
                
                [self setTitleColor:[UIColor colorWithHexString:YMCOLOR_THEME] forState:UIControlStateNormal];
                [self setTitleColor:[UIColor colorWithHexString:YMCOLOR_GRAY] forState:UIControlStateDisabled];
//                [self setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor] size:CGSizeMake(self.frame.size.width, self.frame.size.height)] forState:UIControlStateNormal];
//                //颜色待修改
//                [self setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:YMCOLOR_GRAY] size:CGSizeMake(self.frame.size.width, self.frame.size.height)] forState:UIControlStateDisabled];
            }
                break;
                
            case YMButtonStyleRectangle:
            {
                //搜索界面按钮
                _buttonStyle = 1;
                self.backgroundColor = [UIColor colorWithHexString:YMCOLOR_THEME];
                
                [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [self setTitleColor:[UIColor colorWithHexString:YMCOLOR_GRAY] forState:UIControlStateDisabled];
//                [self setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:YMCOLOR_THEME] size:CGSizeMake(self.frame.size.width, self.frame.size.height)] forState:UIControlStateNormal];
//                //颜色待修改
//                [self setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:YMCOLOR_GRAY] size:CGSizeMake(self.frame.size.width, self.frame.size.height)] forState:UIControlStateDisabled];
            }
                break;
        }
    }
    
    return self;
}

//用于绘制不同样式的按钮圆角
- (void)drawRect:(CGRect)rect
{
    if (_buttonStyle == 0) {
        self.layer.cornerRadius = rect.size.height/2;
        self.layer.masksToBounds = YES;
    } else if (_buttonStyle == 1) {
        self.layer.cornerRadius = [YMCScreenAdapter sizeBy750:10];
        self.layer.masksToBounds = YES;
    }
}

@end
