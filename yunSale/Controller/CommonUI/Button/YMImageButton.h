//
//  YMImageButton.h
//  yunSale
//
//  Created by liushilou on 16/11/15.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, YMImageButtonStyle) {
    YMImageButtonStyleDefault = 0,
    //根据实际情况扩展
};

@interface YMImageButton : UIButton

- (id)initWithTitle:(NSString *)title image:(UIImage *)image style:(YMImageButtonStyle)style;

@end
