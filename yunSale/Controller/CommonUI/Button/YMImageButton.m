//
//  YMImageButton.m
//  yunSale
//
//  Created by liushilou on 16/11/15.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMImageButton.h"

@implementation YMImageButton

- (id)initWithTitle:(NSString *)title image:(UIImage *)image style:(YMImageButtonStyle)style
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        UIImageView *imageview = [[UIImageView alloc] init];
        imageview.image = image;
        [self addSubview:imageview];
        
        [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:20]);
            make.size.mas_equalTo(CGSizeMake([YMCScreenAdapter sizeBy750:120], [YMCScreenAdapter sizeBy750:120]));
            make.centerX.equalTo(self);
        }];

        
        UILabel *lable = [[UILabel alloc] init];
        lable.text = title;
        lable.font = YM_FONT([YMCScreenAdapter sizeBy750:28]);
        lable.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [self addSubview:lable];
        [lable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(imageview.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:20]);
            make.centerX.equalTo(self);
        }];
    }
    return self;
}

@end
