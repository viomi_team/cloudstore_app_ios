//
//  YMTabButton.h
//  yunSale
//
//  Created by liushilou on 16/11/10.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMTabButton : UIButton

- (instancetype)initWithTitle:(NSString *)title;

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title;


@end
