//
//  YMTabButton.m
//  yunSale
//
//  Created by liushilou on 16/11/10.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMTabButton.h"

@interface YMTabButton()

@property (nonatomic,strong) UIView *lineView;

@end


@implementation YMTabButton

- (id)initWithTitle:(NSString *)title
{
    self = [super init];
    if (self) {
        [self.titleLabel setFont:YM_FONT([YMCScreenAdapter fontsizeBy750:26])];
        [self setTitleColor:[UIColor colorWithHexString:YMCOLOR_THEME] forState:UIControlStateSelected];
        [self setTitleColor:[UIColor colorWithHexString:YMCOLOR_GRAY] forState:UIControlStateNormal];
        [self setTitleColor:[UIColor colorWithHexString:YMCOLOR_THEME] forState:UIControlStateHighlighted];
        [self setTitle:title forState:UIControlStateNormal];
        
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_THEME];
        _lineView.hidden = YES;
        [self addSubview:_lineView];
        [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset(0);
            make.right.equalTo(self).with.offset(0);
            make.bottom.equalTo(self).with.offset(0);
            make.height.mas_equalTo(1);
        }];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.titleLabel setFont:YM_FONT([YMCScreenAdapter fontsizeBy750:26])];
        [self setTitleColor:[UIColor colorWithHexString:YMCOLOR_THEME] forState:UIControlStateSelected];
        [self setTitleColor:[UIColor colorWithHexString:YMCOLOR_GRAY] forState:UIControlStateNormal];
        [self setTitleColor:[UIColor colorWithHexString:YMCOLOR_THEME] forState:UIControlStateHighlighted];
        [self setTitle:title forState:UIControlStateNormal];
    }
    return self;
}


- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    if (self.lineView) {
        if (selected) {
            self.lineView.hidden = NO;
        }else{
            self.lineView.hidden = YES;
        }
    }
}


@end
