//
//  YMChartDatas.m
//  yunSale
//
//  Created by liushilou on 17/1/5.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMChartDatas.h"

@implementation YMChartDatas

+ (NSArray *)splitLineDatas
{
    NSArray *array = [NSArray arrayWithObjects:
                  @(15),@(30),@(60),@(90),
                  @(150),@(300),@(600),@(900),
                  @(1500),@(3000),@(6000),@(9000),
                  @(15000),@(30000),@(60000),@(90000),
                  @(150000),@(300000),@(600000),@(900000),
                  @(1500000),@(3000000),@(6000000),@(9000000),
                  @(15000000),@(30000000),@(60000000),@(90000000),
                  @(150000000),@(300000000),@(600000000),@(900000000),
                  nil];
    return array;
}



@end
