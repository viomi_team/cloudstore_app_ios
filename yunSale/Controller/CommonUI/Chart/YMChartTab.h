//
//  YMChartTab.h
//  yunSale
//
//  Created by liushilou on 16/12/6.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YMChart.h"

typedef  NS_ENUM(NSInteger, YMChartTabStyle) {
    YMChartTabStyleAmount,      //显示金额总数
    YMChartTabStylePercentage   //显示百分比
};

@interface YMChartTab : NSObject

+ (NSMutableAttributedString *)stringOfTabs:(NSArray<YMChart *> *)datas withStyle:(YMChartTabStyle)style;   //添加可选的数值显示样式（by 谢立颖 2017,1,15）

+ (UIColor *)colorOfTab:(NSUInteger)index;

+ (CGFloat)heightForAttributedString:(NSMutableAttributedString *)string width:(CGFloat)width;

+ (NSString *)monthOfDate:(NSDate *)date;
+ (NSString *)yearOfDate:(NSDate *)date;


@end
