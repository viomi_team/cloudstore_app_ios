//
//  YMChartTab.m
//  yunSale
//
//  Created by liushilou on 16/12/6.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMChartTab.h"
#import <YMCommon/UIImage+ymc.h>
#import "UIView+RoundedCorner.h"

@implementation YMChartTab

+ (NSMutableAttributedString *)stringOfTabs:(NSArray<YMChart *> *)datas withStyle:(YMChartTabStyle)style {
    NSMutableAttributedString *string = [NSMutableAttributedString new];
    
    CGFloat iconsize = [YMCScreenAdapter sizeBy750:18];
    CGFloat y = [YMCScreenAdapter sizeBy750:0];
    
    NSInteger totalcount = 0;
    for (YMChart *item in datas) {
        totalcount += item.value;
    }
    
    for (YMChart *item in datas) {
        
        NSUInteger index = [datas indexOfObject:item];
        
        NSTextAttachment *setattach = [[NSTextAttachment alloc] init];
        
        UIImage *iconImage = [UIImage imageWithColor:[YMChartTab colorOfTab:index] size:CGSizeMake(iconsize, iconsize)];
        iconImage = [iconImage jm_imageWithRoundedCornersAndSize:CGSizeMake(iconsize, iconsize) andCornerRadius:[YMCScreenAdapter sizeBy750:6]/2];
        
        setattach.image = iconImage;
        setattach.bounds = CGRectMake(0, y, iconsize, iconsize);
        NSAttributedString *setattachString = [NSAttributedString attributedStringWithAttachment:setattach];
        [string appendAttributedString:setattachString];
        
        [string appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@" "]];
        [string appendAttributedString:[[NSMutableAttributedString alloc] initWithString:item.name]];
        
        //添加可选的数值显示样式（by 谢立颖 2017,1,15）
        if (style == YMChartTabStylePercentage) {
            if (totalcount > 0) {
                CGFloat percentage = item.value/totalcount;
                [string appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"(%.1f%%)",percentage*100]]];
            }
        } else if (style == YMChartTabStyleAmount) {
            //图标中金额默认不显示小数点（by zcm 2017.9.6）
            [string appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"(￥%d)", (int)item.value]]];
        }

        [string appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"  "]];
    }
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    //style.lineSpacing = 10;//增加行高
    paragraphStyle.lineHeightMultiple = 1.5;//行间距是多少倍
    paragraphStyle.alignment = NSTextAlignmentLeft;//对齐方式
    CGFloat length = string.length;
    [string addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, length)];
    
    return string;
}

+ (UIColor *)colorOfTab:(NSUInteger)index {
    UIColor *color = nil;
    switch (index) {
            case 0:
            color = [UIColor colorWithHexString:@"#34495e"];
            break;
            case 1:
            color = [UIColor colorWithHexString:@"#ff4d1c"];
            break;
            case 2:
            color = [UIColor colorWithHexString:@"#1d8acb"];
            break;
            case 3:
            color = [UIColor colorWithHexString:@"#72c156"];
            break;
            case 4:
            color = [UIColor colorWithHexString:@"#fae603"];
            break;
        default:
            color = [UIColor grayColor];
            break;
    }
    return color;
}

+ (CGFloat)heightForAttributedString:(NSMutableAttributedString *)string width:(CGFloat)width {
    CGSize size = [string boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
    return size.height;
}

+ (NSString *)monthOfDate:(NSDate *)date
{
    NSCalendar *calendar = [YMChartTab currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
    NSInteger month = [components month];
    
    NSString *monthString = @"";
    switch (month) {
        case 1:
            monthString = @"一月";
            break;
        case 2:
            monthString = @"二月";
            break;
        case 3:
            monthString = @"三月";
            break;
        case 4:
            monthString = @"四月";
            break;
        case 5:
            monthString = @"五月";
            break;
        case 6:
            monthString = @"六月";
            break;
        case 7:
            monthString = @"七月";
            break;
        case 8:
            monthString = @"八月";
            break;
        case 9:
            monthString = @"九月";
            break;
        case 10:
            monthString = @"十月";
            break;
        case 11:
            monthString = @"十一月";
            break;
        case 12:
            monthString = @"十二月";
            break;
        default:
            break;
    }
    return monthString;
}

+ (NSString *)yearOfDate:(NSDate *)date
{
    NSCalendar *calendar = [YMChartTab currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
    NSInteger year = [components year];
    
    return @(year).stringValue;

}


#pragma mark - 日历获取在9.x之后的系统使用currentCalendar会出异常。在8.0之后使用系统新API。
+ (NSCalendar *)currentCalendar {
    if ([NSCalendar respondsToSelector:@selector(calendarWithIdentifier:)]) {
        return [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    }
    return [NSCalendar currentCalendar];
}


@end
