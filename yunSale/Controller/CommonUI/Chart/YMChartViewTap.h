//
//  YMChartViewTap.h
//  yunSale
//
//  Created by liushilou on 16/11/10.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMStore.h"

@interface YMChartViewTap : UIView

- (id)initWithFrame:(CGRect)frame iconColor:(UIColor *)color text:(NSString *)text percentage:(CGFloat)percentage;

- (instancetype)initWithdatas:(NSArray<YMStore *> *)datas;


@end
