//
//  YMChartViewTap.m
//  yunSale
//
//  Created by liushilou on 16/11/10.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMChartViewTap.h"

@interface YMChartViewTap()

@end

@implementation YMChartViewTap

- (id)initWithFrame:(CGRect)frame iconColor:(UIColor *)color text:(NSString *)text percentage:(CGFloat)percentage {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView *tapview = [[UIView alloc] init];
        tapview.backgroundColor = color;
        tapview.layer.cornerRadius = [YMCScreenAdapter sizeBy750:6];
        [self addSubview:tapview];
        [tapview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self).with.offset(0);
            make.size.mas_equalTo(CGSizeMake([YMCScreenAdapter sizeBy750:18], [YMCScreenAdapter sizeBy750:18]));
        }];
        
        UILabel *textLabel = [[UILabel alloc] init];
        textLabel.text = text;
        textLabel.textColor = [UIColor colorWithHexString:@"#999999"];
        textLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:24]);
        [self addSubview:textLabel];
        [textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(tapview.mas_right).with.offset(5);
            make.right.equalTo(self).with.offset(0);
        }]; 
    }
    return self;
}

- (instancetype)initWithdatas:(NSArray<YMStore *> *)datas {
    self = [super init];
    if (self) {
        
    }
    return self;
}





@end
