//
//  YMHistogramChartActionView.h
//  yunSale
//
//  Created by liushilou on 17/1/5.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMChart.h"

@protocol YMHistogramChartActionViewDelegate <NSObject>

@optional

- (void)YMHistogramChartActionViewClickChartItem:(NSUInteger)index;


- (void)YMHistogramChartActionViewClickTitle;

@end

@interface YMHistogramChartActionView : UIView

@property (nonatomic,weak) id<YMHistogramChartActionViewDelegate> delegate;

@property (nonatomic,copy) NSArray<YMChart *> *datas;

@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *valueLabel;

@property (nonatomic,assign,readonly) NSInteger selectedIndex;  //  selectedIndex == -1 表示选中最后；选中由用户操作定义，而不是代码控制，故用readonly

/**
 *  下拉选择，设置actionView 可以控制下拉选择框的大小
 */
@property (nonatomic,strong) UIView *actionView;

//@property (nonatomic,assign) BOOL selectLastItem;  //初始 = YES; 设置为NO，将首先显示选中Item；

- (instancetype)initWidthDatas:(NSArray<YMChart *> *)datas;


@end
