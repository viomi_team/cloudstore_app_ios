//
//  YMHistogramChartActionView.m
//  yunSale
//
//  Created by liushilou on 17/1/5.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMHistogramChartActionView.h"
#import "YMChartDatas.h"



@interface YMHistogramChartActionView()<UIScrollViewDelegate>

@property (nonatomic,strong) NSArray *tabValues;

@property (nonatomic,assign) CGFloat chartBigValue;

//@property (nonatomic,strong) UILabel *chartTabLabel1;
//@property (nonatomic,strong) UILabel *chartTabLabel2;
//@property (nonatomic,strong) UILabel *chartTabLabel3;
//@property (nonatomic,strong) UILabel *chartTabLabel4;

@property (nonatomic,strong) UIScrollView *scrollView;

@property (nonatomic,assign) CGFloat offsetX;

@property (nonatomic,assign,readwrite) NSInteger selectedIndex;

@end


@implementation YMHistogramChartActionView

- (instancetype)initWidthDatas:(NSArray<YMChart *> *)datas
{
    self = [self init];
    if (self) {
        _datas = [datas copy];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _selectedIndex    = -1;
        
//        _selectLastItem = YES;
        
        _offsetX = 0;
        
        self.backgroundColor = [UIColor whiteColor];
        
        _tabValues = [YMChartDatas splitLineDatas];
        [self drawView];
    }
    return self;
}

- (void)drawView
{
    
    UIView *titleView = [[UIView alloc] init];
    [self addSubview:titleView];
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.top.equalTo(self);
        make.right.equalTo(self);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
    }];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:26]);
    self.nameLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
    [titleView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.centerY.equalTo(titleView);
    }];
    
    _actionView = [[UIView alloc] init];
    _actionView.layer.borderColor = [UIColor colorWithHexString:YMCOLOR_LINE].CGColor;
    _actionView.layer.borderWidth = 0.5;
    [titleView addSubview:_actionView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(titleClick)];
    [_actionView addGestureRecognizer:tap];

    [_actionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLabel.mas_right).with.offset([YMCScreenAdapter sizeBy750:5]);
        make.top.equalTo(titleView).with.offset([YMCScreenAdapter sizeBy750:15]);;
        make.bottom.equalTo(titleView).with.offset([YMCScreenAdapter sizeBy750:-15]);
        make.width.mas_equalTo([YMCScreenAdapter sizeBy750:270]);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    //self.titleLabel.text = @"销售金额总览";
    self.titleLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:26]);
    self.titleLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
    [_actionView addSubview:self.titleLabel];
    
    UIImageView *imageview = [[UIImageView alloc] init];
    imageview.image = [UIImage imageNamed:@"ym_index_selector"];
    [_actionView addSubview:imageview];
    [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_actionView).with.offset(-5);
        make.centerY.equalTo(_actionView);
        make.size.mas_equalTo(CGSizeMake([YMCScreenAdapter sizeBy750:20], [YMCScreenAdapter sizeBy750:16]));
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_actionView).with.offset(2);
        make.centerY.equalTo(_actionView);
        make.right.equalTo(imageview).with.offset(-2);
    }];
    
    self.valueLabel = [[UILabel alloc] init];
    self.valueLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:40]);
    self.valueLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
    [titleView addSubview:self.valueLabel];
    [self.valueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(titleView).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
        make.centerY.equalTo(titleView);
    }];
    
    
    UIView *lineView1 = [[UIView alloc] init];
    lineView1.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [self addSubview:lineView1];
    
    UIView *lineView2 = [[UIView alloc] init];
    lineView2.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [self addSubview:lineView2];
    
    UIView *lineView3 = [[UIView alloc] init];
    lineView3.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [self addSubview:lineView3];
    
    UIView *lineView4 = [[UIView alloc] init];
    lineView4.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [self addSubview:lineView4];
    
    NSArray *lineArray = @[lineView1,lineView2,lineView3,lineView4];
    [lineArray mas_distributeViewsAlongAxis:MASAxisTypeVertical withFixedItemLength:0.5 leadSpacing:[YMCScreenAdapter sizeBy750:124] tailSpacing:[YMCScreenAdapter sizeBy750:80]];
    [lineArray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.right.equalTo(self);
    }];
    
    
//    self.chartTabLabel1 = [self chartTapLabel];
//    [self addSubview:self.chartTabLabel1];
//    self.chartTabLabel2 = [self chartTapLabel];
//    [self addSubview:self.chartTabLabel2];
//    self.chartTabLabel3 = [self chartTapLabel];
//    [self addSubview:self.chartTabLabel3];
//    self.chartTabLabel4 = [self chartTapLabel];
//    [self addSubview:self.chartTabLabel4];
//    
//    [self.chartTabLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).with.offset(5);
//        make.bottom.equalTo(lineView1.mas_top).offset(-5);
//    }];
//    [self.chartTabLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).with.offset(5);
//        make.bottom.equalTo(lineView2.mas_top).offset(-5);
//    }];
//    [self.chartTabLabel3 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).with.offset(5);
//        make.bottom.equalTo(lineView3.mas_top).offset(-5);
//    }];
//    [self.chartTabLabel4 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).with.offset(5);
//        make.bottom.equalTo(lineView4.mas_top).offset(-5);
//    }];
    
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.delegate = self;
    [self addSubview:self.scrollView];
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:24]);
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.bottom.equalTo(self);
    }];
}



- (UILabel *)chartTapLabel
{
    UILabel *label = [[UILabel alloc] init];
    label.font = YM_FONT([YMCScreenAdapter fontsizeBy750:20]);
    label.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
    return label;
}


- (void)setDatas:(NSArray<YMChart *> *)datas
{
    _datas = [datas copy];
    
    for (UIView *view in self.scrollView.subviews) {
        [view removeFromSuperview];
    }
    
//    self.selectedIndex = -1;
    
    // zcm add：数据越界，重置选中项
    if(self.selectedIndex > datas.count - 1) {
        self.selectedIndex = -1;
    }
    
    [self setNeedsDisplay];   //自动调用 drawRect
}


- (void)drawRect:(CGRect)rect
{
    
    CGFloat bigValue = 0;
    for (YMChart *item in self.datas) {
        if (item.value > bigValue) {
            bigValue = item.value;
        }
    }
    //计算表格的最大数值
    [self bigValue:bigValue array:self.tabValues];
    
//    self.chartTabLabel1.text = @(self.chartBigValue).stringValue;
//    self.chartTabLabel2.text = @(self.chartBigValue * (2.0/3)).stringValue;
//    self.chartTabLabel3.text = @(self.chartBigValue * (1.0/3)).stringValue;
//    self.chartTabLabel4.text = @"0";
    
//    CGFloat itemWidth = CGRectGetWidth(rect)/7;
//    CGFloat width = itemWidth * self.datas.count;
//    
//    self.scrollView.contentSize = CGSizeMake(width, 1);
    
    //表格柱状图
    CGFloat chartViewHeight = chartViewHeight = CGRectGetHeight(rect) - [YMCScreenAdapter sizeBy750:124+80];
    
    CGFloat itemWidth = [YMCScreenAdapter sizeBy750:80];
    CGFloat itemSpace = [YMCScreenAdapter sizeBy750:24];

    CGFloat chartX = itemSpace/2;
    
    for (YMChart *item in self.datas) {
        
        NSNumber *value = @(item.value);
        
        NSInteger index = [self.datas indexOfObject:item];

        CGFloat chartHeight = (value.floatValue/self.chartBigValue) * chartViewHeight;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(chartX - itemSpace/2, 0, itemWidth + itemSpace, chartViewHeight + [YMCScreenAdapter sizeBy750:80])];
        view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
        
        UIView *chartView  = [[UIView alloc] initWithFrame:CGRectMake(itemSpace/2, chartViewHeight - chartHeight, itemWidth, chartHeight)];
        [view addSubview:chartView];
        
        view.tag = index;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickItemView:)];
        [view addGestureRecognizer:tap];
        
        UIColor *color = [UIColor colorWithHexString:YMCOLOR_THEME];
        chartView.backgroundColor = color;
        
        [self.scrollView addSubview:view];
        
        UILabel *tablabel = [[UILabel alloc] initWithFrame:CGRectMake(chartX , chartViewHeight, itemWidth + itemSpace, [YMCScreenAdapter sizeBy750:80])];
        tablabel.numberOfLines = 2;
        tablabel.textAlignment = NSTextAlignmentLeft;
        tablabel.tag  = 10000+index;
        tablabel.text = item.name;
        tablabel.font = YM_FONT([YMCScreenAdapter sizeBy750:24]);
        [self.scrollView addSubview:tablabel];
        
        NSInteger currentSelectedIndex = self.selectedIndex;
        if (currentSelectedIndex == -1) {
            currentSelectedIndex = self.datas.count - 1;
        }
        
        if (index == currentSelectedIndex) {
            chartView.alpha    = 1;
            tablabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
            
            self.valueLabel.text = @(item.value).stringValue;
            [self setName:item.name];
        }else{
            chartView.alpha    = 0.6;
            tablabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        }
        
        chartX += itemWidth + itemSpace;
        
    }
    
    self.scrollView.contentSize = CGSizeMake(chartX - [YMCScreenAdapter sizeBy750:12], 1);
    
    if(self.selectedIndex == -1 || self.selectedIndex == self.datas.count - 1) {
        //滚动到底部
        self.offsetX = self.scrollView.contentSize.width - self.scrollView.bounds.size.width;
        if(self.offsetX > 0){
            [self.scrollView setContentOffset:CGPointMake(self.offsetX, 0) animated:NO];
        }
    }else {
        //保持原样不变
        [self.scrollView setContentOffset:CGPointMake(self.offsetX, 0) animated:NO];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    self.offsetX = scrollView.contentOffset.x;
}


- (void)bigValue:(CGFloat)value array:(NSArray *)values {
    NSInteger index = values.count/2 - 1;
    
    if (index < 0) {
        index = 0;
        NSNumber *value = [values objectAtIndex:index];
        self.chartBigValue = value.floatValue;
    }
    
    NSNumber *itemValue = [values objectAtIndex:index];
    
    NSArray *array = nil;
    if (value < itemValue.floatValue) {
        array = [values subarrayWithRange:NSMakeRange(0, index + 1)];
    } else {
        array = [values subarrayWithRange:NSMakeRange(index + 1, values.count - index - 1)];
    }
    
    if (array.count == 1) {
        NSNumber *value = [array objectAtIndex:0];
        self.chartBigValue = value.floatValue;
    } else {
        [self bigValue:value array:array];
    }
}


- (void)clickItemView:(UIGestureRecognizer *)gesture
{
    NSInteger index = gesture.view.tag;
    if (self.selectedIndex == index) {
        return;
    }
    
    self.selectedIndex = index;
    
    for (UIView *view in self.scrollView.subviews) {
        if (view.tag == index) {
            for (UIView *chartview in view.subviews) {
                chartview.alpha = 1;
            }
        }else if(view.tag < 10000){
            for (UIView *chartview in view.subviews) {
                chartview.alpha = 0.6;
            }
        }else if (view.tag == 10000 + index){
            UILabel *tabLabel = (UILabel *)view;
            tabLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
        }else{
            UILabel *tabLabel = (UILabel *)view;
            tabLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        }
    }
    
    YMChart *chart = [self.datas objectAtIndex:self.selectedIndex];
    self.valueLabel.text = @(chart.value).stringValue;
    [self setName:chart.name];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(YMHistogramChartActionViewClickChartItem:)]) {
        [self.delegate YMHistogramChartActionViewClickChartItem:index];
    }
}

- (void)setName:(NSString *)name{
    NSArray *array = [name componentsSeparatedByString:@"\n"];
    if (array.count > 1) {
        if ([name rangeOfString:@"/"].location !=NSNotFound) {
            NSString *result = [name stringByReplacingOccurrencesOfString:@"\n" withString:@""]; //去掉“至”（by 谢立颖 2017，1，13）
            self.nameLabel.text = result;
        }else{
            NSString *text = array[1];
            text = [text noWhitespaceCharacterString];
            if (text.length > 0) {
                NSString *result = [NSString stringWithFormat:@"%@,%@",array[1],array[0]];
                self.nameLabel.text = result;
            }else{
                NSString *result = array[0];
                self.nameLabel.text = result;
            }
        }
    }else{
        self.nameLabel.text = name;
    }
}

- (void)titleClick
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(YMHistogramChartActionViewClickTitle)]) {
        [self.delegate YMHistogramChartActionViewClickTitle];
    }
}


@end
