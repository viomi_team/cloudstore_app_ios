//
//  YMHistogramChartView.h
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

//柱状图

#import <UIKit/UIKit.h>
//#import "YMStore.h"
#import "YMChartTab.h"
#import "YMChart.h"

#define YM_HISTOGRAMCHART_HEIGHT [YMCScreenAdapter sizeBy750:500]



@interface YMHistogramChartView : UIView

@property (nonatomic,copy) NSArray<YMChart *> *datas;

@property (nonatomic,strong) UILabel *titleLabel;

@property (nonatomic,strong) UILabel *totalLabel;

- (instancetype)initWidthDatas:(NSArray<YMChart *> *)datas;


@end
