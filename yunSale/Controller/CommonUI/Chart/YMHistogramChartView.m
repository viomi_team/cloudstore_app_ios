//
//  YMHistogramChartView.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMHistogramChartView.h"
#import <MJRefresh/MJRefresh.h>

@interface YMHistogramChartView()

@property (nonatomic,strong) UIScrollView *chartParentView;

@property (nonatomic,strong) UILabel *nameTapsLabel;

@property (nonatomic,strong) NSArray *tabValues;

@property (nonatomic,assign) CGFloat chartBigValue;

@property (nonatomic,strong) UILabel *chartTabLabel1;
@property (nonatomic,strong) UILabel *chartTabLabel2;
@property (nonatomic,strong) UILabel *chartTabLabel3;
@property (nonatomic,strong) UILabel *chartTabLabel4;
@property (nonatomic,strong) UILabel *chartTabLabel5;

@property (nonatomic,strong) UIView *lineView1;
@property (nonatomic,strong) UIView *lineView2;
@property (nonatomic,strong) UIView *lineView3;
@property (nonatomic,strong) UIView *lineView4;
@property (nonatomic,strong) UIView *lineView5;

@property (nonatomic,strong) NSArray *lineArray;


@property (nonatomic,assign) NSUInteger selectedIndex;

@property (nonatomic,assign) CGFloat chartHeight;

@end

@implementation YMHistogramChartView


- (instancetype)initWidthDatas:(NSArray<YMChart *> *)datas
{
    //为何这个步骤会导致执行initWithFrame。。。
    self = [super init];
    if (self) {
        _datas = [datas copy];
        
        self.backgroundColor = [UIColor whiteColor];
        _tabValues = [NSArray arrayWithObjects:
                        @(200),@(400),@(600),@(800),@(1000),
                        @(2000),@(4000),@(6000),@(8000),@(10000),
                        @(20000),@(40000),@(60000),@(80000),@(100000),
                        @(200000),@(400000),@(600000),@(800000),@(1000000),
                        @(2000000),@(4000000),@(6000000),@(8000000),@(10000000),
                        @(20000000),@(40000000),@(60000000),@(80000000),@(100000000),
                        nil];
       [self drawView];
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        _tabValues = [NSArray arrayWithObjects:
                        @(200),@(400),@(600),@(800),@(1000),
                        @(2000),@(4000),@(6000),@(8000),@(10000),
                        @(20000),@(40000),@(60000),@(80000),@(100000),
                        @(200000),@(400000),@(600000),@(800000),@(1000000),
                        @(2000000),@(4000000),@(6000000),@(8000000),@(10000000),
                        @(20000000),@(40000000),@(60000000),@(80000000),@(100000000),
                        nil];
        [self drawView];
    }
    return self;
}

//- (id)initWithFrame:(CGRect)frame {
//    NSLog(@"555");
//    self = [super initWithFrame:frame];
//    if (self) {
//        self.backgroundColor = [UIColor whiteColor];
//        _moneyValues = [NSArray arrayWithObjects:
//                        @(200),@(400),@(600),@(800),@(1000),
//                        @(2000),@(4000),@(6000),@(8000),@(10000),
//                        @(20000),@(40000),@(60000),@(80000),@(100000),
//                        @(200000),@(400000),@(600000),@(800000),@(1000000),
//                        @(2000000),@(4000000),@(6000000),@(8000000),@(10000000),
//                        @(20000000),@(40000000),@(60000000),@(80000000),@(100000000),
//                        nil];
//        [self drawView];
//        
//    }
//    return self;
//}

- (void)drawView {
    
    UIView *titleView = [[UIView alloc] init];
    [self addSubview:titleView];
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.top.equalTo(self);
        make.right.equalTo(self);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.text = @"销售金额总览";
    self.titleLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:26]);
    self.titleLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
    [titleView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.centerY.equalTo(titleView);
    }];
    
    self.totalLabel = [[UILabel alloc] init];
    self.totalLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:40]);
    self.totalLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
    [titleView addSubview:self.totalLabel];
    [self.totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(titleView).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
        make.centerY.equalTo(titleView);
    }];
    
    UIView *parentview = [[UIView alloc] init];
    [self addSubview:parentview];
    
    self.chartParentView = [[UIScrollView alloc] init];
    self.chartParentView.backgroundColor = [UIColor clearColor];
    [parentview addSubview:self.chartParentView];
    
//    if (self.type == YMHistogramChartShowTabs) {
    self.nameTapsLabel = [[UILabel alloc] init];
    self.nameTapsLabel.textAlignment = NSTextAlignmentLeft;
    self.nameTapsLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:24]);
    self.nameTapsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
    self.nameTapsLabel.numberOfLines = 0;
    [self addSubview:self.nameTapsLabel];
    
    [self.nameTapsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.right.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
        make.bottom.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
    }];
    
    [parentview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND*2 + 75]);
        make.top.equalTo(titleView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.right.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
        make.bottom.equalTo(self.nameTapsLabel.mas_top).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
    }];
    
    [self.chartParentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(parentview);
    }];
//    }else{
//        [parentview mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self).with.offset([YMScreenAdapter sizeBy750:YMSPACE_AROUND]);
//            make.top.equalTo(titleView.mas_bottom).with.offset([YMScreenAdapter sizeBy750:YMSPACE_AROUND]);
//            make.right.equalTo(self).with.offset([YMScreenAdapter sizeBy750:-YMSPACE_AROUND]);
//            make.bottom.equalTo(self).with.offset([YMScreenAdapter sizeBy750:0]);
//        }];
//        [self.chartParentView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.edges.equalTo(parentview);
//        }];
//        
//        //self.chartParentView.mj_header = [];
//        
//    }
    
    
//    if (self.type == YMHistogramChartShowTabs) {
        self.lineView1 = [self lineView];
        [parentview insertSubview:self.lineView1 belowSubview:self.chartParentView];
        
        self.lineView2 = [self lineView];
        [parentview insertSubview:self.lineView2 belowSubview:self.chartParentView];
        
        self.lineView3 = [self lineView];
        [parentview insertSubview:self.lineView3 belowSubview:self.chartParentView];
        
        self.lineView4 = [self lineView];
        [parentview insertSubview:self.lineView4 belowSubview:self.chartParentView];
        
        self.lineView5 = [self lineView];
        [parentview insertSubview:self.lineView5 belowSubview:self.chartParentView];
        
        self.lineArray = [NSArray arrayWithObjects:self.lineView1,self.lineView2,self.lineView3,self.lineView4,self.lineView5, nil];
        [self.lineArray mas_distributeViewsAlongAxis:MASAxisTypeVertical withFixedItemLength:0.5 leadSpacing:0 tailSpacing:0];
        [self.lineArray mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(parentview);
            make.right.equalTo(parentview);
        }];
        
        
        self.chartTabLabel1 = [self chartTapLabel];
        [self addSubview:self.chartTabLabel1];
        self.chartTabLabel2 = [self chartTapLabel];
        [self addSubview:self.chartTabLabel2];
        self.chartTabLabel3 = [self chartTapLabel];
        [self addSubview:self.chartTabLabel3];
        self.chartTabLabel4 = [self chartTapLabel];
        [self addSubview:self.chartTabLabel4];
        self.chartTabLabel5 = [self chartTapLabel];
        [self addSubview:self.chartTabLabel5];
    
        [self.chartTabLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake([YMCScreenAdapter sizeBy750:75], [YMCScreenAdapter sizeBy750:40]));
            make.right.equalTo(self.chartParentView.mas_left).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
            make.centerY.equalTo(self.lineView1);
        }];
        [self.chartTabLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake([YMCScreenAdapter sizeBy750:75], [YMCScreenAdapter sizeBy750:40]));
            make.right.equalTo(self.chartParentView.mas_left).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
            make.centerY.equalTo(self.lineView2);
        }];
        [self.chartTabLabel3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake([YMCScreenAdapter sizeBy750:75], [YMCScreenAdapter sizeBy750:40]));
            make.right.equalTo(self.chartParentView.mas_left).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
            make.centerY.equalTo(self.lineView3);
        }];
        [self.chartTabLabel4 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake([YMCScreenAdapter sizeBy750:75], [YMCScreenAdapter sizeBy750:40]));
            make.right.equalTo(self.chartParentView.mas_left).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
            make.centerY.equalTo(self.lineView4);
        }];
        [self.chartTabLabel5 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake([YMCScreenAdapter sizeBy750:75], [YMCScreenAdapter sizeBy750:40]));
            make.right.equalTo(self.chartParentView.mas_left).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
            make.centerY.equalTo(self.lineView5);
        }];
//    }else{
//        self.lineView1 = [self lineView];
//        [parentview insertSubview:self.lineView1 belowSubview:self.chartParentView];
//        [self.lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(0.5);
//        }];
//        
//        self.lineView2 = [self lineView];
//        [parentview insertSubview:self.lineView2 belowSubview:self.chartParentView];
//        
//        self.lineView3 = [self lineView];
//        [parentview insertSubview:self.lineView3 belowSubview:self.chartParentView];
//        
//        self.lineView4 = [self lineView];
//        [parentview insertSubview:self.lineView4 belowSubview:self.chartParentView];
//        
////        self.lineView5 = [self lineView];
////        [parentview insertSubview:self.lineView5 belowSubview:self.chartParentView];
//        
//        self.lineArray = [NSArray arrayWithObjects:self.lineView1,self.lineView2,self.lineView3,self.lineView4, nil];
//        
//        CGFloat tailSpacing = [YMScreenAdapter sizeBy750:80];
////        if (self.type == YMHistogramChartDays) {
////            tailSpacing = [YMScreenAdapter sizeBy750:60];
////        }else{
////            tailSpacing = [YMScreenAdapter sizeBy750:108];
////        }
//        
//        [self.lineArray mas_distributeViewsAlongAxis:MASAxisTypeVertical withFixedItemLength:0.5 leadSpacing:0 tailSpacing:tailSpacing];
//        
//        [self.lineArray mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(parentview);
//            make.right.equalTo(parentview);
//        }];
//    }
   
}

- (UILabel *)chartTapLabel {
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor colorWithHexString:YMCOLOR_THEME];
    label.font = YM_FONT([YMCScreenAdapter fontsizeBy750:20]);
    label.textColor = [UIColor whiteColor];
    label.layer.cornerRadius = [YMCScreenAdapter sizeBy750:8];
    label.layer.masksToBounds = YES;
    label.textAlignment = NSTextAlignmentCenter;
    return label;
}

- (UIView *)lineView {
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    return lineView;
}

- (void)setDatas:(NSArray<YMChart *> *)datas {
    _datas = [datas copy];
    
//    if (self.type == YMHistogramChartShowTabs) {
        //tabs
        NSMutableAttributedString *tabsString = [YMChartTab stringOfTabs:self.datas withStyle:YMChartTabStyleAmount];
        self.nameTapsLabel.attributedText = tabsString;
//    }
    
    [self setNeedsDisplay];
    
}

- (void)drawRect:(CGRect)rect {
    for (UIView *view in self.chartParentView.subviews) {
        [view removeFromSuperview];
    }

    CGFloat totalMoney = 0;
    CGFloat bigValue = 0;
    
    for (YMChart *item in self.datas) {
        
        if (item.value > bigValue) {
            bigValue = item.value;
        }
        //money统计
        totalMoney += item.value;
    }
    //保留两位小数点显示 （by 谢立颖 2016.12.19）
//    self.totalLabel.text = [NSString stringWithFormat:@"¥%.2f",totalMoney];
    
    //计算表格的最大数值
    [self bigValue:bigValue array:self.tabValues];
    
    NSString *unitText = @"";
    CGFloat moneyText = 0;
    if (self.chartBigValue < 10000) {
        unitText = @"";
        moneyText = self.chartBigValue;
    }else if (self.chartBigValue < 10000000) {
        unitText = @"万";
        moneyText = self.chartBigValue/10000;
    }else{
        unitText = @"亿";
        moneyText = self.chartBigValue/100000000;
    }
    
    self.chartTabLabel1.text = [NSString stringWithFormat:@"%.0f%@",moneyText,unitText];
    
    CGFloat yuValue2 = fmod(moneyText*3,4);
    if (yuValue2 > 0) {
        self.chartTabLabel2.text = [NSString stringWithFormat:@"%.1f%@",moneyText/4 * 3,unitText];
    }else{
        self.chartTabLabel2.text = [NSString stringWithFormat:@"%.0f%@",moneyText/4 * 3,unitText];
    }
    
    CGFloat yuValue3 = fmod(moneyText*2,4);
    if (yuValue3 > 0) {
        self.chartTabLabel3.text = [NSString stringWithFormat:@"%.1f%@",moneyText/4 * 2,unitText];
    }else{
        self.chartTabLabel3.text = [NSString stringWithFormat:@"%.0f%@",moneyText/4 * 2,unitText];
    }
    
    CGFloat yuValue4 = fmod(moneyText,4);
    if (yuValue4 > 0) {
        self.chartTabLabel4.text = [NSString stringWithFormat:@"%.1f%@",moneyText/4,unitText];
    }else{
        self.chartTabLabel4.text = [NSString stringWithFormat:@"%.0f%@",moneyText/4,unitText];
    }

    self.chartTabLabel5.text = @"0";

    //图表
    //表格柱状图
    CGFloat chartViewHeight = 0;
//    if (self.type == YMHistogramChartShowTabs) {
         chartViewHeight = CGRectGetHeight(rect) - [YMCScreenAdapter sizeBy750:172] - CGRectGetHeight(self.nameTapsLabel.frame);
//    }else{
//        chartViewHeight = CGRectGetHeight(rect) - [YMScreenAdapter sizeBy750:124+80];
//    }
    
    CGFloat itemWidth = [YMCScreenAdapter sizeBy750:80];
    CGFloat itemSpace = [YMCScreenAdapter sizeBy750:24];
//    if (self.type != YMHistogramChartShowTabs) {
//        itemWidth = [YMScreenAdapter sizeBy750:60];
////        itemSpace = [YMScreenAdapter sizeBy750:24];
//    }
    CGFloat chartX = itemSpace/2;

    for (YMChart *item in self.datas) {
        
        NSNumber *value = @(item.value);
        
        //颜色
        NSInteger index = [self.datas indexOfObject:item];
        

        //[YMScreenAdapter sizeBy750:38] * 4;

        
        CGFloat chartViewHeight1 = chartViewHeight;
//        if (self.type == YMHistogramChartShowTabs) {
            //用于计算比例，需要减去[YMScreenAdapter sizeBy750:20](tab的高度的一半)
            chartViewHeight1 -= [YMCScreenAdapter sizeBy750:20];
//        }
        CGFloat chartHeight = (value.floatValue/self.chartBigValue) * chartViewHeight1;
        
        
//        if (self.type == YMHistogramChartShowTabs) {
            UIView *view  = [[UIView alloc] initWithFrame:CGRectMake(chartX, chartViewHeight - chartHeight, itemWidth, chartHeight)];
            UIColor *color = [YMChartTab colorOfTab:index];
            view.backgroundColor = color;
            [self.chartParentView addSubview:view];
//        }else{
//            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(chartX, 0, itemWidth, chartViewHeight)];
//            view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
//            
//            UIView *chartView  = [[UIView alloc] initWithFrame:CGRectMake(0, chartViewHeight - chartHeight, itemWidth, chartHeight)];
//            [view addSubview:chartView];
//            
//            view.tag = index;
//            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickItemView:)];
//            [view addGestureRecognizer:tap];
//            
//            UIColor *color = [UIColor colorWithHexString:YMCOLOR_THEME];
//            chartView.backgroundColor = color;
//            
//            [self.chartParentView addSubview:view];
//            
//            UILabel *tablabel = [[UILabel alloc] initWithFrame:CGRectMake(chartX - itemSpace/2, chartViewHeight, itemWidth + itemSpace, [YMScreenAdapter sizeBy750:60])];
//            tablabel.numberOfLines = 2;
//            tablabel.tag = 10000+index;
//            tablabel.textAlignment = NSTextAlignmentCenter;
//            tablabel.text = item.name;
//            tablabel.font = YM_FONT([YMScreenAdapter sizeBy750:24]);
//            [self.chartParentView addSubview:tablabel];
//            
//            if (index == self.datas.count - 1) {
//                chartView.alpha = 1;
//                tablabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
//            }else{
//                chartView.alpha = 0.6;
//                tablabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
//            }
//        }
        
        chartX += itemWidth + itemSpace;
        
    }
    
    self.chartParentView.contentSize = CGSizeMake(chartX - [YMCScreenAdapter sizeBy750:12], 1);
//    if (self.type != YMHistogramChartShowTabs) {
//        self.selectedIndex = self.datas.count - 1;
//        [self.chartParentView setContentOffset:CGPointMake(chartX - CGRectGetWidth(self.chartParentView.frame), 0)];
//    }
}

- (void)bigValue:(CGFloat)value array:(NSArray *)values {
    NSInteger index = values.count/2 - 1;

    if (index < 0) {
        index = 0;
        NSNumber *value = [values objectAtIndex:index];
        self.chartBigValue = value.floatValue;
    }
    
    NSNumber *itemValue = [values objectAtIndex:index];
    
    NSArray *array = nil;
    if (value < itemValue.floatValue) {
        array = [values subarrayWithRange:NSMakeRange(0, index + 1)];
    } else {
        array = [values subarrayWithRange:NSMakeRange(index + 1, values.count - index - 1)];
    }
    
    if (array.count == 1) {
        NSNumber *value = [array objectAtIndex:0];
        self.chartBigValue = value.floatValue;
    } else {
        [self bigValue:value array:array];
    }
}


//- (void)clickItemView:(UIGestureRecognizer *)gesture
//{
//    NSInteger index = gesture.view.tag;
//    if (self.selectedIndex == index) {
//        return;
//    }
//    
//    self.selectedIndex = index;
//    
//    for (UIView *view in self.chartParentView.subviews) {
//        if (view.tag == index) {
//            for (UIView *chartview in view.subviews) {
//                chartview.alpha = 1;
//            }
//        }else if(view.tag < 10000){
//            for (UIView *chartview in view.subviews) {
//                chartview.alpha = 0.6;
//            }
//        }else if (view.tag == 10000 + index){
//            UILabel *tabLabel = (UILabel *)view;
//            tabLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
//        }else{
//            UILabel *tabLabel = (UILabel *)view;
//            tabLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
//        }
//    }
//    
//    if (self.delegate && [self.delegate respondsToSelector:@selector(YMHistogramChartViewClickChartItem:)]) {
//        [self.delegate YMHistogramChartViewClickChartItem:index];
//    }
//}


@end
