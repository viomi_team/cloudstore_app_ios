//
//  YMLineCharView.h
//  yunSale
//
//  Created by liushilou on 16/12/28.
//  Copyright © 2016年 yunmi. All rights reserved.
//

//目前不支持滚动～

#import <UIKit/UIKit.h>
#import "YMChart.h"



@interface YMLineCharView : UIView

@property (nonatomic,copy) NSArray<YMChart *> *datas;

@property (nonatomic,strong) UILabel *titleLabel;

- (instancetype)initWidthDatas:(NSArray<YMChart *> *)datas;

@end
