//
//  YMLineCharView.m
//  yunSale
//
//  Created by liushilou on 16/12/28.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMLineCharView.h"
#import "YMLineChartDetailsView.h"

#import "YMChartDatas.h"


@interface YMLineCharView()

@property (nonatomic,strong) NSArray *tabValues;

@property (nonatomic,assign) CGFloat chartBigValue;

@property (nonatomic,strong) UILabel *chartTabLabel1;
@property (nonatomic,strong) UILabel *chartTabLabel2;
@property (nonatomic,strong) UILabel *chartTabLabel3;
@property (nonatomic,strong) UILabel *chartTabLabel4;

@property (nonatomic,strong) UIScrollView *scrollView;

@end




@implementation YMLineCharView

- (instancetype)initWidthDatas:(NSArray<YMChart *> *)datas
{
    self = [self init];
    if (self) {
        _datas = [datas copy];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        _tabValues = [YMChartDatas splitLineDatas];
        //[NSArray arrayWithObjects:
//                      @(15),@(30),@(60),@(90),
//                      @(150),@(300),@(600),@(900),
//                      @(1500),@(3000),@(6000),@(9000),
//                      @(15000),@(30000),@(60000),@(90000),
//                      @(150000),@(300000),@(600000),@(900000),
//                      nil];
        [self drawView];
    }
    return self;
}

- (void)drawView
{
    UIView *titleView = [[UIView alloc] init];
    [self addSubview:titleView];
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.top.equalTo(self);
        make.right.equalTo(self);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
    }];
    
    self.titleLabel = [[UILabel alloc] init];
//    self.titleLabel.text = @"123456";
    self.titleLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:26]);
    self.titleLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
    [titleView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(titleView).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
        make.centerY.equalTo(titleView);
    }];
    
    UIView *lineView1 = [[UIView alloc] init];
    lineView1.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [self addSubview:lineView1];
    
    UIView *lineView2 = [[UIView alloc] init];
    lineView2.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [self addSubview:lineView2];
    
    UIView *lineView3 = [[UIView alloc] init];
    lineView3.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [self addSubview:lineView3];
    
    UIView *lineView4 = [[UIView alloc] init];
    lineView4.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [self addSubview:lineView4];
    
    NSArray *lineArray = @[lineView1,lineView2,lineView3,lineView4];
    [lineArray mas_distributeViewsAlongAxis:MASAxisTypeVertical withFixedItemLength:0.5 leadSpacing:[YMCScreenAdapter sizeBy750:124] tailSpacing:[YMCScreenAdapter sizeBy750:80]];
    [lineArray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.right.equalTo(self);
    }];
    
//    self.chartTabLabel1 = [self chartTapLabel];
//    [self addSubview:self.chartTabLabel1];
//    self.chartTabLabel2 = [self chartTapLabel];
//    [self addSubview:self.chartTabLabel2];
//    self.chartTabLabel3 = [self chartTapLabel];
//    [self addSubview:self.chartTabLabel3];
//    self.chartTabLabel4 = [self chartTapLabel];
//    [self addSubview:self.chartTabLabel4];
//    
//    [self.chartTabLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).with.offset(5);
//        make.bottom.equalTo(lineView1.mas_top).offset(-5);
//    }];
//    [self.chartTabLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).with.offset(5);
//        make.bottom.equalTo(lineView2.mas_top).offset(-5);
//    }];
//    [self.chartTabLabel3 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).with.offset(5);
//        make.bottom.equalTo(lineView3.mas_top).offset(-5);
//    }];
//    [self.chartTabLabel4 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).with.offset(5);
//        make.bottom.equalTo(lineView4.mas_top).offset(-5);
//    }];

    self.scrollView = [[UIScrollView alloc] init];
    [self addSubview:self.scrollView];
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:24]);
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.bottom.equalTo(self);
    }];
    
}

- (UILabel *)chartTapLabel
{
    UILabel *label = [[UILabel alloc] init];
    label.font = YM_FONT([YMCScreenAdapter fontsizeBy750:20]);
    label.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
    return label;
}


- (void)setDatas:(NSArray<YMChart *> *)datas
{
    _datas = [datas copy];
    
    for (UIView *view in self.scrollView.subviews) {
        [view removeFromSuperview];
    }
    
    [self setNeedsDisplay];
}


- (void)drawRect:(CGRect)rect
{
    
    CGFloat bigValue = 0;
    for (YMChart *item in self.datas) {
        if (item.value > bigValue) {
            bigValue = item.value;
        }
    }
    //计算表格的最大数值
    [self bigValue:bigValue array:self.tabValues];
    
    NSLog(@"bigValue:%f",bigValue);
    NSLog(@"self.tabValues:%@",self.tabValues);
    NSLog(@"chartBigValue:%f",self.chartBigValue);
//    self.chartTabLabel1.text = [NSString stringWithFormat:@"￥%.0f", self.chartBigValue];
//    self.chartTabLabel2.text = [NSString stringWithFormat:@"￥%.0f", self.chartBigValue * (2.0/3)];
//    self.chartTabLabel3.text = [NSString stringWithFormat:@"￥%.0f", self.chartBigValue * (1.0/3)];
//    self.chartTabLabel4.text = [NSString stringWithFormat:@"0"];

    
    CGFloat itemWidth = CGRectGetWidth(rect)/7;
    CGFloat width = itemWidth * self.datas.count;
    
    self.scrollView.contentSize = CGSizeMake(width, 1);
    YMLineChartDetailsView *detailsView = [[YMLineChartDetailsView alloc] initWithFrame:CGRectMake(0, 0, width, CGRectGetHeight(rect) - [YMCScreenAdapter sizeBy750:124])];
    detailsView.datas = self.datas;
    detailsView.chartBigValue = self.chartBigValue;
    [self.scrollView addSubview:detailsView];
//    [detailsView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(self.scrollView);
//    }];
    
    [detailsView draw];
    
    
//    CGFloat itemWidth = CGRectGetWidth(rect)/7;
//    CGFloat width = itemWidth * self.datas.count;
//    
//    self.scrollView.contentSize = CGSizeMake(width, 1);
    
//    chartView *chartview = [[chartView alloc] init];
//    chartview.datas = self.datas;
//    [self.scrollView addSubview:chartview];
//    [chartview mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(self.scrollView);
//    }];
    
    
    
    
//    CGFloat y = [YMScreenAdapter sizeBy750:24];
//    CGFloat chartHeight = CGRectGetHeight(rect) - y - [YMScreenAdapter sizeBy750:72];
//    CGFloat space = chartHeight/3;
//    
//    for (NSInteger i = 0; i < 4; i ++) {
//        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, y, width, 0.5)];
//        lineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
//        [self addSubview:lineView];
//        
//        y += space + 0.5;
//    }
//    
//    
//    CGFloat bigValue = 0;
//
//    for (YMChart *item in self.datas) {
//        
//        if (item.value > bigValue) {
//            bigValue = item.value;
//        }
//    }
//    //计算表格的最大数值
//    [self bigValue:bigValue array:self.tabValues];
//    
//    //创建贝塞尔曲线
//    UIBezierPath *path = [UIBezierPath bezierPath];
//    CGFloat x = itemWidth/2;
//    
//    for (YMChart *item in self.datas) {
//        
//        CGFloat chartValue = chartHeight - item.value/self.chartBigValue * chartHeight + [YMScreenAdapter sizeBy750:24];
//        
//        CGPoint point = CGPointMake(x, chartValue);
//        
//        NSUInteger index = [self.datas indexOfObject:item];
//        if (index == 0) {
//            [path moveToPoint:point];
//        }else{
//            [path addLineToPoint:point];
//        }
//        
//        [self addArcAtpoint:point];
//        
//        x += itemWidth;
//    }
//    
//    UIColor *stroke = [UIColor colorWithRed:29/255.0 green:138/255.0 blue:203/255.0 alpha:0.6];               //设置红色画笔线
//    [stroke set];                                       //填充颜色
//    [path setLineWidth:4];
//    [path stroke];                                     //贝塞尔线进行画笔填充
    
    
    
    
    
    
//    
//    //创建贝塞尔曲线
//    UIBezierPath *mPath = [UIBezierPath bezierPath];
//    [mPath moveToPoint:CGPointMake(0, 200)]; //创建一个点
//    [self addArcAtpoint:CGPointMake(0, 200)];
//    
//    [mPath addQuadCurveToPoint:CGPointMake(50, 150) controlPoint:CGPointMake(30, 150)];
//    [self addArcAtpoint:CGPointMake(50, 150)];
//    
////    [mPath addQuadCurveToPoint:CGPointMake(100, 125) controlPoint:CGPointMake(75, 135.5)];
//    //[mPath addCurveToPoint:CGPointMake(50, 150) controlPoint1:CGPointMake(120, 200) controlPoint2:CGPointMake(130, 50)];
//    [mPath addCurveToPoint:CGPointMake(100, 125) controlPoint1:CGPointMake(70, 150) controlPoint2:CGPointMake(80, 125)];
//    [self addArcAtpoint:CGPointMake(100, 125)];
//    [mPath addCurveToPoint:CGPointMake(150, 50) controlPoint1:CGPointMake(120, 125) controlPoint2:CGPointMake(130, 50)];
//    //[mPath addQuadCurveToPoint:CGPointMake(150, 50) controlPoint:CGPointMake(115, 120)];
//    [self addArcAtpoint:CGPointMake(150, 50)];
//    [mPath addQuadCurveToPoint:CGPointMake(200, 200) controlPoint:CGPointMake(170, 50)];
//    [self addArcAtpoint:CGPointMake(200, 200)];
    
    
    
    
//    [mPath addCurveToPoint:CGPointMake(50, 150) controlPoint1:CGPointMake(10, 195) controlPoint2:CGPointMake(40, 156)];
    
    
    //    [mPath addQuadCurveToPoint:CGPointMake(50, 150) controlPoint:CGPointMake(0, 150)];
    //    [mPath addQuadCurveToPoint:CGPointMake(50, 150) controlPoint:CGPointMake(0, 150)];
    //    [mPath addQuadCurveToPoint:CGPointMake(50, 150) controlPoint:CGPointMake(0, 150)];
    //    [mPath addQuadCurveToPoint:CGPointMake(50, 150) controlPoint:CGPointMake(0, 150)];
    
    //    [mPath addLineToPoint:CGPointMake(100, 300)]; // 加条线,从点移动到另一个点
    //    [mPath addLineToPoint:CGPointMake(300, 300)]; // 加条线,从点移动到另一个点
    //[mPath closePath]; // 关闭贝塞尔线
    

}
- (void)bigValue:(CGFloat)value array:(NSArray *)values {
    NSInteger index = values.count/2 - 1;
    
    if (index < 0) {
        index = 0;
        NSNumber *value = [values objectAtIndex:index];
        //        self.chartBigValue = value.floatValue ;
        self.chartBigValue = value.floatValue + value.floatValue /8;  //最大加增加1/8,留点空间给数字Label显示
    }
    
    NSNumber *itemValue = [values objectAtIndex:index];
    
    NSArray *array = nil;
    if (value < itemValue.floatValue) {
        array = [values subarrayWithRange:NSMakeRange(0, index + 1)];
    } else {
        array = [values subarrayWithRange:NSMakeRange(index + 1, values.count - index - 1)];
    }
    
    if (array.count == 1) {
        NSNumber *value = [array objectAtIndex:0];
//        self.chartBigValue = value.floatValue ;
        self.chartBigValue = value.floatValue + value.floatValue /8; //最大加增加1/8,留点空间给数字Label显示
    } else {
        [self bigValue:value array:array];
    }
}


@end
