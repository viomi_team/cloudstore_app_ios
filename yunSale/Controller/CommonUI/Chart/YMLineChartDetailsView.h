//
//  YMLineChartDetailsView.h
//  yunSale
//
//  Created by liushilou on 17/1/3.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMChart.h"

@interface YMLineChartDetailsView : UIView

@property (nonatomic,strong) NSArray *tabValues;

@property (nonatomic,assign) CGFloat chartBigValue;

@property (nonatomic,copy) NSArray<YMChart *> *datas;

- (void)draw;

@end
