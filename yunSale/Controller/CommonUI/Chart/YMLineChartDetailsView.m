//
//  YMLineChartDetailsView.m
//  yunSale
//
//  Created by liushilou on 17/1/3.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMLineChartDetailsView.h"

@implementation YMLineChartDetailsView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)draw
{
    self.backgroundColor = [UIColor clearColor];
    
    [self setNeedsDisplay];
}



- (void)drawRect:(CGRect)rect
{
    CGFloat itemWidth = CGRectGetWidth(rect)/7;
    //    CGFloat width = itemWidth * self.datas.count;
    CGFloat chartHeight = CGRectGetHeight(rect) - [YMCScreenAdapter sizeBy750:80];
    
    //创建贝塞尔曲线
    UIBezierPath *path = [UIBezierPath bezierPath];
    CGFloat x = itemWidth/2;
    
    for (YMChart *item in self.datas) {
        
        CGFloat chartValue = chartHeight - item.value/self.chartBigValue * chartHeight;
        
        CGPoint point = CGPointMake(x, chartValue);
        
        NSUInteger index = [self.datas indexOfObject:item];
        if (index == 0) {
            [path moveToPoint:point];
        }else{
            [path addLineToPoint:point];
        }
        
        [self addArcAtpoint:point];
        
        UILabel *valueLabel = [[UILabel alloc] initWithFrame:CGRectMake(x - itemWidth/2, chartValue - 20, itemWidth, 20)];
        valueLabel.textAlignment = NSTextAlignmentCenter;
        valueLabel.textColor = [UIColor colorWithHexString:@"#34495e"];
        valueLabel.font = YM_FONT([YMCScreenAdapter sizeBy750:20]);
        valueLabel.text = [NSString stringWithFormat:@"%.0f", item.value];
        //@(item.value).stringValue;
       // valueLabel.text = @"12345678";
        //[NSString stringWithFormat:@"￥%.0f", item.value];
        [self addSubview:valueLabel];
        
        UILabel *tablabel = [[UILabel alloc] initWithFrame:CGRectMake(x - itemWidth/2, chartHeight, itemWidth, [YMCScreenAdapter sizeBy750:80])];
        tablabel.numberOfLines = 2;
        tablabel.textAlignment = NSTextAlignmentCenter;
        tablabel.text = item.name;
        tablabel.font = YM_FONT([YMCScreenAdapter sizeBy750:24]);
        tablabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [self addSubview:tablabel];
        
        x += itemWidth;
    }
    
    UIColor *stroke = [UIColor colorWithRed:29/255.0 green:138/255.0 blue:203/255.0 alpha:0.6];
    [stroke set];                                       //填充颜色
    [path setLineWidth:[YMCScreenAdapter sizeBy750:4]];
    [path stroke];                                     //贝塞尔线进行画笔填充
    
    
}

- (void)addArcAtpoint:(CGPoint)point
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path addArcWithCenter:point radius:[YMCScreenAdapter sizeBy750:10] startAngle:0 endAngle:2*M_PI clockwise:YES];
    UIColor *fillColor = [UIColor colorWithHexString:@"#1d8acb"];          //设置颜色
    [fillColor set];                                    //填充颜色
    [path fill];                                       //贝塞尔线进行填充
    
}




@end
