//
//  YMPiechartView.h
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

// 饼图

#import <UIKit/UIKit.h>
//#import "YMStore.h"
#import "YMChartTab.h"
#import "YMChart.h"


#define YM_PIECHART_HEIGHT [YMCScreenAdapter sizeBy750:500]


@interface YMPieChartView : UIView

@property (nonatomic,copy) NSArray<YMChart *> *datas;

@end
