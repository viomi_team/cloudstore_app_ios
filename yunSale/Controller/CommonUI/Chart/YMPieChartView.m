//
//  YMPiechartView.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMPieChartView.h"


@interface YMPieChartView()

@property (nonatomic,strong) NSMutableArray *layerArray;

@end



@implementation YMPieChartView

- (id)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        _layerArray = [NSMutableArray new];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        _layerArray = [NSMutableArray new];
    }
    return self;
}

- (void)setDatas:(NSArray<YMChart *> *)datas {
    _datas = [datas copy];
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    //Terminating app due to uncaught exception 'NSGenericException', reason: '*** Collection <CALayerArray: 0x17405d640> was mutated while being enumerated.'
    //当程序出现这个提示的时候，是因为你一边便利数组，又同时修改这个数组里面的内容，导致崩溃
    //疑问：为什么会修改呢。。。操作view不会出问题呀。。
//    for (CALayer *layer in self.layer.sublayers) {
//        [layer removeFromSuperlayer];
//    }
    
    for (CAShapeLayer *layer in self.layerArray) {
        [layer removeFromSuperlayer];
    }
    [self.layerArray removeAllObjects];
    
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    CGFloat startAngle = -M_PI/2;
    
    //tabs
    NSMutableAttributedString *tabsString = [YMChartTab stringOfTabs:self.datas withStyle:YMChartTabStylePercentage];
    
    CGFloat radius = YM_PIECHART_HEIGHT/2 - [YMCScreenAdapter sizeBy750:30];
    CGPoint centerPoint = CGPointMake(self.bounds.size.width/2, radius + [YMCScreenAdapter sizeBy750:30]);
    
    if (!self.datas || self.datas.count == 0) {
        [self drawCricleLayer:centerPoint startAngle:startAngle endAngle:M_PI * 1.5 radius:radius index:-1];
    }else{
        UILabel *tabsLabel = [[UILabel alloc] init];
        tabsLabel.textAlignment = NSTextAlignmentLeft;
        tabsLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:24]);
        tabsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        tabsLabel.numberOfLines = 0;
        tabsLabel.attributedText = tabsString;
        [self addSubview:tabsLabel];
        [tabsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.right.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
            make.bottom.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
        }];
    }
    
    CGFloat totalcount = 0;
    for (YMChart *chart in self.datas) {
        NSNumber *count = @(chart.value);;
        totalcount += count.floatValue;
    }
    
    for (YMChart *chart in self.datas) {

        NSNumber *count = @(chart.value);
        
        CGFloat percentage = count.floatValue/totalcount;
        
        CGFloat angle = percentage * M_PI * 2;
        CGFloat endAngle = angle + startAngle;
        
        NSLog(@"percentage:%f",percentage);
        NSInteger index = [self.datas indexOfObject:chart];
        [self drawCricleLayer:centerPoint startAngle:startAngle endAngle:endAngle radius:radius index:index];
        
        startAngle = endAngle;
    }
    
    
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    CGFloat centerRadius = radius - [YMCScreenAdapter sizeBy750:80];
    [path moveToPoint:centerPoint];
    [path addArcWithCenter:centerPoint radius:centerRadius startAngle:0 endAngle:2*M_PI clockwise:YES];
    [path closePath];
    
    layer.path = path.CGPath;
    layer.strokeColor = [UIColor whiteColor].CGColor;
    layer.fillColor = [UIColor whiteColor].CGColor;
    [self.layer addSublayer:layer];
    [self.layerArray addObject:layer];
    
    UILabel *totleLabel = [[UILabel alloc] init];
    totleLabel.text = @(totalcount).stringValue;
    totleLabel.font = [UIFont boldSystemFontOfSize:[YMCScreenAdapter sizeBy750:80]];
    totleLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    [totleLabel sizeToFit];
    [self addSubview:totleLabel];
    [totleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self).with.offset(radius-[YMCScreenAdapter sizeBy750:15]);
    }];
    
    UILabel *tipLabel = [[UILabel alloc] init];
    tipLabel.text = @"订单";
    tipLabel.font = YM_FONT([YMCScreenAdapter sizeBy750:26]);
    tipLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    [tipLabel sizeToFit];
    [self addSubview:tipLabel];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(totleLabel.mas_bottom).with.offset(0);
    }];
}

- (void)drawCricleLayer:(CGPoint)centerPoint startAngle:(CGFloat)startAngle endAngle:(CGFloat)endAngle radius:(CGFloat)radius index:(NSInteger)index {
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:centerPoint];
    [path addArcWithCenter:centerPoint radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES];
    [path closePath];
    
    layer.path = path.CGPath;
    
    //NSInteger index = [self.datas indexOfObject:store];
    UIColor *color = [YMChartTab colorOfTab:index];

    layer.strokeColor = color.CGColor;
    layer.fillColor = color.CGColor;
    
    [self.layer addSublayer:layer];
    
    [self.layerArray addObject:layer];
}

@end
