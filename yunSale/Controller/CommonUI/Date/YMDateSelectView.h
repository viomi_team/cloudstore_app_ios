//
//  YMDateSelectView.h
//  yunSale
//
//  Created by liushilou on 16/12/1.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMDateSelectView : UIView

@property (nonatomic,copy,readonly) NSString *tip;

@property (nonatomic,copy,readonly) NSString *placeHoder;

@property (nonatomic,copy) NSString *dateValue;

- (instancetype)initWithTip:(NSString *)tip placeHoder:(NSString *)placeHoder;

@end
