//
//  YMDateSelectView.m
//  yunSale
//
//  Created by liushilou on 16/12/1.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMDateSelectView.h"
#import <ActionSheetPicker.h>
#import "YMDate.h"

@interface YMDateSelectView()

@property (nonatomic,strong) UILabel *valueLabel;

@end


@implementation YMDateSelectView

- (instancetype)initWithTip:(NSString *)tip placeHoder:(NSString *)placeHoder
{
    self = [super init];
    if (self) {
        _tip = [tip copy];
        _placeHoder = [placeHoder copy];
        
        UILabel *tipLabel = [[UILabel alloc] init];
        tipLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE]);
        tipLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        tipLabel.text = tip;
        tipLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:tipLabel];
        [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset(0);
            make.centerY.equalTo(self).with.offset(5);
            make.width.mas_equalTo([YMCScreenAdapter fontsizeBy750:140]);
        }];
        
        UIView *view = [[UIView alloc] init];
        [self addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(tipLabel.mas_right).with.offset([YMCScreenAdapter sizeBy750:30]);
            make.top.equalTo(self);
            make.bottom.equalTo(self);
            make.right.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND_MID]);
        }];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickAction)];
        [view addGestureRecognizer:tap];
        
        UIImageView *iconView = [[UIImageView alloc] init];
        iconView.image = [UIImage imageNamed:@"ym_index_selector"];
        [view addSubview:iconView];
        [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(view).with.offset(5);
            make.right.equalTo(view);
            make.size.mas_equalTo(CGSizeMake([YMCScreenAdapter sizeBy750:20], [YMCScreenAdapter sizeBy750:16]));
        }];
        
        self.valueLabel = [[UILabel alloc] init];
        self.valueLabel.text = placeHoder;
        self.valueLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
        self.valueLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        [view addSubview:self.valueLabel];
        [self.valueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(view);
            make.centerY.equalTo(view).with.offset(5);
            make.right.equalTo(iconView.mas_left);
        }];
        
        UIView *lineView  = [[UIView alloc] init];
        lineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [view addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(view);
            make.bottom.equalTo(view);
            make.right.equalTo(view);
            make.height.mas_equalTo(0.5);
        }];
    }
    return self;
}


#pragma action
- (void)clickAction
{
    NSDate *date = [NSDate date];
    if (self.dateValue) {
        date = [YMDate dateWithFormatString:self.dateValue];
    }

    @weakify(self);
    ActionSheetDatePicker *picker = [[ActionSheetDatePicker alloc] initWithTitle:self.tip datePickerMode:UIDatePickerModeDate selectedDate:date doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        @strongify(self);
        
        NSString *dateString = [YMDate stringForDislayFromDate:[YMDate dateToZoneEightWithDate:selectedDate] withFormat:YMDateStringFormat_YMD];
        
        self.dateValue = dateString;

    } cancelBlock:^(ActionSheetDatePicker *picker) {

    } origin:self.superview];

    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:nil action:nil];
    [picker setCancelButton:cancelButton];

    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:nil action:nil];
    [picker setDoneButton:doneButton];
    
    [picker showActionSheetPicker];
}

#pragma setter getter

- (void)setDateValue:(NSString *)dateValue
{
     _dateValue= [dateValue copy];
    
    self.valueLabel.text = dateValue;
}



@end
