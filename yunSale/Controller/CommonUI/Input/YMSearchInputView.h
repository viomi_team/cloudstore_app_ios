//
//  YMSearchInputView.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMSearchInputView : UIView

@property (nonatomic, strong) UITextField *textField;

- (id)initWithTitle:(NSString *)title placeHolder:(NSString *)placeHolder;

@end
