//
//  YMSearchInputView.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMSearchInputView.h"

@implementation YMSearchInputView

- (id)initWithTitle:(NSString *)title placeHolder:(NSString *)placeHolder {
    self = [super init];
    if (self) {
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = title;
        titleLabel.textAlignment = NSTextAlignmentRight;
        titleLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        titleLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE]);
        [self addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset(0);
            make.bottom.equalTo(self).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.width.mas_equalTo([YMCScreenAdapter fontsizeBy750:140]);
        }];
        
        _textField = [[UITextField alloc] init];
        _textField.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        _textField.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE]);
        _textField.placeholder = placeHolder;
        [self addSubview:_textField];
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(titleLabel.mas_right).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROOUND_BIG]);
            make.centerY.equalTo(titleLabel);
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:80]);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:550]);
        }];
        
        UIView *bottomLineView = [[UIView alloc] init];
        bottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [self addSubview:bottomLineView];
        [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self);
            make.left.equalTo(_textField.mas_left);
            make.height.mas_equalTo(0.5);
            make.right.equalTo(self);
        }];
    }
    
    return self;
}

@end
