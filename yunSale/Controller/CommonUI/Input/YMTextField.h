//
//  YMTextField.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, YMTextFieldStyle) {
    YMTextFieldStyleNormal,
    YMTextFieldStyleVisualEffect,
    YMTextFieldStyleSendcodeAction,
};
typedef NS_ENUM(NSInteger,YMTextFieldStatus)
{
    YMTextFieldStatusError,
    YMTextFieldStatusNormal,
    YMTextFieldStatusSendcoded
};


@protocol YMTextFieldDelegate;  //提前声明


@interface YMTextField : UITextField

@property (nonatomic,weak) id<YMTextFieldDelegate> myDelegate;

- (id)initWithStyle:(YMTextFieldStyle)style;

-(void)changeStatus:(YMTextFieldStatus)status;

+ (BOOL)isLegalString:(NSString *)string;

@end



@protocol YMTextFieldDelegate <NSObject>

@optional
//returnKeyType == UIReturnKeyDone时，并且键盘消失时，会执行。
- (void)YMTextFieldDone:(YMTextField *)textfield;

- (void)YMTextFieldAction:(YMTextField *)textfield;

@end
