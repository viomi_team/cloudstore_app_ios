//
//  YMTextField.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMTextField.h"
#import <YMCommon/UIColor+ymc.h>

@interface YMTextField() <UITextFieldDelegate>

@property (nonatomic,strong) UIColor *color;

@property (nonatomic,assign) YMTextFieldStyle style;

@property (nonatomic,strong) UIButton *actionBtn;

@property (nonatomic,assign) NSInteger count;

@end

@implementation YMTextField

- (id)initWithStyle:(YMTextFieldStyle)style {
    self = [super init];
    
    if (self) {
        _style = style;
        
        self.backgroundColor = [UIColor clearColor];
        self.delegate = self;
        self.textAlignment = NSTextAlignmentCenter;
        self.font = YM_FONT([YMCScreenAdapter sizeBy750:32]);
        self.layer.borderWidth = 1;
        
        if (style == YMTextFieldStyleNormal || style == YMTextFieldStyleSendcodeAction) {
            //普通样式
            if (style == YMTextFieldStyleSendcodeAction) {
                self.textAlignment = NSTextAlignmentLeft;
            }
            self.color = [UIColor colorWithHexString:YMCOLOR_THEME];
            self.tintColor = self.color;
            self.textColor = self.color;
            self.layer.borderColor = [[UIColor colorWithHexString:YMCOLOR_LINE] CGColor];
        } else if(style == YMTextFieldStyleVisualEffect) {
            //登陆界面使用的样式
            self.color = [UIColor whiteColor];
            self.tintColor = self.color;
            self.textColor = self.color;
            self.layer.borderColor = [[UIColor whiteColor] CGColor];
            
        }
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    self.layer.cornerRadius = rect.size.height/2;
    self.layer.masksToBounds = YES;
    
    CGRect frame = CGRectMake(0, 0, rect.size.height/2, 1);
    UIView *view = [[UIView alloc] initWithFrame:frame];
    self.leftViewMode = UITextFieldViewModeAlways;
    self.leftView = view;
    
    if (self.style == YMTextFieldStyleSendcodeAction) {
        
        UIView *btnview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [YMCScreenAdapter sizeBy750:300], rect.size.height)];
        self.actionBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        self.actionBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        self.actionBtn.titleLabel.font = YM_FONT([YMCScreenAdapter sizeBy750:32]);
        [self.actionBtn setTitleColor:[UIColor colorWithHexString:YMCOLOR_THEME] forState:UIControlStateNormal];
        [self.actionBtn setTitleColor:[UIColor colorWithHexString:YMCOLOR_DARK_GRAY] forState:UIControlStateHighlighted];
        [self.actionBtn setTitleColor:[UIColor colorWithHexString:YMCOLOR_GRAY] forState:UIControlStateDisabled];
        [self.actionBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [self.actionBtn addTarget:self action:@selector(actionBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [btnview addSubview:self.actionBtn];
        [self.actionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(btnview).with.offset(0);
            make.top.equalTo(btnview).with.offset(0);
            make.bottom.equalTo(btnview).with.offset(0);
            make.right.equalTo(btnview).with.offset(-rect.size.height/2);
        }];
        self.rightViewMode = UITextFieldViewModeAlways;
        self.rightView = btnview;
    } else {
        self.rightViewMode = UITextFieldViewModeAlways;
        self.rightView = view;
    }
}

#pragma mark - textFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.textColor = self.color;
    [textField setValue:[UIColor colorWithHexString:@"#CCCCCC"] forKeyPath:@"_placeholderLabel.textColor"];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        //发现下一个响应，所以设置
        [nextResponder becomeFirstResponder];
    } else {
        //没有找到，所以移除键盘
        [textField resignFirstResponder];
        if (textField.returnKeyType == UIReturnKeyDone) {
            if (self.myDelegate && [self.myDelegate respondsToSelector:@selector(YMTextFieldDone:)]) {
                [self.myDelegate YMTextFieldDone:self];
            }
        }
    }
    
    return YES;
}

#pragma mark - Public method
-(void)changeStatus:(YMTextFieldStatus)status {
    if (status == YMTextFieldStatusError) {
        self.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
        [self setValue:[UIColor colorWithHexString:YMCOLOR_RED] forKeyPath:@"_placeholderLabel.textColor"];
    }else if(status == YMTextFieldStatusSendcoded){
        
        self.actionBtn.enabled = NO;
        self.count = 60;
        [self countDown];
        
    }else{
        self.textColor = self.color;
        [self setValue:[UIColor colorWithHexString:@"#CCCCCC"] forKeyPath:@"_placeholderLabel.textColor"];
    }
}

//此方法将丢弃~~，NSString已经封装过一次。by zcm.
+ (BOOL)isLegalString:(NSString *)string {
    if ([NSString isEmptyString:string]) {
        return NO;
    }
    
    //判断字符串是否存在空格
    if ([string isContainSpace]) {
        return NO;
    }
    
    return YES;
}


#pragma mark private method
- (void)countDown
{
    NSString *title = [NSString stringWithFormat:@"%d秒后重新获取",(int)self.count];
    self.actionBtn.titleLabel.text = title;
    [self.actionBtn setTitle:title forState:UIControlStateDisabled];
    
    self.count -= 1;
    if (self.count < 0) {
        self.actionBtn.titleLabel.text = @"获取验证码";
        //[self.actionBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        self.actionBtn.enabled = YES;
        return;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self countDown];
    });
}




#pragma mark action

- (void)actionBtnClick
{
    if (self.myDelegate && [self.myDelegate respondsToSelector:@selector(YMTextFieldAction:)]) {
        [self.myDelegate YMTextFieldAction:self];
    }
}




@end
