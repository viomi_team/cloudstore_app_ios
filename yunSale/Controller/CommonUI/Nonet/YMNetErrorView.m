//
//  YMNoNetView.m
//  yunSale
//
//  Created by liushilou on 16/12/2.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMNetErrorView.h"

@interface YMNetErrorView()

@property (nonatomic,strong) UIImageView *iconView;
@property (nonatomic,strong) UILabel *messageLabel;
@property (nonatomic,strong) UIButton *reloadBtn;

@property (nonatomic,copy) void (^actionBlock)();

@end


@implementation YMNetErrorView

- (instancetype)initWithActionBlock:(void (^)())block
{
    self = [super init];
    if (self) {
        _actionBlock = [block copy];
        
        self.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        
        UIView *parentView = [[UIView alloc] init];
        [self addSubview:parentView];
        [parentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.centerY.equalTo(self);
        }];
        
        _iconView = [[UIImageView alloc] init];
        _iconView.contentMode = UIViewContentModeBottom;
        _iconView.image = [UIImage imageNamed:@"ym_nonet"];
        [parentView addSubview:_iconView];
        [_iconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(parentView);
            make.size.mas_equalTo(CGSizeMake([YMScreenAdapter sizeBy750:406], [YMScreenAdapter sizeBy750:241]));
            make.centerX.equalTo(parentView);
        }];
        
        _messageLabel = [[UILabel alloc] init];
        _messageLabel.textAlignment = NSTextAlignmentCenter;
        _messageLabel.numberOfLines = 0;
        _messageLabel.font = YM_FONT([YMScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
        _messageLabel.textColor = [UIColor colorWithHexString:YMCOLOR_DARK_GRAY];
        [parentView addSubview:_messageLabel];
        [_messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_iconView.mas_bottom).with.offset([YMScreenAdapter sizeBy750:60]);
            make.left.equalTo(parentView).with.offset(20);
            make.right.equalTo(parentView).with.offset(-20);
            make.centerX.equalTo(parentView);
        }];
        
        _reloadBtn = [[UIButton alloc] init];
        _reloadBtn.layer.borderColor = [UIColor colorWithHexString:YMCOLOR_LINE].CGColor;
        _reloadBtn.layer.borderWidth = 1;
        [_reloadBtn setTitle:@"重新加载" forState:UIControlStateNormal];
        [_reloadBtn setTitleColor:[UIColor colorWithHexString:YMCOLOR_DARK_GRAY] forState:UIControlStateNormal];
        [_reloadBtn.titleLabel setFont:YM_FONT([YMScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE])];
        [_reloadBtn addTarget:self action:@selector(reloadAction) forControlEvents:UIControlEventTouchUpInside];
        [parentView addSubview:_reloadBtn];
        [_reloadBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_messageLabel.mas_bottom).with.offset([YMScreenAdapter sizeBy750:60]);
            make.bottom.equalTo(parentView).with.offset(0);
            make.size.mas_equalTo(CGSizeMake([YMScreenAdapter sizeBy750:300], [YMScreenAdapter sizeBy750:80]));
            make.centerX.equalTo(parentView);
        }];
        
    }
    return self;
}


- (void)reloadAction
{
    self.actionBlock();
//    self.actionBlock = nil;
//    [self removeFromSuperview];
}

#pragma setter

- (void)setErorType:(YMRequestStatus)erorType
{
    _erorType = erorType;
    if (erorType == YMRequestNetError) {
        self.iconView.image = [UIImage imageNamed:@"ym_nonet"];
    }else if(erorType == YMRequestNoData){
        self.iconView.image = [UIImage imageNamed:@"ym_server_nodata"];
        [_reloadBtn setTitle:@"重新查询" forState:UIControlStateNormal];
    }else if(erorType == YMRequestNoDataAndNoBtn){
        self.iconView.image = [UIImage imageNamed:@"ym_server_nodata"];
        _reloadBtn.hidden = YES;
    }else{
        self.iconView.image = [UIImage imageNamed:@"ym_server_error"];
    }
}

- (void)setMessage:(NSString *)message
{
    _message = [message copy];
    self.messageLabel.text = message;
}

@end
