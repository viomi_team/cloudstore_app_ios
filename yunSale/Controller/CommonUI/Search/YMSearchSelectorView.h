//
//  YMSearchSelectorView.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/18.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMSearchSelectorView : UIView

@property (nonatomic, strong) UIButton *selectorBtn;

@property (nonatomic, strong) NSString *placeHolderString;

@property (nonatomic, strong) UILabel *placeHolderLabel;

- (id)initWithTitle:(NSString *)title placeHolder:(NSString *)placeHolder;

@end
