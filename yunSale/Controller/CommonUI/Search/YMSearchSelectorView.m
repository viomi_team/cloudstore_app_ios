//
//  YMSearchSelectorView.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/18.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMSearchSelectorView.h"

@implementation YMSearchSelectorView

- (id)initWithTitle:(NSString *)title placeHolder:(NSString *)placeHolder
{
    self = [super init];
    if (self) {
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE]);
        titleLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        titleLabel.text = title;
        [self addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset([YMCScreenAdapter fontsizeBy750:YMSPACE_AROUND_MID]);
            make.bottom.equalTo(self).with.offset(-[YMCScreenAdapter sizeBy750:18]);
        }];
        
        _placeHolderLabel = [[UILabel alloc] init];
        _placeHolderLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE]);
        _placeHolderLabel.textColor = [UIColor lightGrayColor];
        _placeHolderLabel.text = placeHolder;
        [self addSubview:_placeHolderLabel];
        [_placeHolderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(titleLabel.mas_right).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.centerY.equalTo(titleLabel.mas_centerY);
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:80]);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:550]);
        }];
        
        UIImageView *selectImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ym_index_selector"]];
        [self addSubview:selectImageView];
        [selectImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.centerY.equalTo(titleLabel.mas_centerY);
        }];
        
        UIView *bottomLineView = [[UIView alloc] init];
        bottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [self addSubview:bottomLineView];
        [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_placeHolderLabel.mas_left);
            make.bottom.equalTo(self);
            make.right.equalTo(selectImageView.mas_right);
            make.height.mas_equalTo(0.5);
        }];
        
        _selectorBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        _selectorBtn.backgroundColor = [UIColor clearColor];
        [self addSubview:_selectorBtn];
        [_selectorBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_placeHolderLabel);
        }];
    }
    
    return self;
}

- (void)setPlaceHolderString:(NSString *)placeHolderString
{
    _placeHolderLabel.text = [placeHolderString copy];
}

@end
