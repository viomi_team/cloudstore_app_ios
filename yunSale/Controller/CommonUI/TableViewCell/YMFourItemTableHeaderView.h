//
//  YMFourItemTableHeaderView.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/22.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, YMFourItemHeaderViewStyle) {
    YMFourItemHeaderViewStyleStoreDaily,
    YMFourItemHeaderViewStyleSalesData,
    YMFourItemHeaderViewStyleDealerManager,
    YMFourItemHeaderViewStyleStoreManager,
    YMFourItemHeaderViewSytleDealerReport
};

@interface YMFourItemTableHeaderView : UIView

@property (nonatomic,strong) UILabel *firstLabel;
@property (nonatomic,strong) UILabel *secondLabel;
@property (nonatomic,strong) UILabel *thirdLabel;
@property (nonatomic,strong) UILabel *fourthLabel;

//
//@property (strong, nonatomic)NSString* fourthItemString;

- (id)initWithFrame:(CGRect)frame style:(YMFourItemHeaderViewStyle)style;

@end
