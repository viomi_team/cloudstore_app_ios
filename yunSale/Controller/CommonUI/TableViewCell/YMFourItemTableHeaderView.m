//
//  YMFourItemTableHeaderView.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/22.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMFourItemTableHeaderView.h"

@implementation YMFourItemTableHeaderView

- (id)initWithFrame:(CGRect)frame style:(YMFourItemHeaderViewStyle)style
{
//    self.fourthItemString = @"--";
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        CGFloat width1 = 0,width2 = 0,width3 = 0,width4 = 0;
        NSString *titleString1 = @"", *titleString2 = @"", *titleString3 = @"", *titleString4 = @"";
        
        //门店日报的页面样式
        if (style == YMFourItemHeaderViewStyleStoreDaily) {
            width1 = 190;
            width2 = 210;
            width3 = 150;
            width4 = 95;
            titleString1 = @"日期";
            titleString2 = @"门店名称";
            titleString3 = @"微信新会员";
            titleString4 = @"订单数";
        }
        //销售数据页面的样式
        else if (style == YMFourItemHeaderViewStyleSalesData) {
            width1 = 260;
            width2 = 120;
            width3 = 150;
            width4 = 140;
            titleString1 = @"渠道名称";
            titleString2 = @"订单数";
            titleString3 = @"销售额";
            titleString4 = @"提成";
        }
        //经销商管理页面的样式
        else if (style == YMFourItemHeaderViewStyleDealerManager) {
            width1 = 260;
            width2 = 165;
            width3 = 90;
            width4 = 110;
            titleString1 = @"经销商名称";
            titleString2 = @"经销商等级";
            titleString3 = @"店员数";
            titleString4 = @"状态";
        }
        //门店管理页面的样式
        else if (style == YMFourItemHeaderViewStyleStoreManager) {
            width1 = 230;
            width2 = 210;
            width3 = 90;
            width4 = 110;
            titleString1 = @"门店名称";
            titleString2 = @"上级经销商";
            titleString3 = @"店员数";
            titleString4 = @"状态";
        }
        
        //经销商报表页面的样式
        else if (style == YMFourItemHeaderViewSytleDealerReport) {
            width1 = 220;
            width2 = 220;
            width3 = 140;
            width4 = 90;
            titleString1 = @"经销商名称";
            titleString2 = @"城市运营名称";
            titleString3 = @"新增微信会员数";
            titleString4 = @"-";
        }
        
        self.firstLabel = [[UILabel alloc] init];
        self.firstLabel.text = titleString1;
        self.firstLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        self.firstLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        [self addSubview:self.firstLabel];
        [self.firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROOUND_BIG]);
            make.centerY.equalTo(self);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:width1]);
        }];
        
        self.secondLabel = [[UILabel alloc] init];
        self.secondLabel.text = titleString2;
        if (style == YMFourItemHeaderViewStyleSalesData) {
            self.secondLabel.textAlignment = NSTextAlignmentCenter;
        }
        self.secondLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        self.secondLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        [self addSubview:self.secondLabel];
        [self.secondLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.firstLabel.mas_right).with.offset(5);
            make.centerY.equalTo(self);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:width2]);
        }];
        
        self.thirdLabel = [[UILabel alloc] init];
        self.thirdLabel.text = titleString3;
        self.thirdLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        self.thirdLabel.textAlignment = NSTextAlignmentCenter;
        self.thirdLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        [self addSubview:self.thirdLabel];
        [self.thirdLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.secondLabel.mas_right).with.offset(5);
            make.centerY.equalTo(self);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:width3]);
        }];
        
        self.fourthLabel = [[UILabel alloc] init];
        self.fourthLabel.text = titleString4;
        if (style == YMFourItemHeaderViewSytleDealerReport) {
            self.fourthLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
            self.fourthLabel.numberOfLines = 2;
            self.fourthLabel.textAlignment = NSTextAlignmentRight;
        }else{
            self.fourthLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
            self.fourthLabel.textAlignment = NSTextAlignmentCenter;
        }
        
        self.fourthLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        [self addSubview:self.fourthLabel];
        [self.fourthLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.centerY.equalTo(self);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:width4]);
        }];
        
        UIView *bottomLineView = [[UIView alloc] init];
        bottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [self addSubview:bottomLineView];
        [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
    }
    return self;
}

@end
