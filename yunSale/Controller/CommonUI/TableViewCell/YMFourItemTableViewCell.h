//
//  YMFourItemTableViewCell.h
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

#define OneLineHeight [@"One" heightWithWidth:[YMCScreenAdapter sizeBy750:200] fontsize:[YMCScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE]]

typedef NS_ENUM(NSInteger, YMFourItemCellStyle) {
    YMFourItemCellStyleStoreDaily,
    YMFourItemCellStyleSalesData,
    YMFourItemCellStyleDealerManager,
    YMFourItemCellStyleStoreManager,
};

@interface YMFourItemTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *firstLabel;

@property (nonatomic, strong) UILabel *secondLabel;

@property (nonatomic, strong) UILabel *thirdLabel;

@property (nonatomic, strong) UILabel *fourthLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier cellStyle:(YMFourItemCellStyle)cellStyle;

@end
