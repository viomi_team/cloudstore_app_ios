//
//  YMFourItemTableViewCell.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMFourItemTableViewCell.h"


@implementation YMFourItemTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier cellStyle:(YMFourItemCellStyle)cellStyle
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        CGFloat width1 = 0,width2 = 0,width3 = 0,width4 = 0;
        //NSTextAlignment
        
        //门店日报页面样式
        if (cellStyle == YMFourItemCellStyleStoreDaily) {
            width1 = 190;
            width2 = 210;
            width3 = 150;
            width4 = 95;
        }
        //销售数据页面样式
        else if (cellStyle == YMFourItemCellStyleSalesData)
        {
            width1 = 260;
            width2 = 120;
            width3 = 150;
            width4 = 140;
        }
        //经销商管理页面样式
        else if (cellStyle == YMFourItemCellStyleDealerManager)
        {
            width1 = 260;
            width2 = 165;
            width3 = 90;
            width4 = 110;
        }
        //门店管理页面样式
        else if (cellStyle == YMFourItemCellStyleStoreManager)
        {
            width1 = 230;
            width2 = 210;
            width3 = 90;
            width4 = 110;
        }
        
        //门店日报的样式
        self.firstLabel = [[UILabel alloc] init];
        self.firstLabel.textAlignment = NSTextAlignmentLeft;
        self.firstLabel.numberOfLines = 2;
        self.firstLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
        self.firstLabel.textColor = [UIColor colorWithHexString:YMCOLOR_DARK_GRAY];
        [self.contentView addSubview:self.firstLabel];
        [self.firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROOUND_BIG]);
            make.centerY.equalTo(self.contentView);
            make.width.mas_equalTo([YMCScreenAdapter intergerSizeBy750:width1]);
        }];
        
        self.secondLabel = [[UILabel alloc] init];
        if (cellStyle == YMFourItemCellStyleSalesData) {
            self.secondLabel.textAlignment = NSTextAlignmentCenter;
        }
        self.secondLabel.numberOfLines = 2;
        self.secondLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
        self.secondLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [self.contentView addSubview:self.secondLabel];
        [self.secondLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.firstLabel.mas_right).with.offset(5);
            make.centerY.equalTo(self.contentView);
            make.width.mas_equalTo([YMCScreenAdapter intergerSizeBy750:width2]);
        }];
        
        self.thirdLabel = [[UILabel alloc] init];
        self.thirdLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
        self.thirdLabel.textAlignment = NSTextAlignmentCenter;
        self.thirdLabel.numberOfLines = 2;
        self.thirdLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [self.contentView addSubview:self.thirdLabel];
        [self.thirdLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.secondLabel.mas_right).with.offset(5);
            make.centerY.equalTo(self.contentView);
            make.width.mas_equalTo([YMCScreenAdapter intergerSizeBy750:width3]);
        }];
        
        self.fourthLabel = [[UILabel alloc] init];
        self.fourthLabel.numberOfLines = 2;
        self.fourthLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
        self.fourthLabel.textAlignment = NSTextAlignmentCenter;
        self.fourthLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [self.contentView addSubview:self.fourthLabel];
        [self.fourthLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.thirdLabel.mas_right).with.offset(5);
            make.centerY.equalTo(self.contentView);
            make.width.mas_equalTo([YMCScreenAdapter intergerSizeBy750:width4]);
        }];
        
        if (cellStyle != YMFourItemCellStyleSalesData) {
            UIImageView *indicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ym_index_more"]];
            [self.contentView addSubview:indicatorImageView];
            [indicatorImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self.contentView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
                make.centerY.equalTo(self.contentView);
            }];
        }
    }
    return self;
}

@end
