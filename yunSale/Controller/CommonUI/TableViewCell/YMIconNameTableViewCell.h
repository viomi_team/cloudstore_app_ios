//
//  YMIconNameTableViewCell.h
//  yunSale
//
//  Created by liushilou on 16/11/11.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const kYMIconNameTableViewCell = @"YMIconNameTableViewCell";


@interface YMIconNameTableViewCell : UITableViewCell

@property (nonatomic,strong) UIImageView *iconImageView;
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *detailLabel;
@property (nonatomic,assign) BOOL showSeparatorLine;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;


@end
