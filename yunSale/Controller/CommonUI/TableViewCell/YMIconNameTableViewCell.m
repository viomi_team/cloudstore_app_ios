//
//  YMIconNameTableViewCell.m
//  yunSale
//
//  Created by liushilou on 16/11/11.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMIconNameTableViewCell.h"

@interface YMIconNameTableViewCell()

@property (nonatomic,strong) UIView *separatorLineView;

@end;


@implementation YMIconNameTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _iconImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_iconImageView];
        
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE]);
        _nameLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        [self.contentView addSubview:_nameLabel];
        
        _detailLabel = [[UILabel alloc] init];
        _detailLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        _detailLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        [self.contentView addSubview:_detailLabel];
        
        CGFloat iconSize = [YMCScreenAdapter fontsizeBy750:42];
        
        [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.centerY.equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(iconSize, iconSize));
        }];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_iconImageView.mas_right).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.centerY.equalTo(self.contentView);
            make.right.equalTo(self.contentView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);  
        }];
        [_detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.right.equalTo(self.contentView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        }];
        
        _separatorLineView = [[UIView alloc] init];
        _separatorLineView.hidden = YES;
        _separatorLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [self.contentView addSubview:_separatorLineView];
        [_separatorLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel.mas_left);
            make.right.equalTo(self.contentView).with.offset(0);
            make.top.equalTo(self.contentView).with.offset(0);
            make.height.mas_equalTo(0.5);
        }];
        
    }
    return self;
}

- (void)setShowSeparatorLine:(BOOL)showSeparatorLine
{
    _showSeparatorLine = showSeparatorLine;
    _separatorLineView.hidden = !showSeparatorLine;
    
}


@end
