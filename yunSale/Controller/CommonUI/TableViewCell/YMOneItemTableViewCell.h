//
//  YMOneItemTableViewCell.h
//  yunSale
//
//  Created by liushilou on 16/11/11.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const kYMOneItemTableViewCell = @"YMOneItemTableViewCell";

@interface YMOneItemTableViewCell : UITableViewCell

@property (nonatomic,strong) UILabel *titleLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

- (void)isHidenBottomLine:(BOOL)isHidden;

@end
