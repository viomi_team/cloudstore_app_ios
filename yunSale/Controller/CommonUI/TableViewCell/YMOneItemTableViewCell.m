//
//  YMOneItemTableViewCell.m
//  yunSale
//
//  Created by liushilou on 16/11/11.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMOneItemTableViewCell.h"

@interface YMOneItemTableViewCell ()

@property (nonatomic,strong) UIView *line;

@end

@implementation YMOneItemTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:26]);
        _titleLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [self.contentView addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.contentView);
        }];
        
        _line = [[UIView alloc] init];
        _line.hidden = YES;
        _line.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [self.contentView addSubview:_line];
        
        [_line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.height.mas_equalTo(0.5);
        }];
        
    }
    return self;
}

- (void)isHidenBottomLine:(BOOL)isHidden; {
    self.line.hidden = isHidden;
}

@end
