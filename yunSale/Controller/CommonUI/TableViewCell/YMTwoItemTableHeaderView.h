//
//  YMTwoItemTableHeaderView.h
//  yunSale
//
//  Created by liushilou on 16/11/10.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMTwoItemTableHeaderView : UIView

- (id)initWithFrame:(CGRect)frame leftText:(NSString *)lefttext rightText:(NSString *)righttext;


@end
