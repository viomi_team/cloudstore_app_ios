//
//  YMTwoItemTableHeaderView.m
//  yunSale
//
//  Created by liushilou on 16/11/10.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMTwoItemTableHeaderView.h"

@implementation YMTwoItemTableHeaderView

- (id)initWithFrame:(CGRect)frame leftText:(NSString *)lefttext rightText:(NSString *)righttext {

    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        UILabel *leftLabel = [[UILabel alloc] init];
        leftLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        leftLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        leftLabel.text = lefttext;
        [self addSubview:leftLabel];
        
        UILabel *rightLabel = [[UILabel alloc] init];
        rightLabel.textAlignment = NSTextAlignmentCenter;
        rightLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        rightLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        rightLabel.text = righttext;
        [self addSubview:rightLabel];
        
        [leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.centerY.equalTo(self);
        }];
        
        [rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.centerY.equalTo(self);
            make.width.mas_equalTo([YMCScreenAdapter fontsizeBy750:100]);
        }];
        
        UIView *lineview = [[UIView alloc] init];
        lineview.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [self addSubview:lineview];
        [lineview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset(0);
            make.right.equalTo(self).with.offset(0);
            make.bottom.equalTo(self).with.offset(0);
            make.height.mas_equalTo(0.5);
        }];
    }
    return self;
}

@end
