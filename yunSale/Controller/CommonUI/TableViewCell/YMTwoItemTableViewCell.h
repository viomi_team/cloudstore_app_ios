//
//  YMTwoItemTableViewCell.h
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef  NS_ENUM(NSInteger, YMTwoItemCellStyle) {
    YMTwoItemCellStyleCenter,   //detailLabel 居中，并写死Label宽度
    YMTwoItemCellStyleRight,    //detailLabel 写死距右边距，Label 宽度由内容决定
    YMTwoItemCellStyleLeft      //detailLabel 置于 contentView 的中部，并向左边的 titleLabel 对齐，主要用在门店管理和经销商管理详情页
};

@interface YMTwoItemTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *detailsLabel;
@property (nonatomic, strong) UIView *bottomLineView;

//@property (nonatomic, strong) UIScrollView *detailScrollView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Style:(YMTwoItemCellStyle)labelStyle;

//显示底部分割线
- (void)showBottomLineView;

//YMTwoItemCellStyleRight样式中，设置 label 可左右滚动
//- (void)setDetailLabelScrollable;

@end
