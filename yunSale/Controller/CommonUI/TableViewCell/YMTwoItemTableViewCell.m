//
//  YMTwoItemTableViewCell.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMTwoItemTableViewCell.h"

@implementation YMTwoItemTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier Style:(YMTwoItemCellStyle)labelStyle  {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
    _titleLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
    [self.contentView addSubview:_titleLabel];
    
    //作 left 和 top 的约束
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROOUND_BIG]);
        make.top.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:34]);
    }];
    
    _detailsLabel = [[UILabel alloc] init];
    _detailsLabel.text = @"-";
    _detailsLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
    _detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
    
    //detailLabel 置于 contentView 的中部，并向左边的 titleLabel 对齐，主要用在门店管理和经销商管理详情页
    if (labelStyle == YMTwoItemCellStyleLeft) {
        [self.contentView addSubview:_detailsLabel];
        [_detailsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:246]);
            make.centerY.equalTo(self.contentView);
        }];
    }
    //detailLabel 居中，并写死Label宽度
    else if (labelStyle == YMTwoItemCellStyleCenter) {
        [self.contentView addSubview:_detailsLabel];
        _detailsLabel.textAlignment = NSTextAlignmentCenter;
        [_detailsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            //写死宽度时，若内容太多，超出 Label 的部分会被压缩成"..."，所以把约束改成下面的形式 （by 谢立颖 2016/12/12）
            make.centerX.equalTo(self.contentView.mas_right).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND+50]);
            make.centerY.equalTo(self.contentView);
            make.right.greaterThanOrEqualTo(self.contentView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        }];
    }
    //detailLabel 写死距右边距，Label 宽度由内容决定
    else if (labelStyle == YMTwoItemCellStyleRight) {
        [self.contentView addSubview:_detailsLabel];
        _detailsLabel.numberOfLines = 2;
        _detailsLabel.textAlignment = NSTextAlignmentRight;
        [_detailsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.top.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:34]);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:530]);
        }];
    }
    
    _bottomLineView = [[UIView alloc] init];
    _bottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [self addSubview:_bottomLineView];
    [_bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.width.mas_equalTo(YMSCREEN_WIDTH);
        make.centerX.equalTo(self);
        make.height.mas_equalTo(0.5);
    }];
    
    _bottomLineView.hidden = YES;
    
    return self;
}

- (void)showBottomLineView {
    _bottomLineView.hidden = NO;
}

@end
