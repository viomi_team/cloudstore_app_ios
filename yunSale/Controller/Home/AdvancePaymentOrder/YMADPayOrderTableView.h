//
//  YMADPayOrderTableView.h
//  yunSale
//
//  Created by liushilou on 17/1/3.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMOrderAPI.h"

@interface YMADPayOrderTableView : UIView

@property (nonatomic,copy) NSArray *salesDatas;

@property (nonatomic,strong) YMOrderAPI *orderAPI;

//已加载数据
@property (nonatomic,assign) BOOL fetched;

- (instancetype)init;


- (void)fetchData:(YMAdPayOrderType)type;


@end
