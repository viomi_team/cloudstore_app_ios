//
//  YMADPayOrderTableView.m
//  yunSale
//
//  Created by liushilou on 17/1/3.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMADPayOrderTableView.h"
#import "YMLineCharView.h"
#import "YMAdPayOrderHeaderView.h"
#import "YMAdPayOrderTableViewCell.h"
#import "YMAdPayOrderChartTableViewCell.h"
#import "YMChartTab.h"
#import "YMDate.h"
#import <MBProgressHUD.h>


@interface YMADPayOrderTableView() <UITableViewDelegate, UITableViewDataSource, YMAdPayOrderHeaderViewDelegate>

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,copy) NSArray *adpayOrderDatas;

@property (nonatomic,assign) YMAdPayOrderType type;

@property (nonatomic,assign) CGFloat chartCellHeight;

@property (nonatomic,strong) NSMutableArray *headerViews;
//@property (nonatomic,strong) YMAdPayOrderHeaderView *headerView;

@property (nonatomic,strong) UIView *footerView;

@property (nonatomic,assign) BOOL loadedChart;

@property (nonatomic,strong) NSDateFormatter *dateFormatter;

//@property (nonatomic,strong) NSMutableArray *showDetailsControlList;
@property (nonatomic,assign) NSInteger openSection;

@property (nonatomic,strong) NSMutableArray *detailsList;


@end



@implementation YMADPayOrderTableView

- (instancetype)init {
    self = [super init];
    if (self) {
        _openSection = -1;
        _detailsList = [NSMutableArray new];
        _headerViews = [NSMutableArray new];
        _chartCellHeight = [YMCScreenAdapter sizeBy750:504];
        
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        //        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.separatorColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        self.tableView.separatorInset = UIEdgeInsetsMake(0,0, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    return self;
}

#pragma mark - UITableViewDelegate & UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.adpayOrderDatas.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }else{
        
        if (self.openSection != section) {
            return 0;
        }else{
            id details = [self.detailsList objectAtIndex:section - 1];  //减1是因为chartView占用了一个section
            if ([details isKindOfClass:[NSArray class]]) {
                NSArray *detail = details;
                if (detail.count == 0) {
                    return 1;
                }else{
                    return detail.count;
                }
            }else{
                return 0;
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return self.chartCellHeight;
    }else{
        id details = [self.detailsList objectAtIndex:indexPath.section - 1];
        if ([details isKindOfClass:[NSArray class]]) {
            NSArray *detail = details;
            if (detail.count == 0) {
                return [YMCScreenAdapter intergerSizeBy750:100];;
            }
        }
        
        return [YMCScreenAdapter intergerSizeBy750:240];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return [YMCScreenAdapter sizeBy640:24];
    }else{
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    }else{
        return [YMCScreenAdapter sizeBy750:100];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section != 0) {
        
        YMAdPayOrderHeaderView *headerView = [self.headerViews objectAtIndex:section-1];
        
        NSDictionary *item = [self.adpayOrderDatas objectAtIndex:section - 1];
        
        NSNumber *beginDateNumber = [item notNullObjectForKey:@"beginDate"];
        
        NSDate *beginDate = [NSDate dateWithTimeIntervalSince1970:beginDateNumber.longLongValue/1000];
        
        NSString *beginDateString = [self.dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:beginDateNumber.longLongValue/1000]];

//        NSString *name      = [YMDate stringwithType:self.type beginDate:beginDate endDate:endDate formatter:dateFormatter index:i yearType:2];
        
        //lsl tip
        if (self.type == YMAdPayOrderDay) {
            if (section == 1) {
                headerView.timeLabel.text = @"昨天";
            }else{
                if ([[YMDate monthOfDate:beginDate withType:YMDateType_Integer] intValue] == 1 && [[YMDate dayOfDate:beginDate ] intValue]== 1) {
                    headerView.timeLabel.text = [NSString stringWithFormat:@"%d 年 %2d 月 %2d 日", [[YMDate yearOfDate:beginDate] intValue], [[YMDate monthOfDate:beginDate withType:YMDateType_Integer] intValue], [[YMDate dayOfDate:beginDate] intValue]];
                }
                headerView.timeLabel.text = [NSString stringWithFormat:@"%@ 月 %2d 日", [YMDate monthOfDate:beginDate withType:YMDateType_Integer], [[YMDate dayOfDate:beginDate] intValue]];
            }
        }else if(self.type == YMAdPayOrderWeek){
            if (section == 1) {
                headerView.timeLabel.text = @"本周";
            }else{
                NSNumber *endDate = [item notNullObjectForKey:@"endDate"];
                
                NSString *endDateString = [self.dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:endDate.longLongValue/1000]];
                
                headerView.timeLabel.text = [NSString stringWithFormat:@"%@至%@",beginDateString,endDateString];
            }
        }else{
            NSString *month = [YMChartTab monthOfDate:[NSDate dateWithTimeIntervalSince1970:beginDateNumber.longLongValue/1000]];
            NSString *year = [YMChartTab yearOfDate:[NSDate dateWithTimeIntervalSince1970:beginDateNumber.longLongValue/1000]];
            
            if (section == 1 || [month isEqualToString:@"十二月"]) {
                headerView.timeLabel.text = [NSString stringWithFormat:@"%@,%@",year,month];
            }else{
                headerView.timeLabel.text = month;
            }
        }
        
        NSDictionary *data = [item notNullObjectForKey:@"data"];
        if (data) {
            NSNumber *purchaseOrderAmount = [data notNullObjectForKey:@"purchaseOrderAmount"];
            if (!purchaseOrderAmount) {
                purchaseOrderAmount = @(0);
            }
            headerView.amountLabel.text = [NSString stringWithFormat:@"¥%.2f",purchaseOrderAmount.floatValue];
        }else{
            headerView.amountLabel.text = @"0";
        }
        
        //指示图标旋转，仿射变换
        if (section == self.openSection) {
            headerView.indicateImageView.transform = CGAffineTransformMakeRotation(M_PI);
        } else {
            headerView.indicateImageView.transform = CGAffineTransformIdentity;
        }
        
        return headerView;
    }else{
        return nil;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 1) {
        return nil;
    } else {
        if (!self.footerView) {
            self.footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [YMCScreenAdapter sizeBy640:26])];
            self.footerView.backgroundColor = [UIColor colorWithHexString:@"#f5f5f5"];
            
            UIView *toplineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0.5)];
            toplineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
            [self.footerView addSubview:toplineView];
            
            UIView *bottomlineView = [[UIView alloc] init];
            bottomlineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
            [self.footerView addSubview:bottomlineView];
            [bottomlineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.footerView).with.offset(0);
                make.right.equalTo(self.footerView).with.offset(0);
                make.bottom.equalTo(self.footerView).with.offset(0);
                make.height.mas_equalTo(0.5);
            }];
        }
        
        return self.footerView;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        static NSString *reuseIdentifier = @"chartCell";
        YMAdPayOrderChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (!cell) {
            cell = [[YMAdPayOrderChartTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        if (self.adpayOrderDatas) {
            if (!self.loadedChart) {
                self.loadedChart = YES;
                [cell configCell:self.adpayOrderDatas type:self.type];
            }
        }
        return cell;
    }else{
        
        id details = [self.detailsList objectAtIndex:indexPath.section - 1];
        if ([details isKindOfClass:[NSArray class]]) {
            NSArray *detail = details;
            if (detail.count == 0) {
                static NSString *reuseIdentifier = @"emptyCell";
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
                if (!cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.contentView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
                cell.textLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:24]);
                cell.textLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
                cell.textLabel.text = @"没有订单数据";
                return cell;
            }else{
                static NSString *reuseIdentifier = @"orderCell";
                YMAdPayOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
                if (!cell) {
                    cell = [[YMAdPayOrderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier type:YMAdPayOrderTableViewCellNodate];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                NSDictionary *item = [detail objectAtIndex:indexPath.row];
                
                NSString *channelName = [item notNullObjectForKey:@"channelName"];
                if (!channelName) {
                    channelName = @"-";
                }
                NSString *secondChannel = [item notNullObjectForKey:@"secondChannel"];
                if (!secondChannel) {
                    secondChannel = @"-";
                }
                NSString *rootChannel = [item notNullObjectForKey:@"rootChannel"];
                if (!rootChannel) {
                    rootChannel = @"-";
                }
                NSNumber *purchaseMachineCount = [item notNullObjectForKey:@"purchaseMachineCount"];
                if (!purchaseMachineCount) {
                    purchaseMachineCount = @0;
                }
                NSNumber *purchaseOrderCount = [item notNullObjectForKey:@"purchaseOrderCount"];
                if (!purchaseOrderCount) {
                    purchaseOrderCount = @0;
                }
                NSNumber *purchaseOrderAmount = [item notNullObjectForKey:@"purchaseOrderAmount"];
                if (!purchaseOrderAmount) {
                    purchaseOrderAmount = @0;
                }
                
                cell.channelNameView.valueLabel.text = channelName;
                cell.secondChannelView.valueLabel.text = secondChannel;
                cell.rootChannelView.valueLabel.text = rootChannel;
                cell.purchaseMachineCountView.valueLabel.text = purchaseMachineCount.stringValue;
                cell.purchaseOrderCountView.valueLabel.text = purchaseOrderCount.stringValue;
                cell.purchaseOrderAmountView.valueLabel.text = [NSString stringWithFormat:@"¥%.2f",purchaseOrderAmount.floatValue];
                return cell;
            }
        }
        return [UITableViewCell new];
    }
}

//cell的动画
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section != 0) {
        CATransform3D transform = CATransform3DIdentity;
        //        transform = CATransform3DRotate(transform, 0, 0, 0, 1);//渐变
        transform = CATransform3DTranslate(transform, 0, -10, 300);//左边水平移动
        //                transform = CATransform3DScale(transform, 0, 0, 0);//由小变大
        
        cell.layer.transform = transform;
        cell.layer.opacity = 0.0;
        
        [UIView animateWithDuration:0.6 animations:^{
            cell.layer.transform = CATransform3DIdentity;
            cell.layer.opacity = 1;
        }];
    }
}

#pragma mark - YMAdPayOrderHeaderViewDelegate
- (void)YMAdPayOrderHeaderViewClick:(YMAdPayOrderHeaderView *)view {
    [self fetchDetails:view];
}

#pragma mark - Fetch
- (void)fetchData:(YMAdPayOrderType)type {
    self.type = type;
    
    self.fetched = YES;
    
    if (!self.orderAPI) {
        self.orderAPI = [[YMOrderAPI alloc] init];
    }
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    //显示错误提示视图时，若先前的提示视图存在，则先把其移除
    [YMCAlertView removeNetErrorInView:self];
    
    
    [self.orderAPI fetchAdPayOrders:type completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        
        [MBProgressHUD hideHUDForView:self animated:YES];
        
        if (status == YMCRequestSuccess) {
            NSArray *result = [data notNullObjectForKey:@"result"];
            self.adpayOrderDatas = result;
            
            for (NSInteger i = 0; i < result.count; i++) {
                
                YMAdPayOrderHeaderView *headerView = [[YMAdPayOrderHeaderView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), [YMCScreenAdapter sizeBy750:100])];
                headerView.delegate = self;
                headerView.tag = i + 1;
                [self.headerViews addObject:headerView];
                
                [self.detailsList addObject:@(0)];
            }
            
            [self.tableView reloadData];
        }else{
            @weakify(self);
            [YMCAlertView showNetErrorInView:self type:status message:message actionBlock:^{
                @strongify(self);
                [self fetchData:self.type];
            }];
        }
    }];
}

- (void)fetchDetails:(YMAdPayOrderHeaderView *)view {
    NSInteger section = view.tag;
    
    id details = [self.detailsList objectAtIndex:section - 1];
    if ([details isKindOfClass:[NSArray class]]) {
        [self reloadSection:view];
        
        return;
    }
    
    NSDictionary *item = [self.adpayOrderDatas objectAtIndex:section - 1];
    NSNumber *beginDate = [item notNullObjectForKey:@"beginDate"];
    NSString *beginDateString = [self.dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:beginDate.longLongValue/1000]];
    NSNumber *endDate = [item notNullObjectForKey:@"endDate"];
    NSString *endDateString = [self.dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:endDate.longLongValue/1000]];
    
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    [self.orderAPI fetchAdPayOrdersDetails:beginDateString endDate:endDateString completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        
        [MBProgressHUD hideHUDForView:self animated:YES];
        
        if (status == YMCRequestSuccess) {
            
            NSArray *result = [data notNullObjectForKey:@"result"];
            if (result) {
                [self.detailsList replaceObjectAtIndex:section-1 withObject:result];
            }
            
            [self reloadSection:view];
            
        }else{
            [YMCAlertView showMessage:message];
        }
    }];
}

#pragma mark - Private
- (void)reloadSection:(YMAdPayOrderHeaderView *)view {
    NSInteger section = view.tag;
    if (self.openSection != section) {
        self.openSection = section;
    } else {
        self.openSection = -1;
    }
    
    [self.tableView reloadData];
    
    //    if (showDetails.boolValue) {
    //        [UIView animateWithDuration:0.5 animations:^{
    //            view.indicateImageView.transform = CGAffineTransformIdentity;
    //        }];
    //
    //        [self.showDetailsControlList replaceObjectAtIndex:section - 1 withObject:@(NO)];
    //    }else{
    //        [UIView animateWithDuration:0.5 animations:^{
    //            view.indicateImageView.transform = CGAffineTransformMakeRotation(M_PI);
    //        }];
    //
    //        for (NSInteger i=0;i<self.showDetailsControlList.count;i++) {
    //            if (i == section - 1) {
    //                [self.showDetailsControlList replaceObjectAtIndex:section - 1 withObject:@(YES)];
    //            }else{
    //                [self.showDetailsControlList replaceObjectAtIndex:section - 1 withObject:@(NO)];
    //            }
    //
    //        }
    //
    //
    //    }
    
    //    NSIndexSet *set = [[NSIndexSet alloc] initWithIndex:section];
    //
    //    [self.tableView reloadSections:set withRowAnimation:UITableViewRowAnimationFade];
}



@end
