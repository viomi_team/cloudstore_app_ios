//
//  YMAdPayOrderChartTableViewCell.h
//  yunSale
//
//  Created by liushilou on 17/1/4.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMAdPayOrderChartTableViewCell : UITableViewCell

- (void)configCell:(NSArray *)datas type:(NSInteger)type;

@end
