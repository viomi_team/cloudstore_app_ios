//
//  YMAdPayOrderChartTableViewCell.m
//  yunSale
//
//  Created by liushilou on 17/1/4.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMAdPayOrderChartTableViewCell.h"
#import "YMLineCharView.h"
//#import "YMChartTab.h"
#import "YMDate.h"

@interface YMAdPayOrderChartTableViewCell()

@property (nonatomic,strong) YMLineCharView *chartView;

@end


@implementation YMAdPayOrderChartTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _chartView = [[YMLineCharView alloc] init];
        [self.contentView addSubview:_chartView];
        [_chartView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset(0);
            make.right.equalTo(self.contentView).with.offset(0);
            make.top.equalTo(self.contentView).with.offset(0);
            make.bottom.equalTo(self.contentView).with.offset(0);
        }];
    }
    return self;
}

- (void)configCell:(NSArray *)datas type:(NSInteger)type {
    NSString *title = @"";
    if (type == 1) {
        title = @"近7天预付款订单折线图";
    } else if (type == 2) {
        title = @"近7周预付款订单折线图";
    } else {
        title = @"近7月预付款订单折线图";
    }
    self.chartView.titleLabel.text = title;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd"];
    
    NSMutableArray *chartDatas = [NSMutableArray new];
    
    for (NSInteger i = datas.count - 1; i >= 0; i--) {
        
        NSDictionary *item = [datas objectAtIndex:i];
        
        NSNumber *beginDate = [item notNullObjectForKey:@"beginDate"];
        //NSString *beginDateString = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:beginDate.longLongValue/1000]];
        NSNumber *endDate = [item notNullObjectForKey:@"endDate"];
        //NSString *endDateString = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:endDate.longLongValue/1000]];
        
         NSString *name = [YMDate stringwithType:type beginDate:beginDate endDate:endDate formatter:dateFormatter index:i yearType:2];
        
//        NSString *name = @"";
//        if (type == 1) {
//            if (i == 0) {
//                name = @"昨天";
//            }else{
//                name = beginDateString;
//            }
//        }else{
//            if (type == 3) {
//                //月
//                NSString *month = [YMChartTab monthOfDate:[NSDate dateWithTimeIntervalSince1970:endDate.longLongValue/1000]];
//                NSString *year = [YMChartTab yearOfDate:[NSDate dateWithTimeIntervalSince1970:endDate.longLongValue/1000]];
//                
//                if (i == 0 || [month isEqualToString:@"十二月"]) {
//                    name = [NSString stringWithFormat:@"%@\n%@",month,year];
//                }else{
//                    name = [NSString stringWithFormat:@"%@\n ",month];
//                }
//            }else{
//                //周
//                if (i == 0) {
//                    name = @"本周";
//                }else{
//                    name = [NSString stringWithFormat:@"%@\n%@",beginDateString,endDateString];
//                }
//            }
//        }
        
        
        
        CGFloat value = 0;
        NSDictionary *data = [item notNullObjectForKey:@"data"];
        if (data) {
//            NSNumber *purchaseOrderCount = [data notNullObjectForKey:@"purchaseOrderCount"];
            NSNumber *purchaseOrderCount = [data notNullObjectForKey:@"purchaseOrderAmount"];
            if (!purchaseOrderCount) {
                purchaseOrderCount = @(0);
            }
            value = purchaseOrderCount.floatValue;
        }else{
            value = 0;
        }
        
        YMChart *chart = [[YMChart alloc] init];
        chart.name = name;
        chart.value = value;
        
        [chartDatas addObject:chart];
    }
    
    [self.chartView setDatas:chartDatas];
}

@end
