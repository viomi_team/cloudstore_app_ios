//
//  YMAdPayOrderHeaderView.h
//  yunSale
//
//  Created by liushilou on 17/1/3.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YMAdPayOrderHeaderViewDelegate;

@interface YMAdPayOrderHeaderView : UIView

@property (nonatomic,weak) id<YMAdPayOrderHeaderViewDelegate> delegate;

@property (nonatomic,strong) UILabel *timeLabel;

@property (nonatomic,strong) UILabel *amountLabel;

@property (nonatomic,strong) UIImageView *indicateImageView;

- (instancetype)initWithFrame:(CGRect)frame;

@end

@protocol YMAdPayOrderHeaderViewDelegate <NSObject>

@required
- (void)YMAdPayOrderHeaderViewClick:(YMAdPayOrderHeaderView *)view;

@end
