//
//  YMAdPayOrderHeaderView.m
//  yunSale
//
//  Created by liushilou on 17/1/3.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMAdPayOrderHeaderView.h"

@implementation YMAdPayOrderHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        _timeLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        [self addSubview:_timeLabel];
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.centerY.equalTo(self);
        }];

        _indicateImageView = [[UIImageView alloc] init];
        _indicateImageView.image = [UIImage imageNamed:@"ym_index_down"];
        [self addSubview:_indicateImageView];
        [_indicateImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(self).with.offset([YMScreenAdapter sizeBy750:-48]);
            make.right.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROOUND_BIG]);
            make.centerY.equalTo(self);
            make.size.mas_equalTo(CGSizeMake([YMCScreenAdapter sizeBy750:24], [YMCScreenAdapter sizeBy750:24]));
        }];

        _amountLabel = [[UILabel alloc] init];
        _amountLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
        _amountLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        [self addSubview:_amountLabel];
        [_amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(_indicateImageView.mas_left).with.offset([YMScreenAdapter sizeBy750:-48]);
            make.right.equalTo(_indicateImageView.mas_left).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
            make.centerY.equalTo(self);
        }];

        UILabel *label = [[UILabel alloc] init];
        label.text = @"订单金额:";
        label.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        label.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        [self addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_amountLabel.mas_left).with.offset(0);
            make.centerY.equalTo(self);
        }];

        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [self addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset(0);
            make.bottom.equalTo(self).with.offset(0);
            make.right.equalTo(_amountLabel.mas_right);
            //make.right.equalTo(self).with.offset([YMScreenAdapter intergerSizeBy750:-120]);//会导致约束报错。。。
            make.height.mas_equalTo(0.5);
        }];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickAction:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)clickAction:(UIGestureRecognizer *)gesture {
    [self.delegate YMAdPayOrderHeaderViewClick:self];
}



@end
