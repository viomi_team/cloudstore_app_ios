//
//  YMAdPayOrderTableViewCell.h
//  yunSale
//
//  Created by liushilou on 17/1/3.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger,YMAdPayOrderTableViewCellType)
{
    YMAdPayOrderTableViewCellNodate,
    YMAdPayOrderTableViewCellHasdate,
};


@interface YMAdPayOrderTableViewCellItemView : UIView

@property (nonatomic,strong) UILabel *valueLabel;

@property (nonatomic,strong) UILabel *nameLabel;

@end

@interface YMAdPayOrderTableViewCell : UITableViewCell

@property (nonatomic,strong) UILabel *timeLabel;

@property (nonatomic,strong) YMAdPayOrderTableViewCellItemView *channelNameView;//门店
@property (nonatomic,strong) YMAdPayOrderTableViewCellItemView *secondChannelView;//分销商
@property (nonatomic,strong) YMAdPayOrderTableViewCellItemView *rootChannelView;//城市运营
@property (nonatomic,strong) YMAdPayOrderTableViewCellItemView *purchaseMachineCountView;//净水器台数
@property (nonatomic,strong) YMAdPayOrderTableViewCellItemView *purchaseOrderCountView;//订单数量
@property (nonatomic,strong) YMAdPayOrderTableViewCellItemView *purchaseOrderAmountView;//订单金额

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier type:(YMAdPayOrderTableViewCellType)type;


@end
