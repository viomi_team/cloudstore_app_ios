//
//  YMAdPayOrderTableViewCell.m
//  yunSale
//
//  Created by liushilou on 17/1/3.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMAdPayOrderTableViewCell.h"


@implementation YMAdPayOrderTableViewCellItemView

- (instancetype)init
{
    self = [super init];
    if (self) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        _nameLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:20]);
        [self addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.mas_centerY).with.offset(-6);
            make.left.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.right.equalTo(self);
        }];
        
        _valueLabel = [[UILabel alloc] init];
        _valueLabel.numberOfLines = 2;
        _valueLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
        _valueLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:24]);
        [self addSubview:_valueLabel];
        [_valueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.mas_centerY).with.offset(-2);
            make.left.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.right.equalTo(self);
        }];
    }
    return self;
}

@end


@implementation YMAdPayOrderTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier type:(YMAdPayOrderTableViewCellType)type {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        
        UIView *timeView = [[UIView alloc] init];
        timeView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:timeView];
        
        CGFloat timeViewHeight = 0;
        if (type == YMAdPayOrderTableViewCellHasdate) {
            timeViewHeight = [YMCScreenAdapter sizeBy750:100];
            
            _timeLabel = [[UILabel alloc] init];
            _timeLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:24]);
            _timeLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
            [timeView addSubview:_timeLabel];
            [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(timeView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
                make.centerY.equalTo(timeView);
            }];
            
            UIView *lineview1 = [[UIView alloc] init];
            lineview1.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
            [timeView addSubview:lineview1];
            [lineview1 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(timeView);
                make.left.equalTo(timeView);
                make.right.equalTo(timeView);
                make.height.mas_equalTo(0.5);
            }];
            
            UIView *lineview2 = [[UIView alloc] init];
            lineview2.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
            [timeView addSubview:lineview2];
            [lineview2 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(timeView);
                make.left.equalTo(timeView);
                make.right.equalTo(timeView);
                make.height.mas_equalTo(0.5);
            }];
        }else{
            timeView.hidden = YES;
        }
        
        [timeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.height.mas_equalTo(timeViewHeight);
        }];
        
        
        UIView *topView = [[UIView alloc] init];
        [self.contentView addSubview:topView];
        UIView *bottomView = [[UIView alloc] init];
        [self.contentView addSubview:bottomView];
        
        CGFloat bottomSpace = 0;
        if (type == YMAdPayOrderTableViewCellHasdate) {
            bottomSpace = [YMCScreenAdapter intergerSizeBy750:24];
            topView.backgroundColor = [UIColor whiteColor];
            bottomView.backgroundColor = [UIColor whiteColor];
        }
            
        [topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(timeView.mas_bottom).with.offset(0);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
        }];
        [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topView.mas_bottom);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView).with.offset(-bottomSpace);
            make.height.equalTo(topView);
        }];
        
        _channelNameView = [[YMAdPayOrderTableViewCellItemView alloc] init];
        _channelNameView.nameLabel.text = @"门店名称";
        [topView addSubview:_channelNameView];
        _secondChannelView = [[YMAdPayOrderTableViewCellItemView alloc] init];
        _secondChannelView.nameLabel.text = @"经销商名称";
        [topView addSubview:_secondChannelView];
        _rootChannelView = [[YMAdPayOrderTableViewCellItemView alloc] init];
        _rootChannelView.nameLabel.text = @"城市运营名称";
        [topView addSubview:_rootChannelView];
        [_channelNameView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topView);
            make.left.equalTo(topView);
            make.bottom.equalTo(topView);
        }];
        [_secondChannelView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topView);
            make.left.equalTo(_channelNameView.mas_right);
            make.bottom.equalTo(topView);
            make.width.equalTo(_channelNameView);
        }];
        [_rootChannelView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topView);
            make.left.equalTo(_secondChannelView.mas_right);
            make.bottom.equalTo(topView);
            make.right.equalTo(topView).with.offset(-[YMCScreenAdapter sizeBy750:12]);
            make.width.equalTo(_channelNameView);
        }];
        
        _purchaseMachineCountView = [[YMAdPayOrderTableViewCellItemView alloc] init];
        _purchaseMachineCountView.nameLabel.text = @"净水器台数";
        [bottomView addSubview:_purchaseMachineCountView];
        _purchaseOrderCountView = [[YMAdPayOrderTableViewCellItemView alloc] init];
        _purchaseOrderCountView.nameLabel.text = @"订单数量";
        [bottomView addSubview:_purchaseOrderCountView];
        _purchaseOrderAmountView = [[YMAdPayOrderTableViewCellItemView alloc] init];
        _purchaseOrderAmountView.nameLabel.text = @"订单金额";
        [bottomView addSubview:_purchaseOrderAmountView];
        
        [_purchaseMachineCountView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(bottomView);
            make.left.equalTo(bottomView);
            make.bottom.equalTo(bottomView);
        }];
        [_purchaseOrderCountView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(bottomView);
            make.left.equalTo(_purchaseMachineCountView.mas_right);
            make.bottom.equalTo(bottomView);
            make.width.equalTo(_purchaseMachineCountView);
        }];
        [_purchaseOrderAmountView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(bottomView);
            make.left.equalTo(_purchaseOrderCountView.mas_right);
            make.bottom.equalTo(bottomView);
            make.right.equalTo(bottomView);
            make.width.equalTo(_purchaseMachineCountView);
        }];
        
        if (type == YMAdPayOrderTableViewCellHasdate) {
            UIView *lineview3 = [[UIView alloc] init];
            lineview3.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
            [self.contentView addSubview:lineview3];
            [lineview3 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.contentView).with.offset(-bottomSpace);
                make.right.equalTo(self.contentView);
                make.left.equalTo(self.contentView);
                make.height.mas_equalTo(0.5);
            }];
        }
    }
    return self;
}

@end
