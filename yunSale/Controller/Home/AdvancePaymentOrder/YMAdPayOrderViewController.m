//
//  YMAdPayOrderViewController.m
//  yunSale
//
//  Created by liushilou on 16/12/28.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMAdPayOrderViewController.h"
#import "YMLineCharView.h"
#import "YMADPayOrderTableView.h"
#import "YMAdPayOrdersSearchViewController.h"
#import <MBProgressHUD.h>
#import <UMMobClick/MobClick.h>

@interface YMAdPayOrderViewController ()

@end

@implementation YMAdPayOrderViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick event:@"Home_Advance_Payment_Order"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];

    self.navigationItem.title = @"预付款提货";
    
    self.items = [NSArray arrayWithObjects:@"日",@"周",@"月", nil];
    
    YMADPayOrderTableView *dayTableView = [[YMADPayOrderTableView alloc] init];
    YMADPayOrderTableView *weekTableView = [[YMADPayOrderTableView alloc] init];
    YMADPayOrderTableView *monthTableView = [[YMADPayOrderTableView alloc] init];
    self.itemContentViews = [NSArray arrayWithObjects:dayTableView,weekTableView,monthTableView, nil];
    
    [self appearControllerOfIndex:0];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ym_index_search"] style:UIBarButtonItemStylePlain target:self action:@selector(searchAction)];
    [self.navigationItem setRightBarButtonItem:rightButton];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)dealloc {
    NSLog(@"%s",__func__);
    for (YMADPayOrderTableView *tableview in self.itemContentViews) {
        [tableview.orderAPI cancle];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma 重写父类
- (void)appearControllerOfIndex:(NSUInteger)index {
    YMADPayOrderTableView *controller = [self.itemContentViews objectAtIndex:index];
    if (!controller.fetched) {
        if (index == 0) {
            [controller fetchData:YMAdPayOrderDay];
        }else if (index == 1){
            [controller fetchData:YMAdPayOrderWeek];
        }else{
            [controller fetchData:YMAdPayOrderMonth];
        }
    }
}

- (void)searchAction {
    YMAdPayOrdersSearchViewController *controller = [[YMAdPayOrdersSearchViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}


@end
