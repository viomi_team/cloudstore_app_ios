//
//  YMAdPayOrdersSearchResultViewController.h
//  yunSale
//
//  Created by liushilou on 17/1/5.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMAdPayOrdersSearchResultViewController : UIViewController

@property (nonatomic,strong) NSDictionary *queryDatas;


@end
