//
//  YMAdPayOrdersSearchViewController.m
//  yunSale
//
//  Created by liushilou on 17/1/4.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMAdPayOrdersSearchViewController.h"
#import "YMSearchInputView.h"
#import "YMDateSelectView.h"
#import "YMButton.h"
#import "YMOrderAPI.h"
#import "YMAdPayOrdersSearchResultViewController.h"
#import <sys/utsname.h>

@interface YMAdPayOrdersSearchViewController () <UITextFieldDelegate>

@property (nonatomic, strong) YMSearchInputView *channelNameInputView;
@property (nonatomic, strong) YMSearchInputView *secondChannelInputView;
@property (nonatomic, strong) YMSearchInputView *rootChannelInputView;
@property (nonatomic, strong) YMDateSelectView *beginDateSelectorView;
@property (nonatomic, strong) YMDateSelectView *endDateSelectorView;


@end

@implementation YMAdPayOrdersSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self drawView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Draw
- (void)drawView
{
    self.navigationItem.title = @"预付款订单查询";
    
    self.view.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    
    //搜索输入框
    self.channelNameInputView = [[YMSearchInputView alloc] initWithTitle:@"门店名称" placeHolder:@"请输入门店名称"];
    self.channelNameInputView.textField.returnKeyType = UIReturnKeySearch;
    self.channelNameInputView.textField.delegate = self;
    [self.parentScrollView addSubview:self.channelNameInputView];
    [self.channelNameInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(YMSCREEN_WIDTH);
        make.centerX.equalTo(self.parentScrollView);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.top.equalTo(self.parentScrollView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
    }];
    
    //搜索输入框
    self.secondChannelInputView = [[YMSearchInputView alloc] initWithTitle:@"经销商" placeHolder:@"请输入经销商名称"];
    self.secondChannelInputView.textField.returnKeyType = UIReturnKeySearch;
    self.secondChannelInputView.textField.delegate = self;
    [self.parentScrollView addSubview:self.secondChannelInputView];
    [self.secondChannelInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(YMSCREEN_WIDTH);
        make.centerX.equalTo(self.parentScrollView);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.top.equalTo(self.channelNameInputView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:15]);
    }];
    
    self.beginDateSelectorView = [[YMDateSelectView alloc] initWithTip:@"开始时间" placeHoder:@"选择开始时间"];
    [self.parentScrollView addSubview:self.beginDateSelectorView];
    
    //当角色为城市运营时，屏蔽城市运营搜索框
    if ([[YMUserManager shareinstance].roleId intValue] != 21) {
        self.rootChannelInputView = [[YMSearchInputView alloc] initWithTitle:@"城市运营" placeHolder:@"请输入城市运营名称"];
        self.rootChannelInputView.textField.returnKeyType = UIReturnKeySearch;
        self.rootChannelInputView.textField.delegate = self;
        [self.parentScrollView addSubview:self.rootChannelInputView];
        [self.rootChannelInputView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(YMSCREEN_WIDTH);
            make.centerX.equalTo(self.parentScrollView);
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
            make.top.equalTo(self.secondChannelInputView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:15]);
        }];
        
        [self.beginDateSelectorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.rootChannelInputView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:15]);
            make.width.mas_equalTo(YMSCREEN_WIDTH);
            make.centerX.equalTo(self.parentScrollView);
            make.height.mas_equalTo([YMCScreenAdapter fontsizeBy750:100]);
        }];
    } else {
        [self.beginDateSelectorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.secondChannelInputView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:15]);
            make.width.mas_equalTo(YMSCREEN_WIDTH);
            make.centerX.equalTo(self.parentScrollView);
            make.height.mas_equalTo([YMCScreenAdapter fontsizeBy750:100]);
        }];
    }
    
    self.endDateSelectorView = [[YMDateSelectView alloc] initWithTip:@"结束时间" placeHoder:@"选择结束时间"];
    [self.parentScrollView addSubview:self.endDateSelectorView];
    [self.endDateSelectorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.beginDateSelectorView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:15]);
        make.width.mas_equalTo(YMSCREEN_WIDTH);
        make.centerX.equalTo(self.parentScrollView);
        make.height.mas_equalTo([YMCScreenAdapter fontsizeBy750:100]);
    }];
    
    //搜索按钮
    YMButton *searchBtn = [[YMButton alloc] initWithStyle:YMButtonStyleRectangle];
    [searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    [self.parentScrollView addSubview:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(YMSCREEN_WIDTH - YMSPACE_AROUND*2);
        make.centerX.equalTo(self.parentScrollView);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.top.equalTo(self.endDateSelectorView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
    }];
    
    //下划线
    UIView *bottomLineView = [[UIView alloc] init];
    bottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [self.parentScrollView addSubview:bottomLineView];
    [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(YMSCREEN_WIDTH);
        make.centerX.equalTo(self.parentScrollView);
        make.height.mas_equalTo(0.5);
        make.top.equalTo(searchBtn.mas_bottom).with.offset(5);
        make.bottom.equalTo(self.parentScrollView);
    }];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self searchAction];
    return YES;
}

//scrollView针对不同尺寸的设备而滚动不同的偏移量
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    DeviceScreenSizeType deviceScreenType = [YMDevice deviceScreenSizeType];
    
    if (textField == self.channelNameInputView.textField) {
        self.offset = 20;
    } else if (textField == self.secondChannelInputView.textField) {
        
        if (deviceScreenType == iPhone4 || deviceScreenType == iPhone5) {
            self.offset = 55;
        } else if (deviceScreenType == iPhone6) {
            self.offset = 30;
        } else {
            self.offset = 0;
        }
    } else if (textField == self.rootChannelInputView.textField) {
        
        if ((deviceScreenType == iPhone4 || deviceScreenType == iPhone5)) {
            self.offset = 80;
        } else if (deviceScreenType == iPhone6){
            self.offset = 40;
        } else {
            self.offset = 0;
        }
    }
}

#pragma mark - Action
- (void)searchAction {
    NSString *channelName = self.channelNameInputView.textField.text ;
    NSString *secondChannel = self.secondChannelInputView.textField.text;
    NSString *rootChannel = self.rootChannelInputView.textField.text;
    
    NSString *beginTime = self.beginDateSelectorView.dateValue;
    NSString *endTime = self.endDateSelectorView.dateValue;

    channelName = channelName ? channelName : @"";
    secondChannel = secondChannel ? secondChannel : @"";
    rootChannel = rootChannel ? rootChannel : @"";
    beginTime = beginTime ? beginTime : @"";
    endTime = endTime ? endTime : @"";
    
//    if ([beginTime compare:endTime options:NSNumericSearch] == NSOrderedDescending) {
//        [YMAlertView showHubInView:self.view message:@"结束时间应该大于开始时间"];
//        return;
//    }
    
    /*  zcm bug修改
     *  描述：只有beginTime,endTime时，无法正确的构建data
     *  Printing description of data:
            {
                channelName = "";
                secondChannel = "";
            }
     *  原因：rootChannel = nil 导致NSDictionary 认为rootChannel是字典结尾，默认不添加rootChannel之后的值
     */
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                          channelName,@"channelName",
                          secondChannel,@"secondChannel",
                          rootChannel,@"rootChannel",
                          beginTime,@"beginTime",
                          endTime,@"endTime",
//                          @(1),@"pageNum",
//                          @(10),@"pageSize",
                          nil];
    
    YMAdPayOrdersSearchResultViewController *controller = [[YMAdPayOrdersSearchResultViewController alloc] init];
    controller.queryDatas = data;
    [self.view endEditing:YES];
    [self.navigationController pushViewController:controller animated:YES];
 
}

@end
