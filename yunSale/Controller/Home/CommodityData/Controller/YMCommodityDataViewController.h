//
//  YMCommodityDataViewController.h
//  yunSale
//
//  Created by ChengMinZhang on 2017/9/27.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMScrollItemsBaseViewController.h"

@interface YMCommodityDataViewController : YMScrollItemsBaseViewController

@end
