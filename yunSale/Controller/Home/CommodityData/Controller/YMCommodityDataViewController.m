//
//  YMCommodityDataViewController.m
//  yunSale
//
//  Created by ChengMinZhang on 2017/9/27.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#define TOP_SHOWMESSAGE (self.view.frame.size.height - [YMCScreenAdapter sizeBy750:400])
#define CENTER_SHOWMESSAGE 0
#define BUTTON_SHOWMESSAGE [YMCScreenAdapter sizeBy750:200]

#import <MJExtension.h>
#import "YMCommodityDataViewController.h"
#import "YMCommodityDataTableView.h"

#import <UMMobClick/MobClick.h>

@interface YMCommodityDataViewController ()<YMCommodityDataTableViewDelegate>

@property (nonatomic, strong) YMOrderAPI *networkAPI;

@end

@implementation YMCommodityDataViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick event:@"Home_Commodity_Datas"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    self.networkAPI = [[YMOrderAPI alloc] init];
    self.navigationItem.title = @"商品数据";
    
    YMCommodityDataTableView *dayTableView = [[YMCommodityDataTableView alloc] init];
    YMCommodityDataTableView *monthTableView = [[YMCommodityDataTableView alloc] init];
    YMCommodityDataTableView *yearTableView = [[YMCommodityDataTableView alloc] init];
    
    dayTableView.dateType = YMCommodityDateDay;
    monthTableView.dateType = YMCommodityDateMonth;
    yearTableView.dateType = YMCommodityDateYear;
    
    dayTableView.delegate = self;
    monthTableView.delegate = self;
    yearTableView.delegate = self;
    
    [self setItems:@[@"日报表",@"月报表",@"年报表"]];
    [self setItemContentViews:@[dayTableView,monthTableView,yearTableView]];
    [self appearControllerOfIndex:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [self.networkAPI cancle];
}
#pragma talbeView 代理
- (void)tableView:(YMCommodityDataTableView *)tableView didSelectChartTitleAtIndex:(NSInteger)index
{
    YMCommoditySourceType sourceType = index;
    
    if(tableView.sourceType != sourceType) {
        //刷新表，重新配置数据
        tableView.sourceType = sourceType;
        tableView.isChartFetched = NO;
        tableView.pageNum = 0;
        tableView.totalPageNum = 0;
      [self fetchChartData:tableView.dateType sourceType:sourceType];
    }
}

-(void)tableView:(YMCommodityDataTableView *)tableView didSelectCharItemAtIndex:(NSInteger)index
{
    if(tableView.isChartFetched) {
        
        YMCommodityData *data = tableView.selectedChartItem;
        tableView.pageNum = 0;
        tableView.totalPageNum = 0;
        tableView.detailData = [NSArray array];  //清空数据
        
        [self fetchDetailData:tableView.dateType sourceType:tableView.sourceType reportDate:data.reportDate pageNum:tableView.pageNum + 1];
    }
}

-(void)tableView:(YMCommodityDataTableView *)tableView footerRefreshWithPageNum:(NSInteger)pageNum
{
    if(tableView.isChartFetched) {
        YMCommodityData *data = tableView.selectedChartItem;
        
        [self fetchDetailData:tableView.dateType sourceType:tableView.sourceType reportDate:data.reportDate pageNum:pageNum + 1];
    }
}
#pragma 重写父类
- (void)appearControllerOfIndex:(NSUInteger)index
{
    YMCommodityDataTableView *tableView = [self.itemContentViews objectAtIndex:index];
    
    if(!tableView.isChartFetched) {
        [self fetchChartData:tableView.dateType sourceType:tableView.sourceType];
    }
}

#pragma 网络请求
- (void)fetchChartData:(YMCommodityDateType )dateType sourceType:(YMCommoditySourceType)sourceType
{
    YMCommodityDataTableView *tableView = [self.itemContentViews objectAtIndex:dateType];
    
    [YMCAlertView removeNetErrorInView:tableView];
    
    [MBProgressHUD showHUDAddedTo:tableView animated:YES];
    
    @weakify(self);
    [self.networkAPI fetchCommodityData:dateType sourceType:sourceType successBlock:^(NSDictionary *data) {
        @strongify(self);
        
        [MBProgressHUD hideHUDForView:tableView animated:YES];

        //刷新成功
        tableView.isChartFetched = YES;
        
        //清空数据
        tableView.chartData = [NSArray array];
        tableView.detailData = [NSArray array];
        
        NSArray *chartData = [data notNullObjectForKey:@"datas"];
        
        // 非法数据
        if(!chartData || ![chartData isKindOfClass:[NSArray class]]) {
            [tableView reloadData];
            [self showNoticeWithMessage:@"服务器抽风啦，稍后再试哈~" inView:tableView animation:YES position:TOP_SHOWMESSAGE];
            return ;
        }
        
        //配置数据
        NSMutableArray *list = [NSMutableArray array];
        
        [chartData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if([obj isKindOfClass:[NSDictionary class]]) {
                YMCommodityData *commodityData = [YMCommodityData mj_objectWithKeyValues:obj];
                if(commodityData) {
                    [list addObject:commodityData];
                }
            }
        }];
        
        //空数据
        if(chartData.count == 0) {
            [tableView reloadData];
            [self showNoticeWithMessage:@"空空如也~" inView:tableView animation:YES position:TOP_SHOWMESSAGE];
            return;
        }
        
        //倒叙数组，刷新表
        tableView.chartData = [[list reverseObjectEnumerator] allObjects];
        [tableView reloadData];
        
    } failBlcok:^(YMCRequestStatus status, NSString *message) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:tableView animated:YES];
        
        tableView.isChartFetched = NO;
        
        [YMCAlertView showNetErrorInView:tableView type:status message:message actionBlock:^{
            [self fetchChartData:dateType sourceType:sourceType];
        }];
    }];
}


- (void)fetchDetailData:(YMCommodityDateType )dateType sourceType:(YMCommoditySourceType)sourceType reportDate:(NSString *)reportDate pageNum:(NSInteger) pageNum
{
    YMCommodityDataTableView *tableView = [self.itemContentViews objectAtIndex:dateType];
    
    [YMCAlertView removeNetErrorInView:tableView];
    
    if(pageNum <= 1) {
      [MBProgressHUD showHUDAddedTo:tableView animated:YES];
    }
    
    @weakify(self);
    
    [self.networkAPI fetchCommodityDetail:dateType reportDate:reportDate sourceType:sourceType pageNum:pageNum successBlock:^(NSDictionary *data) {
        @strongify(self);
        
        [MBProgressHUD hideHUDForView:tableView animated:YES];
        //解析数据
        NSDictionary *records = [data notNullObjectForKey:@"records"];
        NSNumber *nowPage = [records notNullObjectForKey:@"nowPage"];
        NSNumber *totalPageNum = [records notNullObjectForKey:@"totalPageNum"];
        NSArray *list = [records notNullObjectForKey:@"list"];
        //非法数据
        if(!list || !records || ![records isKindOfClass:[NSDictionary class]] ||
           ![list isKindOfClass:[NSArray class]]) {
            [tableView reloadSetion:1];
            [tableView endRefshingWithNoMoreData:YES];
            return ;
        }
        //解析数据
        __block NSMutableArray<YMCommodityData *> *commodityDetail = [NSMutableArray arrayWithArray:tableView.detailData];
        
        [list enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if([obj isKindOfClass:[NSDictionary class]]) {
                
                NSArray *columns = [obj notNullObjectForKey:@"columns"];
                
                YMCommodityData *data = [[YMCommodityData alloc] init];
                
                if(columns && [columns isKindOfClass:[NSArray class]] && columns.count == 3) {
                    [columns enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        if(idx == 0) {
                            data.commodityName = [obj notNullObjectForKey:@"value"];
                        }
                        if(idx == 1) {
                            NSNumber *quantity = [obj notNullObjectForKey:@"value"];
                            data.quantity = quantity.integerValue;
                        }
                        if(idx == 2) {
                            NSNumber *amount = [obj notNullObjectForKey:@"value"];
                            data.amount = amount.floatValue;
                        }
                    }];
                }else {
                    commodityDetail = nil;
                }
                [commodityDetail addObject:data];
            }
        }];
        //非法数据
        if(!commodityDetail) {
            [tableView reloadSetion:1];
            [tableView endRefshingWithNoMoreData:NO];
            [self showNoticeWithMessage:@"服务器抽风啦，稍后再试哈~" inView:tableView animation:YES position:BUTTON_SHOWMESSAGE];
            return ;
        }
        
        //是否超页
        if(nowPage.integerValue >= totalPageNum.integerValue) {
            [tableView endRefshingWithNoMoreData:YES];
        }else {
            [tableView endRefshingWithNoMoreData:NO];
        }
        //最后加载数据
        tableView.pageNum = nowPage.integerValue;
        tableView.totalPageNum = totalPageNum.integerValue;
        tableView.detailData = [commodityDetail copy];
        
        [tableView reloadSetion:1];
        
    } failBlcok:^(YMCRequestStatus status, NSString *message) {
        @strongify(self);
        
        [MBProgressHUD hideHUDForView:tableView animated:YES];
        
        [tableView endRefshingWithNoMoreData:NO];
        
        [self showNoticeWithMessage:message inView:tableView animation:YES position:BUTTON_SHOWMESSAGE];
    }];
}

- (void)showNoticeWithMessage:(NSString *)message inView:(UIView *)view animation:(BOOL)animation position:(CGFloat) position;
{
    MBProgressHUD *hudNotice = [MBProgressHUD showHUDAddedTo:view animated:animation];
    [hudNotice setOffset:CGPointMake(0, - position)];
    [hudNotice setMargin:10.0f];
    [hudNotice setMode:MBProgressHUDModeText];
    [hudNotice setAnimationType:MBProgressHUDAnimationZoom];
    [hudNotice setRemoveFromSuperViewOnHide:YES];
    
    hudNotice.label.numberOfLines = 0;
    hudNotice.label.text = message;
    hudNotice.label.font = [UIFont systemFontOfSize:[YMCScreenAdapter fontsizeBy750:26]];
    [hudNotice hideAnimated:animation afterDelay:3.0f];
}

@end
