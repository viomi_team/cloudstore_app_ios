//
//  YMCommodityChartTableViewCell.h
//  yunSale
//
//  Created by ChengMinZhang on 2017/9/29.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMCommodityData.h"
#import "YMAPIManager.h"

static NSString * const kChartCellReuseIdentifier = @"kChartCellReuseIdentifier";

@interface YMCommodityChartTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) NSInteger quantity;
@property (nonatomic, assign) CGFloat   amount;

@property (nonatomic, assign) BOOL loadCharted;

@property (nonatomic, assign) YMCommodityDateType dateType;
@property (nonatomic, assign) YMCommoditySourceType sourceTyoe;

@property (nonatomic, strong) NSArray<YMCommodityData *> *chartData;

@property (nonatomic, assign, readonly) NSInteger selectedIndex;   

@property (nonatomic, copy) void(^selectIndex)(NSInteger index);
@property (nonatomic, copy) void(^clickTitle)(YMCommoditySourceType sourceType);

@end
