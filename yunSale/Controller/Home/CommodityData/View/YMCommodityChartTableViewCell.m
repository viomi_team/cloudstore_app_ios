//
//  YMCommodityChartTableViewCell.m
//  yunSale
//
//  Created by ChengMinZhang on 2017/9/29.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMHistogramChartActionView.h"
#import "YMCommodityChartTableViewCell.h"

@interface YMCommodityChartTableViewCell ()<YMHistogramChartActionViewDelegate>

@property (nonatomic, strong) YMHistogramChartActionView *chartView;

@property (nonatomic, strong) UILabel *amountLabel;
@property (nonatomic, strong) UILabel *quantityLabel;

@property (nonatomic, assign, readwrite) NSInteger selectedIndex;

@end

@implementation YMCommodityChartTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _chartView = [[YMHistogramChartActionView alloc] init];
        _chartView.delegate = self;
        _chartView.nameLabel.text = @"";
        _chartView.valueLabel.textColor = [UIColor clearColor]; // 屏蔽valueLabel
        
        _loadCharted = NO;
        
        _selectedIndex = -1;
        
        [self.contentView addSubview:_chartView];
        
        UIView *containView = [[UIView alloc] init];
        containView.backgroundColor = [UIColor clearColor];
        
        UILabel *quantityTitle = [[UILabel alloc] init];
        quantityTitle.text = @"销售量:";
        quantityTitle.font = YM_FONT([YMCScreenAdapter fontsizeBy750:20]);
        quantityTitle.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        quantityTitle.textAlignment = NSTextAlignmentRight;
        
        UILabel *amountTitle = [[UILabel alloc] init];
        amountTitle.text = @"销售额:";
        amountTitle.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        amountTitle.font = YM_FONT([YMCScreenAdapter fontsizeBy750:20]);
        amountTitle.textAlignment = NSTextAlignmentRight;
        
        _quantityLabel = [[UILabel alloc] init];
        _quantityLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
        _quantityLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:20]);
        _quantityLabel.textAlignment = NSTextAlignmentRight;
        [_quantityLabel sizeToFit];
        
        _amountLabel = [[UILabel alloc] init];
        _amountLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
        _amountLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:20]);
        _amountLabel.textAlignment = NSTextAlignmentRight;
        [_amountLabel sizeToFit];
        
        [_chartView addSubview:containView];
        [containView addSubview:quantityTitle];
        [containView addSubview:amountTitle];
        [containView addSubview:_amountLabel];
        [containView addSubview:_quantityLabel];
        
        [_chartView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
        
        [_chartView.actionView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@([YMCScreenAdapter sizeBy750:200]));
        }];
        
        [containView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.mas_top).with.offset([YMCScreenAdapter sizeBy750:12]);
            make.right.equalTo(self.mas_right).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:300]);
        }];
        
        [quantityTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.equalTo(containView);
            make.bottom.equalTo(amountTitle.mas_top);
            make.right.equalTo(_amountLabel.mas_left);
        }];
        
        [amountTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.equalTo(containView);
            make.top.equalTo(quantityTitle.mas_bottom);
            make.right.equalTo(_amountLabel.mas_left);
        }];
        
        [_quantityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.equalTo(containView);
            make.bottom.equalTo(_amountLabel.mas_top);
            make.left.equalTo(quantityTitle.mas_right);
        }];
        
        [_amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.right.equalTo(containView);
            make.top.equalTo(_quantityLabel.mas_bottom);
            make.left.equalTo(amountTitle.mas_right);
        }];
    }
    return self;
    
}

- (void)YMHistogramChartActionViewClickChartItem:(NSUInteger)index
{
    if(self.selectIndex) {
        
        self.selectedIndex = index; // zcm add 记录选中项
        
        self.selectIndex(index);
    }
}

- (void)YMHistogramChartActionViewClickTitle
{
    if(self.clickTitle) {
        
        NSString *title = self.title;
        
        if([title isEqualToString:@"云分销"]) {
            self.sourceTyoe = YMCommoditySourceViomi;
        }
        
        if([title isEqualToString:@"米家"]) {
            self.sourceTyoe = YMCommoditySourceMijia;
        }
        
        if([title isEqualToString:@"天猫"]) {
            self.sourceTyoe = YMCommoditySourceTianmao;
        }
        
//        self.chartView.selectLastItem = NO;
        
        
        
        self.clickTitle(self.sourceTyoe);
    }
}

#pragma 懒加载
- (void)setTitle:(NSString *)title
{
    self.chartView.titleLabel.text = title;
}

-(NSString *)title
{
    return self.chartView.titleLabel.text;
}

- (void)setQuantity:(NSInteger)quantity
{
    _quantity = quantity;
    
    if(quantity < 0)  {
        _quantityLabel.text = @"--";
    }else if(quantity <= 10000) {
       _quantityLabel.text = [NSString stringWithFormat:@"%ld",(long)quantity];
    }else {
        _quantityLabel.text = [NSString stringWithFormat:@"%.2f(万)",(long)quantity/10000.0f];
    }
}

- (void)setAmount:(CGFloat )amount
{
    
    _amount = amount;
    
    amount = amount / 100.0f;
    
    if(amount < 0)  {
        _amountLabel.text = @"--";
    }else if (amount == 0) {
        _amountLabel.text = @"￥0";
    }
    else if(amount < 10000) {
        _amountLabel.text = [NSString stringWithFormat:@"￥%.2ld",(long)amount];
    }else if(amount < 100000000){
        _amountLabel.text = [NSString stringWithFormat:@"￥%.2f(万)",(long)amount/10000.0f];
    }else {
        _amountLabel.text = [NSString stringWithFormat:@"￥%.2f(亿)",(long)amount/100000000.0f];
    }
}

- (void)setChartData:(NSArray<YMCommodityData *> *)chartData
{
    if(!chartData) {
        return;  //不能为空数据
    }
    NSMutableArray<YMChart *> *datas = [NSMutableArray array];
    
    [chartData enumerateObjectsUsingBlock:^(YMCommodityData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSString *reportDate = obj.reportDate;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        if(self.dateType == YMCommodityDateDay) {
            [formatter setDateFormat:@"yyyy-MM-dd"]; //转化前格式
            NSDate *date = [formatter dateFromString:reportDate];
            [formatter setDateFormat:@"MM/dd"];  //转化后格式
            reportDate = [formatter stringFromDate:date];
        }
        
        if(self.dateType == YMCommodityDateMonth) {
            [formatter setDateFormat:@"yyyy-MM"]; //转化前格式
            NSDate *date = [formatter dateFromString:reportDate];
            [formatter setDateFormat:@"MM月"];  //转化后格式
            reportDate = [formatter stringFromDate:date];
        }
        
        if(self.dateType == YMCommodityDateYear) {
            [formatter setDateFormat:@"yyyy"]; //转化前格式
            NSDate *date = [formatter dateFromString:reportDate];
            [formatter setDateFormat:@"yyyy年"];  //转化后格式
            reportDate = [formatter stringFromDate:date];
        }
        YMChart *chartObject = [[YMChart alloc] init];
        chartObject.name = reportDate;
        chartObject.value = obj.amount;
        [datas addObject:chartObject];
    }];
    
    _chartData = chartData;
    
    [self.chartView setDatas:datas];
}




@end
