//
//  YMCommodityDataTableView.h
//  yunSale
//
//  Created by ChengMinZhang on 2017/9/27.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMOrderAPI.h"
#import "YMCommodityData.h"

@class YMCommodityDataTableView,YMCommodityChartTableViewCell;

@protocol YMCommodityDataTableViewDelegate <NSObject>

@optional

- (void)tableView:(YMCommodityDataTableView *)tableView didSelectCharItemAtIndex:(NSInteger )index;

- (void)tableView:(YMCommodityDataTableView *)tableView didSelectChartTitleAtIndex:(NSInteger)index;

- (void)tableView:(YMCommodityDataTableView *)tableView footerRefreshWithPageNum:(NSInteger )pageNum;

@end

@interface YMCommodityDataTableView : UIView

@property (nonatomic, assign) BOOL isChartFetched;
@property (nonatomic, assign) NSInteger pageNum,totalPageNum;
@property (nonatomic, strong) YMCommodityData *selectedChartItem;

@property (nonatomic, assign) YMCommodityDateType dateType;
@property (nonatomic, assign) YMCommoditySourceType sourceType;

@property (nonatomic, strong) NSArray<YMCommodityData *> *chartData;
@property (nonatomic, strong) NSArray<YMCommodityData *> *detailData;

@property (nonatomic, weak) id<YMCommodityDataTableViewDelegate> delegate;



- (void)reloadData;
- (void)reloadSetion:(NSInteger)section; // 0 - 刷新柱状图； 1 - 刷新表格

- (void)endRefshingWithNoMoreData:(BOOL)noData;

@end


