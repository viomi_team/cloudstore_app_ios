//
//  YMCommodityDataTableView.m
//  yunSale
//
//  Created by ChengMinZhang on 2017/9/27.
//  Copyright © 2017年 yunmi. All rights reserved.
//
#import <MJRefresh.h>
#import "YMOneItemTableViewCell.h"
#import <ActionSheetStringPicker.h>
#import "YMCommodityDataTableView.h"
#import "YMCommodityChartTableViewCell.h"
#import "YMCommodityDetailTableViewCell.h"

@interface YMCommodityDataTableView () <UITableViewDelegate,UITableViewDataSource>

@property(nonatomic, strong) UITableView *tableView;

@property (nonnull, strong) YMCommodityChartTableViewCell *chartCell;

@end

@implementation YMCommodityDataTableView

- (instancetype)init {
    self = [super init];
    if (self) {
        //初始化一些数据
        _isChartFetched = NO;
        _pageNum = 0;
        _totalPageNum = 0;
        _selectedChartItem = [[YMCommodityData alloc] init];
        
        _sourceType = YMCommoditySourceViomi;
        _chartData = [NSArray array];
        _detailData = [NSArray array];
        
        _tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        if([[UIDevice currentDevice] systemVersion].floatValue >= 11.0) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        
        [self addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
        
        
    }
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.chartData.count ? 2: 0;  //柱状图 + 详情列表
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0) {
        return self.chartData.count ? 1 : 0;
    }else {
        return self.detailData.count ? self.detailData.count + 1 : 2;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0) {
        return  [YMCScreenAdapter sizeBy750:504];
    }else {
        return  [YMCScreenAdapter intergerSizeBy750:100] ; //详情页高度
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return section == 0? [YMCScreenAdapter sizeBy750:24] : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //柱状图
    if(indexPath.section == 0) {
        self.chartCell = [tableView dequeueReusableCellWithIdentifier:kChartCellReuseIdentifier];
        //初始化表格
        if(!self.chartCell) {
            self.chartCell = [[YMCommodityChartTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kChartCellReuseIdentifier];
            self.chartCell.selectionStyle = UITableViewCellSelectionStyleNone;
            self.chartCell.title = @"云分销";
            self.chartCell.quantity = [self.chartData lastObject].quantity;
            self.chartCell.amount = [self.chartData lastObject].amount;
            self.chartCell.dateType = self.dateType;
            self.chartCell.sourceTyoe = self.sourceType;
            
            //选中一条数据
            @weakify(self);
            self.chartCell.selectIndex = ^(NSInteger index) {
                @strongify(self);
                [self tableView:self didSelectCharItemAtIndex:index];
            };
            //选择Title
            self.chartCell.clickTitle = ^(YMCommoditySourceType sourceType) {
                @strongify(self);
                [self tableViewChartCell:self.chartCell selectedTitleAtRow:sourceType];
            };
        }
        
        if(!self.chartCell.loadCharted) {
            self.chartCell.loadCharted = YES;
            self.chartCell.chartData = self.chartData;
        }
        return self.chartCell;
    }
    //表格详情
    if(indexPath.section == 1) {
        //初始化
        YMCommodityDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDetailCellReuseIdentifier];
        if(!cell) {
            cell = [[YMCommodityDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kDetailCellReuseIdentifier];
        }
        //表头
        if(indexPath.row == 0) {
            cell.firstTitle = @"商品名称";
            cell.secondTitle = @"销售数量";
            cell.thirdTitle = @"销售金额";
        }else {
            //没有数据的时候显示为空
            if(self.detailData.count == 0) {
                YMOneItemTableViewCell *oneItemCell = [[YMOneItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kYMOneItemTableViewCell];
                oneItemCell.titleLabel.text = @"没有数据哦";
                oneItemCell.titleLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
                oneItemCell.contentView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
                oneItemCell.selectionStyle = UITableViewCellSelectionStyleNone;
                return oneItemCell;
            }else{
                //正常配置数据
                YMCommodityData *data = [self.detailData objectAtIndex:indexPath.row - 1];
                cell.firstTitle = data.commodityName;
                cell.secondTitle = [NSString stringWithFormat:@"%ld",(long)data.quantity];
                cell.thirdTitle = [NSString stringWithFormat:@"%.2f",data.amount / 100.0f];
            }
        }
        cell.contentView.backgroundColor = indexPath.row % 2 ? [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND] : [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    return nil;
}

//点击标题
- (void)tableViewChartCell:(YMCommodityChartTableViewCell *) cell selectedTitleAtRow:(YMCommoditySourceType )sourceType
{
    sourceType = sourceType - 1;
    
    NSArray *array = @[@"云分销",@"米家",@"天猫"];
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:@"请选择类型" rows:array initialSelection:sourceType doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        //更新title
        cell.title = selectedValue;
        //更新数据
        [self tableView:self didSelectChartTitleAtIndex:selectedIndex + 1];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        //do something...
    } origin:self];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [cancelButton setTintColor:[UIColor colorWithHexString:YMCOLOR_THEME]];
    [doneButton setTintColor:[UIColor colorWithHexString:YMCOLOR_THEME]];
    [picker setCancelButton:cancelButton];
    [picker setDoneButton:doneButton];
    
    [picker showActionSheetPicker];
}
- (void)tableView:(YMCommodityDataTableView *)tableView didSelectChartTitleAtIndex:(NSInteger)index
{
    if([self.delegate respondsToSelector:@selector(tableView:didSelectChartTitleAtIndex:)]) {
        
        [self.delegate tableView:self didSelectChartTitleAtIndex:index];
    }
}

- (void)tableView:(YMCommodityDataTableView *)tableView didSelectCharItemAtIndex:(NSInteger )index
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(tableView:didSelectCharItemAtIndex:)] &&
       tableView.isChartFetched && tableView.chartData.count >= index + 1) {
        
        self.selectedChartItem = [self.chartData objectAtIndex:index];
        self.chartCell.quantity = self.selectedChartItem.quantity;
        self.chartCell.amount = self.selectedChartItem.amount;
        self.chartCell.loadCharted = NO;
        
        [self.delegate tableView:tableView didSelectCharItemAtIndex:index];
    }
}

- (void)footerRefresh
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(tableView:footerRefreshWithPageNum:)] &&
       self.isChartFetched && self.chartData.count > 0 && self.detailData.count > 0) {
        [self.tableView.mj_footer beginRefreshing];
        [self.delegate tableView:self footerRefreshWithPageNum:self.pageNum];
    }
}
//刷新表
- (void)reloadData
{
    [self.tableView reloadData];
    if(self.chartCell) {
        self.chartCell.chartData = self.chartData; //更新表格数据源
    }
//    //默认选中最后一条数据
//    [self tableView:self didSelectCharItemAtIndex:self.chartData.count - 1];
    
    [self tableView:self didSelectCharItemAtIndex:self.chartCell.selectedIndex];
}
//局部刷新
- (void)reloadSetion:(NSInteger)section;
{
    NSIndexSet *indexSet = [[NSIndexSet alloc]initWithIndex:section];
    [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)endRefshingWithNoMoreData:(BOOL)noData
{
    if(noData) {
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }else {
        [self.tableView.mj_footer endRefreshing];
    }
}

@end
