//
//  YMCommodityDataHeaderView.h
//  yunSale
//
//  Created by ChengMinZhang on 2017/9/29.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const kDetailCellReuseIdentifier = @"kDetailCellReuseIdentifier";

@interface YMCommodityDetailTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *firstTitle;

@property (nonatomic, strong) NSString *secondTitle;

@property (nonatomic, strong) NSString *thirdTitle;

@end
