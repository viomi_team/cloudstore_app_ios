//
//  YMCommodityDataHeaderView.m
//  yunSale
//
//  Created by ChengMinZhang on 2017/9/29.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMCommodityDetailTableViewCell.h"
@interface YMCommodityDetailTableViewCell()

@property (nonatomic, strong) UILabel *firstLabel;
@property (nonatomic, strong) UILabel *secondLabel;
@property (nonatomic, strong) UILabel *thirdLabel;
@property (nonatomic, strong) UIView *lineView;

@end

@implementation YMCommodityDetailTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        _firstLabel = [[UILabel alloc] init];
        _firstLabel.numberOfLines = 0;
        _firstLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        _firstLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        _firstLabel.textAlignment = NSTextAlignmentLeft;
        
        _secondLabel = [[UILabel alloc] init];
        _secondLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        _secondLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        _secondLabel.textAlignment = NSTextAlignmentCenter;
        
        _thirdLabel = [[UILabel alloc] init];
        _thirdLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        _thirdLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        _thirdLabel.textAlignment = NSTextAlignmentCenter;
        
        [self.contentView addSubview:_firstLabel];
        [self.contentView addSubview:_secondLabel];
        [self.contentView addSubview:_thirdLabel];
        
        [_firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.left.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.width.mas_equalTo(YMSCREEN_WIDTH / 5 * 2);
        }];
        
        [_secondLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.left.equalTo(_firstLabel.mas_right);
            make.right.equalTo(_thirdLabel.mas_left);
        }];
        
        [_thirdLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.width.mas_equalTo((YMSCREEN_WIDTH / 5 * 3)/2);
            make.right.equalTo(self.contentView);//.with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        }];
    }
    return self;
}

#pragma 懒加载
- (void)setFirstTitle:(NSString *)firstTitle
{
    if(firstTitle && ![firstTitle isEqualToString:@""]) {
        self.firstLabel.text = firstTitle;
    }else {
        self.firstLabel.text = @"--";
    }
}

- (void)setSecondTitle:(NSString *)secondTitle
{
    if(secondTitle && ![secondTitle isEqualToString:@""]) {
     self.secondLabel.text = secondTitle;
    }else {
        self.secondLabel.text = @"--";
    }
}

- (void)setThirdTitle:(NSString *)thirdTitle
{
    if([thirdTitle isEqualToString:@"销售金额"]) {
        self.thirdLabel.text = thirdTitle;
        return;
    }
    
    if(thirdTitle && ![thirdTitle isEqualToString:@""]) {
        self.thirdLabel.text = [NSString stringWithFormat:@"￥%@",thirdTitle];
    }else {
        self.thirdLabel.text = @"￥--";
    }
}

//- (void)setIsEmpty:(BOOL)isEmpty
//{
//    if(isEmpty) {
//        self.firstLabel.hidden = YES;
//        self.thirdLabel.hidden = YES;
//        
//        self.secondLabel.text = @"没有数据哦";
//        [self.secondLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.edges.equalTo(self.contentView);
//        }];
//        [self layoutIfNeeded];
//    }else {
//        self.firstLabel.hidden = NO;
//        self.thirdLabel.hidden = NO;
//        
//        self.secondLabel.textAlignment = NSTextAlignmentCenter;
//        [self.secondLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(self.contentView);
//            make.left.equalTo(_firstLabel.mas_right);
//            make.right.equalTo(_thirdLabel.mas_left);
//        }];
//        
//        [self layoutIfNeeded];
//    }
//}

@end
