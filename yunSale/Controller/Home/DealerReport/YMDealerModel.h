//
//  YMDealerModel.h
//  yunSale
//
//  Created by liushilou on 17/1/17.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YMDealerModel : NSObject

@property (nonatomic,assign) NSInteger channelId;
@property (nonatomic,strong) NSString *channelName;
@property (nonatomic,strong) NSString *day;
@property (nonatomic,assign) CGFloat orderAmount;
@property (nonatomic,assign) NSInteger orderCount;
@property (nonatomic,assign) NSInteger parttimeCount;
@property (nonatomic,strong) NSString *rootChannel;
@property (nonatomic,strong) NSString *secondChannel;
@property (nonatomic,assign) NSInteger staffCount;
@property (nonatomic,assign) NSInteger terminalCount;
@property (nonatomic,assign) NSInteger userCount;

@end
