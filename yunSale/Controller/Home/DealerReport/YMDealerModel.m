//
//  YMDealerModel.m
//  yunSale
//
//  Created by liushilou on 17/1/17.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMDealerModel.h"

@implementation YMDealerModel


- (NSString *)description
{
    return [NSString stringWithFormat:@"userCount:%d,terminalCount:%d,staffCount:%d,parttimeCount:%d,orderCount:%d,orderAmount:%ld",self.userCount,self.terminalCount,self.staffCount,self.parttimeCount,self.orderCount,(long)self.orderCount];
}


@end
