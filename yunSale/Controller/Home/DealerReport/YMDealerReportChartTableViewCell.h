//
//  YMDealerReportChartTableViewCell.h
//  yunSale
//
//  Created by liushilou on 17/1/5.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMHistogramChartActionView.h"


@interface YMDealerReportChartTableViewCell : UITableViewCell

@property (nonatomic,strong) YMHistogramChartActionView *chartView;

@property (nonatomic, strong) NSArray *dataTypeArray;//由调用者传入,图表显示 orderAmount / staffCount

- (void)configCell:(NSArray *)datas dateType:(NSInteger)dateType dataType:(NSInteger)dataType;

@end
