//
//  YMDealerReportChartTableViewCell.m
//  yunSale
//
//  Created by liushilou on 17/1/5.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMDealerReportChartTableViewCell.h"
//#import "YMChartTab.h"
#import "YMDate.h"


@interface YMDealerReportChartTableViewCell()

@property (nonatomic, strong) UIButton *orderButton;
@property (nonatomic, strong) UIButton *userButton;

@property (nonatomic, strong) NSArray *datas;

@property (nonatomic, assign) NSInteger type;


@end

@implementation YMDealerReportChartTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _chartView = [[YMHistogramChartActionView alloc] init];
        [self.contentView addSubview:_chartView];
        //默认【会员数】
        self.chartView.titleLabel.text  = @" 新增微信会员数";
        
        UIView *bottomView = [[UIView alloc] init];
        [self.contentView addSubview:bottomView];
        
        [_chartView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset(0);
            make.right.equalTo(self.contentView).with.offset(0);
            make.top.equalTo(self.contentView).with.offset(0);
            make.bottom.equalTo(self.contentView).with.offset(0);
        }];
        
    }
    return self;
}

- (void)configCell:(NSArray *)datas dateType:(NSInteger)dateType dataType:(NSInteger)dataType{
    
    self.datas = datas;
    self.type  = dateType;

    [self updateChart:dataType];
}

- (void)updateChart:(NSInteger)datatype
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd"];
    
    NSMutableArray *chartDatas     = [NSMutableArray new];
    
    for (NSInteger i = self.datas.count - 1; i >= 0; i--) {
        
        NSDictionary *item  = [self.datas objectAtIndex:i];
        NSNumber *beginDate = [item notNullObjectForKey:@"beginDate"];
        NSNumber *endDate   = [item notNullObjectForKey:@"endDate"];
        
        NSString *name      = [YMDate stringwithType:self.type beginDate:beginDate endDate:endDate formatter:dateFormatter index:i yearType:2];
        
        
//        NSString *name = @"";
//        if (self.type == 1) {
//            if (i == 0) {
//                name = @"昨天";
//            }else{
//                name = beginDateString;
//            }
//        }else{
//            if (self.type == 3) {
//                //月
//                NSString *month = [YMChartTab monthOfDate:[NSDate dateWithTimeIntervalSince1970:endDate.longLongValue/1000]];
//                NSString *year = [YMChartTab yearOfDate:[NSDate dateWithTimeIntervalSince1970:endDate.longLongValue/1000]];
//                
//                if (i == 0 || [month isEqualToString:@"十二月"]) {
//                    name = [NSString stringWithFormat:@"%@\n%@",month,year];
//                }else{
//                    name = [NSString stringWithFormat:@"%@\n ",month];
//                }
//            }else{
//                //周
//                if (i == 0) {
//                    name = @"本周";
//                }else{
//                    name = [NSString stringWithFormat:@"%@\n%@",beginDateString,endDateString];
//                }
//            }
//        }

        CGFloat value = 0;
        NSDictionary *data = [item notNullObjectForKey:@"data"];
        if (data && self.dataTypeArray) {
            NSNumber *count = nil;
            if(datatype <= self.dataTypeArray.count){
                //根据传进来的datatype，绘制柱状图值  （by ChengMin Zhang  2017/1/16 ）
                count = [data notNullObjectForKey:[[self.dataTypeArray objectAtIndex:datatype] objectForKey:@"filed"]];  //取得字段
            }
            
            if (!count) {
                count = @(0);
            }
            value = count.floatValue;
        }else{
            value = 0;
        }
        
        
        YMChart *chart = [[YMChart alloc] init];
        chart.name = name;
        chart.value = value;
        
        [chartDatas addObject:chart];
    }
    
    [self.chartView setDatas:chartDatas];
    
}



@end
