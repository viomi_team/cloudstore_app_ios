//
//  YMDealerReportSearchResultTableViewCell.h
//  yunSale
//
//  Created by liushilou on 17/1/6.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMDealerReportTableViewCell.h"

@interface YMDealerReportSearchResultTableViewCell : UITableViewCell

@property (nonatomic,strong) UILabel *timeLabel;

//@property (nonatomic,strong) UILabel *orderCountLabel;

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *secondChannelView;

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *rootChannelView;

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *orderCountView;

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *staffCount;

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *parttimeCountView;

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *terminalCountView;

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *orderAmountView;

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *userCountView;

@end
