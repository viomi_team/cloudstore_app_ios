//
//  YMDealerReportSearchResultTableViewCell.m
//  yunSale
//
//  Created by liushilou on 17/1/6.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMDealerReportSearchResultTableViewCell.h"

@implementation YMDealerReportSearchResultTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        
        UIView *timeView = [[UIView alloc] init];
        timeView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:timeView];
        
        CGFloat timeViewHeight = [YMCScreenAdapter sizeBy750:100];
        
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:24]);
        _timeLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [timeView addSubview:_timeLabel];
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(timeView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.centerY.equalTo(timeView);
        }];
        
//        _orderCountLabel = [[UILabel alloc] init];
//        _orderCountLabel.font = YM_FONT([YMScreenAdapter fontsizeBy750:24]);
//        _orderCountLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
//        [timeView addSubview:_orderCountLabel];
//        [_orderCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(timeView).with.offset([YMScreenAdapter sizeBy750:-YMSPACE_AROUND]);
//            make.centerY.equalTo(timeView);
//        }];
        
//        UILabel *orderCountTipLabel = [[UILabel alloc] init];
//        orderCountTipLabel.text = @"订单数：";
//        orderCountTipLabel.font = YM_FONT([YMScreenAdapter fontsizeBy750:24]);
//        orderCountTipLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
//        [timeView addSubview:orderCountTipLabel];
//        [orderCountTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(_orderCountLabel.mas_left);
//            make.centerY.equalTo(timeView);
//        }];
        
        UIView *lineview1 = [[UIView alloc] init];
        lineview1.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [timeView addSubview:lineview1];
        [lineview1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(timeView);
            make.left.equalTo(timeView);
            make.right.equalTo(timeView);
            make.height.mas_equalTo(0.5);
        }];
        UIView *lineview2 = [[UIView alloc] init];
        lineview2.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [timeView addSubview:lineview2];
        [lineview2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(timeView);
            make.left.equalTo(timeView);
            make.right.equalTo(timeView);
            make.height.mas_equalTo(0.5);
        }];
        
        [timeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.height.mas_equalTo(timeViewHeight);
        }];
        
        
        UIView *topView = [[UIView alloc] init];
        [self.contentView addSubview:topView];
        UIView *bottomView = [[UIView alloc] init];
        [self.contentView addSubview:bottomView];
        UIView *thirdView = [[UIView alloc] init];
        [self.contentView addSubview:thirdView];

        CGFloat bottomSpace = [YMCScreenAdapter intergerSizeBy750:24];
        topView.backgroundColor = [UIColor whiteColor];
        bottomView.backgroundColor = [UIColor whiteColor];
        thirdView.backgroundColor = [UIColor whiteColor];
        
        
        [topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(timeView.mas_bottom);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:120]);
        }];
        [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topView.mas_bottom);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.height.equalTo(topView);
        }];
        [thirdView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(bottomView.mas_bottom);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.height.equalTo(topView);
            make.bottom.equalTo(self.contentView).with.offset(-bottomSpace);
        }];
        
        _secondChannelView = [[YMDealerReportTableViewCellItemView alloc] init];
        _secondChannelView.nameLabel.text = @"经销商名称";
        [topView addSubview:_secondChannelView];
        _rootChannelView = [[YMDealerReportTableViewCellItemView alloc] init];
        _rootChannelView.nameLabel.text = @"城市运营名称";
        [topView addSubview:_rootChannelView];
        _userCountView = [[YMDealerReportTableViewCellItemView alloc] init];
        _userCountView.nameLabel.text = @"新增微信会员数";
        [topView addSubview:_userCountView];

        [_secondChannelView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topView);
            make.left.equalTo(topView);
            make.bottom.equalTo(topView);
        }];
        [_rootChannelView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topView);
            make.left.equalTo(_secondChannelView.mas_right);
            make.bottom.equalTo(topView);
            make.width.equalTo(_secondChannelView);
        }];
        [_userCountView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topView);
            make.left.equalTo(_rootChannelView.mas_right);
            make.bottom.equalTo(topView);
            make.right.equalTo(topView);
            make.width.equalTo(_secondChannelView);
        }];
        
        
        _staffCount = [[YMDealerReportTableViewCellItemView alloc] init];
        _staffCount.nameLabel.text = @"新增店员";
        [bottomView addSubview:_staffCount];
        _parttimeCountView = [[YMDealerReportTableViewCellItemView alloc] init];
        _parttimeCountView.nameLabel.text = @"新增兼职店员";
        [bottomView addSubview:_parttimeCountView];
        _terminalCountView = [[YMDealerReportTableViewCellItemView alloc] init];
        _terminalCountView.nameLabel.text = @"新增门店";
        [bottomView addSubview:_terminalCountView];
        
        [_staffCount mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(bottomView);
            make.left.equalTo(bottomView);
            make.bottom.equalTo(bottomView);
        }];
        [_parttimeCountView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(bottomView);
            make.left.equalTo(_staffCount.mas_right);
            make.bottom.equalTo(bottomView);
            make.width.equalTo(_staffCount);
        }];
        [_terminalCountView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(bottomView);
            make.left.equalTo(_parttimeCountView.mas_right);
            make.bottom.equalTo(bottomView);
            make.right.equalTo(bottomView);
            make.width.equalTo(_staffCount);
        }];
        
        _orderCountView = [[YMDealerReportTableViewCellItemView alloc] init];
        _orderCountView.nameLabel.text = @"订单数";
        [thirdView addSubview:_orderCountView];
        [_orderCountView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(thirdView);
            make.left.equalTo(thirdView);
            make.bottom.equalTo(thirdView);
            make.width.equalTo(_staffCount);
        }];
        
        _orderAmountView = [[YMDealerReportTableViewCellItemView alloc] init];
        _orderAmountView.nameLabel.text = @"订单金额";
        [thirdView addSubview:_orderAmountView];
        [_orderAmountView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(thirdView);
            make.left.equalTo(_orderCountView.mas_right);
            make.bottom.equalTo(thirdView);
            make.width.equalTo(_staffCount);
        }];
        
        UIView *lineview3 = [[UIView alloc] init];
        lineview3.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [self.contentView addSubview:lineview3];
        [lineview3 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.contentView).with.offset(-bottomSpace);
            make.right.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.height.mas_equalTo(0.5);
        }];
    }
    return self;
}

@end
