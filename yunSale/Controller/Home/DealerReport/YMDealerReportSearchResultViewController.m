//
//  YMDealerReportSearchResultViewController.m
//  yunSale
//
//  Created by liushilou on 17/1/6.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMDealerReportSearchResultViewController.h"
#import "YMDealerReportSearchResultTableViewCell.h"
#import "YMOrderAPI.h"
#import <MJRefresh/MJRefresh.h>
#import <MBProgressHUD.h>


@interface YMDealerReportSearchResultViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) YMOrderAPI *orderAPI;

@property (nonatomic,assign) NSInteger pageNum;

@property (nonatomic,strong) NSMutableArray *datas;

@end

@implementation YMDealerReportSearchResultViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.datas = [NSMutableArray new];
    
    [self drawView];
    
    //上拉加载
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(fetchData)];
    self.pageNum = 1;
    //加载数据
    [self fetchData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)drawView {
    self.navigationItem.title = @"经销商日报查询结果";
    self.tableView = [[UITableView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma UITableViewDelegate,UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.datas.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [YMCScreenAdapter intergerSizeBy750:500];  //370
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"orderCell";
    YMDealerReportSearchResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell) {
        cell = [[YMDealerReportSearchResultTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *item = [self.datas objectAtIndex:indexPath.row];
    
    NSString *day = [item notNullObjectForKey:@"day"];
    
    NSString *secondChannel = [item notNullObjectForKey:@"secondChannel"];
    if (!secondChannel) {
        secondChannel = @"-";
    }
    NSString *rootChannel = [item notNullObjectForKey:@"rootChannel"];
    if (!rootChannel) {
        rootChannel = @"-";
    }
    
    NSNumber *orderAmount   = [item notNullObjectForKey:@"orderAmount"];
    NSNumber *orderCount    = [item notNullObjectForKey:@"orderCount"];
    NSNumber *staffCount    = [item notNullObjectForKey:@"staffCount"];
    NSNumber *terminalCount = [item notNullObjectForKey:@"terminalCount"];
    NSNumber *parttimeCount = [item notNullObjectForKey:@"parttimeCount"];
    NSNumber *userCount     = [item notNullObjectForKey:@"userCount"];
    
    //数据配置
    cell.timeLabel.text = day;
    cell.orderCountView.valueLabel.text     = orderCount.stringValue;
    cell.secondChannelView.valueLabel.text  = secondChannel;
    cell.rootChannelView.valueLabel.text    = rootChannel;
    cell.staffCount.valueLabel.text         = staffCount.stringValue;
    cell.parttimeCountView.valueLabel.text  = parttimeCount.stringValue;
    cell.terminalCountView.valueLabel.text  = terminalCount.stringValue;
    cell.orderAmountView.valueLabel.text    = [NSString stringWithFormat:@"¥%@",orderAmount];
    cell.userCountView.valueLabel.text      = userCount.stringValue;
    
    
//    NSDictionary *item = [self.datas objectAtIndex:indexPath.row];
//
//    NSString *day = [item notNullObjectForKey:@"day"];
//
//    NSString *channelName = [item notNullObjectForKey:@"channelName"];
//    if (!channelName) {
//        channelName = @"-";
//    }
//    NSString *secondChannel = [item notNullObjectForKey:@"secondChannel"];
//    if (!secondChannel) {
//        secondChannel = @"-";
//    }
//    NSString *rootChannel = [item notNullObjectForKey:@"rootChannel"];
//    if (!rootChannel) {
//        rootChannel = @"-";
//    }
//    NSNumber *purchaseMachineCount = [item notNullObjectForKey:@"purchaseMachineCount"];
//    if (!purchaseMachineCount) {
//        purchaseMachineCount = @0;
//    }
//    NSNumber *purchaseOrderCount = [item notNullObjectForKey:@"purchaseOrderCount"];
//    if (!purchaseOrderCount) {
//        purchaseOrderCount = @0;
//    }
//    NSNumber *purchaseOrderAmount = [item notNullObjectForKey:@"purchaseOrderAmount"];
//    if (!purchaseOrderAmount) {
//        purchaseOrderAmount = @0;
//    }
//    
//    cell.timeLabel.text = day;
//    cell.channelNameView.valueLabel.text = channelName;
//    cell.secondChannelView.valueLabel.text = secondChannel;
//    cell.rootChannelView.valueLabel.text = rootChannel;
//    cell.purchaseMachineCountView.valueLabel.text = purchaseMachineCount.stringValue;
//    cell.purchaseOrderCountView.valueLabel.text = purchaseOrderCount.stringValue;
//    cell.purchaseOrderAmountView.valueLabel.text = [NSString stringWithFormat:@"¥%@",purchaseOrderAmount];
    return cell;
}


- (void)fetchData {
    if (!self.orderAPI) {
        self.orderAPI = [[YMOrderAPI alloc] init];
    }
    
    if (self.pageNum == 1) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [YMCAlertView removeNetErrorInView:self.view];
    }
    
    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:self.queryDatas];
    [data setObject:@(self.pageNum) forKey:@"pageNum"];
    [data setObject:@(10) forKey:@"pageSize"];
    
    [self.orderAPI fetchDealerReportSearch:data completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {

        if (self.pageNum == 1) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        
        if (status == YMCRequestSuccess) {
            NSDictionary *result = [data notNullObjectForKey:@"result"];
            NSArray *list = [result notNullObjectForKey:@"list"];
            [self.datas addObjectsFromArray:list];
            
            NSNumber *totalPageNum = [result notNullObjectForKey:@"totalPageNum"];
            
            [self.tableView.mj_footer endRefreshing];
            if (totalPageNum.integerValue <= self.pageNum) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                //数据空的处理
                if(!list || list.count == 0){
                    @weakify(self)
                    [YMCAlertView showNetErrorInView:self.view type:YMCRequestNoData message:YM_NOSEARCHDATA_TEXT actionBlock:^{
                        @strongify(self);                    [self.navigationController popViewControllerAnimated:YES];
                    }];
                }
            }else{
                self.pageNum += 1;
            }
            [self.tableView reloadData];
        }else{
            [self.tableView.mj_footer endRefreshing];
            if (self.pageNum == 1) {
                //首次加载
                @weakify(self);
                [YMCAlertView showNetErrorInView:self.view type:status message:message actionBlock:^{
                    @strongify(self);
                    
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [self fetchData];
                }];
            }else{
                [YMCAlertView showMessage:message];
            }
        }
    }];
}

@end
