//
//  YMDealerReportSearchViewController.m
//  yunSale
//
//  Created by liushilou on 17/1/6.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMDealerReportSearchViewController.h"
#import "YMSearchInputView.h"
#import "YMDateSelectView.h"
#import "YMButton.h"
#import "YMOrderAPI.h"
#import "YMDealerReportSearchResultViewController.h"



@interface YMDealerReportSearchViewController ()<UITextFieldDelegate>

//@property (nonatomic, strong) YMSearchInputView *channelNameInputView;
@property (nonatomic, strong) YMSearchInputView *secondChannelInputView;
@property (nonatomic, strong) YMSearchInputView *rootChannelInputView;
@property (nonatomic, strong) YMDateSelectView *beginDateSelectorView;
@property (nonatomic, strong) YMDateSelectView *endDateSelectorView;

@end

@implementation YMDealerReportSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self drawView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Draw
- (void)drawView
{
    self.navigationItem.title = @"数据报表查询";
    
    self.view.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    
//    //搜索输入框
//    self.channelNameInputView = [[YMSearchInputView alloc] initWithTitle:@"门店名称" placeHolder:@"请输入门店名称"];
//    [self.parentScrollView addSubview:self.channelNameInputView];
//    [self.channelNameInputView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.parentScrollView);
//        make.right.equalTo(self.parentScrollView);
//        make.height.mas_equalTo([YMScreenAdapter sizeBy750:100]);
//        make.top.equalTo(self.parentScrollView).with.offset([YMScreenAdapter sizeBy750:YMSPACE_AROUND]);
//    }];
    
    //搜索输入框
    self.secondChannelInputView = [[YMSearchInputView alloc] initWithTitle:@"经销商" placeHolder:@"请输入经销商名称"];
    [self.parentScrollView addSubview:self.secondChannelInputView];
    [self.secondChannelInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.parentScrollView);
        make.right.equalTo(self.view);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.top.equalTo(self.parentScrollView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
    }];
    self.secondChannelInputView.textField.delegate = self;
    

    self.rootChannelInputView = [[YMSearchInputView alloc] initWithTitle:@"城市运营" placeHolder:@"请输入城市运营名称"];
    //城市运营不给搜索
    if([[YMUserManager shareinstance].roleId intValue] != 21){
        [self.parentScrollView addSubview:self.rootChannelInputView];
        [self.rootChannelInputView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.parentScrollView);
            make.right.equalTo(self.view);
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
            make.top.equalTo(self.secondChannelInputView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:15]);
        }];
        self.rootChannelInputView.textField.delegate = self;
        
        self.beginDateSelectorView = [[YMDateSelectView alloc] initWithTip:@"开始时间" placeHoder:@"选择开始时间"];
        [self.parentScrollView addSubview:self.beginDateSelectorView];
        [self.beginDateSelectorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.rootChannelInputView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:15]);
            make.left.equalTo(self.parentScrollView);
            //        make.right.equalTo(self.parentScrollView);
            make.right.equalTo(self.view);
            make.height.mas_equalTo([YMCScreenAdapter fontsizeBy750:100]);
        }];
    }else {
        self.beginDateSelectorView = [[YMDateSelectView alloc] initWithTip:@"开始时间" placeHoder:@"选择开始时间"];
        [self.parentScrollView addSubview:self.beginDateSelectorView];
        [self.beginDateSelectorView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.secondChannelInputView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:15]);
            make.left.equalTo(self.parentScrollView);
            make.right.equalTo(self.view);
            make.height.mas_equalTo([YMCScreenAdapter fontsizeBy750:100]);
        }];
    }
    
    self.endDateSelectorView = [[YMDateSelectView alloc] initWithTip:@"结束时间" placeHoder:@"选择结束时间"];
    [self.parentScrollView addSubview:self.endDateSelectorView];
    [self.endDateSelectorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.beginDateSelectorView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:15]);
        make.left.equalTo(self.parentScrollView);
//        make.right.equalTo(self.parentScrollView);
         make.right.equalTo(self.view);
        make.height.mas_equalTo([YMCScreenAdapter fontsizeBy750:100]);
    }];
    
    //搜索按钮
    YMButton *searchBtn = [[YMButton alloc] initWithStyle:YMButtonStyleRectangle];
    [searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    [self.parentScrollView addSubview:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.parentScrollView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.right.equalTo(self.view).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.top.equalTo(self.endDateSelectorView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
    }];
    
    //下划线
    UIView *bottomLineView = [[UIView alloc] init];
    bottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [self.parentScrollView addSubview:bottomLineView];
    [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.parentScrollView);
        make.right.equalTo(self.parentScrollView);
        make.height.mas_equalTo(0.5);
        make.top.equalTo(searchBtn.mas_bottom).with.offset(5);
        make.bottom.equalTo(self.parentScrollView);
    }];
}

#pragma mark - UITextFieldDelegate
//scrollView针对不同尺寸的设备而滚动不同的偏移量
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    DeviceScreenSizeType deviceScreenType = [YMDevice deviceScreenSizeType];
    
    if (textField == self.secondChannelInputView.textField) {
        self.offset = 20;
    } else if (textField == self.rootChannelInputView.textField) {
        
        if (deviceScreenType == iPhone4 || deviceScreenType == iPhone5) {
            self.offset = 55;
        } else if (deviceScreenType == iPhone6) {
            self.offset = 30;
        } else {
            self.offset = 0;
        }
    }
}

- (void)searchAction {
   // NSString *channelName = self.channelNameInputView.textField.text;
    NSString *secondChannel = self.secondChannelInputView.textField.text;
    NSString *rootChannel = self.rootChannelInputView.textField.text;
    
    NSString *beginTime = self.beginDateSelectorView.dateValue;
    NSString *endTime = self.endDateSelectorView.dateValue;
    if (!beginTime) {
        beginTime = @"";
    }
    if (!endTime) {
        endTime = @"";
    }
    
    //    if ([beginTime compare:endTime options:NSNumericSearch] == NSOrderedDescending) {
    //        [YMAlertView showHubInView:self.view message:@"结束时间应该大于开始时间"];
    //        return;
    //    }
    
    NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
//                          channelName,@"channelName",
                          secondChannel,@"secondChannel",
                          rootChannel,@"rootChannel",
                          beginTime,@"beginTime",
                          endTime,@"endTime",
                          //                          @(1),@"pageNum",
                          //                          @(10),@"pageSize",
                          nil];
    
    YMDealerReportSearchResultViewController *controller = [[YMDealerReportSearchResultViewController alloc] init];
    controller.queryDatas = data;
    [self.view endEditing:YES];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
