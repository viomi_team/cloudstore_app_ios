//
//  YMDealerReportTableView.h
//  yunSale
//
//  Created by ChengMinZhang on 2016/12/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMOrderAPI.h"

@interface YMDealerReportTableView : UIView

//@property (nonatomic,copy) NSArray *salesDatas;

@property (nonatomic,strong) YMOrderAPI *orderAPI;

//已加载数据
@property (nonatomic,assign) BOOL fetched;

- (void)fetchData:(YMAdPayOrderType)type;

@end
