//
//  YMDealerReportTableView.m
//  yunSale
//
//  Created by ChengMinZhang on 2016/12/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//
#import <ActionSheetStringPicker.h>
#import "YMDealerReportTableView.h"
#import "YMFourItemTableHeaderView.h"
#import "YMDealerReportTableViewCell.h"
#import "YMDealerReportChartTableViewCell.h"
#import "YMDate.h"
#import "YMDealerModel.h"

//枚举类型：图表title
typedef NS_ENUM(NSInteger, YMChartDataType) {
    YMChartDataWeChatUserCount = 0,      //新增微信会员
    YMChartDataStaffCount,      //新增店员
    YMChartDataParttimeCount,   //新增兼职店员
    YMChartDataTerminalCount,   //新增门店数量
    YMChartDataOrderCount,      //新增订单数
    YMChartDataOrderAmount      //新增订单金额
};


@interface YMDealerReportTableView() <UITableViewDelegate, UITableViewDataSource,YMHistogramChartActionViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, assign) CGFloat chartCellHeight;

@property (nonatomic, strong) YMFourItemTableHeaderView *headerView;

@property (nonatomic, assign) YMAdPayOrderType type;

@property (nonatomic, strong) NSString *lastDate;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@property (nonatomic, strong) NSArray *chartDatas;
@property (nonatomic, strong) NSArray *detailsDatas;

@property (nonatomic, assign) NSInteger openRow;

@property (nonatomic, assign) BOOL loadedChart;

@property (nonatomic, assign) YMChartDataType selectedChartDataType; //记录柱状图当前显示的类型，默认【新增微信会员数】
@property (nonatomic, strong) NSArray* chartDataTypeArray;

@property (nonatomic, strong) YMDealerReportChartTableViewCell *chartCell;

@end

@implementation YMDealerReportTableView

- (instancetype)init {
    self = [super init];
    if (self) {
        //这里做一些简单的初始化工作。
        _openRow = -1;
        _chartCellHeight = [YMCScreenAdapter sizeBy750:504];
        
        self.tableView = [[UITableView alloc] init];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
        [self addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        _selectedChartDataType = YMChartDataWeChatUserCount;
        
        _chartDataTypeArray = @[@{@"filed":@"userCount",@"name":@"新增微信会员数"},
                                @{@"filed":@"staffCount",@"name":@"新增店员数"},
                                @{@"filed":@"parttimeCount",@"name":@"新增兼职店员数"},
                                @{@"filed":@"terminalCount",@"name":@"新增门店"},
                                @{@"filed":@"orderCount",@"name":@"订单数"},
                                @{@"filed":@"orderAmount",@"name":@"订单金额"}];
    }
    return self;
}

#pragma mark - UITableViewDelegate & UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (section == 0) {
//        return 1;
//    } else {
//        if (self.detailsDatas.count > 0) {
//            return self.detailsDatas.count;
//        }else{
//            return 1;
//        }
//    }
    return (section != 0 && self.detailsDatas.count > 0)? self.detailsDatas.count : 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
//        return self.chartCellHeight;
//    } else {
//        if (self.detailsDatas.count > 0) {
//            if (self.openRow == indexPath.row) {
//                return [YMScreenAdapter intergerSizeBy750:340];
//            }else{
//                return [YMScreenAdapter intergerSizeBy750:100];
//            }
//        }else{
//            return [YMScreenAdapter intergerSizeBy750:100];
//        }
//    }
    return (indexPath.section == 0)? self.chartCellHeight :
    (self.detailsDatas.count > 0 && self.openRow == indexPath.row)? [YMCScreenAdapter intergerSizeBy750:340] : [YMCScreenAdapter intergerSizeBy750:100];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    if (section == 0) {
//        return [YMScreenAdapter sizeBy750:24];
//    }else{
//        return 0;
//    }
    return section == 0? [YMCScreenAdapter sizeBy750:24] : 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return 0;
//    }else{
//        return [YMScreenAdapter sizeBy750:80];
//    }
    return section != 0? [YMCScreenAdapter sizeBy750:80] : 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if(section == 1) {
        if(!self.headerView){
            self.headerView = [[YMFourItemTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [YMCScreenAdapter sizeBy750:80]) style:YMFourItemHeaderViewSytleDealerReport];
            self.headerView.thirdLabel.numberOfLines = 2;  //新增微信会员数需要两行显示
        }
        
        NSDictionary *dic = [self.chartDataTypeArray objectAtIndex:self.selectedChartDataType];
        
        NSString *name = [dic objectForKey:@"name"];
        
        self.headerView.thirdLabel.text = name;
    
        return self.headerView;
    }else {
        return  nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        
        static NSString *reuseIdentifier = @"chartCell";
        self.chartCell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (!self.chartCell) {
            self.chartCell = [[YMDealerReportChartTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
            self.chartCell.selectionStyle = UITableViewCellSelectionStyleNone;
            self.chartCell.chartView.delegate = self;
            self.chartCell.dataTypeArray = self.chartDataTypeArray;   // （By ChengMin Zhang  2017/1/16）
        }
        if (self.chartDatas.count > 0) {
            if (!self.loadedChart) {
                self.loadedChart = YES;
                [self.chartCell configCell:self.chartDatas dateType:self.type dataType:self.selectedChartDataType];
            }
        }
        return self.chartCell;
    } else {
        if (self.detailsDatas.count > 0) {
            static NSString *reuseIdentifier = @"detailCell";
            YMDealerReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
            if (!cell) {
                cell = [[YMDealerReportTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            /*从获取的data中拉取数据*/
            YMDealerModel *item      = [self.detailsDatas objectAtIndex:indexPath.row];
            NSString *secondChannel = item.secondChannel;
            //[item notNullObjectForKey:@"secondChannel"];//经销商名称
            if (!secondChannel) {
                secondChannel = @"--";
            }
            
            NSString *rootChannel   = item.rootChannel;
            //[item notNullObjectForKey:@"rootChannel"];  //城市运营名称
            if (!rootChannel) {
                rootChannel = @"--";
            }

            cell.secondChannelLabel.text = secondChannel;
            cell.rootChannelLabel.text   = rootChannel;
            
            NSString *firstString = nil;
            NSString *secondString = nil;
            NSString *thirdString = nil;
            NSString *fourthString = nil;
            NSString *fifthString = nil;
            NSString *sixthString = nil;
            
            secondString = @(item.staffCount).stringValue;
            thirdString = @(item.parttimeCount).stringValue;
            fourthString = @(item.terminalCount).stringValue;
            fifthString = @(item.orderCount).stringValue;
            sixthString = [NSString stringWithFormat:@"%.2f",item.orderAmount];
            
            
            
            if (self.selectedChartDataType == YMChartDataWeChatUserCount) {
                firstString = @(item.userCount).stringValue;
            }else if (self.selectedChartDataType == YMChartDataStaffCount) {
                firstString = @(item.staffCount).stringValue;
            }else if (self.selectedChartDataType == YMChartDataParttimeCount) {
                firstString = @(item.parttimeCount).stringValue;
            }else if (self.selectedChartDataType == YMChartDataTerminalCount) {
                firstString = @(item.terminalCount).stringValue;
            }else if (self.selectedChartDataType == YMChartDataOrderCount) {
                firstString = @(item.orderCount).stringValue;
            }else if (self.selectedChartDataType == YMChartDataOrderAmount) {
                firstString = [NSString stringWithFormat:@"%.2f",item.orderAmount];
            }
        
            //数据配置

            cell.firstLabel.text = firstString;
            cell.secondView.valueLabel.text = @(item.staffCount).stringValue;
            cell.thirdView.valueLabel.text  = @(item.parttimeCount).stringValue;
            cell.fourthView.valueLabel.text = @(item.terminalCount).stringValue;
            cell.fifthView.valueLabel.text  = @(item.orderCount).stringValue;
            cell.sixthView.valueLabel.text  = [NSString stringWithFormat:@"%.2f",item.orderAmount];
            cell.seventhView.valueLabel.text  = @(item.userCount).stringValue;
            
            if (self.openRow == indexPath.row) {
                cell.showDetails = YES;
            }else{
                cell.showDetails = NO;
            }
            
            return cell;
        }else{
            static NSString *reuseIdentifier = @"emptyCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.contentView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
            cell.textLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:24]);
            cell.textLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
            cell.textLabel.text = @"数据为空";
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            return cell;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    if (self.openRow == indexPath.row) {
//        self.openRow = -1;
//    }else{
//        self.openRow = indexPath.row;
//    }
    self.openRow = (self.openRow == indexPath.row)? -1 : indexPath.row;
    [self.tableView reloadData];
}

#pragma YMHistogramChartActionViewDelegate
- (void)YMHistogramChartActionViewClickChartItem:(NSUInteger)index {
    [self fetchDetailsData:index];
}

//函数用处：选择图表主题
//例   子：默认显示【新增微信会员数】
//特别提示:【YMHistogramChartActionView】回调
- (void)YMHistogramChartActionViewClickTitle{
    
    NSArray * rolesArray = @[[[self.chartDataTypeArray objectAtIndex:0] objectForKey:@"name"],
                             [[self.chartDataTypeArray objectAtIndex:1] objectForKey:@"name"],
                             [[self.chartDataTypeArray objectAtIndex:2] objectForKey:@"name"],
                             [[self.chartDataTypeArray objectAtIndex:3] objectForKey:@"name"],
                             [[self.chartDataTypeArray objectAtIndex:4] objectForKey:@"name"],
                             [[self.chartDataTypeArray objectAtIndex:5] objectForKey:@"name"],];
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:@"请选择类型" rows:rolesArray initialSelection:self.selectedChartDataType doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        self.chartCell.chartView.titleLabel.text = [rolesArray objectAtIndex:selectedIndex];
        self.selectedChartDataType = selectedIndex;
        
        self.loadedChart = NO;
        
//        self.chartCell.chartView.selectLastItem = NO;   //表将不会默认滚动到底部
        
        NSArray *sorteArray = [self sortArray:self.detailsDatas];
        
        self.detailsDatas = sorteArray;
        
        [self.tableView reloadData];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        //Nothing to do
    } origin:self];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:nil action:nil];
    [picker setCancelButton:cancelButton];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:nil action:nil];
    [picker setDoneButton:doneButton];
    
    [picker showActionSheetPicker];
    
}

#pragma mark - Fetch
- (void)fetchData:(YMAdPayOrderType)type {
    self.fetched = YES;
    self.type = type;
    self.lastDate = [self.dateFormatter stringFromDate:[NSDate date]];
    [self fetchData];
}


- (void)fetchData {
    if (!self.orderAPI) {
        self.orderAPI = [[YMOrderAPI alloc] init];
    }
    [YMCAlertView removeNetErrorInView:self];
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    [self.orderAPI fetchDealerReport:self.type lastDate:self.lastDate completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        [MBProgressHUD hideHUDForView:self animated:YES];
        if (status == YMCRequestSuccess) {
            NSArray *result = [data notNullObjectForKey:@"result"];
            self.chartDatas = result;
            [self.tableView reloadData];
            
            [self fetchDetailsData:self.chartDatas.count - 1];
        }else{
            @weakify(self);
            [YMCAlertView showNetErrorInView:self type:status message:message actionBlock:^{
                @strongify(self);
                [self fetchData];
            }];
        }
    }];
}

- (void)fetchDetailsData:(NSInteger)index {
    NSInteger currentIndex = self.chartDatas.count - index - 1;
    NSDictionary *item = [self.chartDatas objectAtIndex:currentIndex];
    
    NSNumber *beginDate = [item notNullObjectForKey:@"beginDate"];
    NSNumber *endDate = [item notNullObjectForKey:@"endDate"];
    NSString *beginDateString = [self.dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:beginDate.longLongValue/1000]];
    NSString *endDateString = [self.dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:endDate.longLongValue/1000]];
    
    if (self.headerView) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/dd"];
        NSString *dateString = [YMDate stringwithType:self.type beginDate:beginDate endDate:endDate formatter:formatter index:currentIndex yearType:1];
        self.headerView.fourthLabel.text = dateString;
    }
    
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    [self.orderAPI fetchDealerReportDetails:beginDateString endDate:endDateString completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        
        [MBProgressHUD hideHUDForView:self animated:YES];
        
        if (status == YMCRequestSuccess) {
            NSArray *result = [data notNullObjectForKey:@"result"];
            
            NSArray *modelArray = [YMDealerModel mj_objectArrayWithKeyValuesArray:result];
            
            NSArray *sorteArray = [self sortArray:modelArray];
            
            self.detailsDatas = sorteArray;
            
            [self.tableView reloadData];
        }else{
            [YMCAlertView showMessage:message];
        }
    }];
}

- (NSArray *)sortArray:(NSArray *)array
{
    NSString *keyname = [[self.chartDataTypeArray objectAtIndex:self.selectedChartDataType] notNullObjectForKey:@"filed"];
    
    // 这里的key写的是@property的名称
    NSSortDescriptor *desc = [NSSortDescriptor sortDescriptorWithKey:keyname ascending:NO];
    // 按顺序添加排序描述器
    NSArray *descs = [NSArray arrayWithObjects:desc,nil];
    
    NSArray *sorteArray = [array sortedArrayUsingDescriptors:descs];
    
    return sorteArray;
}


@end

