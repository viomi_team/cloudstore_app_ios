//
//  YMDealerReportTableViewCell.h
//  yunSale
//
//  Created by ChengMinZhang on 2016/12/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMDealerReportTableViewCellItemView.h"


//@interface YMDealerReportTableViewCellItemView : UIView
//
//@property (nonatomic,strong) UILabel *valueLabel;
//
//@property (nonatomic,strong) UILabel *nameLabel;
//
//@end




@interface YMDealerReportTableViewCell : UITableViewCell


@property (nonatomic, strong) UILabel *secondChannelLabel;//经销商名称

@property (nonatomic, strong) UILabel *rootChannelLabel;//城市运营名称

@property (nonatomic, strong) UILabel *firstLabel; //默认新增微信会员数

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *secondView;//默认新增店员数

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *thirdView;//默认新增兼职店员数

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *fourthView;//默认新增门店

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *fifthView; //默认订单数

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *sixthView;//默认订单金额

@property (nonatomic, strong) YMDealerReportTableViewCellItemView *seventhView;//默认微信会员

//@property (nonatomic, strong) UILabel *terminalCountLabel;

@property (nonatomic, assign) BOOL showDetails;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

- (void)reSetViewItmes:(NSMutableArray *)array;  //用户选择了图表类型后，下方的详细列表View改变。

@end
