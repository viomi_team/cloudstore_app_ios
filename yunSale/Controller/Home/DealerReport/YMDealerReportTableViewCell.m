//
//  YMDealerReportTableViewCell.m
//  yunSale
//
//  Created by ChengMinZhang on 2016/12/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMDealerReportTableViewCell.h"

//@implementation YMDealerReportTableViewCellItemView
//
//- (instancetype)init
//{
//    self = [super init];
//    if (self) {
//        _nameLabel = [[UILabel alloc] init];
//        _nameLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
//        _nameLabel.font = YM_FONT([YMScreenAdapter fontsizeBy750:24]);
//        [self addSubview:_nameLabel];
//        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.bottom.equalTo(self.mas_centerY).with.offset(-5);
//            make.left.equalTo(self).with.offset([YMScreenAdapter sizeBy750:YMSPACE_AROUND]);
//            make.right.equalTo(self);
//        }];
//        _valueLabel = [[UILabel alloc] init];
//        _valueLabel.numberOfLines = 2;
//        _valueLabel.text = @"-";
//        _valueLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
//        _valueLabel.font = YM_FONT([YMScreenAdapter fontsizeBy750:28]);
//        [self addSubview:_valueLabel];
//        [_valueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(self.mas_centerY).with.offset(-2);
//            make.left.equalTo(self).with.offset([YMScreenAdapter sizeBy750:YMSPACE_AROUND]);
//            make.right.equalTo(self);
//        }];
//    }
//    return self;
//}
//
//@end


@interface YMDealerReportTableViewCell()

@property (assign, nonatomic) CGFloat width1;
@property (assign, nonatomic) CGFloat width2;
@property (assign, nonatomic) CGFloat width3;
@property (assign, nonatomic) CGFloat width4;

@property (nonatomic, strong) UIImageView *indicateImageView;
@property (nonatomic, strong) UIView *bottomContainerView;
@property (nonatomic, strong) UIView *bottomLine;

@end

@implementation YMDealerReportTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.width1 = 220;
        self.width2 = 220;
        self.width3 = 140;
        
        UIView *topView = [[UIView alloc] init];
        topView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:topView];
        [topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.top.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.height.mas_equalTo([YMCScreenAdapter intergerSizeBy750:100]);
        }];
        
        //经销商名称
        self.secondChannelLabel = [[UILabel alloc] init];
        self.secondChannelLabel.textAlignment = NSTextAlignmentLeft;
        self.secondChannelLabel.numberOfLines = 2;
        self.secondChannelLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
        self.secondChannelLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [topView addSubview:self.secondChannelLabel];
        [self.secondChannelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(topView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.centerY.equalTo(topView);
            make.width.mas_equalTo([YMCScreenAdapter intergerSizeBy750:self.width1]);
        }];
        
        //城市运营商名称
        self.rootChannelLabel = [[UILabel alloc] init];
        self.rootChannelLabel.numberOfLines = 2;
        self.rootChannelLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
        self.rootChannelLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [topView addSubview:self.rootChannelLabel];
        [self.rootChannelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.secondChannelLabel.mas_right).with.offset(5);
            make.centerY.equalTo(topView);
            make.width.mas_equalTo([YMCScreenAdapter intergerSizeBy750:self.width2]);
        }];

        //新增微信会员数
        self.firstLabel = [[UILabel alloc] init];
        self.firstLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
        self.firstLabel.textAlignment = NSTextAlignmentCenter;
        self.firstLabel.numberOfLines = 2;
        self.firstLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [topView addSubview:self.firstLabel];
        [self.firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.rootChannelLabel.mas_right).with.offset(5);
            make.centerY.equalTo(topView);
            make.width.mas_equalTo([YMCScreenAdapter intergerSizeBy750:self.width3]);
        }];
        
        UIView *lineView1 = [[UIView alloc] init];
        lineView1.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [topView addSubview:lineView1];
        [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(topView);
            make.left.equalTo(topView);
            make.right.equalTo(topView).with.offset(-[YMCScreenAdapter sizeBy750:94]);
            make.height.mas_equalTo(0.5);
        }];
        
        self.indicateImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ym_index_down"]];
        [topView addSubview:self.indicateImageView];
        [self.indicateImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).with.offset(-[YMCScreenAdapter sizeBy750:35]);
            make.centerY.equalTo(topView);
        }];
        
        self.bottomContainerView = [[UIView alloc] init];
        self.bottomContainerView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        [self.contentView addSubview:self.bottomContainerView];
        [self.bottomContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topView.mas_bottom);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
        }];
        
//        //分割线，和第三个Label的右边作约束
//        UIView *topLine = [[UIView alloc] init];
//        topLine.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
//        [self.contentView addSubview:topLine];
//        [topLine mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.contentView);
//            make.bottom.equalTo(self.bottomContainerView.mas_top).with.offset(0);
//            make.height.mas_equalTo(0.5);
//            make.width.mas_equalTo([YMScreenAdapter sizeBy750:self.width1+self.width2+self.width3] + 10 + YMSPACE_AROUND);
//        }];
       
        self.secondView = [[YMDealerReportTableViewCellItemView alloc] init];
        self.secondView.nameLabel.text = @"新增店员数";
        [self.bottomContainerView addSubview:self.secondView];
        [self.secondView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bottomContainerView).with.offset(0);
            make.top.equalTo(self.bottomContainerView).with.offset(0);
            make.height.mas_equalTo([YMCScreenAdapter intergerSizeBy750:120]);
            //make.width.mas_equalTo([YMScreenAdapter intergerSizeBy750:self.width1]);
        }];
        
        self.thirdView = [[YMDealerReportTableViewCellItemView alloc] init];
        self.thirdView.nameLabel.text = @"新增兼职店员数";
        [self.bottomContainerView addSubview:self.thirdView];
        [self.thirdView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.secondView.mas_right).with.offset(5);
            make.top.equalTo(self.bottomContainerView);
            make.size.equalTo(self.secondView);
//            make.height.mas_equalTo([YMScreenAdapter intergerSizeBy750:120]);
//            make.width.mas_equalTo([YMScreenAdapter intergerSizeBy750:self.width2]);
        }];
        
        self.fourthView = [[YMDealerReportTableViewCellItemView alloc] init];
        self.fourthView.nameLabel.text = @"新增门店";
        [self.bottomContainerView addSubview:self.fourthView];
        [self.fourthView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.thirdView.mas_right).with.offset(5);
            make.top.equalTo(self.bottomContainerView);
            make.right.equalTo(self.bottomContainerView);
            make.size.equalTo(self.secondView);
//            make.height.mas_equalTo([YMScreenAdapter intergerSizeBy750:120]);
//            make.width.mas_equalTo([YMScreenAdapter intergerSizeBy750:self.width3]);
        }];
        
        self.fifthView = [[YMDealerReportTableViewCellItemView alloc] init];
        self.fifthView.nameLabel.text = @"订单数";
        [self.bottomContainerView addSubview:self.fifthView];
        [self.fifthView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.bottomContainerView).with.offset(0);
            make.top.equalTo(self.secondView.mas_bottom).with.offset(0);
            make.size.equalTo(self.secondView);
//            make.height.mas_equalTo([YMScreenAdapter intergerSizeBy750:120]);
//            make.width.mas_equalTo([YMScreenAdapter intergerSizeBy750:self.width1]);
        }];
        
        
        self.sixthView = [[YMDealerReportTableViewCellItemView alloc] init];
        self.sixthView.nameLabel.text = @"订单金额";
        [self.bottomContainerView addSubview:self.sixthView];
        [self.sixthView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.fifthView.mas_right).with.offset(5);
            make.top.equalTo(self.thirdView.mas_bottom).with.offset(0);
            make.size.equalTo(self.thirdView);
            //            make.height.mas_equalTo([YMScreenAdapter intergerSizeBy750:120]);
            //            make.width.mas_equalTo([YMScreenAdapter intergerSizeBy750:self.width1]);
        }];
        
        self.seventhView = [[YMDealerReportTableViewCellItemView alloc] init];
        self.seventhView.nameLabel.text = @"新增微信会员数";
        [self.bottomContainerView addSubview:self.seventhView];
        [self.seventhView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.sixthView.mas_right).with.offset(5);
            make.top.equalTo(self.secondView.mas_bottom).with.offset(0);
            make.size.equalTo(self.secondView);
            make.right.equalTo(self.bottomContainerView);
        }];
        
        
        UIView *lineView2 = [[UIView alloc] init];
        lineView2.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [self.bottomContainerView addSubview:lineView2];
        [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.bottomContainerView);
            make.left.equalTo(self.bottomContainerView);
            make.right.equalTo(self.bottomContainerView);
            make.height.mas_equalTo(0.5);
        }];
        
        //初始化，设置 bottomContainerView 为隐藏状态
        [self.bottomContainerView setHidden:YES];
    }
    return self;
}

//重写约束，控制cell的展开或收起
- (void)setShowDetails:(BOOL)showDetails {
    _showDetails = showDetails;
    
    if (showDetails) {
//        [self.bottomLine mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(self.contentView);
//        }];
        self.indicateImageView.image = [UIImage imageNamed:@"ym_index_up"];
        [self.bottomContainerView setHidden:NO];
        
    } else {
//        [self.bottomLine mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(self.thirdLabel.mas_right);
//        }];
        self.indicateImageView.image = [UIImage imageNamed:@"ym_index_down"];
        [self.bottomContainerView setHidden:YES];
    }
}

//函数用途：重新设置表格的Items
//调用时机：当用选择了一个柱状图类型后
//另外说明：self.firstView已经改变了，不用管。
- (void)reSetViewItmes:(NSMutableArray *)array{
    if(array && array.count <= 6){
        self.secondView.nameLabel.text = [[array  objectAtIndex:1] objectForKey:@"name"];
        self.thirdView.nameLabel.text  = [[array  objectAtIndex:2] objectForKey:@"name"];
        self.fourthView.nameLabel.text = [[array  objectAtIndex:3] objectForKey:@"name"];
        self.fifthView.nameLabel.text  = [[array  objectAtIndex:4] objectForKey:@"name"];
        self.sixthView.nameLabel.text  = [[array  objectAtIndex:5] objectForKey:@"name"];
    }
    
}



@end
