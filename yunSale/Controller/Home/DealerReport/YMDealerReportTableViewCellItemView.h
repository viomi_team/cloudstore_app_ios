//
//  YMDealerReportTableViewCellItemView.h
//  yunSale
//
//  Created by liushilou on 17/1/6.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMDealerReportTableViewCellItemView : UIView

@property (nonatomic,strong) UILabel *valueLabel;

@property (nonatomic,strong) UILabel *nameLabel;

@end
