//
//  YMDealerReportTableViewCellItemView.m
//  yunSale
//
//  Created by liushilou on 17/1/6.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMDealerReportTableViewCellItemView.h"

@implementation YMDealerReportTableViewCellItemView

- (instancetype)init
{
    self = [super init];
    if (self) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        _nameLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:24]);
        [self addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.mas_centerY).with.offset(-5);
            make.left.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.right.equalTo(self);
        }];
        _valueLabel = [[UILabel alloc] init];
        _valueLabel.numberOfLines = 2;
        _valueLabel.text = @"-";
        _valueLabel.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
        _valueLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:24]);
        [self addSubview:_valueLabel];
        [_valueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.mas_centerY).with.offset(-2);
            make.left.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.right.equalTo(self);
        }];
    }
    return self;
}

@end
