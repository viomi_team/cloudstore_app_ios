//
//  YMDealerReportViewController.h
//  yunSale
//
//  Created by ChengMinZhang on 2016/12/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMScrollItemsBaseViewController.h"

@interface YMDealerReportViewController : YMScrollItemsBaseViewController

@end
