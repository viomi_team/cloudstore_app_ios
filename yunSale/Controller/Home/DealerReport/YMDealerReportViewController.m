//
//  YMDealerReportViewController.m
//  yunSale
//
//  Created by ChengMinZhang on 2016/12/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//
#import <UMMobClick/MobClick.h>
#import "YMDealerReportTableView.h"
#import "YMDealerReportViewController.h"
#import "YMDealerReportSearchViewController.h"

@interface YMDealerReportViewController ()

@end

@implementation YMDealerReportViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick event:@"Home_Dealer_Report"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self drawMainView];
}


#pragma mark - Draw
- (void)drawMainView {
    self.navigationItem.title = @"数据报表";
    
    self.items = [NSArray arrayWithObjects:@"日报表",@"周报表",@"月报表", nil];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ym_index_search"] style:UIBarButtonItemStylePlain target:self action:@selector(searchAction)];
    [self.navigationItem setRightBarButtonItem:rightButton];
    
    YMDealerReportTableView *dayTableView = [[YMDealerReportTableView alloc] init];
    YMDealerReportTableView *weekTableView = [[YMDealerReportTableView alloc] init];
    YMDealerReportTableView *monthTableView = [[YMDealerReportTableView alloc] init];
    
    self.itemContentViews = [NSArray arrayWithObjects:dayTableView, weekTableView, monthTableView, nil];
    [self appearControllerOfIndex:0];
}

- (void)dealloc
{
    NSLog(@"%s",__func__);
    for (YMDealerReportTableView *tableview in self.itemContentViews) {
        [tableview.orderAPI cancle];
    }
}

#pragma mark - Action
- (void)searchAction {
    YMDealerReportSearchViewController *controller = [[YMDealerReportSearchViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

#pragma 重写父类
- (void)appearControllerOfIndex:(NSUInteger)index
{
    YMDealerReportTableView *controller = [self.itemContentViews objectAtIndex:index];
    if (!controller.fetched) {
        if (index == 0) {
            [controller fetchData:YMAdPayOrderDay];
        }else if (index == 1){
            [controller fetchData:YMAdPayOrderWeek];
        }else{
            [controller fetchData:YMAdPayOrderMonth];
        }
    }
}

@end
