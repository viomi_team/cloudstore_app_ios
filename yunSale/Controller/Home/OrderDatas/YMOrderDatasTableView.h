//
//  YMOrderDatasTableView.h
//  yunSale
//
//  Created by ChengMinZhang on 2017/8/17.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMOrderAPI.h"
#import "YMOrderData.h"

@class YMOrderDatasTableView;

@protocol YMOrderDatasTableViewDelegate <NSObject>

- (void)orderTableView:(YMOrderDatasTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

- (void)orderTableView:(YMOrderDatasTableView *)tableView footerRefreshWithPageNum:(NSInteger )pageNum;

@end

@interface YMOrderDatasTableView : UIView

@property (nonatomic, assign) YMOrderSource orderSource;   //订单来源

@property (nonatomic, assign) NSInteger pageNum,totalPageNum;  //当前页数，总页数

@property (nonatomic, strong) NSMutableArray<YMOrderData*> *orderDatas;

@property (nonatomic, strong) UILabel *orderAmountLabel;   //订单总数

@property (nonatomic, weak) id<YMOrderDatasTableViewDelegate> delegate;

- (void)reloadData;

- (void)endRefreshingWithNoMoreData:(BOOL)isNoMoreData; //结束上拉刷新

@end
