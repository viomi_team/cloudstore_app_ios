//
//  YMOrderDatasTableView.m
//  yunSale
//
//  Created by ChengMinZhang on 2017/8/17.
//  Copyright © 2017年 yunmi. All rights reserved.
//
#import "YMDate.h"
#import <Masonry.h>
#import "YMOrderAPI.h"
#import <MJRefresh/MJRefresh.h>
#import <YMCommon/YMCCommonUtils.h>
#import "YMOrderDatasTableView.h"
#import "YMProductInfoCell.h"
#import <UIImageView+WebCache.h>
#import "YMOneItemTableViewCell.h"
#import "YMOrderHeaderFooterView.h"

@interface YMOrderDatasTableView () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@property (nonatomic, strong) NSNumberFormatter *numberFormatter;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, assign) BOOL isRoldId21;

@end

@implementation YMOrderDatasTableView

- (instancetype)init{
    self = [super init];
    if(self) {
        _pageNum = 0;
        _totalPageNum = NSIntegerMax;
        _orderDatas = [NSMutableArray array];
        
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        
        _numberFormatter = [[NSNumberFormatter alloc] init];
        [_numberFormatter setPositiveFormat:@"0.0"];
        
        _isRoldId21 = [[YMUserManager shareinstance].roleId intValue] == 21;
        
        [self drawView];
        
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    }
    return self;
}

- (void)drawView {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(0);
        make.right.equalTo(self).with.offset(0);
        make.top.equalTo(self).with.offset(0);
        make.bottom.equalTo(self).with.offset(-[YMCScreenAdapter sizeBy750:100]);
    }];
    
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
        make.bottom.equalTo(self);
    }];
    
    UIView *topLine = [[UIView alloc] init];
    topLine.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE_DARK];
    [bottomView addSubview:topLine];
    [topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bottomView);
        make.left.equalTo(bottomView);
        make.right.equalTo(bottomView);
        make.top.equalTo(@0.5);
    }];
    
    _orderAmountLabel = [[UILabel alloc] init];
    _orderAmountLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:26]);
    _orderAmountLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GREEN];
    _orderAmountLabel.text = @"0";
    [bottomView addSubview:_orderAmountLabel];
    [_orderAmountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bottomView);
        make.right.equalTo(bottomView).with.offset(-[YMCScreenAdapter sizeBy750:26]);
    }];
    
    UILabel *descriptionLabel = [[UILabel alloc] init];
    descriptionLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:26]);
    descriptionLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
    descriptionLabel.text = @"当前订单合计：";
    [bottomView addSubview:descriptionLabel];
    [descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_orderAmountLabel.mas_left);
        make.centerY.equalTo(_orderAmountLabel.mas_centerY);
    }];

}

#pragma mark - UITableViewDelegate & UITableDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.orderDatas.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    YMOrderData *order = [self.orderDatas objectAtIndex:section];

    if(!self.isRoldId21)
    {
        return order.skuInfoList.count > 2 ? 3 : order.skuInfoList.count;

    }else {
        return order.skuImgs.count > 2 ? 3 : order.skuImgs.count;
    }
}

//header高
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [YMCScreenAdapter sizeBy750:YMORDERTALBE_HEADER_HEIGHT];
}

//行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    YMOrderData *orderData = [self.orderDatas objectAtIndex:indexPath.section];
    
    NSInteger skuNum = orderData.skuInfoList.count;
    
    return skuNum > 2 && indexPath.row == 2 ? [YMCScreenAdapter sizeBy750:55] : [YMCScreenAdapter sizeBy750:130];
}

//footer高
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return [YMCScreenAdapter sizeBy750:YMORDERTALBE_FOOTER_HEIGHT];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    static NSString *reuseIdentifier = @"headerView";
    YMOrderHeaderFooterView *header = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:reuseIdentifier];
    if(!header) {
        header = [[YMOrderHeaderFooterView alloc] initWithReuseIdentifier:reuseIdentifier];
        header.forwordTitle.font = YMC_FONT([YMCScreenAdapter fontsizeBy750:24]);
        header.forwordTitle.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        header.subTitle.font = YMC_FONT([YMCScreenAdapter fontsizeBy750:24]);
        header.subTitle.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
    }
    
    //配置Heder数据
    YMOrderData *orderData = [self.orderDatas objectAtIndex:section];
    NSString *orderCode = orderData.orderCode;
    NSString *payTypeDesc = orderData.payTypeDesc;
    NSString *payStatusDesc = orderData.dealPhaseDesc;
    
    header.forwordTitle.text = [NSString stringWithFormat:@"订单编号：%@",orderCode];
    header.subTitle.text = [NSString stringWithFormat:@"%@ | %@",payTypeDesc,payStatusDesc];
    return header;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    static NSString *reuseIdentifier = @"footerView";
    YMOrderHeaderFooterView *footer = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:reuseIdentifier];
    if(!footer) {
        footer = [[YMOrderHeaderFooterView alloc] initWithReuseIdentifier:reuseIdentifier];
        footer.forwordTitle.font = YMC_FONT([YMCScreenAdapter fontsizeBy750:24]);
        footer.forwordTitle.textColor = [UIColor colorWithHexString:YMCOLOR_LIGHT_GRAY];
        footer.subTitle.font = YMC_FONT([YMCScreenAdapter fontsizeBy750:28]);
        footer.subTitle.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
        footer.bottomLine.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE_DARK];
    }
    
    //配置footer数据
    YMOrderData *orderData = [self.orderDatas objectAtIndex:section];
    NSNumber *createdTime = orderData.createdTime;
    NSTimeInterval time = createdTime.longLongValue/1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *createdTimeString = [self.dateFormatter stringFromDate:date];
    footer.forwordTitle.text = createdTimeString;
    
    NSNumber *paymentPriceNumber = [NSNumber numberWithFloat:orderData.paymentPrice];
    NSString *paymentPriceString = [self.numberFormatter stringFromNumber:paymentPriceNumber];
    footer.subTitle.text = [NSString stringWithFormat:@"￥%@",paymentPriceString];
    return footer;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YMOrderData *order = [self.orderDatas objectAtIndex:indexPath.section];
    
    NSArray *skuList = !self.isRoldId21 ? order.skuInfoList : order.skuImgs;
    
    //【省略号Cell】
    if(indexPath.row ==2 && skuList.count > 2) {
        YMOneItemTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kYMOneItemTableViewCell];
        if(!cell) {
            cell = [[YMOneItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kYMOneItemTableViewCell];
        }
    
        cell.titleLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        cell.titleLabel.text = @"...";
        [cell isHidenBottomLine:NO];
        [cell.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(cell.mas_left).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND*3.5]);
        }];
        
        return cell;
    }
    //【商品详情Cell】城市运营&非城市运营
    static NSString *reuseIdentifier = @"productInfoCell";
    
    YMProductInfoCell *productCell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(!productCell) {
        productCell = [[YMProductInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
        [productCell.productQuantityLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(productCell.productNameLabel.mas_centerY);
        }];
        [productCell.productNameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:480]);
        }];
        productCell.productPriceLabel.hidden = YES;
    }
    
    //配置数据
    YMSkuInfo *skuInfo = [skuList objectAtIndex:indexPath.row];
    if(!self.isRoldId21)
    {
        //新数据模板
        productCell.productNameLabel.text = skuInfo.name;
        productCell.productQuantityLabel.text = [NSString stringWithFormat:@"×%d",skuInfo.quantity];
        [productCell.productImageView sd_setImageWithURL:[NSURL URLWithString:skuInfo.imgUrl] placeholderImage:[UIImage imageNamed:@"ym_home_placeholder"]];
    }else {
        //旧版数据模板
        productCell.productNameLabel.text = skuInfo.name;
        productCell.productQuantityLabel.text = @"×1";
        [productCell.productImageView sd_setImageWithURL:[NSURL URLWithString:skuInfo.url] placeholderImage:[UIImage imageNamed:@"ym_home_placeholder"]];
    }
    
    return productCell;
}

//选中
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.delegate &&
       [self.delegate respondsToSelector:@selector(orderTableView:didSelectRowAtIndexPath:)]) {
        [self.delegate orderTableView:self didSelectRowAtIndexPath:indexPath];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - 刷新数据
- (void)footerRefresh {
    if(self.delegate &&
       [self.delegate respondsToSelector:@selector(orderTableView:footerRefreshWithPageNum:)]) {
        [self.delegate orderTableView:self footerRefreshWithPageNum:self.pageNum];
    }
}

- (void)endRefreshingWithNoMoreData:(BOOL)isNoMoreData {
    isNoMoreData ? [self.tableView.mj_footer endRefreshingWithNoMoreData] : [self.tableView.mj_footer endRefreshing];
}

- (void)reloadData {
    [self.tableView reloadData];
}


@end
