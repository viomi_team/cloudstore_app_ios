//
//  YMOrderDatasViewController.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMDate.h"
#import "YMOrderAPI.h"
#import "YMOrderData.h"
#import "YMOrderDatasTableView.h"
#import "YMOrderDatasViewController.h"
#import "YMOrderDetailsViewController.h"
#import "YMOrderSearchViewController.h"
#import "YMOrderSearchResultViewController.h"
#import <MJExtension.h>
#import <MBProgressHUD.h>
#import <UMMobClick/MobClick.h>
#import <MJRefresh/MJRefresh.h>

@interface YMOrderDatasViewController () <YMOrderDatasSearchDelegate,YMOrderDatasTableViewDelegate>

@property (nonatomic, strong) YMOrderAPI *orderDataAPI;

@end

@implementation YMOrderDatasViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick event:@"Home_Order_Datas"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"订单数据";
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ym_index_search"] style:UIBarButtonItemStylePlain target:self action:@selector(searchBtnAction)];
    
    [self.navigationItem setRightBarButtonItem:rightButton];
    
    self.orderDataAPI = [[YMOrderAPI alloc] init];
    
    self.items = [NSArray arrayWithObjects:@"云分销订单",@"米家订单",@"天猫订单", nil];
    
    //云分销订单+米家订单+天猫订单（超级管理员、财会、仓管角色）
    if([[YMUserManager shareinstance].roleId intValue] == 1 ||
       [[YMUserManager shareinstance].roleId intValue] == 40 ||
       [[YMUserManager shareinstance].roleId intValue] == 50)
    {
        YMOrderDatasTableView *yunmiOrderDatasTableView = [[YMOrderDatasTableView alloc] init];
        YMOrderDatasTableView *mijiaOrderDatasTableView = [[YMOrderDatasTableView alloc] init];
        YMOrderDatasTableView *tianmaoOrderDatasTableView = [[YMOrderDatasTableView alloc] init];
        
        yunmiOrderDatasTableView.orderSource = YMOrderSourceYunmi;
        mijiaOrderDatasTableView.orderSource = YMOrderSourceMijia;
        tianmaoOrderDatasTableView.orderSource = YMOrderSourceTianmao;
        
        yunmiOrderDatasTableView.delegate = self;
        mijiaOrderDatasTableView.delegate = self;
        tianmaoOrderDatasTableView.delegate = self;
        
        self.itemContentViews = [NSArray arrayWithObjects:yunmiOrderDatasTableView,mijiaOrderDatasTableView,tianmaoOrderDatasTableView, nil];
        
        [self appearControllerOfIndex:0];
        
        return;
    }
    
    //普通用户 - 只有一个tableView
    YMOrderDatasTableView *tableView = [[YMOrderDatasTableView alloc] init];
    tableView.delegate = self;
    tableView.orderSource = YMOrderSourceYunmi;
    
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self fetchDataOfSource:tableView pageNum:0];
}


- (void)dealloc
{
    NSLog(@"%s",__func__);
    [self.orderDataAPI cancle];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)appearControllerOfIndex:(NSUInteger)index
{
    YMOrderDatasTableView *tableView = [self.itemContentViews objectAtIndex:index];
    
    tableView.pageNum == 0 ? [self fetchDataOfSource:tableView pageNum:0] : NULL;
}

#pragma mark Action
- (void)searchBtnAction
{
    YMOrderSearchViewController *searchVC = [[YMOrderSearchViewController alloc] init];
    searchVC.delegate = self;
    [self.navigationController pushViewController:searchVC animated:YES];
}

#pragma mark delegate YMOrderDatasTableViewDelegate
//tableView选中
- (void)orderTableView:(YMOrderDatasTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    YMOrderData *orderData = [tableView.orderDatas objectAtIndex:indexPath.section];
    
    YMOrderDetailsViewController *detailsVC = [[YMOrderDetailsViewController alloc] init];
    
    detailsVC.orderCode = orderData.orderCode.integerValue;
    detailsVC.orderSource = tableView.orderSource;
    
    [self.navigationController pushViewController:detailsVC animated:YES];
}

//tableView上拉刷新
- (void)orderTableView:(YMOrderDatasTableView *)tableView footerRefreshWithPageNum:(NSInteger)pageNum
{
    pageNum += 1;
    
    [self fetchDataOfSource:tableView pageNum:pageNum];
}

#pragma mark delegate YMOrderDatasSearchDelegate
//搜索页点击返回后，然后再推到结果页
- (void)YMOrderDatasSearchActionWithSource:(YMOrderSource)orderSource finished:(NSString *)beginDate endDate:(NSString *)endDate
{
    YMOrderSearchResultViewController *resultVC = [[YMOrderSearchResultViewController alloc] init];
    
    resultVC.orderSource = orderSource;
    
    resultVC.orderSourceName = [self.items objectAtIndex:orderSource];
    
    resultVC.beginDate = beginDate;
    resultVC.endDate = endDate;
    
    [self.navigationController pushViewController:resultVC animated:YES];
    
}

#pragma mark fetchData
- (void)fetchDataOfSource:(YMOrderDatasTableView *)tableView pageNum:(NSInteger)pageNum
{
    [YMCAlertView removeNetErrorInView:tableView];
    
    if(tableView.orderDatas.count == 0 || !tableView.orderDatas) {
        
        [MBProgressHUD showHUDAddedTo:tableView animated:YES];
    }
    
    @weakify(self);
    [self.orderDataAPI fetchOrderDataWithSource:tableView.orderSource BeginDate:@"" endDate:@"" pageNum:pageNum completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        @strongify(self);
        
        [MBProgressHUD hideHUDForView:tableView animated:YES];
        
        //获取失败
        if(status != YMCRequestSuccess) {
            
            [tableView endRefreshingWithNoMoreData:NO];
            
            //第一次没数据
            if(tableView.pageNum == 0)
            {
                //天猫尚未开通 特别的，不展示button
                if(tableView.orderSource == YMOrderSourceTianmao) {
                    message = [message containsString:@"无法识别"] ? @"该功能尚未开通,敬请期待" : message;  //避免显示“[2]无法显示”
                    [YMCAlertView showNetErrorInView:tableView type:YMCRequestNoDataAndNoBtn message:message actionBlock:^{ }];
                }else {
                    [YMCAlertView showNetErrorInView:tableView type:status message:message actionBlock:^{
                        [self fetchDataOfSource:tableView pageNum:pageNum];
                    }];
                }
            }else {
                [YMCAlertView showMessage:message];
            }
            return ;
        }
        
        //获取成功
        NSDictionary *result = [data notNullObjectForKey:@"result"];
        NSString *nowPage = [result notNullObjectForKey:@"nowPage"];
        NSString *totalPageNum = [result notNullObjectForKey:@"totalPageNum"];
        NSString *totalCount = [result notNullObjectForKey:@"totalCount"];
        
        //更新当前页
        tableView.pageNum = nowPage.integerValue;
        tableView.totalPageNum = totalPageNum.integerValue;
        tableView.orderAmountLabel.text = [NSString stringWithFormat:@"%@",totalCount];
        
        if(pageNum > totalPageNum.integerValue) {
            
            [tableView endRefreshingWithNoMoreData:YES];
            return;
        }
        
        //配置数据
        [tableView endRefreshingWithNoMoreData:NO];
        
        NSArray<YMOrderData *> *list = [result notNullObjectForKey:@"list"];
        
        [list enumerateObjectsUsingBlock:^(YMOrderData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            YMOrderData *orderData = [YMOrderData mj_objectWithKeyValues:obj];
            
            [tableView.orderDatas addObject:orderData];
        }];
        
        [tableView reloadData];
    }];
}
@end
