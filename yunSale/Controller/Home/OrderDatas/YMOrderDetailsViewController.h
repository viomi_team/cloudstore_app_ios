//
//  YMOrderDetailsViewController.h
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMOrderAPI.h"

@interface YMOrderDetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSDictionary *orderDetailDic;
//
@property (nonatomic, copy) NSDictionary *profitDic;
//
@property (nonatomic, copy) NSArray *skuInfoList;

@property (nonatomic, assign) NSInteger orderCode;

@property (nonatomic, assign) YMOrderSource orderSource;

@end
