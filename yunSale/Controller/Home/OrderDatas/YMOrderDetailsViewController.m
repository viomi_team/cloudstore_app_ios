//
//  YMOrderDetailsViewController.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMOrderDetailsViewController.h"
#import "YMTwoItemTableViewCell.h"
#import "YMProductInfoCell.h"
#import <UIImageView+WebCache.h>
#import "YMDate.h"
#import <MBProgressHUD.h>
#import <UMMobClick/MobClick.h>

@interface YMOrderDetailsViewController ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) YMOrderAPI *orderDetailsAPI;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@property (nonatomic, strong) NSNumberFormatter *numberFormatter;

@property (nonatomic, strong) NSArray *orderSourceName;

@property (nonatomic, assign) BOOL enableMultiLine;

@end

@implementation YMOrderDetailsViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick beginLogPageView:@"订单详情"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [MobClick endLogPageView:@"订单详情"];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _profitDic = [self.orderDetailDic notNullObjectForKey:@"profit"];
    self.navigationItem.title = @"订单详情";
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    self.numberFormatter = [[NSNumberFormatter alloc] init];
    [self.numberFormatter setPositiveFormat:@"0.0"];
    
    self.orderSourceName = @[@"云分销",@"米家",@"天猫"];
    
    [self drawView];
    [self fetchOrderDetails];
}

- (void)dealloc {
    NSLog(@"%s",__func__);
    if (self.orderDetailsAPI) {
        [self.orderDetailsAPI cancle];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Draw
- (void)drawView {
    self.tableView = [[UITableView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
}

#pragma mark - UITableViewDelegate & UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    if (!self.orderDetailDic) {
//        return 0;
//    }else{
//        return 6;
//    }
//    
    return self.orderDetailDic? 6 : 0;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    switch (section) {
//        case 0:
//            return 1;
//            break;
//            
//        case 1:
//            return 3;
//            break;
//            
//        case 2:
//            return self.skuInfoList.count;
//            break;
//            
//        case 3:
//            return 5;
//            break;
//            
//        case 4:
//            return 1;
//            break;
//            
//        case 5:
//            return 0;
//        default:
//            return 0;
//            break;
//    }
    return section == 0? 1 :
    section == 1? 3 :
    section == 2? self.skuInfoList.count :
    section == 3? 6 :
    section == 4? 1 : 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        return [YMCScreenAdapter intergerSizeBy750:208];
    } else if (indexPath.section == 1 && indexPath.row == 1 && self.enableMultiLine) {
        //31为label双行显示时的高度，34为label到cell的contentView 的 top 距离
        return 31 + 2*[YMCScreenAdapter sizeBy750:34];
    } else {
        return [YMCScreenAdapter sizeBy750:100];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    //    if (section == 4) {
    //        return [YMScreenAdapter sizeBy750:24];
    //    } else if (section == 5) {
    //        return [YMScreenAdapter sizeBy750:42];
    //    } else {
    //        return 0;
    //    }
    return section == 4? [YMCScreenAdapter sizeBy750:24] : section == 5? [YMCScreenAdapter sizeBy750:42] : 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    //    if (section >= 0 && section <= 2) {
    //        return [YMScreenAdapter sizeBy750:24];
    //    } else {
    //        return 0;
    //    }
    return (section >= 0 && section <= 2)? [YMCScreenAdapter sizeBy750:24] : 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    //    if (section == 5) {
    //        return [UIView drawLineViewWithWidth:YMSCREEN_WIDTH height:[YMScreenAdapter sizeBy750:42] color:[UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND]];
    //    } else {
    //        return [UIView drawLineViewWithWidth:YMSCREEN_WIDTH height:[YMScreenAdapter sizeBy750:24] color:[UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND]];
    //    }
    return section == 5? [UIView drawLineViewWithWidth:YMSCREEN_WIDTH height:[YMCScreenAdapter sizeBy750:42] color:[UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND]] : [UIView drawLineViewWithWidth:YMSCREEN_WIDTH height:[YMCScreenAdapter sizeBy750:24] color:[UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND]];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView drawLineViewWithWidth:YMSCREEN_WIDTH height:[YMCScreenAdapter sizeBy750:24] color:[UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        static NSString *reuseIdentifier = @"infoCell";
        YMProductInfoCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        
        if (!cell) {
            cell = [[YMProductInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSDictionary *productDic = _skuInfoList[indexPath.row];
        //配置商品图标
        
        NSString *urlString = [productDic notNullObjectForKey:@"imgUrl"];
        
//        SDWebImageManager *manager = [SDWebImageM0anager sharedManager];
//        [manager downloadImageWithURL:[NSURL URLWithString:urlString] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//            cell.productImageView.image = image;
//        }];
        
        [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"ym_home_placeholder"]];
        
        cell.productNameLabel.text = [productDic notNullObjectForKey:@"name"];
        
        NSNumber *originalPriceNumber = [productDic notNullObjectForKey:@"originalPrice"];
        NSString *originalPriceString = [self.numberFormatter stringFromNumber:originalPriceNumber];
        cell.productPriceLabel.text = [NSString stringWithFormat:@"￥%@", originalPriceString];
        
        cell.productQuantityLabel.text = [NSString stringWithFormat:@"x %@", [productDic notNullObjectForKey:@"quantity"]];
        
        ((self.skuInfoList.count > 1) && (indexPath.row < self.skuInfoList.count - 1))? [cell isHidenBottomLine:NO] : [cell isHidenBottomLine:YES];
        
        return cell;
    } else {
        static NSString *reuseIdentifier = @"twoItemCell";
        YMTwoItemTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (!cell) {
            cell = [[YMTwoItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier Style:YMTwoItemCellStyleRight];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = indexPath.row %2 == 0?
        [UIColor whiteColor] : [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        
        //配置cell
        switch (indexPath.section) {
            case 0: {
                cell.titleLabel.text = @"订单编号：";
                cell.detailsLabel.text = [self.orderDetailDic notNullObjectForKey:@"orderCode"];
                cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
            }
                break;
                
            case 1: {
                switch (indexPath.row) {
                    case 0: {
                        cell.titleLabel.text = @"收货姓名：";
                        cell.detailsLabel.text = [self.orderDetailDic notNullObjectForKey:@"linkmanName"];
                        cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                        
                    }
                        break;
                        
                    case 1: {
                        cell.titleLabel.text = @"收货地址：";
                        cell.detailsLabel.text = [NSString stringWithFormat:@"%@%@", [self.orderDetailDic notNullObjectForKey:@"fullDivisionName"], [self.orderDetailDic notNullObjectForKey:@"address"]];
                        if (self.enableMultiLine) {
                            cell.detailsLabel.textAlignment = NSTextAlignmentLeft;
                        }
                        cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                        
                        
                    }
                        break;
                        
                    case 2: {
                        cell.titleLabel.text = @"手机号码：";
                        cell.detailsLabel.text = [self.orderDetailDic notNullObjectForKey:@"linkmanPhone"];
                        cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                    }
                        break;
                }
            }
                break;
                
            case 3:
            {
                switch (indexPath.row) {
                    case 0: {
                        cell.titleLabel.text = @"支付方式：";
                        cell.detailsLabel.text = [self.orderDetailDic notNullObjectForKey:@"payTypeDesc"];
                        cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                    }
                        break;
                        
                    case 1: {
                        cell.titleLabel.text = @"支付状态：";
                        cell.detailsLabel.text = [self.orderDetailDic notNullObjectForKey:@"dealPhaseDesc"];
                        cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                    }
                        break;
                        
                    case 2: {
                        cell.titleLabel.text = @"优惠金额：";
                        NSString *discount = [self.numberFormatter stringFromNumber:[self.orderDetailDic notNullObjectForKey:@"discount"]];
                        cell.detailsLabel.text = [NSString stringWithFormat:@"%@", discount];
                        cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                    }
                        break;
                        
                    case 3: {
                        cell.titleLabel.text = @"运费：";
                        cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
                        NSNumber *deliveryFeeNumber = [self.orderDetailDic notNullObjectForKey:@"deliveryFee"] ? [self.orderDetailDic notNullObjectForKey:@"deliveryFee"] : @0;
                        NSString *deliveryFeeString = [self.numberFormatter stringFromNumber:deliveryFeeNumber];
                        cell.detailsLabel.text = [NSString stringWithFormat:@"￥%@", deliveryFeeString];
                    }
                        break;
                        
                    case 4: {
                        cell.titleLabel.text = @"订单金额：";
                        NSString *paymentPrice = [self.numberFormatter stringFromNumber:[self.orderDetailDic notNullObjectForKey:@"paymentPrice"]];
                        cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
                        cell.detailsLabel.text = [NSString stringWithFormat:@"￥%@",paymentPrice];
                    }
                        break;
                        
                    case 5: {
                        cell.titleLabel.text = @"下单时间：";
                        
                        //注意，不能使用createdTime.intValue 或者integerValue ，有些设备会导致计算结果为负数：时间戳13位数
                        NSNumber *createdTime = [self.orderDetailDic notNullObjectForKey:@"createdTime"];
                        NSTimeInterval time = createdTime.longLongValue/1000;
                        NSDate *date=[NSDate dateWithTimeIntervalSince1970:time];
                        NSString *createdTimeString = [self.dateFormatter stringFromDate:date];
                        cell.detailsLabel.text = createdTimeString;
                        cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                    }
                        break;
                }
            }
                break;
                
                //结算状态
            case 4: {
                cell.titleLabel.text = @"结算状态：";
                NSString *statusString = @"";
                
                //根据不同角色来取字典里不同的值
                if ([YMUserManager shareinstance].roleId.integerValue != 21) {
                    statusString = [self.orderDetailDic notNullObjectForKey:@"settleStatesDesc"];
                } else {
                    NSDictionary *dict = [self.orderDetailDic notNullObjectForKey:@"profit"];
                    statusString = [dict notNullObjectForKey:@"statusDesc"];
                }
                
                if (![NSString isEmptyString:statusString]) {
                    cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GREEN];
                    cell.detailsLabel.text = statusString;
                } else {
                    cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
                    //cell 的重用机制可能会导致 Label 显示其它错误的信息，加上这句，当服务器返回数据为空时，强制显示"-"
                    cell.detailsLabel.text = @"-";
                }
            }
                break;
        }
        
        //label 填充内容后再将其设置为可左右滚动，不能缺少这个方法的执行
        //        [cell setDetailLabelScrollable];
        return cell;
    }
}

#pragma mark - Fetch
- (void)fetchOrderDetails {
    if (!self.orderDetailsAPI) {
        self.orderDetailsAPI = [[YMOrderAPI alloc] init];
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.orderDetailsAPI fetchOrderDetailsWithSource:self.orderSource orderCode:self.orderCode completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (status == YMCRequestSuccess) {
            //第三方订单详情数据结构不一样
            if(self.orderSource == YMOrderSourceYunmi) {
                self.orderDetailDic = [data notNullObjectForKey:@"result"];
            }else {
                NSDictionary *result = [data notNullObjectForKey:@"result"];
                NSArray *list = [result notNullObjectForKey:@"list"];
                self.orderDetailDic = (list && list.count > 0) ? list.firstObject : [NSDictionary dictionary];
            }
            
            self.skuInfoList = [self.orderDetailDic notNullObjectForKey:@"skuInfoList"];
            
            NSString *addrString = [NSString stringWithFormat:@"%@%@", [self.orderDetailDic notNullObjectForKey:@"fullDivisionName"], [self.orderDetailDic notNullObjectForKey:@"address"]];
            
            //检测label的长度是否超出530的宽度，超出的话就换行
            self.enableMultiLine = NO;
            CGFloat addrStringWidth = [addrString widthWithFontsize:[YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]];
            if (addrStringWidth > [YMCScreenAdapter sizeBy750:530]) {
                self.enableMultiLine = YES;
            }
            
            [self.tableView reloadData];
        } else {
            @weakify(self);
            [YMCAlertView showNetErrorInView:self.view type:status message:message actionBlock:^{
                @strongify(self);
                
                [self fetchOrderDetails];
            }];
        }
    }];
}

@end
