//
//  YMOrderHeaderFooterView.h
//  yunSale
//
//  Created by ChengMinZhang on 2017/8/30.
//  Copyright © 2017年 yunmi. All rights reserved.
//
#ifndef YMORDERTALBE_DEFINE
#define YMORDERTALBE_DEFINE

#define YMORDERTALBE_HEADER_HEIGHT  70   //header的高

#define YMORDERTALBE_FOOTER_HEIGHT  94   //footer的高

#endif

#import <UIKit/UIKit.h>

@interface YMOrderHeaderFooterView : UITableViewHeaderFooterView

@property (nonatomic, strong) UILabel *forwordTitle;

@property (nonatomic, strong) UILabel *subTitle;

@property (nonatomic, strong) UIView *bottomLine;

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@end
