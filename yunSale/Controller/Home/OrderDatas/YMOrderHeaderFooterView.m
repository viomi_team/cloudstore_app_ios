//
//  YMOrderHeaderFooterView.m
//  yunSale
//
//  Created by ChengMinZhang on 2017/8/30.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <YMCommon/YMCCommonUtils.h>
#import "YMOrderHeaderFooterView.h"

@interface YMOrderHeaderFooterView ()

@property (nonatomic, strong) UIView *container;

@end

@implementation YMOrderHeaderFooterView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if(self) {
        self.container = [[UIView alloc] init];
        self.container.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.container];
        [self.container mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self);
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:YMORDERTALBE_HEADER_HEIGHT]);
        }];
        
        _forwordTitle = [[UILabel alloc] init];
        _forwordTitle.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [self.container addSubview:_forwordTitle];
        [_forwordTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.container);
            make.left.equalTo(self.container).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        }];
        
        _subTitle = [[UILabel alloc] init];
        _subTitle.textAlignment = NSTextAlignmentRight;
        [self.container addSubview:_subTitle];
        [_subTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.container);
            make.right.equalTo(self.container).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        }];
        
        _bottomLine = [[UIView alloc] init];
        _bottomLine.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [self.container addSubview:_bottomLine];
        [_bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.container);
            make.left.equalTo(self.container);
            make.right.equalTo(self.container);
            make.height.mas_equalTo(0.5);
        }];
    }
    return self;
}




@end
