//
//  YMOrderSearchResultViewController.h
//  yunSale
//
//  Created by ChengMinZhang on 2017/8/31.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMOrderDatasTableView.h"

@interface YMOrderSearchResultViewController : UIViewController

@property (nonatomic, assign) YMOrderSource orderSource;

@property (nonatomic, strong) NSString *orderSourceName;

@property (nonatomic, strong) NSString *beginDate,*endDate;

@end
