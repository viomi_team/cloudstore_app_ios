//
//  YMOrderSearchResultViewController.m
//  yunSale
//
//  Created by ChengMinZhang on 2017/8/31.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <MBProgressHUD.h>
#import <MJRefresh.h>
#import <MJExtension.h>
#import <Masonry.h>
#import "YMOrderDetailsViewController.h"
#import "YMOrderSearchResultViewController.h"

@interface YMOrderSearchResultViewController ()<YMOrderDatasTableViewDelegate>

@property (nonatomic, strong) YMOrderAPI *orderApi;

@property (nonatomic, strong) YMOrderDatasTableView *tableView;

@end

@implementation YMOrderSearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@查询结果",self.orderSourceName];
    
    self.orderApi = [[YMOrderAPI alloc] init];
    
    self.tableView = [[YMOrderDatasTableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.orderSource = self.orderSource;
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self fetchResultWithSource:self.tableView pageNum:0 begin:self.beginDate end:self.endDate];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.orderApi cancle];
}

#pragma mark - YMOrderDatasTableViewDelegate
- (void)orderTableView:(YMOrderDatasTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    YMOrderData *orderData = [tableView.orderDatas objectAtIndex:indexPath.section];
    
    YMOrderDetailsViewController *detailsVC = [[YMOrderDetailsViewController alloc] init];
    
    detailsVC.orderCode = orderData.orderCode.integerValue;
    detailsVC.orderSource = tableView.orderSource;
    
    [self.navigationController pushViewController:detailsVC animated:YES];
}

//上拉刷新回掉
- (void)orderTableView:(YMOrderDatasTableView *)tableView footerRefreshWithPageNum:(NSInteger )pageNum {
    
    pageNum += 1;
    
    [self fetchResultWithSource:tableView pageNum:pageNum begin:self.beginDate end:self.endDate];
    
}

#pragma mark - fetch
- (void)fetchResultWithSource:(YMOrderDatasTableView *)tableView pageNum:(NSInteger)pageNum begin:(NSString *)begin end:(NSString *)end {
    [YMCAlertView removeNetErrorInView:self.tableView];
    
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    
    @weakify(self);
    [self.orderApi fetchOrderDataWithSource:tableView.orderSource BeginDate:begin endDate:end pageNum:pageNum completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        @strongify(self);
        
        [MBProgressHUD hideHUDForView:self.tableView animated:YES];
        
        //获取失败
        if(status != YMCRequestSuccess) {
            
            [self.tableView endRefreshingWithNoMoreData:NO];
            
            //天猫尚未开通 特别的，不展示button
            if(tableView.orderSource == YMOrderSourceTianmao) {
                message = [message containsString:@"无法识别"] ? @"该功能尚未开通,敬请期待" : message;  //避免显示“[2]无法显示”
                [YMCAlertView showNetErrorInView:tableView type:YMCRequestNoDataAndNoBtn message:message actionBlock:^{ }];
            }else {
                [YMCAlertView showNetErrorInView:tableView type:status message:message actionBlock:^{
                    [self fetchResultWithSource:tableView pageNum:pageNum begin:begin end:end];
                }];
            }
            
            return ;
        }
        
        //获取成功
        NSDictionary *result = [data notNullObjectForKey:@"result"];
        NSString *nowPage = [result notNullObjectForKey:@"nowPage"];
        NSString *totalPageNum = [result notNullObjectForKey:@"totalPageNum"];
        NSString *totalCount = [result notNullObjectForKey:@"totalCount"];
        
        //更新当前页
        self.tableView.pageNum = nowPage.integerValue;
        self.tableView.totalPageNum = totalPageNum.integerValue;
        self.tableView.orderAmountLabel.text = [NSString stringWithFormat:@"￥%@",totalCount];
        
        if(totalCount.intValue == 0) {
            [YMCAlertView showNetErrorInView:self.tableView type:YMCRequestNoDataAndNoBtn message:@"抱歉，没有匹配的数据哦" actionBlock:^{ }];
            return;
        }
        
        if(pageNum > totalPageNum.integerValue) {
            [self.tableView endRefreshingWithNoMoreData:YES];
            return;
        }
        
        [self.tableView endRefreshingWithNoMoreData:NO];
        
        NSArray<YMOrderData *> *list = [result notNullObjectForKey:@"list"];
        
        [list enumerateObjectsUsingBlock:^(YMOrderData * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            YMOrderData *orderData = [YMOrderData mj_objectWithKeyValues:obj];
            
            [self.tableView.orderDatas addObject:orderData];
        }];
        
        [self.tableView reloadData];
    }];

}

@end
