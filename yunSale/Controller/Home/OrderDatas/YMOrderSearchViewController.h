//
//  YMOrderSearchViewController.h
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMOrderAPI.h"

@protocol YMOrderDatasSearchDelegate <NSObject>

@required

- (void)YMOrderDatasSearchActionWithSource:(YMOrderSource)orderSource finished:(NSString *)beginDate endDate:(NSString *)endDate;

@end

@interface YMOrderSearchViewController : UIViewController

@property (nonatomic, weak) id <YMOrderDatasSearchDelegate> delegate;

@end
