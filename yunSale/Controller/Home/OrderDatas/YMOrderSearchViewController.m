//
//  YMOrderSearchViewController.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMOrderSearchViewController.h"
#import "YMButton.h"
#import <ActionSheetPicker.h>
#import "YMDate.h"
#import "YMDateSelectView.h"
#import "YMSearchSelectorView.h"
#import <ActionSheetStringPicker.h>

@interface YMOrderSearchViewController ()

@property (nonatomic, strong) YMButton *searchBtn;

@property (nonatomic, copy) NSString *beginDate;

@property (nonatomic, copy) NSString *endDate;

@property (nonatomic, strong) YMDateSelectView *beginDateSelectorView;

@property (nonatomic, strong) YMDateSelectView *endDateSelectorView;

@property (nonatomic, strong) YMSearchSelectorView *orderSourceSelectorView;

@property (nonatomic, strong) NSArray *orderSourceNames;

@property (nonatomic, assign) BOOL isRoldId21;

@end

@implementation YMOrderSearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"订单搜索";
    
    self.isRoldId21 = [[YMUserManager shareinstance].roleId intValue] == 21;
    
    self.orderSourceNames = @[@"云分销订单",@"米家订单",@"天猫订单"];
    
    [self drawView];
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    if(self.endDateSelectorView.dateValue != nil || self.beginDateSelectorView.dateValue != nil){
        self.endDateSelectorView.dateValue   = @"选择结束时间";
        self.beginDateSelectorView.dateValue = @"选择开始时间";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark draw
- (void)drawView
{
    self.view.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    
    //380的高度由设计图计算得出
    UIView *containerView = [[UIView alloc] init];
    containerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:containerView];
    [containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        //        make.height.mas_equalTo([YMScreenAdapter sizeBy750:380]);
    }];
    
    self.beginDateSelectorView = [[YMDateSelectView alloc] initWithTip:@"开始时间" placeHoder:@"选择开始时间"];
    [containerView addSubview:self.beginDateSelectorView];
    [self.beginDateSelectorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(containerView);
        make.left.equalTo(containerView);
        make.right.equalTo(containerView);
        make.height.mas_equalTo([YMCScreenAdapter fontsizeBy750:84]);
    }];
    
    self.endDateSelectorView = [[YMDateSelectView alloc] initWithTip:@"结束时间" placeHoder:@"选择结束时间"];
    [containerView addSubview:self.endDateSelectorView];
    [self.endDateSelectorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.beginDateSelectorView.mas_bottom).with.offset([YMCScreenAdapter fontsizeBy750:16]);
        make.left.equalTo(containerView);
        make.right.equalTo(containerView);
        make.height.mas_equalTo([YMCScreenAdapter fontsizeBy750:84]);
    }];
    
    self.orderSourceSelectorView = [[YMSearchSelectorView alloc] initWithTitle:@"订单来源" placeHolder:@"选择订单来源"];
    [self.orderSourceSelectorView.selectorBtn addTarget:self action:@selector(selectOrderSourceAction) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:self.orderSourceSelectorView];
    [self.orderSourceSelectorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.endDateSelectorView.mas_bottom).with.offset([YMCScreenAdapter fontsizeBy750:16]);
        make.left.equalTo(containerView);
        make.right.equalTo(containerView);
        make.height.mas_equalTo([YMCScreenAdapter fontsizeBy750:84]);

    }];
    
    self.searchBtn = [[YMButton alloc] initWithStyle:YMButtonStyleRectangle];
    [self.searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [self.searchBtn setSelected:YES];
    [self.searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:self.searchBtn];
    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.orderSourceSelectorView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
        make.left.equalTo(containerView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.right.equalTo(containerView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.bottom.equalTo(containerView).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
    }];
    
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [containerView addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(containerView);
        make.left.equalTo(containerView);
        make.right.equalTo(containerView);
        make.height.mas_equalTo(0.5);
    }];
    
    //城市运营隐藏来源选择
    if(self.isRoldId21) {
        self.orderSourceSelectorView.hidden = YES;
        [self.searchBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.endDateSelectorView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
        }];
    }
}

//弹出“选择订单来源”按钮
- (void)selectOrderSourceAction {
    [self.view endEditing:YES];
    
    NSString *selectString = self.orderSourceSelectorView.placeHolderString;
    
    NSInteger selectSection = [self.orderSourceNames containsObject:selectString] ? [self.orderSourceNames indexOfObject:selectString] : 0;
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:@"请选择订单来源" rows:self.orderSourceNames initialSelection:selectSection doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        self.orderSourceSelectorView.placeHolderString = [self.orderSourceNames objectAtIndex:selectedIndex];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:nil action:nil];
    [picker setCancelButton:cancelButton];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:nil action:nil];
    [picker setDoneButton:doneButton];
    
    [picker showActionSheetPicker];
}

- (void)searchAction {
    
    NSString *beginDate = self.beginDateSelectorView.dateValue;
    NSString *endDate = self.endDateSelectorView.dateValue;
    NSString *sourceString = self.orderSourceSelectorView.placeHolderLabel.text;
    
    if([sourceString isEqualToString:@"选择订单来源"] && !self.isRoldId21) {
        [YMCAlertView showMessage:@"请选择订单来源！"];
        return;
    }
    
    NSNumber *sourceNumber = [NSNumber numberWithUnsignedInteger:[self.orderSourceNames indexOfObject:sourceString]];
    YMOrderSource orderSource = self.isRoldId21 ? YMOrderSourceYunmi : sourceNumber.intValue;
    
    if ([NSString isEmptyString:beginDate] && [NSString isEmptyString:endDate]) {
        beginDate = @"";
        endDate = @"";
        [self.navigationController popViewControllerAnimated:NO];
        [_delegate YMOrderDatasSearchActionWithSource:orderSource finished:beginDate endDate:endDate];
    } else {
        
        if (beginDate) {
            if ([NSString isEmptyString:endDate]) {
                [YMCAlertView showMessage:@"请选择结束时间！"];
                return;
            }
        }
        
        if (endDate) {
            if ([NSString isEmptyString:beginDate]) {
                [YMCAlertView showMessage:@"请选择开始时间! "];
                return;
            }
        }
        
        if ([beginDate compare:endDate options:NSNumericSearch] == NSOrderedDescending) {
            [YMCAlertView showMessage:@"结束时间应该大于开始时间"];
            return;
        }
        
        self.beginDate = beginDate;
        self.endDate = endDate;
        
        [self.navigationController popViewControllerAnimated:NO];
        
        [_delegate YMOrderDatasSearchActionWithSource:orderSource finished:[NSString stringWithFormat:@"%@+00:00", beginDate] endDate:[NSString stringWithFormat:@"%@+23:59", endDate]];
    }
}

@end
