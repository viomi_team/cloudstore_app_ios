//
//  YMOrderTableViewCell.h
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMOrderTableViewCell : UITableViewCell

//订单编号
@property (nonatomic, strong) UILabel *orderCodeLabel;
//用户信息
@property (nonatomic, strong) UILabel *userInfoLabel;
//订单日期
@property (nonatomic, strong) UILabel *dateLabel;
//支付金额
@property (nonatomic, strong) UILabel *paymentPriceLabel;
//订单索引
@property (nonatomic, strong) UILabel *orderIndexLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end
