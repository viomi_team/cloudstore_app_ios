//
//  YMOrderTableViewCell.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMOrderTableViewCell.h"

@implementation YMOrderTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _orderCodeLabel = [[UILabel alloc] init];
        _orderCodeLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:26]);
        _orderCodeLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [self.contentView addSubview:_orderCodeLabel];
        
        _userInfoLabel = [[UILabel alloc] init];
        _userInfoLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        _userInfoLabel.textColor = [UIColor colorWithHexString:YMCOLOR_DARK_GRAY];
        [self.contentView addSubview:_userInfoLabel];
        
        _dateLabel = [[UILabel alloc] init];
        _dateLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        _dateLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        [self.contentView addSubview:_dateLabel];
        
        _paymentPriceLabel = [[UILabel alloc] init];
        _paymentPriceLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:32]);
        _paymentPriceLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
        [self.contentView addSubview:_paymentPriceLabel];
        
        UIView *bottomLine = [[UIView alloc] init];
        bottomLine.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [self.contentView addSubview:bottomLine];
        
        _orderIndexLabel = [[UILabel alloc] init];
        _orderIndexLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        _orderIndexLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        [self.contentView addSubview:_orderIndexLabel];
        
        UIImageView *moreImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ym_index_more"]];
        [self.contentView addSubview:moreImageView];
        
        //用户信息Label在Y轴上居中，让其余两个Label向用户信息Label对齐
        [_userInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.left.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        }];
        
        //订单编号
        [_orderCodeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_userInfoLabel.mas_top).with.offset(-[YMCScreenAdapter sizeBy750:18]);
            make.left.equalTo(_userInfoLabel.mas_left);
        }];
        
        //订单索引
        [_orderIndexLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.centerY.equalTo(_orderCodeLabel.mas_centerY);
        }];
        
        //订单日期
        [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_userInfoLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:18]);
            make.left.equalTo(_userInfoLabel.mas_left);
        }];
        
        //more指示图标
        [moreImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_orderIndexLabel.mas_centerX);
            make.bottom.equalTo(_dateLabel.mas_bottom);
        }];
        
        //价格
        [_paymentPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(moreImageView.mas_left).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.bottom.equalTo(_dateLabel.mas_bottom);
        }];
        
        //单元格底部细线
        [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.height.mas_equalTo(0.5);
        }];
    }
    
    return self;
}

@end
