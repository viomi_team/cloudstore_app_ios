//
//  YMProductInfoCell.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/16.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMProductInfoCell : UITableViewCell

//商品图片
@property (nonatomic, strong) UIImageView *productImageView;
//商品名称
@property (nonatomic, strong) UILabel *productNameLabel;
//商品价格
@property (nonatomic, strong) UILabel *productPriceLabel;
//商品数量
@property (nonatomic, strong) UILabel *productQuantityLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
//绘制单元格底部的细线
- (void)isHidenBottomLine:(BOOL)isHidden;

@end
