//
//  YMProductInfoCell.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/16.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMProductInfoCell.h"

@interface YMProductInfoCell()

@property (nonatomic, strong) UIView *bottomLineView;

@end

@implementation YMProductInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _productImageView = [[UIImageView alloc] init];
        _productImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_productImageView];
        [_productImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.top.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.bottom.equalTo(self.contentView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:160]);
        }];
        
        _productNameLabel = [[UILabel alloc] init];
        _productNameLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:26]);
        _productNameLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [self.contentView addSubview:_productNameLabel];
        [_productNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_productImageView.mas_right).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.top.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:48]);
        }];
        
        _productPriceLabel = [[UILabel alloc] init];
        _productPriceLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        _productPriceLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
        [self.contentView addSubview:_productPriceLabel];
        [_productPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_productNameLabel.mas_left);
            make.bottom.equalTo(self.contentView).with.offset(-[YMCScreenAdapter sizeBy750:48]);
        }];
        
        _productQuantityLabel = [[UILabel alloc] init];
        _productQuantityLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        _productQuantityLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        [self.contentView addSubview:_productQuantityLabel];
        [_productQuantityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_productPriceLabel);
            make.right.equalTo(self.contentView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        }];
        
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [self.contentView addSubview:_bottomLineView];
        [_bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.height.mas_equalTo(0.5);
        }];
    }
    
    return self;
}

//是否隐藏商品详情页下方的底线
- (void)isHidenBottomLine:(BOOL)isHidden
{
    self.bottomLineView.hidden = isHidden;
}

@end
