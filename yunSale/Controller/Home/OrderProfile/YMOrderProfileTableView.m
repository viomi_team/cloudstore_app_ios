//
//  YMOrderProfileTableView.m
//  yunSale
//
//  Created by liushilou on 16/11/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMOrderProfileTableView.h"
#import "YMTwoItemTableViewCell.h"
#import "YMPieChartTableViewCell.h"
#import "YMTwoItemTableHeaderView.h"
#import "YMStore.h"
#import "YMChartTab.h"

@interface YMOrderProfileTableView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,copy) NSArray *profileDatas;

@property (nonatomic,assign) YMProfileType type;


@property (nonatomic,assign) CGFloat chartCellHeight;

@property (nonatomic,strong) YMTwoItemTableHeaderView *headerView;

@property (nonatomic,strong) UIView *footerView;

@property (nonatomic,assign) BOOL loadedChart;

@end

@implementation YMOrderProfileTableView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return self;
}

#pragma mark UITableViewDelegate UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (section == 0) {
//        return 1;
//    }else{
//        return self.orderDatas.count;
//    }
    return section != 0? self.orderDatas.count : 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
//
//        return self.chartCellHeight;
//    }else{
//        return [YMScreenAdapter sizeBy750:100];
//    }
    return indexPath.section == 0? self.chartCellHeight : [YMCScreenAdapter sizeBy750:100];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    if (section == 0) {
//        return [YMScreenAdapter sizeBy640:24];
//    }else{
//        return 0;
//    }
    return section == 0? [YMCScreenAdapter sizeBy750:24] : 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return 0;
//    }else{
//        return [YMScreenAdapter sizeBy640:80];
//    }
    return section != 0? [YMCScreenAdapter sizeBy750:80] : 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        if (!self.headerView) {
            self.headerView = [[YMTwoItemTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [YMCScreenAdapter sizeBy750:80]) leftText:@"城市运营" rightText:@"订单数"];
        }
        return self.headerView;
    }else{
        return nil;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 1) {
        return nil;
    }else{
        if (!self.footerView) {
            self.footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [YMCScreenAdapter sizeBy640:26])];
            self.footerView.backgroundColor = [UIColor colorWithHexString:@"#f5f5f5"];
            
            UIView *toplineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0.5)];
            toplineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
            [self.footerView addSubview:toplineView];
            
            UIView *bottomlineView = [[UIView alloc] init];
            bottomlineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
            [self.footerView addSubview:bottomlineView];
            [bottomlineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.footerView).with.offset(0);
                make.right.equalTo(self.footerView).with.offset(0);
                make.bottom.equalTo(self.footerView).with.offset(0);
                make.height.mas_equalTo(0.5);
            }];
        }
        return self.footerView;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        static NSString *reuseIdentifier = @"chartCell";
        YMPieChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (!cell) {
            cell = [[YMPieChartTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        if (self.profileDatas) {
            if (!self.loadedChart) {
                self.loadedChart = YES;
                [cell configCell:self.profileDatas];
            }
        }
        
        return cell;
    } else {
        static NSString *reuseIdentifier = @"orderCell";
        YMTwoItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (!cell) {
            cell = [[YMTwoItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier Style:YMTwoItemCellStyleCenter];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        YMStore *store = [self.orderDatas objectAtIndex:indexPath.row];
        //        NSDictionary *channelInfo = [item notNullObjectForKey:@"channelInfo"];
        NSString *name = store.name;
        //
        //        NSDictionary *salesReportBean = [item notNullObjectForKey:@"salesReportBean"];
        NSNumber *orderCount = @(store.orderCount);
        
        cell.titleLabel.text = name;
        cell.detailsLabel.text = orderCount.stringValue;
        
//        if (indexPath.row%2 == 0) {
//            cell.contentView.backgroundColor = [UIColor whiteColor];
//        }else{
//            cell.contentView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
//        }
        
        cell.contentView.backgroundColor = indexPath.row%2 == 0? [UIColor whiteColor] : [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        
        return cell;
    }
}

#pragma mark fetch
- (void)fetchData:(YMProfileType)type {
    self.type = type;

    self.fetched = YES;
    
    if (!self.orderAPI) {
        self.orderAPI = [[YMOrderAPI alloc] init];
    }
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    [YMCAlertView removeNetErrorInView:self];
    
    //判断角色，若角色为区域管理员，则提示“功能尚未开通，敬请期待”
//    if ([[YMUserManager shareinstance].roleId integerValue] == 1001) {
//        [YMAlertView showNetErrorInView:self type:YMRequestNoDataAndNoBtn message:@"功能尚未开通，敬请期待" actionBlock:nil];
//        return;
//    }
    
    [self.orderAPI fetchOrderProfile:type completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        
        [MBProgressHUD hideHUDForView:self animated:YES];
        if (status == YMCRequestSuccess) {
            
            NSArray *datas = [data notNullObjectForKey:@"datas"];
            
            if (!datas || datas.count == 0)
            {
                [YMCAlertView showNetErrorInView:self type:YMCRequestNoDataAndNoBtn message:@"没有更多数据" actionBlock:nil];
                return;
            }
            
            NSMutableArray *array = [NSMutableArray new];
            
            NSInteger totalCount = 0;
            for (NSDictionary *item in datas) {
                
                NSDictionary *salesReportBean = [item notNullObjectForKey:@"salesReportBean"];
                NSNumber *orderCount = [salesReportBean notNullObjectForKey:@"orderCount"];
                NSDictionary *channelInfo = [item notNullObjectForKey:@"channelInfo"];
                NSString *name = [channelInfo notNullObjectForKey:@"name"];
                
                YMStore *store = [[YMStore alloc] init];
                store.name = name;
                store.orderCount = orderCount.integerValue;
                [array addObject:store];
                
                totalCount += orderCount.integerValue;
            }
            
            // 这里的key写的是@property的名称
            NSSortDescriptor *orderCountDesc = [NSSortDescriptor sortDescriptorWithKey:@"orderCount" ascending:NO];
            // 按顺序添加排序描述器
            NSArray *descs = [NSArray arrayWithObjects:orderCountDesc,nil];
            
            NSArray *storeArray = [array sortedArrayUsingDescriptors:descs];
            
            self.orderDatas = storeArray;
            
            NSInteger count = 0;
            NSMutableArray *profileArray = [NSMutableArray new];
            for (YMStore *store in self.orderDatas) {
                if (store.orderCount > 0) {
                    count += store.orderCount;
                    
                    YMChart *chart = [[YMChart alloc] init];
                    chart.name = store.name;
                    chart.value = store.orderCount;
                    [profileArray addObject:chart];
                    
                    NSInteger index = [self.orderDatas indexOfObject:store];
                    //最多取4个数
                    if (index >= 3) {
                        break;
                    }
                }else{
                    break;
                }
            }
            
            NSInteger othercount = totalCount - count;
            if (othercount > 0) {
                YMChart *chart = [[YMChart alloc] init];
                chart.name = @"其他";
                chart.value = othercount;
                [profileArray addObject:chart];
            }
            
            self.profileDatas = [profileArray copy];
            
            NSMutableAttributedString *tabsString = [YMChartTab stringOfTabs:self.profileDatas withStyle:YMChartTabStylePercentage];
            CGFloat tabHeight = [YMChartTab heightForAttributedString:tabsString width:[UIScreen mainScreen].bounds.size.width - [YMCScreenAdapter sizeBy750:YMSPACE_AROUND]*2];
            self.chartCellHeight = tabHeight + YM_PIECHART_HEIGHT + [YMCScreenAdapter sizeBy750:YMSPACE_AROUND];
            
            
            [self.tableView reloadData];
            
        }else{
            @weakify(self);
            [YMCAlertView showNetErrorInView:self type:status message:message actionBlock:^{
                @strongify(self);

                [self fetchData:self.type];
            }];
        }
    }];
}

@end
