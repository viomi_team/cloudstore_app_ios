//
//  YMOrderProfileViewController.m
//  yunSale
//
//  Created by liushilou on 16/11/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMOrderProfileViewController.h"
#import "YMOrderProfileTableView.h"
#import <UMMobClick/MobClick.h>

@interface YMOrderProfileViewController ()

@end

@implementation YMOrderProfileViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick event:@"Home_Order_Profile"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"订单概况";
    
    self.items = [NSArray arrayWithObjects:@"昨日数据",@"本月数据",@"全部数据", nil];
    
    YMOrderProfileTableView *dayTableView = [[YMOrderProfileTableView alloc] init];
    YMOrderProfileTableView *monthTableView = [[YMOrderProfileTableView alloc] init];
    YMOrderProfileTableView *allTableView = [[YMOrderProfileTableView alloc] init];
    self.itemContentViews = [NSArray arrayWithObjects:dayTableView,monthTableView,allTableView, nil];
    
    [self appearControllerOfIndex:0];
    
}

- (void)dealloc
{
    NSLog(@"%s",__func__);
    for (YMOrderProfileTableView *tableview in self.itemContentViews) {
        [tableview.orderAPI cancle];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma 重写父类
- (void)appearControllerOfIndex:(NSUInteger)index
{
    
    YMOrderProfileTableView *controller = [self.itemContentViews objectAtIndex:index];
    if (!controller.fetched) {
        if (index == 0) {
            [controller fetchData:YMProfileTypeDay];
        }else if (index == 1){
            [controller fetchData:YMProfileTypeMonth];
        }else{
            [controller fetchData:YMProfileTypeAll];
        }
    }
}

@end
