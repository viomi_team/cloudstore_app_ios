//
//  YMPieChartTableViewCell.h
//  yunSale
//
//  Created by liushilou on 16/11/10.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMPieChartView.h"

@interface YMPieChartTableViewCell : UITableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

- (void)configCell:(NSArray<NSDictionary *> *)datas;

@end
