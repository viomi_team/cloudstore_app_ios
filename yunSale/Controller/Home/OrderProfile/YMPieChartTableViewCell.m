//
//  YMPieChartTableViewCell.m
//  yunSale
//
//  Created by liushilou on 16/11/10.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMPieChartTableViewCell.h"


@interface YMPieChartTableViewCell()

@property (nonatomic,strong) YMPieChartView *chartView;

@end



@implementation YMPieChartTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _chartView = [[YMPieChartView alloc] init];
        [self.contentView addSubview:_chartView];
        [_chartView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset(0);
            make.right.equalTo(self.contentView).with.offset(0);
            make.top.equalTo(self.contentView).with.offset(0);
            make.bottom.equalTo(self.contentView).with.offset(0);
        }];
    }
    return self;
}

- (void)configCell:(NSArray *)datas {

    [self.chartView setDatas:datas];
}

@end
