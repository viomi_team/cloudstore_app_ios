//
//  YMSalesDataRootCell.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/24.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMSalesDataRootCell : UITableViewCell

//订单数量
@property (nonatomic, strong) UILabel *orderCountLabel;
//销售金额
@property (nonatomic, strong) UILabel *salesAmountLabel;

@end
