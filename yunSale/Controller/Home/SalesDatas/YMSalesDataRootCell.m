//
//  YMSalesDataRootCell.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/24.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMSalesDataRootCell.h"

@implementation YMSalesDataRootCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.orderCountLabel = [[UILabel alloc] init];
        self.orderCountLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:48]);
        self.orderCountLabel.text = @"－";
        self.orderCountLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
        [self.contentView addSubview:self.orderCountLabel];
        [self.orderCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.top.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        }];
        
        self.salesAmountLabel = [[UILabel alloc] init];
        self.salesAmountLabel.text = @"－";
        self.salesAmountLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
        [self.contentView addSubview:self.salesAmountLabel];
        [self.salesAmountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_centerX).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.top.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.right.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        }];
        
        UILabel *orderLabel = [[UILabel alloc] init];
        orderLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE]);
        orderLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        orderLabel.text = @"订单数量";
        [self.contentView addSubview:orderLabel];
        [orderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.bottom.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
        }];
        
        UILabel *salesLabel = [[UILabel alloc] init];
        salesLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE]);
        salesLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        salesLabel.text = @"销售金额";
        [self.contentView addSubview:salesLabel];
        [salesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_centerX).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.bottom.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
        }];
    }
    
    return self;
}

@end
