//
//  YMSalesDatasTableView.h
//  yunSale
//
//  Created by liushilou on 16/11/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMOrderAPI.h"


@interface YMSalesDatasTableView : UIView

@property (nonatomic,copy) NSArray *salesReportArray;

@property (nonatomic, strong) YMOrderAPI *salesAPI;

//已加载数据
@property (nonatomic,assign) BOOL fetched;

- (instancetype)init;

- (void)fetchData:(YMProfileType)type;

@end
