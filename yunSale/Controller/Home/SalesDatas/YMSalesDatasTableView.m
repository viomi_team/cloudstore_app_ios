//
//  YMSalesDatasTableView.m
//  yunSale
//
//  Created by liushilou on 16/11/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMSalesDatasTableView.h"
#import "YMTabButton.h"

#import "YMTwoItemTableViewCell.h"
#import "YMFourItemTableHeaderView.h"
#import "YMFourItemTableViewCell.h"
#import "YMSalesDataRootCell.h"
//#import "NSNumber+ym.h"
#import <YMCommon/NSNumber+ymc.h>


@interface YMSalesDatasTableView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic, copy) NSDictionary *overAllDict;
@property (nonatomic, copy) NSDictionary *topDict;
@property (nonatomic, copy) NSDictionary *secondDict;
@property (nonatomic, copy) NSDictionary *terminalDict;

@property (nonatomic, assign) YMProfileType type;
//缓存单元格高度
@property (nonatomic, strong) NSMutableArray *cellHeightArray;

@end

@implementation YMSalesDatasTableView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return self;
}

#pragma mark UITableViewDelegate & UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (!self.overAllDict) {
            return 0;
        }
        //角色为城市运营时，只显示4行
        //        else if ([[YMUserManager shareinstance].roleId integerValue] == 21) {
        //            return 4;
        //        } else {
        //            return 5;
        //        }
        return ([[YMUserManager shareinstance].roleId integerValue] == 21)? 4 : 5;
    } else {
        return self.salesReportArray.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        //        if (indexPath.row == 0) {
        //            return [YMScreenAdapter intergerSizeBy750:180];
        //        } else {
        //            return [YMScreenAdapter sizeBy750:100];
        //        }
        return indexPath.row == 0? [YMCScreenAdapter intergerSizeBy750:180] : [YMCScreenAdapter sizeBy750:100];
    } else {
        NSNumber *cellOfHeight = [self.cellHeightArray objectAtIndex:indexPath.row];
        return cellOfHeight.integerValue;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    //    if (section == 1) {
    //        return [YMScreenAdapter intergerSizeBy750:80];
    //    } else {
    //        return 0;
    //    }
    return section == 1? [YMCScreenAdapter intergerSizeBy750:80] : 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    //    if (section == 0) {
    //        return [YMScreenAdapter intergerSizeBy750:24];
    //    } else {
    //        return 0;
    //    }
    return section == 0? [YMCScreenAdapter intergerSizeBy750:24] : 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    YMFourItemTableHeaderView *view = [[YMFourItemTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [YMCScreenAdapter sizeBy750:82]) style:YMFourItemHeaderViewStyleSalesData];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    //    if (section == 0) {
    //        return [UIView drawLineViewWithWidth:YMSCREEN_WIDTH height:[YMScreenAdapter sizeBy750:24] color:[UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND]];
    //    } else {
    //        return nil;
    //    }
    return section == 0? [UIView drawLineViewWithWidth:YMSCREEN_WIDTH height:[YMCScreenAdapter sizeBy750:24] color:[UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND]] : nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            if (indexPath.row == 0) {
                NSString *reuseIdentifier = @"rootCell";
                YMSalesDataRootCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
                if (!cell) {
                    cell = [[YMSalesDataRootCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                NSNumber *orderCount = [self.overAllDict notNullObjectForKey:@"orderCount"];
                if (orderCount) {
                    cell.orderCountLabel.text = [NSString stringWithFormat:@"%@",orderCount];
                }
                
                NSNumber *turnOver = [self.overAllDict notNullObjectForKey:@"turnOver"];
                if (turnOver) {
                
                    NSMutableAttributedString *leadingString = [[NSMutableAttributedString alloc] initWithString:@"￥" attributes:@{NSFontAttributeName : YM_FONT([YMCScreenAdapter fontsizeBy750:48])}];
                    
                    NSAttributedString *salesAmount,*trialString;
                    
                    if(turnOver.floatValue < 10000) {
                        
                        salesAmount = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.2ld",(long)turnOver.floatValue] attributes:@{NSFontAttributeName : YM_FONT([YMCScreenAdapter fontsizeBy750:48])}];
                        
                        trialString = [NSAttributedString new];
                    }else if(turnOver.floatValue < 100000000){
                        
                        salesAmount = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.2f",(long)turnOver.floatValue/10000.0f] attributes:@{NSFontAttributeName : YM_FONT([YMCScreenAdapter fontsizeBy750:48])}];
                        
                        trialString = [[NSAttributedString alloc] initWithString:@"(万)" attributes:@{NSFontAttributeName : YM_FONT([YMCScreenAdapter fontsizeBy750:32])}];
                    }else {
                        
                        salesAmount = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.2f",(long)turnOver.floatValue/100000000.0f] attributes:@{NSFontAttributeName : YM_FONT([YMCScreenAdapter fontsizeBy750:48])}];
                        
                        trialString = [[NSAttributedString alloc] initWithString:@"(亿)" attributes:@{NSFontAttributeName : YM_FONT([YMCScreenAdapter fontsizeBy750:32])}];
                    }
                    
                    [leadingString appendAttributedString:salesAmount];
                    [leadingString appendAttributedString:trialString];
                    
                    cell.salesAmountLabel.attributedText = leadingString;
                }
                
                return cell;
            } else {
                NSString *reuseIdentifier = @"twoItemCell";
                YMTwoItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
                if (!cell) {
                    cell = [[YMTwoItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier Style:YMTwoItemCellStyleRight];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                switch (indexPath.row) {
                    case 1: {
                        if ([self isRegionManager]) {
                            cell.titleLabel.text = @"提成金额";
                            cell.detailsLabel.text = [self p_textOfKeyValue:@"profit" withDict:self.overAllDict];
                        }  else {
                            cell.titleLabel.text = @"一级渠道提成金额";
                            cell.detailsLabel.text = [self p_textOfKeyValue:@"profit" withDict:self.topDict];
                        }
                    }
                        break;
                    case 2: {
                        if ([self isRegionManager]) {
                            cell.titleLabel.text = @"销售费用";
                            cell.detailsLabel.text = [self p_textOfKeyValue:@"salesFee" withDict:self.overAllDict];
                        } else {
                            cell.titleLabel.text = @"二级渠道提成金额";
                            cell.detailsLabel.text = [self p_textOfKeyValue:@"profit" withDict:self.secondDict];
                        }
                    }
                        break;
                    case 3: {
                        if ([self isRegionManager]) {
                            cell.titleLabel.text = @"分享提成";
                            cell.detailsLabel.text = [self p_textOfKeyValue:@"participateProfit" withDict:self.overAllDict];
                        } else {
                            cell.titleLabel.text = @"门店提成金额";
                            cell.detailsLabel.text = [self p_textOfKeyValue:@"profit" withDict:self.terminalDict];
                        }
                    }
                        break;
                    case 4: {
                        cell.titleLabel.text = @"销售费用";
                        cell.detailsLabel.text = [self p_textOfKeyValue:@"salesFee" withDict:self.topDict];
                    }
                        break;
                }
                
                //设置单元格背景颜色
                if (indexPath.row %2 == 0) {
                    cell.backgroundColor = [UIColor whiteColor];
                } else {
                    cell.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
                }
                
                return cell;
            }
        }
            break;
            
            //四项 Item 的 tableViewCell
        case 1: {
            NSString *reuseIdentifier = @"fourItemCell";
            YMFourItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
            if (!cell) {
                cell = [[YMFourItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier cellStyle:YMFourItemCellStyleSalesData];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            //配置单元格的数据
            NSDictionary *dict = self.salesReportArray[indexPath.row];
            NSDictionary *channelInfoDict = [dict notNullObjectForKey:@"channelInfo"];
            NSDictionary *salesReportBeanDict = [dict notNullObjectForKey:@"salesReportBean"];
            cell.firstLabel.text = [NSString stringWithFormat:@"%@", [channelInfoDict notNullObjectForKey:@"name"]];
            cell.secondLabel.text = [NSString stringWithFormat:@"%@", [salesReportBeanDict notNullObjectForKey:@"orderCount"]];
            NSNumber *salesAmount = [salesReportBeanDict notNullObjectForKey:@"turnOver"];
            cell.thirdLabel.text = [NSString stringWithFormat:@"￥%@", [NSNumber formatNumberStringWithNumber:salesAmount]];
            NSNumber *profit = [salesReportBeanDict notNullObjectForKey:@"profit"];
            cell.fourthLabel.text = [NSString stringWithFormat:@"￥%@", [NSNumber formatNumberStringWithNumber:profit]];
            
            if (indexPath.row %2 == 0) {
                cell.backgroundColor = [UIColor whiteColor];
            } else {
                cell.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
            }
            
            return cell;
        }
            break;
            
        default:
            return [[UITableViewCell alloc] init];
            break;
    }
}

/*
 *嵌套有点深，需要优化
 */
#pragma mark Fetch
- (void)fetchData:(YMProfileType)type {
    self.type = type;
    
    self.fetched = YES;
    
    if (!self.salesAPI) {
        self.salesAPI = [[YMOrderAPI alloc] init];
    }
    
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    [YMCAlertView removeNetErrorInView:self];
    
    //第一次网络请求
    [self.salesAPI fetchSalesAnalyse:type completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        
        if (status == YMCRequestSuccess) {
            //整理返回的数据
            if (!data) {
                return;
            }
            
            //当角色为城市运营时，字典的键为 ”result”
            if ([[YMUserManager shareinstance].roleId integerValue] == 21) {
                self.overAllDict = [data notNullObjectForKey:@"result"];
            } else {
                self.overAllDict = [data notNullObjectForKey:@"overAll"];
            }
            
            self.topDict = [data notNullObjectForKey:@"top"];
            self.secondDict = [data notNullObjectForKey:@"second"];
            self.terminalDict = [data notNullObjectForKey:@"terminal"];
            
            //            /*
            //             *后台接口未调试好，区域管理员暂时屏蔽这个接口的功能，等接口调试好再去掉这个判断语句即可
            //             *第一次网络请求成功后再执行第二次网络请求
            //             */
            //            if ([[YMUserManager shareinstance].roleId integerValue] == 1001) {
            //                //若登陆的角色为区域管理员，则不进行第二次网络请求
            //                [MBProgressHUD hideHUDForView:self animated:YES];
            //                [self.tableView reloadData];
            //            } else {
            
            //区域管理员接口调好了
            
            [self.salesAPI fetchSalesReport:type completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
                
                [MBProgressHUD hideHUDForView:self animated:YES];
                
                if (status == YMCRequestSuccess) {
                    NSMutableArray *array = [data notNullObjectForKey:@"datas"];
                    if (!array || array.count == 0) {
                        [YMCAlertView showNetErrorInView:self type:YMCRequestNoDataAndNoBtn message:@"没有更多数据" actionBlock:nil];
                        return;
                    }
                    
                    self.salesReportArray = array;
                    
                    //计算单元格高度
                    self.cellHeightArray = [[NSMutableArray alloc] init];
                    
                    for (NSDictionary *dic in self.salesReportArray) {
                        
                        NSDictionary *channelInfoDict = [dic notNullObjectForKey:@"channelInfo"];
                        NSDictionary *salesReportBeanDict = [dic notNullObjectForKey:@"salesReportBean"];
                        NSString *nameString = [NSString stringWithFormat:@"%@", [channelInfoDict notNullObjectForKey:@"name"]];
                        NSNumber *salesAmount = [salesReportBeanDict notNullObjectForKey:@"turnOver"];
                        NSString *salesString = [NSString stringWithFormat:@"￥%@", [NSNumber formatNumberStringWithNumber:salesAmount]];
                        
                        CGFloat nameHeight = [nameString heightWithWidth:[YMCScreenAdapter sizeBy750:260] fontsize:[YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]];
                        CGFloat salesHeight = [salesString heightWithWidth:[YMCScreenAdapter sizeBy750:150] fontsize:[YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]];
                        
                        CGFloat cellHeight = nameHeight >= salesHeight? nameHeight:salesHeight;
                        
                        cellHeight = cellHeight >= OneLineHeight * 2 ? OneLineHeight * 2: cellHeight;
                        
                        [self.cellHeightArray addObject:[NSNumber numberWithFloat:cellHeight + [YMCScreenAdapter sizeBy750:64]]];
                    }
                    
                    [self.tableView reloadData];
                } else {
                    @weakify(self);
                    [YMCAlertView showNetErrorInView:self type:status message:message actionBlock:^{
                        @strongify(self);
                        
                        [self fetchData:self.type];
                    }];
                }
            }];
            //            }
        } else {
            [MBProgressHUD hideHUDForView:self animated:YES];
            @weakify(self);
            [YMCAlertView showNetErrorInView:self type:status message:message actionBlock:^{
                @strongify(self);
                [self fetchData:self.type];
            }];
        }
    }];
}

#pragma mark - Private
//思考如何移到 YMUserManager 类中，放在这里太多余
- (BOOL)isRegionManager {
    if ([[YMUserManager shareinstance].roleId integerValue] == 21) {
        return YES;
    } else {
        return NO;
    }
}

//只处理金额显示的数据，数值保留两位小数（为了给cellForRowAtIndexPath: 方法瘦身）
- (NSString *)p_textOfKeyValue:(NSString *)keyString withDict:(NSDictionary *)dict {
    id number = [dict notNullObjectForKey:keyString];
    if (number && [number isKindOfClass:[NSNumber class]]) {
        return [NSString stringWithFormat:@"￥%@", [NSNumber formatNumberStringWithNumber:number]];
    } else {
        NSString *valueString = @"-";
        return valueString;
    }
}

@end
