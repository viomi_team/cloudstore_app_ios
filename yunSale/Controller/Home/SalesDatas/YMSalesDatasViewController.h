//
//  YMSalesDatasViewController.h
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMScrollItemsBaseViewController.h"

typedef NS_ENUM(NSUInteger, YMSalesDatasType) {
    YMSalesDatasTypeDay = 0,
    YMSalesDatasTypeMonth = 1,
    YMSalesDatasTypeAll = 2
};

@interface YMSalesDatasViewController : YMScrollItemsBaseViewController

@property (nonatomic, assign) YMSalesDatasType salesDatasType;       //首次进入时显示的页面



@end
