//
//  YMSalesDatasViewController.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMSalesDatasViewController.h"
#import "YMSalesDatasTableView.h"
#import <UMMobClick/MobClick.h>

@interface YMSalesDatasViewController ()


@end

@implementation YMSalesDatasViewController

#pragma mark Life
//- (id)initWithSalesDatasType:(YMSalesDatasType)type {
//    self = [super init];
//    self.salesDatasType = type;
//    return self;
//}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick event:@"Home_Sales_Datas"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"销售数据";
    
    self.items = [NSArray arrayWithObjects:@"昨日数据",@"本月数据",@"全部数据", nil];
    
    YMSalesDatasTableView *dayTableView = [[YMSalesDatasTableView alloc] init];
    YMSalesDatasTableView *monthTableView = [[YMSalesDatasTableView alloc] init];
    YMSalesDatasTableView *allTableView = [[YMSalesDatasTableView alloc] init];
    self.itemContentViews = [NSArray arrayWithObjects:dayTableView,monthTableView,allTableView, nil];
    
    [self appearControllerOfIndex:self.salesDatasType];
//    [self appearControllerOfIndex:1];
}

- (void)dealloc
{
    NSLog(@"%s",__func__);
    for (YMSalesDatasTableView *tableview in self.itemContentViews) {
        [tableview.salesAPI cancle];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - 重写父类
- (void)appearControllerOfIndex:(NSUInteger)index
{
    //更改父类的scrollView的contentOffset来控制首次进入的页面
    [self.scrollView setContentOffset:CGPointMake([UIScreen mainScreen].bounds.size.width * index, 0)];
    
    YMSalesDatasTableView *controller = [self.itemContentViews objectAtIndex:index];
    if (!controller.fetched) {
        if (index == 0) {
            [controller fetchData:YMProfileTypeDay];
        }else if (index == 1){
            [controller fetchData:YMProfileTypeMonth];
        }else{
            [controller fetchData:YMProfileTypeAll];
        }
    }
}


@end
