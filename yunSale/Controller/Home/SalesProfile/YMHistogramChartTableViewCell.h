//
//  YMHistogramChartTableViewCell.h
//  yunSale
//
//  Created by liushilou on 16/11/11.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMHistogramChartView.h"

@interface YMHistogramChartTableViewCell : UITableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

- (void)configCell:(NSArray<NSDictionary *> *)datas;

- (void)configTotal:(NSString *)total;

@end
