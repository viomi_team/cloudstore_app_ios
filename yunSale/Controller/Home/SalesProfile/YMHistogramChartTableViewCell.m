//
//  YMHistogramChartTableViewCell.m
//  yunSale
//
//  Created by liushilou on 16/11/11.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMHistogramChartTableViewCell.h"


@interface YMHistogramChartTableViewCell()

@property (nonatomic,strong) YMHistogramChartView *chartView;

@end

@implementation YMHistogramChartTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _chartView = [[YMHistogramChartView alloc] init];
        [self.contentView addSubview:_chartView];
        [_chartView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset(0);
            make.right.equalTo(self.contentView).with.offset(0);
            make.top.equalTo(self.contentView).with.offset(0);
            make.bottom.equalTo(self.contentView).with.offset(0);
        }];
    }
    return self;
}

- (void)configCell:(NSArray *)datas {
    
    [self.chartView setDatas:datas];
}

- (void)configTotal:(NSString *)total
{
    self.chartView.totalLabel.text = total;
}

@end
