//
//  YMSalesProfileTableView.h
//  yunSale
//
//  Created by liushilou on 16/11/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMOrderAPI.h"

@interface YMSalesProfileTableView : UIView

@property (nonatomic,copy) NSArray *salesDatas;

@property (nonatomic,strong) YMOrderAPI *orderAPI;

//已加载数据
@property (nonatomic,assign) BOOL fetched;

- (instancetype)init;


- (void)fetchData:(YMProfileType)type;

@end
