//
//  YMSalesProfileTableView.m
//  yunSale
//
//  Created by liushilou on 16/11/29.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMSalesProfileTableView.h"
#import "YMTwoItemTableHeaderView.h"
#import "YMHistogramChartTableViewCell.h"
#import "YMTwoItemTableViewCell.h"
#import "YMStore.h"
#import "YMChartTab.h"
#import <YMCommon/NSNumber+ymc.h>


@interface YMSalesProfileTableView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,copy) NSArray *profileDatas;

@property (nonatomic,assign) YMProfileType type;

@property (nonatomic,assign) CGFloat chartCellHeight;

@property (nonatomic,strong) YMTwoItemTableHeaderView *headerView;

@property (nonatomic,strong) UIView *footerView;

@property (nonatomic,assign) BOOL loadedChart;

@property (nonatomic,strong) NSString *totalValue;

//@property (nonatomic,assign) float totalValue;

@end

@implementation YMSalesProfileTableView

- (instancetype)init {
    self = [super init];
    if (self) {
        
        self.chartCellHeight = YM_HISTOGRAMCHART_HEIGHT;
        
        self.tableView = [[UITableView alloc] init];
        self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return self;
}

#pragma mark - UITableViewDelegate & UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (section == 0) {
//        return 1;
//    }else{
//        return self.salesDatas.count;
//    }
    return section != 0? self.salesDatas.count : 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
//        return self.chartCellHeight;
//    }else{
//        return [YMScreenAdapter sizeBy750:100];
//    }
    return indexPath.section == 0? self.chartCellHeight : [YMCScreenAdapter sizeBy750:100];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    if (section == 0) {
//        return [YMScreenAdapter sizeBy640:24];
//    }else{
//        return 0;
//    }
    return section == 0? [YMCScreenAdapter sizeBy750:24] : 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return 0;
//    }else{
//        return [YMScreenAdapter sizeBy640:80];
//    }
    return section != 0? [YMCScreenAdapter sizeBy750:80] : 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        if (!self.headerView) {
            self.headerView = [[YMTwoItemTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [YMCScreenAdapter sizeBy750:80]) leftText:@"城市运营" rightText:@"销售金额"];
        }
        return self.headerView;
    }else{
        return nil;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 1) {
        return nil;
    } else {
        if (!self.footerView) {
            self.footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [YMCScreenAdapter sizeBy640:26])];
           self.footerView.backgroundColor = [UIColor colorWithHexString:@"#f5f5f5"];
            
            UIView *toplineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 0.5)];
            toplineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
            [self.footerView addSubview:toplineView];
            
            UIView *bottomlineView = [[UIView alloc] init];
            bottomlineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
            [self.footerView addSubview:bottomlineView];
            [bottomlineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.footerView).with.offset(0);
                make.right.equalTo(self.footerView).with.offset(0);
                make.bottom.equalTo(self.footerView).with.offset(0);
                make.height.mas_equalTo(0.5);
            }];
        }
        
        return self.footerView;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        
        static NSString *reuseIdentifier = @"chartCell";
        YMHistogramChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (!cell) {
            cell = [[YMHistogramChartTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        if (self.profileDatas) {
            if (!self.loadedChart) {
                self.loadedChart = YES;
                [cell configCell:self.profileDatas];
//                [cell configTotal:[NSString stringWithFormat:@"¥%.2f",self.totalValue]];
                [cell configTotal:[NSString stringWithFormat:@"¥%@",self.totalValue]];
            }
        }
        return cell;
    }else{
        
        static NSString *reuseIdentifier = @"orderCell";
        YMTwoItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (!cell) {
            cell = [[YMTwoItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier Style:YMTwoItemCellStyleRight];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        YMStore *store = [self.salesDatas objectAtIndex:indexPath.row];
        
        cell.titleLabel.text = store.name;
//        NSString *salesMoneyString = [NSNumber formatNumberStringWithNumber:@(store.salesMoney)];
//        cell.detailsLabel.text = [NSString stringWithFormat:@"¥%@", store.salesMoney];
        
        cell.detailsLabel.text = [NSString stringWithFormat:@"¥%@", store.saleMoneyString];
        
//        if (indexPath.row%2 == 0) {
//            cell.contentView.backgroundColor = [UIColor whiteColor];
//        }else{
//            cell.contentView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
//        }
        
        cell.contentView.backgroundColor = indexPath.row%2 == 0? [UIColor whiteColor] : [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        
        return cell;
    }
}

#pragma mark - Fetch
- (void)fetchData:(YMProfileType)type {
    self.type = type;
    
    self.fetched = YES;
    
    if (!self.orderAPI) {
        self.orderAPI = [[YMOrderAPI alloc] init];
    }
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    //显示错误提示视图时，若先前的提示视图存在，则先把其移除
    [YMCAlertView removeNetErrorInView:self];

    [self.orderAPI fetchOrderProfile:type completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
 
        [MBProgressHUD hideHUDForView:self animated:YES];
        if (status == YMCRequestSuccess) {
            
            NSArray *datas = [data notNullObjectForKey:@"datas"];
            
            if (!datas || datas.count == 0) {
                [YMCAlertView showNetErrorInView:self type:YMCRequestNoDataAndNoBtn message:@"没有更多数据" actionBlock:nil];
                return;
            }
            
            NSMutableArray *array = [NSMutableArray new];
            
            
//            CGFloat totalValue = 0;
            
            NSDecimalNumber *totalValue = [[NSDecimalNumber alloc] initWithString:@"0.00"];
            
            for (NSDictionary *item in datas) {
                
//                NSDictionary *salesReportBean = [item notNullObjectForKey:@"salesReportBean"];
//                NSNumber *turnOver = [salesReportBean notNullObjectForKey:@"turnOver"];
//                
//                NSDictionary *channelInfo = [item notNullObjectForKey:@"channelInfo"];
//                NSString *name = [channelInfo notNullObjectForKey:@"name"];
//                
//                YMStore *store = [[YMStore alloc] init];
//                store.name = name;
//                store.salesMoney = turnOver.floatValue;
//                [array addObject:store];
//                
//                totalValue += turnOver.floatValue;
                
                NSDictionary *channelInfo = [item notNullObjectForKey:@"channelInfo"];
                NSString *name = [channelInfo notNullObjectForKey:@"name"];
                
                NSDictionary *salesReportBean = [item notNullObjectForKey:@"salesReportBean"];
                NSNumber *turnOverNum = [salesReportBean notNullObjectForKey:@"turnOver"];
                
                //使用NSDecimalNumber才能精确计算totalValue （by zcm 2017.9.6）
                NSDecimalNumber *turnOver = [NSDecimalNumber decimalNumberWithString:turnOverNum.stringValue];
                totalValue = [totalValue decimalNumberByAdding:turnOver];
                
                YMStore *store = [[YMStore alloc] init];
                store.name = name;
                store.salesMoney = turnOver.floatValue;
                //float将会丢失精度，暂时还没想到更好的解决方法，先用string保证精度
                store.saleMoneyString = [NSNumber formatNumberStringWithNumber:turnOverNum];
                
                [array addObject:store];
            }
            
            self.totalValue = [NSNumber formatNumberStringWithNumber:totalValue];
            
            // 这里的key写的是@property的名称
            NSSortDescriptor *salesMoneyDesc = [NSSortDescriptor sortDescriptorWithKey:@"salesMoney" ascending:NO];
            // 按顺序添加排序描述器
            NSArray *descs = [NSArray arrayWithObjects:salesMoneyDesc,nil];
            
            NSArray *sorteArray = [array sortedArrayUsingDescriptors:descs];
            
            self.salesDatas = sorteArray;
            
            NSMutableArray *profileArray = [NSMutableArray new];
            for (YMStore *store in self.salesDatas) {
                if (store.salesMoney > 0) {
                    
                    YMChart *chart = [[YMChart alloc] init];
                    chart.name = store.name;
                    chart.value = store.salesMoney;
                    [profileArray addObject:chart];
                    
                    NSInteger index = [self.salesDatas indexOfObject:store];
                    if (index >= 5) {
                        break;
                    }
                }else{
                    break;
                }
            }
            
            self.profileDatas = [profileArray copy];
            
            CGFloat tabHeight = 0;
            if (self.profileDatas.count != 0) {
                NSMutableAttributedString *tabsString = [YMChartTab stringOfTabs:self.profileDatas withStyle:YMChartTabStyleAmount];
                tabHeight = [YMChartTab heightForAttributedString:tabsString width:[UIScreen mainScreen].bounds.size.width - [YMCScreenAdapter sizeBy750:YMSPACE_AROUND]*2];
                tabHeight += [YMCScreenAdapter sizeBy750:YMSPACE_AROUND];
            }
            self.chartCellHeight = tabHeight + YM_HISTOGRAMCHART_HEIGHT;
            
            [self.tableView reloadData];
            
        }else{
            
//            //判断角色，若角色为区域管理员，则提示“功能尚未开通，敬请期待”
//            if ([[YMUserManager shareinstance].roleId integerValue] == 1001) {
//                [YMAlertView showNetErrorInView:self type:YMRequestNoDataAndNoBtn message:@"功能尚未开通，敬请期待" actionBlock:nil];
//                return;
//            } else {
                @weakify(self);
                [YMCAlertView showNetErrorInView:self type:status message:message actionBlock:^{
                    @strongify(self);
                    
                    [self fetchData:self.type];
                }];
//            }
        }
    }];
}

@end
