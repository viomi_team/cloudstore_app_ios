//
//  YMSalesProfileViewController.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMSalesProfileViewController.h"
#import "YMTabButton.h"
#import "YMOrderAPI.h"
#import "YMTwoItemTableHeaderView.h"
#import "YMHistogramChartTableViewCell.h"
#import "YMTwoItemTableViewCell.h"
#import "YMStore.h"
#import "YMSalesProfileTableView.h"
#import <UMMobClick/MobClick.h>


@interface YMSalesProfileViewController ()



@end

@implementation YMSalesProfileViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick event:@"Home_Sales_Profile"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"销售概况";
    
    self.items = [NSArray arrayWithObjects:@"昨日数据",@"本月数据",@"全部数据", nil];
    
    YMSalesProfileTableView *dayTableView = [[YMSalesProfileTableView alloc] init];
    YMSalesProfileTableView *monthTableView = [[YMSalesProfileTableView alloc] init];
    YMSalesProfileTableView *allTableView = [[YMSalesProfileTableView alloc] init];
    self.itemContentViews = [NSArray arrayWithObjects:dayTableView,monthTableView,allTableView, nil];
    
    [self appearControllerOfIndex:0];
}

- (void)dealloc
{
    NSLog(@"%s",__func__);
    for (YMSalesProfileTableView *tableview in self.itemContentViews) {
        [tableview.orderAPI cancle];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma 重写父类
- (void)appearControllerOfIndex:(NSUInteger)index
{
    YMSalesProfileTableView *controller = [self.itemContentViews objectAtIndex:index];
    if (!controller.fetched) {
        if (index == 0) {
            [controller fetchData:YMProfileTypeDay];
        }else if (index == 1){
            [controller fetchData:YMProfileTypeMonth];
        }else{
            [controller fetchData:YMProfileTypeAll];
        }
    }
}

@end
