//
//  YMStoreDailySearchViewController.h
//  yunSale
//
//  Created by ChengMinZhang on 2016/12/12.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YMStoreDailySearchDelegate <NSObject>

@required

-(void)YMStoreDailyDidSearchFinishedWithStoreName:(NSString *)storeName beginDate:(NSString *)beginDate
        endDate:(NSString *)endDate agencyName:(NSString *)agencyName;

@end

@interface YMStoreDailySearchViewController : UIViewController

//@property (nonatomic, copy) NSString *storeName;
//
//@property (nonatomic, copy) NSString *beginDate;
//
//@property (nonatomic, copy) NSString *endDate;
//
//@property (nonatomic, copy) NSString *agencyName;

@property (nonatomic, weak) id<YMStoreDailySearchDelegate> delegate;

@end
