//
//  YMStoreDailySearchViewController.m
//  yunSale
//
//  Created by ChengMinZhang on 2016/12/12.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMStoreDailySearchViewController.h"
#import "YMButton.h"
#import <ActionSheetPicker.h>
#import "YMDate.h"
#import <UMMobClick/MobClick.h>
#import "YMDateSelectView.h"
#import "YMSearchInputView.h"

@interface YMStoreDailySearchViewController ()

@property (nonatomic, strong) YMSearchInputView *storeNameView;

@property (nonatomic, strong) YMDateSelectView *beginDateSelectorView;

@property (nonatomic, strong) YMDateSelectView *endDateSelectorView;

@property (nonatomic, strong) YMSearchInputView *agencyNameView;

@property (nonatomic, strong) YMButton *searchBtn;

@end

@implementation YMStoreDailySearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [MobClick event:@"Home_Store_Daily"];
    
    [self drawView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark draw view
- (void)drawView{
    self.view.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    
    //containerView
    UIView *containerView = [[UIView alloc] init];
    containerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:containerView];
    [containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
    
    //storeName
    self.storeNameView = [[YMSearchInputView alloc] initWithTitle:@"门店名称" placeHolder:@"请输入门店名称"];
    [containerView addSubview:self.storeNameView];
    [self.storeNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(containerView);
        make.left.equalTo(containerView);
        make.right.equalTo(containerView);
        make.height.mas_equalTo([YMCScreenAdapter fontsizeBy750:84]);
    }];
    
    //beginDate
    self.beginDateSelectorView = [[YMDateSelectView alloc] initWithTip:@"开始时间" placeHoder:@"选择开始时间"];
    [containerView addSubview:self.beginDateSelectorView];
    [self.beginDateSelectorView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.equalTo(self.storeNameView.mas_bottom).with.offset([YMCScreenAdapter fontsizeBy750:16]);
        make.left.equalTo(containerView);
        make.right.equalTo(containerView);
        make.height.mas_equalTo([YMCScreenAdapter fontsizeBy750:84]);
    }];
    
    //endDate
    self.endDateSelectorView = [[YMDateSelectView alloc] initWithTip:@"结束时间" placeHoder:@"选择结束时间"];
    [containerView addSubview:self.endDateSelectorView];
    [self.endDateSelectorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.beginDateSelectorView.mas_bottom).with.offset([YMCScreenAdapter fontsizeBy750:16]);
        make.left.equalTo(containerView);
        make.right.equalTo(containerView);
        make.height.mas_equalTo([YMCScreenAdapter fontsizeBy750:84]);
    }];
    
    //agencyName
    self.agencyNameView = [[YMSearchInputView alloc] initWithTitle:@"分销商" placeHolder:@"请输入分销名称"];
    [containerView addSubview:self.agencyNameView];
    [self.agencyNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.endDateSelectorView.mas_bottom).with.offset([YMCScreenAdapter fontsizeBy750:16]);
        make.left.equalTo(containerView);
        make.right.equalTo(containerView);
        make.height.mas_equalTo([YMCScreenAdapter fontsizeBy750:84]);
    }];
    
    //search button
    self.searchBtn = [[YMButton alloc] initWithStyle:YMButtonStyleRectangle];
    [self.searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [self.searchBtn setSelected:YES];
    [self.searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:self.searchBtn];
    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.agencyNameView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
        make.left.equalTo(containerView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.right.equalTo(containerView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.bottom.equalTo(containerView).with.offset([YMCScreenAdapter sizeBy750:-YMSPACE_AROUND]);
    }];
    
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [containerView addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(containerView);
        make.left.equalTo(containerView);
        make.right.equalTo(containerView);
        make.height.mas_equalTo(0.5);
    }];
}

#pragma mark endEdit
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(void)searchAction{
    NSString *storeName = self.storeNameView.textField.text;
    NSString *agencyName = self.agencyNameView.textField.text;
    
    NSString *beginDate = @"" ;
    NSString *endDate = @"" ;
    
    if(self.beginDateSelectorView.dateValue != nil){
        
        beginDate = self.beginDateSelectorView.dateValue;
        beginDate = [NSString stringWithFormat:@"%@+00:00", beginDate];
    }
    
    if(self.endDateSelectorView.dateValue != nil){
        
        endDate = self.endDateSelectorView.dateValue;
        endDate = [NSString stringWithFormat:@"%@+00:00", endDate];
    }
    
    if([beginDate isEqualToString:@""] && [endDate isEqualToString:@""]){
        if ([beginDate compare:endDate options:NSNumericSearch] == NSOrderedDescending) {
            [YMCAlertView showHubInView:self.view message:@"结束时间应该大于开始时间"];
            return;
        }
    }
    
    [self.delegate YMStoreDailyDidSearchFinishedWithStoreName:storeName beginDate:beginDate endDate:endDate agencyName:agencyName];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
