//
//  YMStoreDailyViewController.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMStoreDailyViewController.h"
#import "YMFourItemTableViewCell.h"
#import "YMFourItemTableHeaderView.h"
#import "YMStoreDailySearchViewController.h"
#import "YMStoreDetailsViewController.h"
#import "YMOrderAPI.h"
#import <MJRefresh/MJRefresh.h>
#import <UMMobClick/MobClick.h>

@interface YMStoreDailyViewController () <UITableViewDelegate, UITableViewDataSource,YMStoreDailySearchDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) YMOrderAPI *storeDailyAPI;
//订单列表数组
@property (nonatomic, strong) NSMutableArray *dailyListsArray;
//数据页数
@property (nonatomic, assign) NSInteger pageNumber;
//数据总页数（由服务器返回）
@property (nonatomic, assign) NSInteger totalPageCount;

@property (nonatomic, copy) NSString *beginDateString;

@property (nonatomic, copy) NSString *endDateString;

@property (nonatomic, copy) NSString *storeNameString;    //门店名称

@property (nonatomic, copy) NSString *agencyNameString;         //经销商名称

@property (nonatomic, assign) BOOL isSearchAction;
//缓存单元格高度
@property (nonatomic, strong) NSMutableArray *cellHeightArray;

@property (nonatomic,strong) YMStoreDailySearchViewController *searchVC;

@end

@implementation YMStoreDailyViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick beginLogPageView:@"门店日报"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [MobClick endLogPageView:@"门店日报"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ym_index_search"] style:UIBarButtonItemStylePlain target:self action:@selector(searchAction)];
    [self.navigationItem setRightBarButtonItem:rightButton];
    
    [self drawView];
    
    //初始化数据
    self.dailyListsArray = [[NSMutableArray alloc] init];
    self.pageNumber       = 1;
    self.beginDateString  = @"";
    self.endDateString    = @"";
    self.storeNameString  = @"";
    self.agencyNameString = @"";
    self.isSearchAction   = NO;
    
#warning ios10的模拟器在这里崩溃。。。
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(fetchData)];
    //    [self.tableView.mj_footer beginRefreshing];
    
    //加载数据
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self fetchData];
}

- (void)dealloc
{
    NSLog(@"%s",__func__);
    if (self.storeDailyAPI) {
        [self.storeDailyAPI cancle];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Draw
- (void)drawView
{
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"门店日报";
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dailyListsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *heightOfCell = [self.cellHeightArray objectAtIndex:indexPath.row];
    return heightOfCell.integerValue;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [YMCScreenAdapter intergerSizeBy750:82];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    YMFourItemTableHeaderView *view = [[YMFourItemTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [YMCScreenAdapter sizeBy750:82]) style:YMFourItemHeaderViewStyleStoreDaily];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseIdentifier = @"FourItemCell";
    YMFourItemTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell) {
        cell = [[YMFourItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier cellStyle:YMFourItemCellStyleStoreDaily];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    //配置数据
    cell.firstLabel.text = [NSString stringWithFormat:@"%@", [self.dailyListsArray[indexPath.row] notNullObjectForKey:@"day"]];
    NSString *storeName = [self.dailyListsArray[indexPath.row] notNullObjectForKey:@"channelName"];
    if ([NSString isEmptyString:storeName]) {
        cell.secondLabel.text = @"-";
    } else {
        cell.secondLabel.text = [NSString stringWithFormat:@"%@", [self.dailyListsArray[indexPath.row] notNullObjectForKey:@"channelName"]];
    }
    cell.thirdLabel.text = [NSString stringWithFormat:@"%@", [self.dailyListsArray[indexPath.row] notNullObjectForKey:@"userCount"]];
    cell.fourthLabel.text = [NSString stringWithFormat:@"%@", [self.dailyListsArray[indexPath.row] notNullObjectForKey:@"orderCount"]];
    
    if (indexPath.row %2 == 0) {
        cell.backgroundColor = [UIColor whiteColor];
    } else {
        cell.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    YMStoreDetailsViewController *detailsVC = [[YMStoreDetailsViewController alloc] init];
    detailsVC.dailyDetailsDict = self.dailyListsArray[indexPath.row];
    [self.navigationController pushViewController:detailsVC animated:YES];
    
}

#pragma mark Action
- (void)searchAction
{
    self.searchVC = [[YMStoreDailySearchViewController alloc] init];
    //因订单数据和门店日报使用同一个搜索类，所以把导航栏标题放在这里设置
    self.searchVC.navigationItem.title = @"门店搜索";
    
    self.searchVC.delegate = self;
    
    if (!self.searchVC) {
//        self.searchVC = [[YMStoreDailySearchViewController alloc] init];
//        //因订单数据和门店日报使用同一个搜索类，所以把导航栏标题放在这里设置
//        self.searchVC.navigationItem.title = @"门店搜索";
//
//        @weakify(self);
//        self.searchVC.PassValueBlock = ^(NSString *begin, NSString *end) {
//            @strongify(self);
//            //回调搜索时间并重新初始化网络请求的参数，清空订单列表数组
//            self.beginDateString = begin;
//            self.endDateString = end;
//            self.pageNumber = 1;
//            self.isSearchAction = YES;
//            
//            [self.dailyListsArray removeAllObjects];
//            [self.tableView reloadData];
//            
//            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//            [self fetchData];
//            
//        };
    }
    [self.navigationController pushViewController:self.searchVC animated:YES];
}

#pragma mark Fetch
- (void)fetchData
{
    if(!self.storeDailyAPI) {
        self.storeDailyAPI = [[YMOrderAPI alloc] init];
    }
    
    //显示错误提示视图时，若先前的提示视图存在，则先把其移除
    [YMCAlertView removeNetErrorInView:self.view];
    
    [self.storeDailyAPI fetchStoreDailyDataWithBeginDate:self.beginDateString endDate:self.endDateString storeName:self.storeNameString agencyName:self.agencyNameString pageNum:self.pageNumber completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {

        [MBProgressHUD hideHUDForView:self.view animated:YES];

        if (status == YMCRequestSuccess) {
            
            NSDictionary *dict = [data notNullObjectForKey:@"result"];
            self.totalPageCount = [[dict notNullObjectForKey:@"totalPageNum"] integerValue];
            NSArray *array = [dict notNullObjectForKey:@"list"];
            
            if (!array || array.count == 0)
            {
                if (self.pageNumber == 1 && !self.isSearchAction)
                {
                    [YMCAlertView showNetErrorInView:self.view type:YMCRequestNoDataAndNoBtn message:YM_NOMOREDATA_TEXT actionBlock:nil];
                }
                else
                {
                    @weakify(self);
                    [YMCAlertView showNetErrorInView:self.view type:YMCRequestNoData message:YM_NOSEARCHDATA_TEXT actionBlock:^{
                        @strongify(self);
                        [self searchAction];
                    }];
                }
                
                return;
            }
            
            [self.dailyListsArray addObjectsFromArray:array];
            
            //计算单元格高度
            self.cellHeightArray = [[NSMutableArray alloc] init];
            
            for (NSDictionary *dic in self.dailyListsArray) {
                NSString *storeName = [dic notNullObjectForKey:@"channelName"];
                if ([NSString isEmptyString:storeName]) {
                    storeName = @"-";
                }
                CGFloat nameHeight = [storeName heightWithWidth:[YMCScreenAdapter sizeBy750:190] fontsize:[YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]];
                
                [self.cellHeightArray addObject:[NSNumber numberWithFloat:nameHeight + [YMCScreenAdapter sizeBy750:64]]];
            }
            
            [self.tableView.mj_footer endRefreshing];
            
            if (self.pageNumber >= self.totalPageCount) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                self.pageNumber += 1;
            }
            [self.tableView reloadData];
            
        } else {
            [self.tableView.mj_footer endRefreshing];
            if (self.pageNumber == 1) {
                //首次加载
                @weakify(self);
                [YMCAlertView showNetErrorInView:self.view type:status message:message actionBlock:^{
                    @strongify(self);
                    
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [self fetchData];
                }];
            }else{
                [YMCAlertView showMessage:message];
            }
        }
    }];
}

#pragma mark stroreDaily delegate
-(void)YMStoreDailyDidSearchFinishedWithStoreName:(NSString *)storeName beginDate:(NSString *)beginDate endDate:(NSString *)endDate agencyName:(NSString *)agencyName{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    //重置参数
    self.pageNumber = 1 ;
    self.isSearchAction = YES ;
    [self.dailyListsArray removeAllObjects];
    
    self.agencyNameString = agencyName;
    self.endDateString    = endDate ;
    self.storeNameString  = storeName ;
    self.beginDateString  = beginDate ;
    
    [self fetchData];
}

@end
