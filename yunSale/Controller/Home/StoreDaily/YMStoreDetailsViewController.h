//
//  YMStoreDetailsViewController.h
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMStoreDetailsViewController : UIViewController

@property (nonatomic, copy) NSDictionary *dailyDetailsDict;

@end
