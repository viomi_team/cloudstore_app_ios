//
//  YMStoreDetailsViewController.m
//  yunSale
//
//  Created by liushilou on 16/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMStoreDetailsViewController.h"
#import "YMTwoItemTableViewCell.h"
#import <UMMobClick/MobClick.h>

@interface YMStoreDetailsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation YMStoreDetailsViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick beginLogPageView:@"日报详情"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [MobClick endLogPageView:@"日报详情"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"日报详情";
    
    [self drawView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Draw
- (void)drawView
{
    self.view.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 3;
    } else {
        return 5;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [YMCScreenAdapter intergerSizeBy750:100];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return [YMCScreenAdapter intergerSizeBy750:24];
    } else {
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [UIView drawLineViewWithWidth:YMSCREEN_WIDTH height:[YMCScreenAdapter sizeBy750:24] color:[UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseIdentifier = @"twoItemCell";
    YMTwoItemTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell) {
        cell = [[YMTwoItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier Style:YMTwoItemCellStyleRight];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //配置单元格的数据
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    cell.titleLabel.text = @"门店名称：";
                    NSString *storeName = [self.dailyDetailsDict notNullObjectForKey:@"channelName"];
                    if ([NSString isEmptyString:storeName]) {
                        cell.detailsLabel.text = @"-";
                    } else {
                        cell.detailsLabel.text = storeName;
                    }
                    cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                }
                    break;
                    
                case 1:
                {
                    cell.titleLabel.text = @"所属城市运营：";
                    NSString *rootChannelString = [self.dailyDetailsDict notNullObjectForKey:@"rootChannel"];
                    if ([NSString isEmptyString:rootChannelString]) {
                        cell.detailsLabel.text = @"-";
                    } else {
                        cell.detailsLabel.text = rootChannelString;
                    }
                    cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                }
                    break;
                    
                case 2:
                {
                    cell.titleLabel.text = @"所属分销商：";
                    NSString *string = [self.dailyDetailsDict notNullObjectForKey:@"secondChannel"];
                    if ([NSString isEmptyString:string]) {
                        cell.detailsLabel.text = @"无";
                        cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
                    } else {
                        cell.detailsLabel.text = string;
                        cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                    }
                }
                    break;
            }
        }
            break;
            
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    cell.titleLabel.text = @"当日订单量：";
                    cell.detailsLabel.text = [NSString stringWithFormat:@"%@", [self.dailyDetailsDict notNullObjectForKey:@"orderCount"]];
                    cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                }
                    break;
                    
                case 1:
                {
                    cell.titleLabel.text = @"当日销售额：";
                    cell.detailsLabel.text = [NSString stringWithFormat:@"￥%@", [self.dailyDetailsDict notNullObjectForKey:@"orderAmount"]];
                    cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
                }
                    break;
                    
                case 2:
                {
                    cell.titleLabel.text = @"微信新会员：";
                    cell.detailsLabel.text = [NSString stringWithFormat:@"%@", [self.dailyDetailsDict notNullObjectForKey:@"userCount"]];
                    cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                }
                    break;
                    
                case 3:
                {
                    cell.titleLabel.text = @"新增销售顾问：";
                    cell.detailsLabel.text = [NSString stringWithFormat:@"%@", [self.dailyDetailsDict notNullObjectForKey:@"staffCount"]];
                    cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                }
                    break;
                    
                case 4:
                {
                    cell.titleLabel.text = @"新增兼职销售顾问：";
                    cell.detailsLabel.text = [NSString stringWithFormat:@"%@", [self.dailyDetailsDict notNullObjectForKey:@"parttimeCount"]];
                    cell.detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
                }
                    break;
            }
            break;
        }
    }
    
    if (indexPath.row %2 == 0) {
        cell.backgroundColor = [UIColor whiteColor];
    } else {
        cell.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    }
    
    return cell;
}

@end
