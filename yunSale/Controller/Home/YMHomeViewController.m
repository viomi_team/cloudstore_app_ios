//
//  YMHomeViewController.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/2.
//  Copyright © 2016年 yunmi. All rights reserved.
//


#import "YMHomeViewController.h"
#import "YMOrderProfileViewController.h"
#import "YMSalesProfileViewController.h"
#import "YMSalesDatasViewController.h"
#import "YMOrderDatasViewController.h"
#import "YMStoreDailyViewController.h"
#import "YMAdPayOrderViewController.h"
#import "YMDealerReportViewController.h"
#import "YMCommodityDataViewController.h"
#import <SDImageCache.h>
#import "YMLoginViewController.h"
//#import "YMNavigationController.h"
#import <YMCommon/YMCBaseNavigationController.h>
#import "YMImageButton.h"
#import "YMImagesLoopView.h"
#import "YMOrderAPI.h"
#import <MJRefresh/MJRefresh.h>
#import "YMUserAPI.h"
//#import "NSNumber+ym.h"

#import <MBProgressHUD.h>
#import <YMCommon/NSNumber+ymc.h>
#import <YMCommon/YMCAppUpdateAPI.h>

#import <UMMobClick/MobClick.h>

typedef NS_ENUM(NSUInteger,YMFunType) {
    YMFunTypeSalesDatas = 0,
    YMFunTypeOrderDatas,
    YMFunTypeStoreDaily,
    YMFunTypeSalesProfile,
    YMFunTypeOrderProfile,
    YMFunTypeDealerReport,
    YMFunTypeAdPayOrder,
    YMFunTypeCommodityData,
    YMFunTypeOther,
};

#define YM_FUNVIEW_HEIGHT [YMCScreenAdapter sizeBy750:420]


@interface YMHomeViewController ()


@property (nonatomic, strong) UIScrollView *containerScrollView;

@property (nonatomic, strong) YMImagesLoopView *imagesLooplView;
//今日订单数量
@property (nonatomic, strong) UILabel *dayOrderCountLabel;
//今日销售总额
@property (nonatomic, strong) UILabel *dayMoneyLabel;
//本月订单数量
@property (nonatomic, strong) UILabel *monthOrderCountLabel;
//本月销售总额
@property (nonatomic, strong) UILabel *monthMoneyLabel;

@property (nonatomic, assign) BOOL needLoadData;

@property (nonatomic, strong) YMOrderAPI *dayAPI;
@property (nonatomic, strong) YMOrderAPI *monthAPI;

@property (nonatomic, strong) YMCAppUpdateAPI *appUpdateAPI;
@property (nonatomic, strong) YMUserAPI *userAPI;

@property (nonatomic, copy) NSArray *funArray;

@property (nonatomic, strong) UIView *funsView;

@property (nonatomic, assign) BOOL needUpdateFunsView;

@end

@implementation YMHomeViewController

#pragma mark life

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.needLoadData = YES;
    self.needUpdateFunsView = YES;
    
    [self updateToken];
    
    [self drawView];
    
    //下拉刷新
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(p_loadData)];
    header.lastUpdatedTimeLabel.hidden = YES;
    self.containerScrollView.mj_header = header;
    
    [self p_loadImages];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginNotificationEvent) name:@"ym_login_event" object:nil];
    
    //检测升级
    self.appUpdateAPI = [[YMCAppUpdateAPI alloc] init];
    [self.appUpdateAPI checkVersion:YM_APPID];
    
}

- (void)dealloc {
    NSLog(@"%s",__func__);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (self.dayAPI) {
        [self.dayAPI cancle];
    }
    if (self.monthAPI) {
        [self.monthAPI cancle];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //必须要登录才能操作其他
    if (![[YMUserManager shareinstance] islogin]) {
        YMLoginViewController *controller = [[YMLoginViewController alloc] init];
        YMCBaseNavigationController *nav = [[YMCBaseNavigationController alloc] initWithRootViewController:controller];
        [self presentViewController:nav animated:NO completion:nil];
    }else{
        //zcm add：友盟主页统计
        [MobClick event:@"Home"];
        
        //开始轮播图片
        [self.imagesLooplView startTimer];
        
        if (self.needLoadData) {
            [self p_loadData];
        }
        
        if (self.needUpdateFunsView) {
            [self updateFuns];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //停止轮播图片
    [self.imagesLooplView stopTimer];
}


#pragma mark draw
- (void)drawView {
    //容器视图
    self.containerScrollView = [[UIScrollView alloc] init];
    self.containerScrollView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    self.containerScrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.containerScrollView];
    [self.containerScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    //图片循环
    self.imagesLooplView = [[YMImagesLoopView alloc] init];
    [self.containerScrollView addSubview:self.imagesLooplView];
    [self.imagesLooplView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.top.equalTo(self.containerScrollView).with.offset(0);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:360]);
    }];
    
    
    //本日
    UIView *dayView = [[UIView alloc] init];
    dayView.backgroundColor = [UIColor whiteColor];
    [self.containerScrollView addSubview:dayView];
    [dayView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.top.equalTo(self.imagesLooplView.mas_bottom).with.offset(0);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:180]);
    }];
    
    UITapGestureRecognizer *dayGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dayGestureAction)];
    [dayView addGestureRecognizer:dayGesture];
    
    self.dayOrderCountLabel = [self p_redLabel];
    [dayView addSubview:self.dayOrderCountLabel];
    self.dayOrderCountLabel.text = @"-";
    
    self.dayMoneyLabel = [self p_redLabel];
    [dayView addSubview:self.dayMoneyLabel];
    self.dayMoneyLabel.text = @"-";
    
    UILabel *dayOrderTipLabel = [self p_grayLabel];
    dayOrderTipLabel.text = @"昨日订单";
    [dayView addSubview:dayOrderTipLabel];
    
    UILabel *dayMoneyTipLabel = [self p_grayLabel];
    dayMoneyTipLabel.text = @"昨日销售";
    [dayView addSubview:dayMoneyTipLabel];
    
    [self.dayOrderCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(dayOrderTipLabel.mas_left);
        make.top.equalTo(dayView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
    }];
    [self.dayMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(dayMoneyTipLabel.mas_left);
        make.top.equalTo(dayView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
    }];
    
    [dayOrderTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.dayOrderCountLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.left.equalTo(dayView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
    }];
    [dayMoneyTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.dayMoneyLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.left.equalTo(dayView.mas_centerX).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
    }];
    
    //本月
    UIView *monthOrderView = [[UIView alloc] init];
    monthOrderView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    [self.containerScrollView addSubview:monthOrderView];
    [monthOrderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.top.equalTo(dayOrderTipLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
    }];
    
    UITapGestureRecognizer *monthGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(monthGestureAction)];
    [monthOrderView addGestureRecognizer:monthGesture1];
    
    UILabel *monthOrderTipLabel = [self p_grayLabel];
    monthOrderTipLabel.text = @"本月订单";
    [monthOrderView addSubview:monthOrderTipLabel];
    [monthOrderTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(monthOrderView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.centerY.equalTo(monthOrderView);
    }];
    
    self.monthOrderCountLabel = [self p_themeLabel];
    self.monthOrderCountLabel.text = @"-";
    [monthOrderView addSubview:self.monthOrderCountLabel];
    [self.monthOrderCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(monthOrderView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.centerY.equalTo(monthOrderView);
    }];
    
    UIView *monthMoneyView = [[UIView alloc] init];
    monthMoneyView.backgroundColor = [UIColor whiteColor];
    [self.containerScrollView addSubview:monthMoneyView];
    [monthMoneyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.top.equalTo(monthOrderView.mas_bottom).with.offset(0);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
    }];
    
    UITapGestureRecognizer *monthGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(monthGestureAction)];
    [monthMoneyView addGestureRecognizer:monthGesture2];
    
    UILabel *monthMoneyTipLabel = [self p_grayLabel];
    monthMoneyTipLabel.text = @"本月销售";
    [monthMoneyView addSubview:monthMoneyTipLabel];
    [monthMoneyTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(monthMoneyView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.centerY.equalTo(monthMoneyView);
    }];
    
    self.monthMoneyLabel = [self p_themeLabel];
    self.monthMoneyLabel.text = @"-";
    [monthMoneyView addSubview:self.monthMoneyLabel];
    [self.monthMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(monthMoneyView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.centerY.equalTo(monthMoneyView);
    }];
    
    //分割线
    UIView *seperatorView = [self p_seperatorView:YES];
    [self.containerScrollView addSubview:seperatorView];
    [seperatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.top.equalTo(monthMoneyView.mas_bottom).with.offset(0);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
    }];
    
    //按钮
    self.funsView = [[UIView alloc] init];
    self.funsView.backgroundColor = [UIColor whiteColor];
    [self.containerScrollView addSubview:self.funsView];
    [self.funsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.top.equalTo(seperatorView.mas_bottom).with.offset(0);
        make.height.mas_equalTo(YM_FUNVIEW_HEIGHT);
    }];
    
    //底部logo
    UIView *logoView = [self p_seperatorView:NO];
    [self.containerScrollView addSubview:logoView];
    [logoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.top.equalTo(self.funsView.mas_bottom).with.offset(0);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.bottom.equalTo(self.containerScrollView).with.offset(0);
    }];
    UIImageView *logo = [[UIImageView alloc] init];
    logo.image = [UIImage imageNamed:@"ym_index_logo"];
    [logoView addSubview:logo];
    [logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake([YMCScreenAdapter sizeBy750:180], [YMCScreenAdapter sizeBy750:30]));
        make.center.equalTo(logoView);
    }];
}

- (void)updateFuns {
    for (UIView *view in self.funsView.subviews) {
        [view removeFromSuperview];
    }
    
    
    if ([YMUserManager shareinstance].roleId.integerValue == 1){
        //超级管理员不显示门店日报
        self.funArray = [NSArray arrayWithObjects:@(YMFunTypeDealerReport), @(YMFunTypeAdPayOrder), @(YMFunTypeSalesProfile), @(YMFunTypeOrderProfile), @(YMFunTypeSalesDatas), @(YMFunTypeOrderDatas), @(YMFunTypeCommodityData), @(YMFunTypeOther), nil];
//    }else if ([YMUserManager shareinstance].roleId.integerValue == 1001) {
//        //区域管理员不显示销售概况和订单概况（后台未完成）
//        self.funArray = [NSArray arrayWithObjects:@(YMFunTypeDealerReport), @(YMFunTypeAdPayOrder), @(YMFunTypeSalesDatas), @(YMFunTypeOrderDatas), @(YMFunTypeStoreDaily), @(YMFunTypeOther), nil];
    }else {
        self.funArray = [NSArray arrayWithObjects:@(YMFunTypeDealerReport), @(YMFunTypeAdPayOrder), @(YMFunTypeSalesProfile), @(YMFunTypeOrderProfile), @(YMFunTypeSalesDatas), @(YMFunTypeOrderDatas), @(YMFunTypeStoreDaily), @(YMFunTypeOther), nil];
    }
    
    CGFloat hspace = [YMCScreenAdapter sizeBy750:60];
    CGFloat aroundSpace = [YMCScreenAdapter sizeBy750:YMSPACE_AROUND];
    CGFloat x = aroundSpace,y = 0;
    CGFloat width = ([UIScreen mainScreen].bounds.size.width - hspace * 3 - aroundSpace * 2)/4;
    CGFloat height = (YM_FUNVIEW_HEIGHT - aroundSpace)/2;
    for (NSNumber *index in self.funArray) {
        
        NSString *name = nil;
        NSString *imagename = nil;
        if (index.integerValue == YMFunTypeSalesDatas) {
            name = @"销售数据";
            imagename = @"ym_index_sales_datas";
        }else if (index.integerValue == YMFunTypeOrderDatas) {
            name = @"订单数据";
            imagename = @"ym_index_order_datas";
        }else if (index.integerValue == YMFunTypeStoreDaily) {
            name = @"门店日报";
            imagename = @"ym_index_store_daily";
        }else if (index.integerValue == YMFunTypeSalesProfile) {
            name = @"销售概况";
            imagename = @"ym_index_sales_profile";
        }else if (index.integerValue == YMFunTypeOrderProfile) {
            name = @"订单概况";
            imagename = @"ym_index_order_profile";
        }else if (index.integerValue == YMFunTypeDealerReport) {
            name = @"数据报表";
            imagename = @"ym_index_dealer_report";
        }else if (index.integerValue == YMFunTypeAdPayOrder) {
            name = @"预付款提货";
            imagename = @"ym_index_adpay"; //待定
        }else if (index.integerValue == YMFunTypeCommodityData) {
            name = @"商品数据";
            imagename = @"ym_index_commdity_data"; 
        }else {
            name = @"即将开启";
            imagename = @"ym_index_viomi";
        }
        
        YMImageButton *btn = [[YMImageButton alloc] initWithTitle:name image:[UIImage imageNamed:imagename] style:YMImageButtonStyleDefault];
        btn.frame = CGRectMake(x, y, width, height);
        [btn addTarget:self action:@selector(funAction:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = index.integerValue;
        [self.funsView addSubview:btn];
        
        x += width + hspace;
        if (x > [UIScreen mainScreen].bounds.size.width) {
            x = aroundSpace;
            y += height;
        }
    }
    self.needUpdateFunsView = NO;
}

#pragma mark - Private view
- (UIView *)p_seperatorView:(BOOL)showBottomLine {
    UIView *seperatorView = [[UIView alloc] init];
    seperatorView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    
    UIView *lineView1 = [[UIView alloc] init];
    lineView1.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [seperatorView addSubview:lineView1];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(seperatorView).with.offset(0);
        make.right.equalTo(seperatorView).with.offset(0);
        make.top.equalTo(seperatorView).with.offset(0);
        make.height.mas_equalTo(0.5);
    }];
    if (showBottomLine) {
        UIView *lineView2 = [[UIView alloc] init];
        lineView2.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [seperatorView addSubview:lineView2];
        [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(seperatorView).with.offset(0);
            make.right.equalTo(seperatorView).with.offset(0);
            make.bottom.equalTo(seperatorView).with.offset(0);
            make.height.mas_equalTo(0.5);
        }];
    }
    return seperatorView;
}

- (UILabel *)p_redLabel {
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
    label.font = YM_FONT([YMCScreenAdapter fontsizeBy750:48]);
    return label;
}

- (UILabel *)p_grayLabel {
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
    label.font = YM_FONT([YMCScreenAdapter fontsizeBy750:28]);
    return label;
}

- (UILabel *)p_themeLabel {
    UILabel *label = [[UILabel alloc] init];
    label.textColor = [UIColor colorWithHexString:YMCOLOR_THEME];
    label.font = YM_FONT([YMCScreenAdapter fontsizeBy750:28]);
    return label;
}

#pragma mark - Private Action
- (void)funAction:(UIButton *)button
{
    if (button.tag == YMFunTypeSalesDatas) {
        YMSalesDatasViewController *controller = [[YMSalesDatasViewController alloc] init];
        [controller setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:controller animated:YES];
    }else if (button.tag == YMFunTypeOrderDatas) {
        YMOrderDatasViewController *controller = [[YMOrderDatasViewController alloc] init];
        [controller setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:controller animated:YES];
    }else if (button.tag == YMFunTypeStoreDaily) {
        YMStoreDailyViewController *controller = [[YMStoreDailyViewController alloc] init];
        [controller setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:controller animated:YES];
    }else if (button.tag == YMFunTypeSalesProfile) {
        YMSalesProfileViewController *controller = [[YMSalesProfileViewController alloc] init];
        [controller setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:controller animated:YES];
    }else if (button.tag == YMFunTypeOrderProfile) {
        YMOrderProfileViewController *controller = [[YMOrderProfileViewController alloc] init];
        [controller setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:controller animated:YES];
    }else if (button.tag == YMFunTypeDealerReport){
        YMDealerReportViewController *controller = [[YMDealerReportViewController alloc] init];
        [controller setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:controller animated:YES];
    }else if (button.tag == YMFunTypeAdPayOrder){
        YMAdPayOrderViewController *controller = [[YMAdPayOrderViewController alloc] init];
        [controller setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:controller animated:YES];
    }else if (button.tag == YMFunTypeCommodityData){
        YMCommodityDataViewController *controller = [[YMCommodityDataViewController alloc] init];
        [controller setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:controller animated:YES];
    }else {
        
    }
}

//
//- (void)p_saleDataAction {
//    YMSalesDatasViewController *controller = [[YMSalesDatasViewController alloc] init];
//    [controller setHidesBottomBarWhenPushed:YES];
//    [self.navigationController pushViewController:controller animated:YES];
//}
//
//- (void)p_orderDataAction {
//    YMOrderDatasViewController *controller = [[YMOrderDatasViewController alloc] init];
//    [controller setHidesBottomBarWhenPushed:YES];
//    [self.navigationController pushViewController:controller animated:YES];
//}
//
//- (void)p_storeDailyAction {
//    YMStoreDailyViewController *controller = [[YMStoreDailyViewController alloc] init];
//    [controller setHidesBottomBarWhenPushed:YES];
//    [self.navigationController pushViewController:controller animated:YES];
//}
//
//- (void)p_orderProfileAction {
//    YMOrderProfileViewController *controller = [[YMOrderProfileViewController alloc] init];
//    [controller setHidesBottomBarWhenPushed:YES];
//    [self.navigationController pushViewController:controller animated:YES];
//}
//
//- (void)p_saleProfileAction {
//    
//    YMSalesProfileViewController *controller = [[YMSalesProfileViewController alloc] init];
//    [controller setHidesBottomBarWhenPushed:YES];
//    [self.navigationController pushViewController:controller animated:YES];
//}

- (void)p_loadImages {
    self.imagesLooplView.images = [NSArray arrayWithObjects:@"ym_barner_X5",@"ym_barner_X3",@"ym_barner_V1",nil];
}

//获取订单、销售数据
- (void)p_loadData {
#warning 该接口经常访问失败.....同一个用户不能并发。。。日
    if (!self.dayAPI) {
        self.dayAPI = [[YMOrderAPI alloc] init];
    }
    //当天
    [self.dayAPI fetchSalesAnalyse:YMProfileTypeDay completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        
        if (status == YMCRequestSuccess) {
            //角色不同要做不同处理
            NSDictionary *dailyOverAllDict = nil;
            
            if ([[YMUserManager shareinstance].roleId integerValue] == 21) {
                dailyOverAllDict = [data notNullObjectForKey:@"result"];
            } else {
                dailyOverAllDict = [data notNullObjectForKey:@"overAll"];
            }
            
            if (!dailyOverAllDict) {
                return;
            }
            
            NSString *dailyOrderCount = [[dailyOverAllDict notNullObjectForKey:@"orderCount"] stringValue];
            if (![NSString isEmptyString:dailyOrderCount]) {
                self.dayOrderCountLabel.text = [NSString stringWithFormat:@"%@", dailyOrderCount];
            }
            
            NSNumber *dailyAmount = [dailyOverAllDict notNullObjectForKey:@"turnOver"];
            if (dailyAmount) {
                NSMutableAttributedString *symbolString = [[NSMutableAttributedString alloc] initWithString:@"￥" attributes:@{NSFontAttributeName : YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE]), NSForegroundColorAttributeName : [UIColor colorWithHexString:YMCOLOR_RED]}];
                NSAttributedString *salesAmount = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [NSNumber formatNumberStringWithNumber:dailyAmount]] attributes:@{NSFontAttributeName : YM_FONT([YMCScreenAdapter fontsizeBy750:48]), NSForegroundColorAttributeName : [UIColor colorWithHexString:YMCOLOR_RED]}];
                [symbolString appendAttributedString:salesAmount];
                
                self.dayMoneyLabel.attributedText = symbolString;
            }
            
            NSLog(@"day");
        }
        
        //当月
        if (!self.monthAPI) {
            self.monthAPI = [[YMOrderAPI alloc] init];
        }
        
        [self.monthAPI fetchSalesAnalyse:YMProfileTypeMonth completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
            
            [self.containerScrollView.mj_header endRefreshing];
            
            if (status == YMCRequestSuccess) {
                
                //角色不同要做不同处理
                NSDictionary *monthlyOverAllDict = nil;
                
                if ([[YMUserManager shareinstance].roleId integerValue] == 21)                 {
                    monthlyOverAllDict = [data notNullObjectForKey:@"result"];
                } else {
                    monthlyOverAllDict = [data notNullObjectForKey:@"overAll"];
                }
                
                if (!monthlyOverAllDict) {
                    return;
                }
                
                NSString *monthlyOrderCount = [[monthlyOverAllDict notNullObjectForKey:@"orderCount"] stringValue];
                if (![NSString isEmptyString:monthlyOrderCount]) {
                    self.monthOrderCountLabel.text = [NSString stringWithFormat:@"%@", monthlyOrderCount];
                }
                
                NSNumber *monthlyAmount = [monthlyOverAllDict notNullObjectForKey:@"turnOver"];
                if (monthlyAmount) {
                    self.monthMoneyLabel.text = [NSString stringWithFormat:@"¥%@", [NSNumber formatNumberStringWithNumber:monthlyAmount]];
                }
                NSLog(@"month");
            }
        }];
    }];
}

//启动app，更新token
- (void)updateToken {
    if ([YMUserManager shareinstance].account) {
        self.userAPI = [[YMUserAPI alloc] init];
        
        [self.userAPI loginWithUserName:[YMUserManager shareinstance].account password:[YMUserManager shareinstance].password completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
            if (status == YMCRequestNetError) {
                
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"密码已修改，请重新登录" preferredStyle:UIAlertControllerStyleAlert];
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [[YMUserManager shareinstance] logout];
                    
                    YMLoginViewController *loginVC = [[YMLoginViewController alloc] init];
                    YMCBaseNavigationController *naviVC = [[YMCBaseNavigationController alloc] initWithRootViewController:loginVC];
                    [self presentViewController:naviVC animated:YES completion:nil];
                }]];
                [self presentViewController:alertController animated:YES completion:^{
                    
                }];
            }
        }];
    }
}

//添加手势操作的方法（by 谢立颖 2017,1,16）
- (void)dayGestureAction {
    YMSalesDatasViewController *controller = [[YMSalesDatasViewController alloc] init];
    [controller setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)monthGestureAction {
    YMSalesDatasViewController *controller = [[YMSalesDatasViewController alloc] init];
    controller.salesDatasType = YMSalesDatasTypeMonth;
    [controller setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - Notification
- (void)loginNotificationEvent
{
    self.needLoadData = YES;
    self.needUpdateFunsView = YES;
    self.dayOrderCountLabel.text = @"-";
    self.dayMoneyLabel.text = @"-";
    self.monthOrderCountLabel.text = @"-";
    self.monthMoneyLabel.text = @"-";
    
    [self p_loadData];
}

@end
