//
//  YMImagesLoopView.h
//  yunSale
//
//  Created by liushilou on 16/11/17.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger,YMLoopViewImageType) {
    YMLoopViewImageTypeName,
    YMLoopViewImageTypeUrl,
    YMLoopViewImageTypeImage
};


@interface YMImagesLoopView : UIView

@property (nonatomic,assign) YMLoopViewImageType type;

@property (nonatomic,copy) NSArray *images;

- (instancetype)initWithType:(YMLoopViewImageType)type images:(NSArray *)images;

//暂停timer
- (void)stopTimer;
//启动timer
- (void)startTimer;

@end
