//
//  YMImagesLoopView.m
//  yunSale
//
//  Created by liushilou on 16/11/17.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMImagesLoopView.h"
#import <MSWeakTimer.h>
#import <UIImageView+WebCache.h>

@interface YMImagesLoopView()<UIScrollViewDelegate>

@property (nonatomic,strong) UIScrollView *scrollView;

@property (nonatomic,assign) NSInteger currentIndex;

@property (nonatomic,strong) UIImageView *leftImageView;
@property (nonatomic,strong) UIImageView *centerImageView;
@property (nonatomic,strong) UIImageView *rightImageView;

@property (nonatomic,strong) UIView *tabsView;

@property (nonatomic,strong) MSWeakTimer *timer;

//timer手动计数，避免手动滚动图片后，又马上切换图片
@property (nonatomic,assign) NSInteger timerCount;

@end

@implementation YMImagesLoopView

- (instancetype)initWithType:(YMLoopViewImageType)type images:(NSArray *)images {
    self = [super init];
    if (self) {
        
        _type = type;
        _images = [images copy];
        
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        [self addSubview:_scrollView];
        [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        _leftImageView = [[UIImageView alloc] init];
        _leftImageView.backgroundColor = [UIColor clearColor];
        [_scrollView addSubview:_leftImageView];
        _centerImageView = [[UIImageView alloc] init];
        _centerImageView.backgroundColor = [UIColor clearColor];
        [_scrollView addSubview:_centerImageView];
        _rightImageView = [[UIImageView alloc] init];
        _rightImageView.backgroundColor = [UIColor clearColor];
        [_scrollView addSubview:_rightImageView];
        
        _tabsView = [[UIView alloc] init];
        _tabsView.backgroundColor = [UIColor clearColor];
        [self addSubview:_tabsView];
        [_tabsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.bottom.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:-20]);
            make.size.mas_equalTo(CGSizeMake(0, [YMCScreenAdapter sizeBy750:8]));
        }];
        
        //有人说使用这种方式创建timer,滚动scrollView的时候NSTimer停止了工作。正好省了手动停止。。（实测：只是在滚动时不执行selector，时间还是会计算的。假如时间到了，滚动一结束，就会立马执行selector）
        //        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(p_timer) userInfo:nil repeats:true];
        //
        //        [_timer setFireDate:[NSDate distantFuture]];
        
        _timerCount = 0;
        
        [self p_loadImage];
    }
    return self;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _type = YMLoopViewImageTypeName;
        _images = [NSArray new];
        
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.bounces = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        [self addSubview:_scrollView];
        [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        _leftImageView = [[UIImageView alloc] init];
        _leftImageView.backgroundColor = [UIColor clearColor];
        [_scrollView addSubview:_leftImageView];
        _centerImageView = [[UIImageView alloc] init];
        _centerImageView.backgroundColor = [UIColor clearColor];
        [_scrollView addSubview:_centerImageView];
        _rightImageView = [[UIImageView alloc] init];
        _rightImageView.backgroundColor = [UIColor clearColor];
        [_scrollView addSubview:_rightImageView];
        
        _tabsView = [[UIView alloc] init];
        _tabsView.backgroundColor = [UIColor clearColor];
        [self addSubview:_tabsView];
        [_tabsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.bottom.equalTo(self).with.offset([YMCScreenAdapter sizeBy750:-20]);
            make.size.mas_equalTo(CGSizeMake(0, [YMCScreenAdapter sizeBy750:8]));
        }];
        
        //有人说使用这种方式创建timer,滚动scrollView的时候NSTimer停止了工作。正好省了手动停止。。（实测：只是在滚动时不执行selector，时间还是会计算的。假如时间到了，滚动一结束，就会立马执行selector）
        //        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(p_timer) userInfo:nil repeats:true];
        //
        //        [_timer setFireDate:[NSDate distantFuture]];
        
        _timerCount = 0;
        
    }
    return self;
}

- (void)dealloc {
    NSLog(@"%s",__func__);
    [self.timer invalidate];
}


- (void)drawRect:(CGRect)rect {
    _scrollView.contentSize = CGSizeMake(3*rect.size.width, 0);
    _leftImageView.frame = CGRectMake(0, 0, rect.size.width, rect.size.height);
    _centerImageView.frame = CGRectMake(rect.size.width, 0, rect.size.width, rect.size.height);
    _rightImageView.frame = CGRectMake(rect.size.width * 2, 0, rect.size.width, rect.size.height);
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    self.timerCount = 0;
    
    CGPoint offset = scrollView.contentOffset;
    CGFloat width = self.frame.size.width;
    if (offset.x == 0) {
        self.currentIndex -= 1;
    }else if (offset.x == width) {
        //不变
    }else{
        self.currentIndex += 1;
    }
    
    if (self.currentIndex < 0) {
        self.currentIndex = self.images.count - 1;
    }
    if (self.currentIndex > self.images.count - 1) {
        self.currentIndex = 0;
    }
    
    
    [self p_reloadImage];
}

- (void)setImages:(NSArray *)images {
    _images = [images copy];
    
    for (UIView *view in self.tabsView.subviews) {
        [view removeFromSuperview];
    }
    
    [self p_loadImage];
    //    CGFloat tabWidth = [YMScreenAdapter intergerSizeBy750:40];
    //    CGFloat tabSpace = [YMScreenAdapter intergerSizeBy750:20];
    //    CGFloat tabsWidth = images.count * tabWidth + (images.count - 1) * tabSpace;
    //    [_tabsView mas_updateConstraints:^(MASConstraintMaker *make) {
    //        make.width.mas_equalTo(tabsWidth);
    //    }];
    //
    //    CGFloat x = 0;
    //    for (NSInteger i = 0;i < images.count; i++) {
    //        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(x, 0, tabWidth, [YMScreenAdapter sizeBy750:8])];
    //        view.backgroundColor = [UIColor whiteColor];
    //        view.layer.cornerRadius = [YMScreenAdapter sizeBy750:4];
    //        view.tag = i;
    //        [self.tabsView addSubview:view];
    //
    //        x += tabWidth + tabSpace;
    //    }
    //
    //    _currentIndex = 0;
    //    [self p_reloadImage];
}

#pragma private method

- (void)p_loadImage {
    CGFloat tabWidth = [YMCScreenAdapter intergerSizeBy750:40];
    CGFloat tabSpace = [YMCScreenAdapter intergerSizeBy750:20];
    CGFloat tabsWidth = self.images.count * tabWidth + (self.images.count - 1) * tabSpace;
    [_tabsView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(tabsWidth);
    }];
    
    if (self.images.count > 1) {
        self.scrollView.scrollEnabled = YES;
        CGFloat x = 0;
        for (NSInteger i = 0;i < self.images.count; i++) {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(x, 0, tabWidth, [YMCScreenAdapter sizeBy750:8])];
            view.backgroundColor = [UIColor whiteColor];
            view.layer.cornerRadius = [YMCScreenAdapter sizeBy750:4];
            view.tag = i;
            [self.tabsView addSubview:view];
            
            x += tabWidth + tabSpace;
        }
    } else {
        self.scrollView.scrollEnabled = NO;
    }

    _currentIndex = 0;
    [self p_reloadImage];
}

- (void)p_reloadImage {
    id centerImagename = [self.images objectAtIndex:self.currentIndex];
    
    NSInteger leftIndex = self.currentIndex - 1;
    if (leftIndex < 0) {
        leftIndex = self.images.count - 1;
    }
    id leftImagename = [self.images objectAtIndex:leftIndex];
    
    NSInteger rightIndex = self.currentIndex + 1;
    if (rightIndex > self.images.count - 1) {
        rightIndex = 0;
    }
    
    id rightImagename = [self.images objectAtIndex:rightIndex];
    
    if (self.type == YMLoopViewImageTypeName) {
        self.leftImageView.image = [UIImage imageNamed:leftImagename];
        self.centerImageView.image = [UIImage imageNamed:centerImagename];
        self.rightImageView.image = [UIImage imageNamed:rightImagename];
    } else if (self.type == YMLoopViewImageTypeUrl) {
        [self.leftImageView sd_setImageWithURL:[NSURL fileURLWithPath:leftImagename] placeholderImage:[UIImage imageNamed:@"ym_banner1.jpg"]];
        [self.centerImageView sd_setImageWithURL:[NSURL fileURLWithPath:centerImagename] placeholderImage:[UIImage imageNamed:@"ym_banner2.jpg"]];
        [self.rightImageView sd_setImageWithURL:[NSURL fileURLWithPath:rightImagename] placeholderImage:[UIImage imageNamed:@"ym_banner3.jpg"]];
        
        [SDWebImageDownloader.sharedDownloader setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:leftImagename] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            if (image) {
                self.leftImageView.image = image;
            }
        }];
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:centerImagename] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            if (image) {
                self.centerImageView.image = image;
            }
        }];
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:rightImagename] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            if (image) {
                self.rightImageView.image = image;
            }
        }];
        
    } else if (self.type == YMLoopViewImageTypeImage) {
        self.leftImageView.image = leftImagename;
        self.centerImageView.image = centerImagename;
        self.rightImageView.image = rightImagename;
    }
    
    for (UIView *tabview in self.tabsView.subviews) {
        if (tabview.tag == self.currentIndex) {
            tabview.alpha = 0.6;
        }else{
            tabview.alpha = 0.2;
        }
    }
    CGFloat width = self.frame.size.width;
    [_scrollView setContentOffset:CGPointMake(width, 0)];
}

- (void)p_timer {
    //NSLog(@"1111");
    self.timerCount += 1;
    if (self.timerCount >= 3) {
        
        self.timerCount = 0;
        
        self.currentIndex += 1;
    }
}
//暂停timer
- (void)stopTimer {
    [self.timer invalidate];
}
//启动timer
- (void)startTimer {
    self.timer = [MSWeakTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(p_timer) userInfo:nil repeats:YES dispatchQueue:dispatch_get_main_queue()];
}

@end
