//
//  YMDealerManagerDetailViewController.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/30.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMDealerManagerDetailViewController : UIViewController

@property (nonatomic, copy) NSDictionary *dealerDetailDict;

@end
