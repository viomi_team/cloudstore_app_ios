//
//  YMDealerManagerDetailViewController.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/30.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMDealerManagerDetailViewController.h"
#import "YMDealerManagerHeaderView.h"
#import "YMTwoItemTableViewCell.h"
#import <UMMobClick/MobClick.h>
#import "YMManagerAPI.h"

@interface YMDealerManagerDetailViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) YMDealerManagerHeaderView *imageLoopHeaderView;
//门店电话
@property (nonatomic, strong) UILabel *storePhoneLabel;
//店员人数
@property (nonatomic, strong) UILabel *staffCountLabe;
//联系人
@property (nonatomic, strong) UILabel *userNameLabel;
//联系电话
@property (nonatomic, strong) UILabel *userPhoneLabel;
//当前提成模板
@property (nonatomic, strong) UILabel *profitTemplateLabel;

@property (nonatomic, strong) YMManagerAPI *managerAPI;

@property (nonatomic, assign) NSInteger imageId;

@end

@implementation YMDealerManagerDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"经销商详情";
    
    [self drawView];
    
    //初始化headerView，配置 headerView 数据
    self.imageId = [[self.dealerDetailDict notNullObjectForKey:@"id"] integerValue];
    NSDictionary *headerViewDict = @{@"dealerName":[self.dealerDetailDict notNullObjectForKey:@"name"],
                                     @"channelLevel":[self.dealerDetailDict notNullObjectForKey:@"channelLevel"],
                                     @"category":[self.dealerDetailDict notNullObjectForKey:@"category"],
                                     @"approveStatusDesc":[self.dealerDetailDict notNullObjectForKey:@"approveStatusDesc"],
                                     @"statusDesc":[self.dealerDetailDict notNullObjectForKey:@"statusDesc"]
                                     };
    self.imageLoopHeaderView = [[YMDealerManagerHeaderView alloc] init];
    [self.imageLoopHeaderView configDataWithDealerManagerDict:headerViewDict];
    self.imageLoopHeaderView.imagesLoopView.type = YMLoopViewImageTypeName;
    self.imageLoopHeaderView.imagesLoopView.images = [NSArray arrayWithObjects:@"ym_store_default",nil];
    
    [self fetchImageURL];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.imageLoopHeaderView.imagesLoopView startTimer];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.imageLoopHeaderView.imagesLoopView stopTimer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Draw
- (void)drawView {
    self.view.backgroundColor = [UIColor whiteColor];
    
    //tableView
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    switch (section) {
//        case 0:
//            return 0;
//            break;
//            
//        case 1:
//            return 3;
//            break;
//            
//        case 2:
//            return 1;
//            break;
//            
//        default:
//            return 0;
//            break;
//    }
    return section == 0? 0 : section == 1? 3 : section == 2? 1 : 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [YMCScreenAdapter intergerSizeBy750:100];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return [YMScreenAdapter intergerSizeBy750:528];
//    } else {
//        return 0;
//    }
    return section == 0? [YMCScreenAdapter intergerSizeBy750:528] : 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    switch (section) {
//        case 0:
//            return [YMScreenAdapter intergerSizeBy750:YMSPACE_AROUND];
//            break;
//            
//        case 1:
//            return [YMScreenAdapter intergerSizeBy750:YMSPACE_AROUND];
//            
//        default:
//            return 0.5;
//            break;
//    }
    return (section == 0 || section == 1)? [YMCScreenAdapter intergerSizeBy750:YMSPACE_AROUND] : 0.5;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return self.imageLoopHeaderView;
//    } else {
//        return nil;
//    }
    
    return section == 0? self.imageLoopHeaderView : nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    switch (section) {
//        case 0: {
//            return [UIView drawLineViewWithWidth:YMSCREEN_WIDTH height:[YMScreenAdapter sizeBy750:24] color:[UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND]];
//        }
//            break;
//            
//        case 1:{
//            return [UIView drawLineViewWithWidth:YMSCREEN_WIDTH height:[YMScreenAdapter sizeBy750:24] color:[UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND]];
//        }
//            
//        default:
//            return nil;
//            break;
//    }
    return (section == 0 || section == 1)? [UIView drawLineViewWithWidth:YMSCREEN_WIDTH height:[YMCScreenAdapter sizeBy750:24] color:[UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND]] : nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        //返回 nil 会有内存泄漏报错，因此改为[UITableViewCell new]，实际上不会执行到这一步
        return [UITableViewCell new];
    } else {
        NSString *reuseIdentifier = @"dealerManagerDetailCell";
        YMTwoItemTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (!cell) {
            cell = [[YMTwoItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier Style:YMTwoItemCellStyleLeft];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //配置数据
        switch (indexPath.section) {
            case 1:             {
                switch (indexPath.row) {
                    case 0: {
                        cell.titleLabel.text = @"店员数";
                        cell.detailsLabel.text = [NSString stringWithFormat:@"%@ 人", [self.dealerDetailDict notNullObjectForKey:@"staffNum"]];
                    }
                        break;
                        
                    case 1: {
                        cell.titleLabel.text = @"联系人";
                        NSString *nameString = [self.dealerDetailDict notNullObjectForKey:@"contactName"];
                        if (![NSString isEmptyString:nameString]) {
                            cell.detailsLabel.text = nameString;
                        }
                        
                    }
                        break;
                        
                    case 2: {
                        cell.titleLabel.text = @"联系电话";
                        
                        NSString *phoneString = [self.dealerDetailDict notNullObjectForKey:@"contactMobile"];
                        //当 phoneString 为 nil 时，用 isEmpty 方法会判断错误？怀疑为传入参数有问题
                        if (![NSString isEmptyString:phoneString]) {
                            cell.detailsLabel.text = phoneString;
                        }
                    }
                        break;
                }
            }
                break;
                
            case 2: {
                cell.titleLabel.text = @"当前提成模板";
                NSString *templateNameString = [self.dealerDetailDict notNullObjectForKey: @"templateName"];
                if (![NSString isEmptyString:templateNameString]) {
                    cell.detailsLabel.text = templateNameString;
                }
            }
                break;
        }
        
//        if (indexPath.row%2 == 0) {
//            cell.backgroundColor = [UIColor whiteColor];
//        } else {
//            cell.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
//        }
        
        cell.backgroundColor = indexPath.row%2 == 0 ? [UIColor whiteColor] : [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        
        return cell;
    }
}

#pragma mark - Fetch
//获取经销商相关的图片并展示在轮播图里
- (void)fetchImageURL {
    if (!self.managerAPI) {
        self.managerAPI = [[YMManagerAPI alloc] init];
    }
    
    [self.managerAPI fetchImageUrlWithId:self.imageId completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        if (status == YMCRequestSuccess) {
            NSDictionary *dict = [data notNullObjectForKey:@"result"];
            if (dict) {
                NSMutableArray *urlArray = [[NSMutableArray alloc] init];
                
                //营业执照
                NSString *urlString1 = [dict notNullObjectForKey:@"businessLicense"];
                //组织机构代码证
                NSString *urlString2 = [dict notNullObjectForKey:@"organization"];
                //税务登记证
                NSString *urlString3 = [dict notNullObjectForKey:@"taxRegistration"];
                //门店正面照
                NSString *urlString4 = [dict notNullObjectForKey:@"baseImg"];
                //店头照
                NSString *urlString5 = [dict notNullObjectForKey:@"titleImg"];
                //门牌与店主合照
                NSString *urlString6 = [dict notNullObjectForKey:@"unionImg"];
                
                if (![NSString isEmptyString:urlString1]) {
                    [urlArray addObject:urlString1];
                }
                if (![NSString isEmptyString:urlString2]) {
                    [urlArray addObject:urlString2];
                }
                if (![NSString isEmptyString:urlString3]) {
                    [urlArray addObject:urlString3];
                }
                if (![NSString isEmptyString:urlString4]) {
                    [urlArray addObject:urlString4];
                }
                if (![NSString isEmptyString:urlString5]) {
                    [urlArray addObject:urlString5];
                }
                if (![NSString isEmptyString:urlString6]) {
                    [urlArray addObject:urlString6];
                }
                
                self.imageLoopHeaderView.imagesLoopView.type = YMLoopViewImageTypeUrl;
                self.imageLoopHeaderView.imagesLoopView.images = urlArray;
            }
//           else {
//                self.imageLoopHeaderView.imagesLoopView.type = YMLoopViewImageTypeName;
//                self.imageLoopHeaderView.imagesLoopView.images = [NSArray arrayWithObjects:@"ym_banner1.jpg",@"ym_banner2.jpg",@"ym_banner3.jpg",nil];
//            }
        } else {
//            self.imageLoopHeaderView.imagesLoopView.type = YMLoopViewImageTypeName;
//            self.imageLoopHeaderView.imagesLoopView.images = [NSArray arrayWithObjects:@"ym_banner1.jpg",@"ym_banner2.jpg",@"ym_banner3.jpg",nil];
            
            //请求失败5秒后再次发起网络请求
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self fetchImageURL];
            });
        }
    }];
}

@end
