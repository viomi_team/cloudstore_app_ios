//
//  YMDealerManagerHeaderView.h
//  yunSale
//
//  Created by 谢立颖 on 2016/12/2.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMImagesLoopView.h"

@interface YMDealerManagerHeaderView : UIView

//轮播图
@property (nonatomic, strong) YMImagesLoopView *imagesLoopView;


- (id)init;

//配置数据
- (void)configDataWithDealerManagerDict:(NSDictionary *)dict;

@end
