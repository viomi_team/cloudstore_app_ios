//
//  YMDealerManagerHeaderView.m
//  yunSale
//
//  Created by 谢立颖 on 2016/12/2.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMDealerManagerHeaderView.h"

@interface YMDealerManagerHeaderView()

//用户头像
@property (nonatomic, strong) UIImageView *userIconImageView;
//经销商名称
@property (nonatomic, strong) UILabel *dealerNameLabel;
//经销商等级
@property (nonatomic, strong) UILabel *channelLevelLabel;
//角色
@property (nonatomic, strong) UILabel *roleLabel;
//状态
@property (nonatomic, strong) UILabel *statusLabel;

@end

@implementation YMDealerManagerHeaderView

- (id)init
{
    self = [super init];
    if (self) {
        _imagesLoopView = [[YMImagesLoopView alloc] init];
        _imagesLoopView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_imagesLoopView];
        [_imagesLoopView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self);
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:360]);
        }];
        
        UIView *topContainView = [[UIView alloc] init];
        [self addSubview:topContainView];
        [topContainView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_imagesLoopView.mas_bottom);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:168]);
        }];
        
        _userIconImageView = [[UIImageView alloc] init];
        _userIconImageView.layer.cornerRadius = 8;
        _userIconImageView.layer.masksToBounds = YES;
        [topContainView addSubview:_userIconImageView];
        [_userIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:180]);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:180]);
            make.left.equalTo(topContainView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROOUND_BIG]);
            make.bottom.equalTo(topContainView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROOUND_BIG]);
        }];
        
        _dealerNameLabel = [[UILabel alloc] init];
        _dealerNameLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE]);
        [topContainView addSubview:_dealerNameLabel];
        
        _channelLevelLabel = [[UILabel alloc] init];
        _channelLevelLabel.textAlignment = NSTextAlignmentCenter;
        _channelLevelLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLUE];
        _channelLevelLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        _channelLevelLabel.layer.borderColor = [[UIColor colorWithHexString:YMCOLOR_BLUE] CGColor];
        _channelLevelLabel.layer.cornerRadius = 3;
        _channelLevelLabel.layer.borderWidth = 0.8;
        _channelLevelLabel.layer.masksToBounds = YES;
        [topContainView addSubview:_channelLevelLabel];
        
        _roleLabel = [[UILabel alloc] init];
                _roleLabel.textAlignment = NSTextAlignmentCenter;
        _roleLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
        _roleLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        _roleLabel.layer.borderColor = [[UIColor colorWithHexString:YMCOLOR_RED] CGColor];
        _roleLabel.layer.cornerRadius = 3;
        _roleLabel.layer.borderWidth = 0.8;
        _roleLabel.layer.masksToBounds = YES;
        [topContainView addSubview:_roleLabel];
        
        UILabel *staLabel = [[UILabel alloc] init];
        staLabel.text = @"状态：";
        staLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        staLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        [topContainView addSubview:staLabel];
        
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        [topContainView addSubview:_statusLabel];
        
        [_channelLevelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_dealerNameLabel.mas_left);
            make.bottom.equalTo(topContainView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROOUND_BIG]);
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:50]);
            make.width.mas_equalTo(50);
        }];
        
        [_roleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_channelLevelLabel.mas_right).with.offset([YMCScreenAdapter sizeBy640:YMSPACE_AROUND]);
            make.centerY.equalTo(_channelLevelLabel);
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:50]);
            make.width.mas_equalTo(50);
        }];
        
        [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(topContainView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.centerY.equalTo(_dealerNameLabel);
        }];
        
        [staLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_statusLabel.mas_left);
            make.centerY.equalTo(_statusLabel);
        }];
        
        //因 dealerNameLabel 的右边要和 staLabel 的左边作约束，所以约束放在 staLabel 之后
        [_dealerNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_userIconImageView.mas_right).with.offset([YMCScreenAdapter sizeBy750:38]);
            make.top.equalTo(topContainView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROOUND_BIG]);
            make.right.lessThanOrEqualTo(staLabel.mas_left).with.offset(-[YMCScreenAdapter sizeBy750:18]);
        }];
        
        UIView *bottomLineView = [[UIView alloc] init];
        bottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [topContainView addSubview:bottomLineView];
        [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(topContainView);
            make.left.equalTo(topContainView);
            make.right.equalTo(topContainView);
            make.height.mas_equalTo(0.5);
        }];
    }
    
    return self;
}

- (void)configDataWithDealerManagerDict:(NSDictionary *)dict
{
    self.userIconImageView.image = [UIImage imageNamed:@"ym_index_viomicloud"];
    _dealerNameLabel.text = [dict notNullObjectForKey:@"dealerName"];
    _channelLevelLabel.text = [NSString stringWithFormat:@"%@ 级", [dict notNullObjectForKey:@"channelLevel"]];
    NSInteger roleId = [[dict notNullObjectForKey:@"category"] integerValue];
    switch (roleId) {
        case 1:
            _roleLabel.text = @" KA ";
            break;
            
        case 2:
            _roleLabel.text = @"城市运营";
            break;
            
        default:
            _roleLabel.text = @"-";
            break;
    }
    
    /*
     配置“状态”文字的颜色
     statusLabel 的文字内容由 statusString 和 statusDescString 的内容共同决定
     当 statusString 为“审核通过”，且 statusDescString 为“正常时”，才显示正常，否则，继续判断 statusDescString 的内容
     */
    
    NSString *approveStatusDescString = [dict notNullObjectForKey:@"approveStatusDesc"];
    NSString *statusDescString = [dict notNullObjectForKey:@"statusDesc"];
    
//    
//    if ([statusDescString isEqualToString:@"正常"] && [approveStatusDescString isEqualToString:@"审核通过"]) {
////        _statusLabel.text = statusDescString;
//        _statusLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GREEN];
//    } else if ([statusDescString isEqualToString:@"冻结"]){
////        _statusLabel.text = statusDescString;
//        _statusLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
//    } else {
////        _statusLabel.text = approveStatusDescString;
//        _statusLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
//    }
    
    //zcm 修改 之前这里配置错了
    if ([approveStatusDescString isEqualToString:@"审核通过"])
    {
        if ([statusDescString isEqualToString:@"正常"])
        {
            _statusLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        }
        else
        {
            _statusLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
        }
        _statusLabel.text = statusDescString;
    } else
    {
        _statusLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
        _statusLabel.text = approveStatusDescString;
    }
    
    CGFloat channelLevelLabelWidth = _channelLevelLabel.intrinsicContentSize.width;
    CGFloat roleLabelWidth = _roleLabel.intrinsicContentSize.width;

    [_channelLevelLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(channelLevelLabelWidth + 20);
    }];
    
    [_roleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(roleLabelWidth + 20);
    }];
}

@end
