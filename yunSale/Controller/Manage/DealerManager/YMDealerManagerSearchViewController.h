//
//  YMDealerManagerSearchViewController.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/30.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol YMDealerManagerSearchDelegate <NSObject>

@required

/**
 当开始时间、结束时间和搜索条件都存在时，点击搜索按钮时触发，传回开始时间、结束时间和搜索条件
 
 @param searchName 搜索条件
 */
- (void)YMDealerdidSearchActionFinished:(NSString *)searchName;

@end

@interface YMDealerManagerSearchViewController : UIViewController

@property (nonatomic, weak) id <YMDealerManagerSearchDelegate> delegate;

@end
