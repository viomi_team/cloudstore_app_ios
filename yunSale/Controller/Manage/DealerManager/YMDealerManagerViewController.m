//
//  YMDealerManagerViewController.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/25.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMDealerManagerViewController.h"
#import "YMFourItemTableHeaderView.h"
#import "YMFourItemTableViewCell.h"
#import "YMManagerAPI.h"
#import <MJRefresh/MJRefresh.h>
#import <UMMobClick/MobClick.h>
#import "YMDealerManagerSearchViewController.h"
#import "YMDealerManagerDetailViewController.h"

@interface YMDealerManagerViewController () <UITableViewDelegate, UITableViewDataSource, YMDealerManagerSearchDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) YMManagerAPI *dealerManagerAPI;
//订单列表数组
@property (nonatomic, strong) NSMutableArray *dealerManagerListsArray;
//数据页数
@property (nonatomic, assign) NSInteger pageNumber;
//数据总页数（由服务器返回）
@property (nonatomic, assign) NSInteger totalPageCount;
//搜索条件
@property (nonatomic, strong) NSString *searchNameString;

@property (nonatomic, assign) BOOL isSearchAction;

@property (nonatomic, strong) YMDealerManagerSearchViewController *searchVC;

@property (nonatomic, strong) NSMutableArray *cellHeights;

@end

@implementation YMDealerManagerViewController

#pragma mark Life

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick endEvent:@"Manger_DealerManager"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"经销商管理";
    
    UIBarButtonItem *searchBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ym_index_search"] style:UIBarButtonItemStylePlain target:self action:@selector(searchAction)];
    [self.navigationItem setRightBarButtonItem:searchBtn];
    
    [self drawView];
    
    //初始化数据
    self.dealerManagerListsArray = [[NSMutableArray alloc] init];
    self.cellHeights = [[NSMutableArray alloc] init];
    self.pageNumber = 1;
    self.isSearchAction = NO;
    self.searchNameString = @"";
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self fetchDealerManagerData];
    
    //上拉刷新
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(fetchDealerManagerData)];
}


- (void)dealloc {
    NSLog(@"%s", __func__);
    if (self.dealerManagerAPI) {
        [self.dealerManagerAPI cancle];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark Draw
- (void)drawView {
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - UITableViewDelegate & UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dealerManagerListsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [YMCScreenAdapter intergerSizeBy750:80];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber* thisCellHeight = [self.cellHeights objectAtIndex:indexPath.row];
    
    return thisCellHeight.floatValue;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    YMFourItemTableHeaderView *headerView = [[YMFourItemTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, YMSCREEN_WIDTH, [YMCScreenAdapter intergerSizeBy750:80]) style:YMFourItemHeaderViewStyleDealerManager];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *reuseIdentifier = @"dealerManagerCell";
    YMFourItemTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell)
    {
        cell = [[YMFourItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier cellStyle:YMFourItemCellStyleDealerManager];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    //配置数据
    NSDictionary *detailDict = self.dealerManagerListsArray[indexPath.row];
    cell.firstLabel.text = [detailDict notNullObjectForKey:@"name"];
    cell.secondLabel.text = [NSString stringWithFormat:@"%@ 级经销商", [detailDict notNullObjectForKey:@"channelLevel"]];
    cell.thirdLabel.text = [NSString stringWithFormat:@"%@", [detailDict notNullObjectForKey:@"staffNum"]];
    NSString *approveStatusString = [detailDict notNullObjectForKey:@"approveStatusDesc"];
    
    if ([approveStatusString isEqualToString:@"审核通过"])
    {
        if ([[detailDict notNullObjectForKey:@"statusDesc"] isEqualToString:@"正常"])
        {
            cell.fourthLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        }
        else
        {
            cell.fourthLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
        }
        cell.fourthLabel.text = [NSString stringWithFormat:@"%@", [detailDict notNullObjectForKey:@"statusDesc"]];
    }
    else
    {
        cell.fourthLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
        cell.fourthLabel.text = approveStatusString;
    }
    
    
    if (indexPath.row % 2 == 0)
    {
        cell.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        cell.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    YMDealerManagerDetailViewController *detailVC = [[YMDealerManagerDetailViewController alloc] init];
    detailVC.dealerDetailDict = self.dealerManagerListsArray[indexPath.row];
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - YMDealerManagerSearchDelegate
- (void)YMDealerdidSearchActionFinished:(NSString *)searchName {
    self.searchNameString = searchName;
    self.pageNumber = 1;
    self.isSearchAction = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.dealerManagerListsArray removeAllObjects];
    [self fetchDealerManagerData];
}

#pragma mark Action
- (void)searchAction {
    if (!self.searchVC) {
        self.searchVC = [[YMDealerManagerSearchViewController alloc] init];
        self.searchVC.delegate = self;
    }
    
    [self.navigationController pushViewController:self.searchVC animated:YES];
    
}

#pragma mark Fetch
- (void)fetchDealerManagerData {
    if(!self.dealerManagerAPI)
    {
        self.dealerManagerAPI = [[YMManagerAPI alloc] init];
    }
    
    //显示错误提示视图时，若先前的提示视图存在，则先把其移除
    [YMCAlertView removeNetErrorInView:self.view];
    
    [self.dealerManagerAPI fetchDealerManagerDataWithName:self.searchNameString pageNum:self.pageNumber completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (status == YMCRequestSuccess)
        {
            NSDictionary *resultDict = [data notNullObjectForKey:@"result"];
            NSArray *listArray = [resultDict notNullObjectForKey:@"list"];
            self.totalPageCount = [[resultDict notNullObjectForKey:@"totalPageNum"] integerValue];
            
            for (NSDictionary *dic in listArray) {
                NSString *name = [dic notNullObjectForKey:@"name"];
                NSString *dealerName = [dic notNullObjectForKey:@"%@ 级经销商"];
                
                CGFloat nameHeight = [name heightWithWidth:[YMCScreenAdapter sizeBy750:270] fontsize:[YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]];
                CGFloat dealerNameHeight =[dealerName heightWithWidth:[YMCScreenAdapter sizeBy750:180] fontsize:[YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]];
                
                CGFloat cellHight = nameHeight >= dealerNameHeight? nameHeight:dealerNameHeight ;
                
                cellHight = cellHight >= OneLineHeight * 2 ? OneLineHeight * 2 : cellHight;//行高是否超出两行
                
                [self.cellHeights addObject:[NSNumber numberWithFloat:cellHight + [YMCScreenAdapter sizeBy750:64]]];
            }
            
            if (!listArray || listArray.count == 0)
            {
                if (self.pageNumber == 1 && !self.isSearchAction) {
                    //首次加载
                    [YMCAlertView showNetErrorInView:self.view type:YMCRequestNoDataAndNoBtn message:YM_NOMOREDATA_TEXT actionBlock:nil];
                }
                else
                {
                    self.searchNameString = @"";
                    
                    @weakify(self);
                    [YMCAlertView showNetErrorInView:self.view type:YMCRequestNoData message:YM_NOSEARCHDATA_TEXT actionBlock:^{
                        @strongify(self);
                        [self searchAction];
                    }];
                }
                
                return;
            }
            
            [self.dealerManagerListsArray addObjectsFromArray:listArray];
            
            [self.tableView.mj_footer endRefreshing];
            
            if (self.pageNumber >= self.totalPageCount)
            {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            else
            {
                self.pageNumber ++;
            }
            
            [self.tableView reloadData];
        }
        else
        {
            [self.tableView.mj_footer endRefreshing];
            if (self.pageNumber == 1) {
                //首次加载
                @weakify(self);
                [YMCAlertView showNetErrorInView:self.view type:status message:message actionBlock:^{
                    @strongify(self);
                    
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [self fetchDealerManagerData];
                }];
            }else{
                [YMCAlertView showMessage:message];
            }
        }
    }];
}


@end
