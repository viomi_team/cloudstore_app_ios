//
//  YMStaffManagerCell.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/25.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMStaffManagerCell : UITableViewCell
//用户名
@property (nonatomic, strong) UILabel *nameLabel;
//用户详情
@property (nonatomic, strong) UILabel *userInfoLabel;
//创建时间
@property (nonatomic, strong) UILabel *createdDateLabel;
//用户手机
@property (nonatomic, strong) UILabel *phoneLabel;

@property (nonatomic, strong) UILabel *weChatNameLabel;

@property (nonatomic, strong) UIImageView *weChatImageView;

@end
