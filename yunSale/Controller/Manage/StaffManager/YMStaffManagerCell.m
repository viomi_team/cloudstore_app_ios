//
//  YMStaffManagerCell.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/25.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMStaffManagerCell.h"

@implementation YMStaffManagerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        _nameLabel.numberOfLines = 2;
        _nameLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:32]);
        [self.contentView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:470]);
            //在4寸的设备上显示时，createdDateLabel 的位置离底部太近了，还没找到原因，临时用这种方法解决（by 谢立颖 2017,1,15）
            if ([YMDevice deviceScreenSizeType] == iPhone5) {
                make.top.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:14]);
            } else {
                make.top.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            }
        }];
        
        _userInfoLabel = [[UILabel alloc] init];
        _userInfoLabel.textColor = [UIColor colorWithHexString:YMCOLOR_DARK_GRAY];
        _userInfoLabel.numberOfLines = 2;
        _userInfoLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
        [self.contentView addSubview:_userInfoLabel];
        [_userInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel.mas_left);
            make.top.equalTo(_nameLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:10]);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:470]);
        }];
        
        _createdDateLabel = [[UILabel alloc] init];
        _createdDateLabel.textColor = [UIColor colorWithHexString:YMCOLOR_DARK_GRAY];
        _createdDateLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
        [self.contentView addSubview:_createdDateLabel];
        [_createdDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel.mas_left);
            make.top.equalTo(_userInfoLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:8]);
        }];
        
        _phoneLabel = [[UILabel alloc] init];
        _phoneLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        _phoneLabel.textAlignment = NSTextAlignmentRight;
        _phoneLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
        [self.contentView addSubview:_phoneLabel];
        [_phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.top.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
//            make.width.mas_equalTo([YMScreenAdapter sizeBy750:200]);
        }];
        
        _weChatImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_weChatImageView];
        [_weChatImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_phoneLabel.mas_right);
            make.bottom.equalTo(self.contentView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        }];
        
        _weChatNameLabel = [[UILabel alloc] init];
        _weChatNameLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        _weChatNameLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]);
        [self.contentView addSubview:_weChatNameLabel];
        [_weChatNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_weChatImageView.mas_left).with.offset(-[YMCScreenAdapter sizeBy750:10]);
            make.centerY.equalTo(_weChatImageView);
        }];
    }
    
    return self;
}

+ (CGFloat)cellHeightWithObj:(id)obj {
    return [YMCScreenAdapter sizeBy750:160];
}

@end
