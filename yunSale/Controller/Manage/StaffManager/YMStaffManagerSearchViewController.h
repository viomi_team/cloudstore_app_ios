//
//  YMStaffManagerSearchViewController.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/27.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMSearchSelectorView.h"
#import "YMButton.h"

@protocol YMStaffManagerSearchDelegate <NSObject>

/**
 *  当开始时间、结束时间和搜索条件都存在时，点击搜索按钮时触发，传回开始时间、结束时间和搜索条件
 */
@required
- (void)YMStaffdidSearchActionFinishedWithUserName:(NSString *)userName phoneNum:(NSString *)phoneNum channelName:(NSString *)channelName roldId:(NSString *)roleId;

@end

@interface YMStaffManagerSearchViewController : UIViewController

@property (nonatomic, weak) id <YMStaffManagerSearchDelegate> delegate;

@end
