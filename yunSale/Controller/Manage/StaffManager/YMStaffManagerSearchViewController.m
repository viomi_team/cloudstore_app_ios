//
//  YMStaffManagerSearchViewController.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/27.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMStaffManagerSearchViewController.h"
#import "YMSearchSelectorView.h"
#import <ActionSheetPicker.h>
#import "YMDate.h"
#import "YMSearchInputView.h"
#import <ActionSheetStringPicker.h>

@interface YMStaffManagerSearchViewController ()


@property (nonatomic, strong) YMSearchInputView *userNameView;

@property (nonatomic, strong) YMSearchInputView *phoneView;

@property (nonatomic, strong) YMSearchInputView *channelNameView;

@property (nonatomic, strong) YMSearchSelectorView *roleIdSelectorView;

@property (nonatomic, strong) NSString *roldIdString;

@property (nonatomic, assign) NSUInteger selectedRoldValue;

@end

@implementation YMStaffManagerSearchViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //初始化一些属性
    self.selectedRoldValue = 0;
    self.roldIdString = @"" ;
    
    self.navigationItem.title = @"店员管理";
    [self drawView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark Draw
- (void)drawView
{
    self.view.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    
    UIView *containerView = [[UIView alloc] init];
    containerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:containerView];
    [containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.width.mas_equalTo(YMSCREEN_WIDTH);
        make.centerX.equalTo(self.view);
        make.height.mas_equalTo([YMCScreenAdapter intergerSizeBy750:652]);
    }];
    
    //搜索输入框
    _userNameView = [[YMSearchInputView alloc] initWithTitle:@"用户名称" placeHolder:@"请输入用户名称"];
    [_userNameView.textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [containerView addSubview:_userNameView];
    [_userNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(containerView);
        make.top.equalTo(containerView).with.offset([YMCScreenAdapter sizeBy750:18]);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.width.mas_equalTo(YMSCREEN_WIDTH);
    }];
    
    _phoneView = [[YMSearchInputView alloc] initWithTitle:@"手机号码" placeHolder:@"请输入手机号码"];
    _phoneView.textField.keyboardType = UIKeyboardTypeNumberPad;
    [containerView addSubview:_phoneView];
    [_phoneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(containerView);
        make.top.equalTo(_userNameView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:18]);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.width.mas_equalTo(YMSCREEN_WIDTH);
    }];
    
    _channelNameView = [[YMSearchInputView alloc] initWithTitle:@"所属渠道" placeHolder:@"请输入渠道名称"];
    [_channelNameView.textField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [containerView addSubview:_channelNameView];
    [_channelNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(containerView);
        make.top.equalTo(_phoneView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:18]);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.width.mas_equalTo(YMSCREEN_WIDTH);
    }];
    
    _roleIdSelectorView = [[YMSearchSelectorView alloc] initWithTitle:@"类型选择" placeHolder:@"请选择促销员类型"];
    [_roleIdSelectorView.selectorBtn addTarget:self action:@selector(roleSelectorAction) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:_roleIdSelectorView];
    [_roleIdSelectorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_channelNameView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:18]);
        make.left.equalTo(containerView);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.width.mas_equalTo(YMSCREEN_WIDTH);
    }];
    
    YMButton *searchBtn = [[YMButton alloc] initWithStyle:YMButtonStyleRectangle];
    [searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_roleIdSelectorView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.left.equalTo(containerView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.right.equalTo(containerView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
    }];
    
    UIView *secondBottomLine = [[UIView alloc] init];
    secondBottomLine.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [containerView addSubview:secondBottomLine];
    [secondBottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(containerView);
        make.width.mas_equalTo(YMSCREEN_WIDTH);
        make.centerX.equalTo(containerView);
        make.height.mas_equalTo(0.5);
    }];
}

#pragma mark Delegate
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark Action
- (void)roleSelectorAction
{
    [self.view endEditing:YES];    //当用户点击选择促销员类型按钮时，取消所有用户输入行为
    
    NSArray *rolesArray = @[@"兼职促销员", @"全职促销员", @"店长"];
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:@"请选择促销员类型" rows:rolesArray initialSelection:self.selectedRoldValue doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        switch (selectedIndex) {
            case 0:
                self.roldIdString = @"25";
                break;
                
            case 1:
                self.roldIdString = @"23";
                break;
                
            case 2:
                self.roldIdString = @"21";
                break;
        }
        
        [_roleIdSelectorView setPlaceHolderString:selectedValue];
        
        self.selectedRoldValue = selectedIndex;
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];

    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:nil action:nil];
    [picker setCancelButton:cancelButton];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:nil action:nil];
    [picker setDoneButton:doneButton];
    
    [picker showActionSheetPicker];
}

- (void)searchAction
{
    [_delegate YMStaffdidSearchActionFinishedWithUserName:self.userNameView.textField.text phoneNum:self.phoneView.textField.text channelName:self.channelNameView.textField.text roldId:self.roldIdString];
    [self.navigationController popViewControllerAnimated:YES];
}


@end
