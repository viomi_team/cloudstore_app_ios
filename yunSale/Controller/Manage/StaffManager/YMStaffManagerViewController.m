//
//  YMStaffManagerViewController.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/25.
//  Copyright © 2016年 yunmi. All rights reserved.
//
#import "YMDate.h"
#import "YMManagerAPI.h"
#import "YMStaffManagerCell.h"
#import <MJRefresh/MJRefresh.h>
#import <UMMobClick/MobClick.h>
#import "YMStaffManagerViewController.h"
#import "YMStaffManagerSearchViewController.h"


@interface YMStaffManagerViewController () <UITableViewDelegate, UITableViewDataSource, YMStaffManagerSearchDelegate>

@property (nonatomic, assign) NSInteger pageNumber;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, assign) NSInteger totalPageCount;

@property (nonatomic, strong) YMManagerAPI *staffManagerAPI;

@property (nonatomic, strong) NSMutableArray *staffManagerListArray;

//存储名字和角色Label的高度
@property (nonatomic, strong) NSMutableArray *heightOfNameArray;
@property (nonatomic, strong) NSMutableArray *heightOfRoldNameArray;

@property (nonatomic, assign) BOOL isSearchAction;

//上拉刷新的参数
@property (nonatomic, strong) NSString *roleIdString;
@property (nonatomic, strong) NSString *phoneNumString;
@property (nonatomic, strong) NSString *contactNameString;
@property (nonatomic, strong) NSString *channelNameString;

@end

@implementation YMStaffManagerViewController

#pragma mark Life

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick event:@"Manger_Staff_Manager"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"店员管理";
    
    UIBarButtonItem *searchBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ym_index_search"] style:UIBarButtonItemStylePlain target:self action:@selector(searchAction)];
    [self.navigationItem setRightBarButtonItem:searchBtn];
    
    [self drawView];
    
    //初始化数据
    self.staffManagerListArray = [[NSMutableArray alloc] init];
    self.pageNumber = 1;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.contactNameString = @"";
    self.phoneNumString = @"";
    self.channelNameString = @"";
    self.roleIdString = @"";
    self.isSearchAction = NO;
    
    [self fetchStaffManagerData];           //Changed
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(fetchStaffManagerData)];
}

- (void)dealloc {
    NSLog(@"%s", __func__);
    if (self.staffManagerAPI) {
        [self.staffManagerAPI cancle];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark Draw main view
- (void)drawView {
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark Design table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.staffManagerListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = [YMCScreenAdapter sizeBy750:160] + [[self.heightOfNameArray objectAtIndex:indexPath.row] floatValue] + [[self.heightOfRoldNameArray objectAtIndex:indexPath.row] floatValue];
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *reuseIdentifier = @"staffManagerCell";
    YMStaffManagerCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell) {
        cell = [[YMStaffManagerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    //配置数据
    NSDictionary *dict = self.staffManagerListArray[indexPath.row];
    cell.nameLabel.text = dict[@"name"];
    cell.userInfoLabel.text = [NSString stringWithFormat:@"%@ | %@", dict[@"channelName"], dict[@"roleStr"]];
    NSString *dateString = [dict [@"createdTime"] substringWithRange:NSMakeRange(0, 10)];
    cell.createdDateLabel.text = dateString;
    
    NSString *phoneString = [NSString stringWithFormat:@"%@", dict[@"mobile"]];
    if (![NSString isEmptyString:phoneString]) {
        cell.phoneLabel.text = phoneString;
    }
    
    NSString *weChatNameString = [dict notNullObjectForKey:@"wechatNickName"];
    if ([NSString isEmptyString:weChatNameString]) {
        cell.weChatNameLabel.text = @"未绑定";
        cell.weChatImageView.image = [UIImage imageNamed:@"ym_index_wechat_gray"];
    } else {
        cell.weChatImageView.image = [UIImage imageNamed:@"ym_index_wechat"];
        cell.weChatNameLabel.text = weChatNameString;
    }
    
    
    //
    if (indexPath.row % 2 == 0) {
        cell.backgroundColor = [UIColor whiteColor];
    } else {
        cell.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    }
    
    //选中状态取消
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

//搜索
#pragma YMStaffManagerSearchDelegate
- (void)YMStaffdidSearchActionFinishedWithUserName:(NSString *)userName phoneNum:(NSString *)phoneNum channelName:(NSString *)channelName roldId:(NSString *)roleId {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.staffManagerListArray removeAllObjects];
    
    self.pageNumber = 1;
    self.contactNameString = userName;
    self.phoneNumString = phoneNum;
    self.channelNameString = channelName;
    self.roleIdString = roleId;
    self.isSearchAction = YES;
    
    [self fetchStaffManagerData];
}

#pragma mark Action
- (void)searchAction {
    YMStaffManagerSearchViewController *searchVC = [[YMStaffManagerSearchViewController alloc] init];
    searchVC.delegate = self;
    [self.navigationController pushViewController:searchVC animated:YES];

}

#pragma mark Fetch
- (void)fetchStaffManagerData {
    if(!self.staffManagerAPI) {
        self.staffManagerAPI = [[YMManagerAPI alloc] init];
    }
    
    //显示错误提示视图时，若先前的提示视图存在，则先把其移除
    [YMCAlertView removeNetErrorInView:self.view];
    
    [self.staffManagerAPI fetchStaffManagerSearchDataWithPageNum:self.pageNumber contactName:self.contactNameString phoneNum:self.phoneNumString channelName:self.channelNameString roleId:self.roleIdString completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (status == YMCRequestSuccess) {
            NSDictionary *dict = [data notNullObjectForKey:@"result"];
            NSArray *array = [dict notNullObjectForKey:@"list"];
            self.totalPageCount = [[dict notNullObjectForKey:@"totalPageNum"] integerValue];
            
            self.heightOfNameArray = [NSMutableArray new];
            self.heightOfRoldNameArray = [NSMutableArray new];

            if (!array || array.count == 0) {
                
                if (self.pageNumber == 1 && !self.isSearchAction) {
                    //首次加载
                    [YMCAlertView showNetErrorInView:self.view type:YMCRequestNoDataAndNoBtn message:YM_NOMOREDATA_TEXT actionBlock:nil];
                } else {
                    //再次进入搜索界面时要把搜索条件清空，否则搜索条件依然存在
                    self.contactNameString = @"";
                    self.phoneNumString = @"";
                    self.channelNameString = @"";
                    self.roleIdString = @"";
                    
                    @weakify(self);
                    [YMCAlertView showNetErrorInView:self.view type:YMCRequestNoData message:YM_NOSEARCHDATA_TEXT actionBlock:^{
                        @strongify(self);
                        [self searchAction];
                    }];
                }
                
                return;
            }
            
            [self.staffManagerListArray addObjectsFromArray:array];

            for (NSDictionary *dict in self.staffManagerListArray) {
                NSString *nameString = dict[@"name"];
                NSString *roldNameString = [NSString stringWithFormat:@"%@ | %@", dict[@"channelName"], dict[@"roleStr"]];
                CGFloat heightOfName = [nameString heightWithWidth:[YMCScreenAdapter sizeBy750:470] fontsize:[YMCScreenAdapter fontsizeBy750:32]] - 19.09;
                CGFloat heightOfRoldName = [roldNameString heightWithWidth:[YMCScreenAdapter sizeBy750:470] fontsize:[YMCScreenAdapter fontsizeBy750:YMFONTSIZE_MID]] - 15.51;
//                CGFloat heightOfName = [nameString heightWithWidth:[YMScreenAdapter sizeBy750:470] fontsize:[YMScreenAdapter fontsizeBy750:32]];
//                CGFloat heightOfRoldName = [roldNameString heightWithWidth:[YMScreenAdapter sizeBy750:470] fontsize:[YMScreenAdapter fontsizeBy750:YMFONTSIZE_MID]];
                [self.heightOfNameArray addObject:[NSNumber numberWithFloat:heightOfName]];
                [self.heightOfRoldNameArray addObject:[NSNumber numberWithFloat:heightOfRoldName]];
            }
            
            [self.tableView.mj_footer endRefreshing];
            
            if (self.pageNumber >= self.totalPageCount) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                self.pageNumber++;
            }
            
            [self.tableView reloadData];
            
        } else {
            [self.tableView.mj_footer endRefreshing];
            if (self.pageNumber == 1) {
                //首次加载
                @weakify(self);
                [YMCAlertView showNetErrorInView:self.view type:status message:message actionBlock:^{
                    @strongify(self);
                    
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [self fetchStaffManagerData];
                }];
            } else {
                [YMCAlertView showMessage:message];
            }
        }
    }];
    
}

@end
