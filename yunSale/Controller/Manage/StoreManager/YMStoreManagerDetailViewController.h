//
//  YMStoreManagerDetailViewController.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/28.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMStoreManagerDetailViewController : UIViewController

@property (nonatomic, copy) NSDictionary *storeDetailDict;

@end
