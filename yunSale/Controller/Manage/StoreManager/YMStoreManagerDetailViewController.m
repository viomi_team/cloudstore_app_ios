//
//  YMStoreManagerDetailViewController.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/28.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMStoreManagerDetailViewController.h"
#import "YMImagesLoopView.h"
#import "YMStoreManagerHeaderView.h"
#import "YMTwoItemTableViewCell.h"
#import <UIImageView+WebCache.h>
#import "YMManagerAPI.h"
#import "YMImagesLoopView.h"
#import <UMMobClick/MobClick.h>
#import <MBProgressHUD.h>

@interface YMStoreManagerDetailViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) YMStoreManagerHeaderView *headerView;

//门店电话
@property (nonatomic, strong) UILabel *storePhoneLabel;
//店员人数
@property (nonatomic, strong) UILabel *staffCountLabe;
//联系人
@property (nonatomic, strong) UILabel *userNameLabel;
//联系电话
@property (nonatomic, strong) UILabel *userPhoneLabel;
//当前提成模板
@property (nonatomic, strong) UILabel *profitTemplateLabel;
//
@property (nonatomic, strong) YMManagerAPI *managerAPI;
//获取图片的 ID
@property (nonatomic, assign) NSInteger imageId;

@end

@implementation YMStoreManagerDetailViewController

#pragma mark - Life

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"门店详情";
    self.view.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    
    [self drawView];
    
    self.imageId = [[self.storeDetailDict notNullObjectForKey:@"id"] integerValue];
    
    self.headerView = [[YMStoreManagerHeaderView alloc] init];
    self.headerView.storeName = [self.storeDetailDict notNullObjectForKey:@"name"];
    self.headerView.parentDealerName = [self.storeDetailDict notNullObjectForKey:@"parentName"];
    self.headerView.status = [self.storeDetailDict notNullObjectForKey:@"approveStatusDesc"];
    self.headerView.imagesLoopView.type = YMLoopViewImageTypeName;
    self.headerView.imagesLoopView.images = [NSArray arrayWithObjects:@"ym_store_default",nil];
    
    [self fetchImageURL];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.headerView.imagesLoopView startTimer];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.headerView.imagesLoopView stopTimer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Draw
- (void)drawView
{
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 0;
            break;
            
        case 1:
            return 3;
            break;
            
        case 2:
            return 1;
            break;
            
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [YMCScreenAdapter intergerSizeBy750:100];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return [YMCScreenAdapter intergerSizeBy750:528];
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return [YMCScreenAdapter intergerSizeBy750:YMSPACE_AROUND];
            break;
            
        case 1:
            return [YMCScreenAdapter intergerSizeBy750:YMSPACE_AROUND];
            
        default:
            return 0.5;
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return self.headerView;
    } else {
        return nil;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            return [UIView drawLineViewWithWidth:YMSCREEN_WIDTH height:[YMCScreenAdapter sizeBy750:24] color:[UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND]];
        }
            break;
            
        case 1:
        {
            return [UIView drawLineViewWithWidth:YMSCREEN_WIDTH height:[YMCScreenAdapter sizeBy750:24] color:[UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND]];
        }
            
        default:
            return nil;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [UITableViewCell new];
    } else {
        NSString *reuseIdentifier = @"dealerManagerDetailCell";
        YMTwoItemTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (!cell) {
            cell = [[YMTwoItemTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier Style:YMTwoItemCellStyleLeft];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //配置数据
        switch (indexPath.section)
        {
            case 1:
            {
                switch (indexPath.row)
                {
                    case 0:
                    {
                        cell.titleLabel.text = @"店员数";
                        cell.detailsLabel.text = [NSString stringWithFormat:@"%@ 人", [self.storeDetailDict notNullObjectForKey:@"staffNum"]];
                    }
                        break;
                        
                    case 1:
                    {
                        cell.titleLabel.text = @"联系人";
                        NSString *nameString = [self.storeDetailDict notNullObjectForKey:@"contactName"];
                        if (![NSString isEmptyString:nameString]) {
                            cell.detailsLabel.text = nameString;
                        }
                    }
                        break;
                        
                    case 2:
                    {
                        cell.titleLabel.text = @"联系电话";
                        NSString *phoneString = [self.storeDetailDict notNullObjectForKey: @"contactMobile"];
                        //isEmptyString 实例方法有问题？经常判断有误
                        if (![NSString isEmptyString:phoneString]) {
                            cell.detailsLabel.text = phoneString;
                        }
                    }
                        break;
                }
            }
                break;
                
            case 2:
            {
                cell.titleLabel.text = @"当前提成模板";
                NSString *templateNameString = [self.storeDetailDict notNullObjectForKey: @"templateName"];
                if (![NSString isEmptyString:templateNameString])
                {
                    cell.detailsLabel.text = templateNameString;
                }
                
                break;
            }
        }
//        if (indexPath.row%2 == 0) {
//            cell.backgroundColor = [UIColor whiteColor];
//        } else {
//            cell.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
//        }
        cell.contentView.backgroundColor = indexPath.row%2 == 0? [UIColor whiteColor] : [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
        return cell;
    }
}

#pragma mark - Fetch
//获取门店相关的图片并展示在轮播图里
- (void)fetchImageURL
{
    if (!self.managerAPI) {
        self.managerAPI = [[YMManagerAPI alloc] init];
    }
    
    [self.managerAPI fetchImageUrlWithId:self.imageId completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        if (status == YMCRequestSuccess) {
            NSDictionary *dict = [data notNullObjectForKey:@"result"];
            if (dict) {
                NSMutableArray *urlArray = [[NSMutableArray alloc] init];
                
                //营业执照
                NSString *urlString1 = [dict notNullObjectForKey:@"businessLicense"];
                //组织机构代码证
                NSString *urlString2 = [dict notNullObjectForKey:@"organization"];
                //税务登记证
                NSString *urlString3 = [dict notNullObjectForKey:@"taxRegistration"];
                //门店正面照
                NSString *urlString4 = [dict notNullObjectForKey:@"baseImg"];
                //店头照
                NSString *urlString5 = [dict notNullObjectForKey:@"titleImg"];
                //门牌与店主合照
                NSString *urlString6 = [dict notNullObjectForKey:@"unionImg"];
                
                if (![NSString isEmptyString:urlString1]) {
                    [urlArray addObject:urlString1];
                }
                if (![NSString isEmptyString:urlString2]) {
                    [urlArray addObject:urlString2];
                }
                if (![NSString isEmptyString:urlString3]) {
                    [urlArray addObject:urlString3];
                }
                if (![NSString isEmptyString:urlString4]) {
                    [urlArray addObject:urlString4];
                }
                if (![NSString isEmptyString:urlString5]) {
                    [urlArray addObject:urlString5];
                }
                if (![NSString isEmptyString:urlString6]) {
                    [urlArray addObject:urlString6];
                }
                
                //最后再次判断 urlArray 是否为空，空的话就显示默认图片（by 谢立颖 2017,1,17 好多重复代码，需要优化）
//                if (urlArray.count > 0) {
//                    self.headerView.imagesLoopView.type = YMLoopViewImageTypeUrl;
//                    self.headerView.imagesLoopView.images = urlArray;
//                }else {
//                    self.headerView.imagesLoopView.type = YMLoopViewImageTypeName;
//                    self.headerView.imagesLoopView.images = [NSArray arrayWithObjects:@"ym_store_default",nil];
//                }
                
                if (urlArray.count > 0) {
                    self.headerView.imagesLoopView.type = YMLoopViewImageTypeUrl;
                    self.headerView.imagesLoopView.images = urlArray;
                    
                    return ;
                }
            }
//            else {
//                self.headerView.imagesLoopView.type = YMLoopViewImageTypeName;
//                self.headerView.imagesLoopView.images = [NSArray arrayWithObjects:@"ym_store_default",nil];
//            }
            
            self.headerView.imagesLoopView.type = YMLoopViewImageTypeName;
            self.headerView.imagesLoopView.images = [NSArray arrayWithObjects:@"ym_store_default",nil];
            
        } else {
//            //请求失败
//            self.headerView.imagesLoopView.type = YMLoopViewImageTypeName;
//            self.headerView.imagesLoopView.images = [NSArray arrayWithObjects:@"ym_store_default",nil];
//            //请求失败5秒后再次发起网络请求
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self fetchImageURL];
            });
        }
    }];
}

@end
