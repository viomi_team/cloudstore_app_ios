//
//  YMStoreManagerHeaderView.h
//  yunSale
//
//  Created by 谢立颖 on 2016/12/2.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YMImagesLoopView.h"

@interface YMStoreManagerHeaderView : UIView

//轮播图
@property (nonatomic, strong) YMImagesLoopView *imagesLoopView;
//门店名称
@property (nonatomic, strong) NSString *storeName;
//上一级经销商
@property (nonatomic, strong) NSString *parentDealerName;
//状态
@property (nonatomic, strong) NSString *status;

@end
