//
//  YMStoreManagerHeaderView.m
//  yunSale
//
//  Created by 谢立颖 on 2016/12/2.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMStoreManagerHeaderView.h"

@interface YMStoreManagerHeaderView ()
//用户头像
@property (nonatomic, strong) UIImageView *userIconImageView;
//门店名称
@property (nonatomic, strong) UILabel *storeNameLabel;
//上一级经销商
@property (nonatomic, strong) UILabel *parentDealerNameLabel;
//状态
@property (nonatomic, strong) UILabel *statusLabel;

@end

@implementation YMStoreManagerHeaderView

#pragma mark - Init
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _imagesLoopView = [[YMImagesLoopView alloc] init];
        _imagesLoopView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_imagesLoopView];
        [_imagesLoopView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.top.equalTo(self);
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:360]);
        }];
        
        UIView *topContainView = [[UIView alloc] init];
        [self addSubview:topContainView];
        [topContainView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_imagesLoopView.mas_bottom);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:168]);
        }];
        
        _userIconImageView = [[UIImageView alloc] init];
        _userIconImageView.layer.cornerRadius = 8;
        _userIconImageView.layer.masksToBounds = YES;
        _userIconImageView.image = [UIImage imageNamed:@"ym_index_viomicloud"];
        [topContainView addSubview:_userIconImageView];
        [_userIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo([YMCScreenAdapter sizeBy750:180]);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:180]);
            make.left.equalTo(topContainView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROOUND_BIG]);
            make.bottom.equalTo(topContainView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROOUND_BIG]);
        }];
        
        _storeNameLabel = [[UILabel alloc] init];
        _storeNameLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE]);
        _storeNameLabel.numberOfLines = 2;
        [topContainView addSubview:_storeNameLabel];
        [_storeNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_userIconImageView.mas_right).with.offset([YMCScreenAdapter sizeBy750:38]);
            make.top.equalTo(topContainView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROOUND_BIG]);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:300]);
        }];
        
        _parentDealerNameLabel = [[UILabel alloc] init];
        _parentDealerNameLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        _parentDealerNameLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        [topContainView addSubview:_parentDealerNameLabel];
        [_parentDealerNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(_storeNameLabel.mas_left);
//            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:500]);
            make.left.equalTo(_storeNameLabel.mas_left);
            make.bottom.equalTo(topContainView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROOUND_BIG]);
            make.right.equalTo(topContainView.mas_right);
        }];

        _statusLabel = [[UILabel alloc] init];
        _statusLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        _statusLabel.numberOfLines = 2;
        [topContainView addSubview:_statusLabel];
        
        
        UILabel *staLabel = [[UILabel alloc] init];
        staLabel.text = @"状态：";
        staLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        staLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        [topContainView addSubview:staLabel];
        
        [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(topContainView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:90]);
            make.top.equalTo(staLabel.mas_top);
        }];
        
        [staLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_statusLabel.mas_left);
//            make.centerY.equalTo(_storeNameLabel.mas_centerY);
            make.top.equalTo(_storeNameLabel.mas_top).with.offset([YMCScreenAdapter sizeBy750:5]);
        }];
    }
    
    return self;
}

#pragma mark - Private

- (void)setStoreName:(NSString *)storeName {
    _storeNameLabel.text = ![NSString isEmptyString:storeName] ? storeName : @"--";
}


- (void)setParentDealerName:(NSString *)parentDealerName {
    _parentDealerNameLabel.text = ![NSString isEmptyString:parentDealerName] ? parentDealerName : @"--";
}

- (void)setStatus:(NSString *)status {
    
    status = ![NSString isEmptyString:status] ? status : @"--";

    if ([status isEqualToString:@"审核通过"]) {
        _statusLabel.text = @"正常";
        _statusLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GREEN];
    }else {
        _statusLabel.text = status;
        _statusLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
    }
}

//- (void)configDataWithStoreManagerDict:(NSDictionary *)dict
//{
//    _userIconImageView.image = [UIImage imageNamed:@"ym_index_viomicloud"];
//    _storeNameLabel.text = [dict notNullObjectForKey:@"storeName"];
//    _parentDealerNameLabel.text = [dict notNullObjectForKey:@"parentName"];
//    //配置“状态”文字的颜色
//    NSString *statusString = [dict notNullObjectForKey:@"status"];
//    
//    if ([statusString isEqualToString:@"审核通过"])
//    {
//        _statusLabel.text = @"正常";
//        _statusLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GREEN];
//    }
//    else
//    {
//        _statusLabel.text = statusString;
//        _statusLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
//    }
//}
@end
