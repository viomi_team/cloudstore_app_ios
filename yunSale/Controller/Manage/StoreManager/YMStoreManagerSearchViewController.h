//
//  YMStoreManagerSearchViewController.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/28.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YMStoreManagerSearchDelegate <NSObject>

@required

/**
 当开始时间、结束时间和搜索条件都存在时，点击搜索按钮时触发，传回开始时间、结束时间和搜索条件

 @param searchName 搜索条件
 */
- (void)YMStoredidSearchActionFinished:(NSString *)searchName;

@end

@interface YMStoreManagerSearchViewController : UIViewController

@property (nonatomic, weak) id <YMStoreManagerSearchDelegate> delegate;

@end
