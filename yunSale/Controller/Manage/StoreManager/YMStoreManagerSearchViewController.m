//
//  YMStoreManagerSearchViewController.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/28.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMStoreManagerSearchViewController.h"
#import "YMButton.h"
#import "YMSearchInputView.h"

@interface YMStoreManagerSearchViewController ()

@property (nonatomic, strong) YMSearchInputView *searchInputView;

@end

@implementation YMStoreManagerSearchViewController

#pragma mark - Life
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"门店管理搜索";
    
    [self drawView];
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(self.searchInputView != nil){
        [self.searchInputView.textField performSelector:@selector(selectAll:) withObject:self.searchInputView.textField afterDelay:0.0];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    NSLog(@"%s", __func__);
}

#pragma mark - Draw
- (void)drawView
{
    self.view.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    
    UIView *containerView = [[UIView alloc] init];
    containerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:containerView];
    [containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view);
        make.height.mas_equalTo([YMCScreenAdapter intergerSizeBy750:318]);
    }];
    
    //搜索输入框
    _searchInputView = [[YMSearchInputView alloc] initWithTitle:@"门店搜索" placeHolder:@"请输入门店名称"];
    [containerView addSubview:_searchInputView];
    [_searchInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(containerView);
        make.right.equalTo(containerView);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.top.equalTo(containerView).with.offset(18);
    }];
    
    //搜索按钮
    YMButton *searchBtn = [[YMButton alloc] initWithStyle:YMButtonStyleRectangle];
    [searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(containerView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.right.equalTo(containerView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.top.equalTo(_searchInputView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
    }];
    
    //下划线
    UIView *bottomLineView = [[UIView alloc] init];
    bottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [containerView addSubview:bottomLineView];
    [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(YMSCREEN_WIDTH);
        make.height.mas_equalTo(0.5);
        make.centerX.equalTo(containerView);
        make.bottom.equalTo(containerView);
    }];
}

#pragma mark - Delegate
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - Action
- (void)searchAction
{
    [self.view endEditing:YES];
    [_delegate YMStoredidSearchActionFinished:self.searchInputView.textField.text];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TextField Delegate

@end
