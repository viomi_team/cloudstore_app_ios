//
//  YMManageViewController.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/2.
//  Copyright © 2016年 ;. All rights reserved.
//

#import "YMManageViewController.h"
#import "YMImageButton.h"
#import "YMStaffManagerViewController.h"
#import "YMStoreManagerViewController.h"
#import "YMDealerManagerViewController.h"
#import "YMManagerAPI.h"
#import <MJRefresh/MJRefresh.h>
#import <UMMobClick/MobClick.h>

static NSString *const ymManagerCollectionViewCell = @"ym_manager_cell";
#define YM_FUNVIEW_HEIGHT [YMCScreenAdapter sizeBy750:438]

typedef NS_ENUM(NSUInteger,YMFunType)
{
    YMManagerTypeStaff = 0,
    YMManagerTypeStore,
    YMManagerTypeDealer,
    YMManagerTypeOther,
};

@interface YMManageViewController ()

@property (nonatomic, strong) UIScrollView *containerScrollView;

//门店数量Label
@property (nonatomic, strong) UILabel *storeCountLabel;
//经销商数量Label
@property (nonatomic, strong) UILabel *dealerCountLabel;
//API
@property (nonatomic, strong) YMManagerAPI *countProfileAPI;

@property (nonatomic,assign) BOOL needLoadData;

//功能btns
@property (nonatomic,copy) NSArray *funArray;

@property (nonatomic,strong) UIView *funsView;

@property (nonatomic,assign) BOOL needUpdateFunsView;

@end

@implementation YMManageViewController

#pragma mark - Life

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.needLoadData = YES;
    self.needUpdateFunsView = YES;
    
    [self drawView];
    
    //下拉刷新
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(fetchData)];
    header.lastUpdatedTimeLabel.hidden = YES;
    self.containerScrollView.mj_header = header;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginNotificationEvent) name:@"ym_login_event" object:nil];
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (self.countProfileAPI) {
        [self.countProfileAPI cancle];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobClick event:@"Manager"];
    
    
    if (self.needLoadData) {
        [self fetchData];
    }
    
    if (self.needUpdateFunsView) {
        [self updateFuns];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Draw
- (void)drawView
{
    self.view.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    //容器视图
    self.containerScrollView = [[UIScrollView alloc] init];
    self.containerScrollView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    self.containerScrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.containerScrollView];
    [self.containerScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    UIView *topContainerView = [[UIView alloc] init];
    topContainerView.backgroundColor = [UIColor whiteColor];
    [self.containerScrollView addSubview:topContainerView];
    [topContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.containerScrollView);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:190]);
    }];
    
    self.storeCountLabel = [[UILabel alloc] init];
    self.storeCountLabel.text = @"-";
    self.storeCountLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
    self.storeCountLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:64]);
    self.storeCountLabel.textAlignment = NSTextAlignmentCenter;
    [topContainerView addSubview:self.storeCountLabel];
    [self.storeCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(topContainerView.mas_centerX).with.offset(-YMSCREEN_WIDTH / 4);
        make.top.equalTo(topContainerView).with.offset([YMCScreenAdapter sizeBy750:30]);
    }];
    
    self.dealerCountLabel = [[UILabel alloc] init];
    self.dealerCountLabel.text = @"-";
    self.dealerCountLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
    self.dealerCountLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:64]);
    self.dealerCountLabel.textAlignment = NSTextAlignmentCenter;
    [topContainerView addSubview:self.dealerCountLabel];
    [self.dealerCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(topContainerView.mas_centerX).with.offset(YMSCREEN_WIDTH / 4);
        make.top.equalTo(topContainerView).with.offset([YMCScreenAdapter sizeBy750:30]);
    }];
    
    UILabel *storeLabel = [[UILabel alloc] init];
    storeLabel.text = @"门店";
    storeLabel.textColor = [UIColor colorWithHexString:@"a0b8bb"];
    storeLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:28]);
    storeLabel.textAlignment = NSTextAlignmentCenter;
    [topContainerView addSubview:storeLabel];
    [storeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.storeCountLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:10]);
        make.centerX.equalTo(self.storeCountLabel);
    }];
    
    UILabel *dealerLabel = [[UILabel alloc] init];
    dealerLabel.text = @"经销商";
    dealerLabel.textColor = [UIColor colorWithHexString:@"a0b8bb"];
    dealerLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:28]);
    dealerLabel.textAlignment = NSTextAlignmentCenter;
    [topContainerView addSubview:dealerLabel];
    [dealerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.dealerCountLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:10]);
        make.centerX.equalTo(self.dealerCountLabel);
    }];
    
    UIView *verticalLineView = [[UIView alloc] init];
    verticalLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [topContainerView addSubview:verticalLineView];
    [verticalLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
        make.width.mas_equalTo(0.5);
        make.center.equalTo(topContainerView);
    }];
    
    UIView *firstBottomLineView = [[UIView alloc] init];
    firstBottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [topContainerView addSubview:firstBottomLineView];
    [firstBottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(topContainerView);
        make.width.mas_equalTo(YMSCREEN_WIDTH);
        make.centerX.equalTo(topContainerView);
        make.height.mas_equalTo(0.5);
    }];
    
    self.funsView = [[UIView alloc] init];
    self.funsView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [self.containerScrollView addSubview:self.funsView];
    [self.funsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topContainerView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:24]);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.containerScrollView);
        make.height.mas_equalTo(YM_FUNVIEW_HEIGHT);
    }];
}

- (void)updateFuns
{
    for (UIView *view in self.funsView.subviews) {
        [view removeFromSuperview];
    }

    if ([YMUserManager shareinstance].roleId.integerValue == 1001){
        //区域管理员不显示店员管理
        self.funArray = [NSArray arrayWithObjects:@(YMManagerTypeStore),@(YMManagerTypeDealer),@(YMManagerTypeOther),@(YMManagerTypeOther), nil];
    }else{
        self.funArray = [NSArray arrayWithObjects:@(YMManagerTypeStaff),@(YMManagerTypeStore),@(YMManagerTypeDealer),@(YMManagerTypeOther), nil];
    }
    
    CGFloat space = 0.5;
    CGFloat x = 0,y = space;
    CGFloat width = ([UIScreen mainScreen].bounds.size.width - space)/2;
    CGFloat height = (YM_FUNVIEW_HEIGHT - space * 3)/2;
    for (NSNumber *index in self.funArray) {
        
        NSString *name = nil;
        NSString *imagename = nil;
        if (index.integerValue == YMManagerTypeStaff) {
            name = @"店员管理";
            imagename = @"ym_index_staffManager";
        }else if(index.integerValue == YMManagerTypeStore) {
            name = @"门店管理";
            imagename = @"ym_index_storeManager";
        }else if(index.integerValue == YMManagerTypeDealer) {
            name = @"经销商管理";
            imagename = @"ym_index_dealerManager";
        }else {
            name = @"即将开启";
            imagename = @"ym_index_viomi";
        }
        
        YMImageButton *btn = [[YMImageButton alloc] initWithTitle:name image:[UIImage imageNamed:imagename] style:YMImageButtonStyleDefault];
        
        btn.frame = CGRectMake(x, y, width, height);
        [btn addTarget:self action:@selector(funAction:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = index.integerValue;
        [self.funsView addSubview:btn];
        
        x += width + space;
        if (x >= [UIScreen mainScreen].bounds.size.width) {
            x = 0;
            y += height + space;
        }
    }
    
    self.needUpdateFunsView = NO;
}



#pragma mark - Action

- (void)funAction:(UIButton *)button
{
    if (button.tag == YMManagerTypeStaff) {
        YMStaffManagerViewController *staffManagerVC = [[YMStaffManagerViewController alloc] init];
        [staffManagerVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:staffManagerVC animated:YES];
    }else if (button.tag == YMManagerTypeStore) {
        YMStoreManagerViewController *storeManagerVC = [[YMStoreManagerViewController alloc] init];
        [storeManagerVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:storeManagerVC animated:YES];
    }else if (button.tag == YMManagerTypeDealer) {
        YMDealerManagerViewController *dealerManagerVC = [[YMDealerManagerViewController alloc] init];
        [dealerManagerVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:dealerManagerVC animated:YES];
    }else{
        
    }
}

#pragma mark - Fetch
- (void)fetchData
{
    if(!self.countProfileAPI) {
        self.countProfileAPI = [[YMManagerAPI alloc] init];
    }
    
    [self.countProfileAPI fetchStoreCountCompleteBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        
        [self.containerScrollView.mj_header endRefreshing];
        
        if (status == YMCRequestSuccess) {
            //加载成功后，标记为no,暂时不需要再加载数据
            self.needLoadData = NO;
            
            NSDictionary *resultDict = [data notNullObjectForKey:@"result"];
            NSString *storeCountString = [[resultDict notNullObjectForKey:@"totalCount"] stringValue];
            if (![NSString isEmptyString:storeCountString]) {
                self.storeCountLabel.text = storeCountString;
            } else {
                self.storeCountLabel.text = @"0";
            }
            
            [self.countProfileAPI fetchDealerCountCompleteBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
                if (status == YMCRequestSuccess) {
                    NSDictionary *resultDict = [data notNullObjectForKey:@"result"];
                    NSString *dealerCountString = [[resultDict notNullObjectForKey:@"totalCount"] stringValue];
                    if (![NSString isEmptyString:dealerCountString]) {
                        self.dealerCountLabel.text = dealerCountString;
                    } else {
                        self.dealerCountLabel.text = @"0";
                    }
                } else {
                    [YMCAlertView showMessage:message];
                }
            }];
        } else {
            [YMCAlertView showMessage:message];
        }
    }];
}


#pragma Notification
- (void)loginNotificationEvent
{
    self.needLoadData = YES;
    self.needUpdateFunsView = YES;
    self.storeCountLabel.text = @"-";
    self.dealerCountLabel.text = @"-";
    
    [self fetchData];
}

@end
