//
//  YMFindPswViewController.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/3.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMFindPswViewController.h"
//这边的TextField 这边不能替换，TextField里面有“获取验证码”，耦合性太强。
#import "YMTextField.h"
#import "YMButton.h"
#import "YMUserAPI.h"
#import <UMMobClick/MobClick.h>

@interface YMFindPswViewController () <YMTextFieldDelegate>

//手机号码输入框
@property (nonatomic, strong) YMTextField *phoneTextField;
//验证码输入框
@property (nonatomic, strong) YMTextField *codeTextField;
//确认按钮
@property (nonatomic, strong)  YMButton *confirmBtn;

@property (nonatomic,strong) YMUserAPI *loginAPI;

@property (nonatomic,copy) NSString *phone;

@end

@implementation YMFindPswViewController

#pragma mark - Life
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"找回密码";
    [self drawView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick event:@"Mine_Fortget_password"];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"%s",__func__);
    if (self.loginAPI) {
        [self.loginAPI cancle];
    }
}

#pragma mark draw
- (void)drawView {
    self.view.backgroundColor = [UIColor whiteColor];
    
//    self.phoneTextField = [[YMCTextField alloc] initWithStyle:YMTextFieldStyleSendcodeAction];
    
    self.phoneTextField = [[YMTextField alloc] initWithStyle:YMTextFieldStyleSendcodeAction];
    self.phoneTextField.myDelegate = self;
    self.phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.phoneTextField.placeholder = @"输入手机号码";
    [self.view addSubview:self.phoneTextField];
    [self.phoneTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset([YMCScreenAdapter sizeBy750:40]);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@([YMCScreenAdapter sizeBy750:640]));
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
    }];
    
    self.codeTextField = [[YMTextField alloc] initWithStyle:YMTextFieldStyleNormal];
    self.codeTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.codeTextField.textAlignment = NSTextAlignmentLeft;
    self.codeTextField.placeholder = @"输入验证码";
    [self.view addSubview:self.codeTextField];
    [self.codeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneTextField.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@([YMCScreenAdapter sizeBy750:640]));
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
    }];
    
    self.confirmBtn = [[YMButton alloc] initWithStyle:YMButtonStyleNormal];
    [self.confirmBtn setTitle:@"确定" forState:UIControlStateNormal];
    [self.confirmBtn addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.confirmBtn];
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.codeTextField.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@([YMCScreenAdapter sizeBy750:640]));
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
    }];
}

#pragma mark - YMTextFieldDelegate
- (void)YMTextFieldAction:(YMTextField *)textfield {
    [self sendCodeAction];
}

#pragma mark - Action
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)sendCodeAction {
    [self.view endEditing:YES];
    
    NSString *telephone = [self.phoneTextField.text noWhitespaceAndNewlineCharacterString];
    
    if ([NSString isEmptyString:telephone]) {
        [self.phoneTextField changeStatus:YMTextFieldStatusError];
        [YMCAlertView showHubInView:self.view message:@"请输入手机号码"];
        return;
    }
    
    if (![telephone isValidPhone]) {
        [self.phoneTextField changeStatus:YMTextFieldStatusError];
        [YMCAlertView showHubInView:self.view message:@"请输入正确的手机号码"];
        return;
    }
    
    MBProgressHUD *statusHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    statusHud.mode = MBProgressHUDModeIndeterminate;
    statusHud.label.text = @"发送中...";
    
    if (!self.loginAPI) {
        self.loginAPI = [[YMUserAPI alloc] init];
    }
    
    [self.loginAPI recoverPasswordWithPhoneString:telephone completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        if (status == YMCRequestSuccess) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            self.phone  = telephone;
            [self.phoneTextField changeStatus:YMTextFieldStatusSendcoded];
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [YMCAlertView showMessage:message];
        }
    }];
    
}

- (void)confirmAction {
    NSString *code = [self.codeTextField.text noWhitespaceAndNewlineCharacterString];
    
    if (!self.phone) {
        [self.phoneTextField changeStatus:YMTextFieldStatusError];
        [YMCAlertView showHubInView:self.view message:@"请输入手机号码并获取验证码"];
        return;
    }
    if ([NSString isEmptyString:code]) {
        [self.codeTextField changeStatus:YMTextFieldStatusError];
        [YMCAlertView showHubInView:self.view message:@"请输入验证码"];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if (!self.loginAPI) {
        self.loginAPI = [[YMUserAPI alloc] init];
    }
    
    [self.loginAPI sendCaptchaString:code withPhoneString:self.phone completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        if (status == YMCRequestSuccess) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"密码重置成功，请注意查收短信" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            [YMCAlertView showMessage:message];
        }
    }];
}

@end
