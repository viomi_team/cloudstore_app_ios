//
//  YMYMYMLoginViewController.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/3.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMInputBaseViewController.h"
#import "YMLoginModel.h"


@interface YMLoginViewController : YMInputBaseViewController

@property (nonatomic, strong) YMLoginModel *loginModel;

@end
