//
//  YMYMLoginViewController.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/3.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMLoginViewController.h"
#import "YMFindPswViewController.h"
#import "YMTextField.h"
#import "YMButton.h"
#import "YMAPIManager.h"
#import <MBProgressHUD.h>
#import "YMSecurity.h"
#import "YMUserAPI.h"
#import "YMUserManager.h"
#import <YMCommon/UIImage+ymc.h>

//zcm add：友盟统计
#import "UMMobClick/MobClick.h"

@interface YMLoginViewController()<UINavigationControllerDelegate, UINavigationBarDelegate, YMTextFieldDelegate>

//用户帐号输入框
@property (nonatomic, strong) YMTextField *userNameTextField;
//用户密码输入框
@property (nonatomic, strong) YMTextField *passwordTextField;
//登陆按钮
@property (nonatomic, strong) YMButton *loginBtn;
//忘记密码按钮
@property (nonatomic, strong) UIButton *forgotPswBtn;
//登陆接口
@property (nonatomic, strong) YMUserAPI *loginAPIManager;
//帐号输入框模糊效果视图
@property (nonatomic, strong) UIVisualEffectView *userNameVisualEfView;
//密码输入框模糊效果视图
@property (nonatomic, strong) UIVisualEffectView *pwdVisualEfView;

@end

@implementation YMLoginViewController

#pragma mark - Life
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.offset = 100;
    
    
    [self drawView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick event:@"Mine_Login"];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"%s",__func__);
    if (self.loginAPIManager) {
        [self.loginAPIManager cancle];
    }
}

#pragma mark - Draw
- (void)drawView {
    self.view.backgroundColor = [UIColor clearColor];
    
    //背景图片
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background"]];
    [self.view insertSubview:backgroundImageView belowSubview:self.parentScrollView];
    [backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    //云米管家logo
    UIImageView *logoImageView = [[UIImageView alloc] init];
    logoImageView.backgroundColor = [UIColor clearColor];
    logoImageView.image = [UIImage imageNamed:@"ic_viomi"];
    [self.parentScrollView addSubview:logoImageView];
    [logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.parentScrollView).with.offset([YMCScreenAdapter sizeBy750:199]);
        make.centerX.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake([YMCScreenAdapter sizeBy750:184], [YMCScreenAdapter sizeBy750:184]));
    }];
    
    //“云分销”label
    UILabel *ymLabel = [[UILabel alloc] init];
    ymLabel.backgroundColor = [UIColor clearColor];
    ymLabel.textColor = [UIColor whiteColor];
    ymLabel.font = YM_FONT([YMCScreenAdapter sizeBy750:48]);
    ymLabel.text = @"云分销";
    [self.parentScrollView addSubview:ymLabel];
    [ymLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(logoImageView.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:35]);
        make.centerX.equalTo(self.view);
    }];
    
    //讯息相关label
    UILabel *infoLabel = [[UILabel alloc] init];
    infoLabel.backgroundColor = [UIColor clearColor];
    infoLabel.textColor = [UIColor colorWithWhite:1.0 alpha:0.4];
    infoLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:28]);
    infoLabel.text = @"会员营销・订单查询・商品管理・顾客管理";
    [self.parentScrollView addSubview:infoLabel];
    [infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(ymLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:20]);
        make.centerX.equalTo(self.parentScrollView);
    }];
    
    //模糊效果视图
    self.userNameVisualEfView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
    self.userNameVisualEfView.alpha = 1.0;
    self.userNameVisualEfView.layer.cornerRadius = [YMCScreenAdapter sizeBy750:50];
    self.userNameVisualEfView.layer.masksToBounds = YES;
    [self.parentScrollView addSubview:self.userNameVisualEfView];
    [self.userNameVisualEfView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(infoLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:180]);
        make.width.equalTo(@([YMCScreenAdapter sizeBy750:640]));
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
    }];
    
    //帐号输入框
    self.userNameTextField = [[YMTextField alloc] initWithStyle:YMTextFieldStyleVisualEffect];
    self.userNameTextField.tag = 1;
    self.userNameTextField.keyboardType = UIKeyboardTypeASCIICapable;
    self.userNameTextField.returnKeyType = UIReturnKeyNext;
    self.userNameTextField.placeholder = @"输入帐号";
    [self.parentScrollView addSubview:self.userNameTextField];
    [self.userNameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(infoLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:180]);
        make.width.equalTo(@([YMCScreenAdapter sizeBy750:640]));
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
    }];
    
    //模糊效果视图
    self.pwdVisualEfView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
    self.pwdVisualEfView.alpha = 1.0;
    self.pwdVisualEfView.layer.cornerRadius = [YMCScreenAdapter sizeBy750:50];
    self.pwdVisualEfView.layer.masksToBounds = YES;
    [self.parentScrollView addSubview:self.pwdVisualEfView];
    [self.pwdVisualEfView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userNameTextField.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@([YMCScreenAdapter sizeBy750:640]));
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
    }];
    
    //密码输入框
    self.passwordTextField = [[YMTextField alloc] initWithStyle:YMTextFieldStyleVisualEffect];
    self.passwordTextField.tag = 2;
    self.passwordTextField.myDelegate = self;
    self.passwordTextField.secureTextEntry = YES;
    self.passwordTextField.returnKeyType = UIReturnKeyDone;
    self.passwordTextField.placeholder = @"输入密码";
    [self.parentScrollView addSubview:self.passwordTextField];
    [self.passwordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userNameTextField.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@([YMCScreenAdapter sizeBy750:640]));
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
    }];
    
    //登陆按钮
    self.loginBtn = [[YMButton alloc] initWithStyle:YMButtonStyleWhite];
    [self.loginBtn setTitle:@"登录" forState:UIControlStateNormal];
    [self.loginBtn addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    [self.parentScrollView addSubview:self.loginBtn];
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordTextField.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@([YMCScreenAdapter sizeBy750:640]));
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
    }];
    
    //忘记密码按钮
    self.forgotPswBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.forgotPswBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.forgotPswBtn setTitle:@"忘记密码" forState:UIControlStateNormal];
    [self.forgotPswBtn addTarget:self action:@selector(forgotPswAction) forControlEvents:UIControlEventTouchUpInside];
    [self.parentScrollView addSubview:self.forgotPswBtn];
    [self.forgotPswBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.loginBtn.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:50]);
        make.bottom.equalTo(self.parentScrollView).with.offset([YMCScreenAdapter sizeBy750:-106]);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@([YMCScreenAdapter sizeBy750:300]));
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
    }];
}

#pragma mark - YMTextFieldDelegate
- (void)YMTextFieldDone:(YMTextField *)textfield {
   [self loginAction];
}

#pragma mark - Action
//因为有两种方法可以发起网络请求，为避免重复书写判断部分的代码，暂时把过滤条件的代码统一写到此方法里面
- (void)loginAction {
    NSString *username = [self.userNameTextField.text noWhitespaceAndNewlineCharacterString];
    NSString *password = [self.passwordTextField.text noWhitespaceAndNewlineCharacterString];
    
    if ([NSString isEmptyString:username]) {
        [YMCAlertView showMessage:@"请输入用户名"];
        [self.userNameTextField becomeFirstResponder];
        return;
    }
    if ([NSString isEmptyString:password]) {
        [YMCAlertView showMessage:@"请输入密码"];
        [self.passwordTextField becomeFirstResponder];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self.view endEditing:YES];
    
    if (!self.loginAPIManager) {
        self.loginAPIManager = [[YMUserAPI alloc] init];
    }

    [self.loginAPIManager loginWithUserName:username password:password completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        if (status == YMCRequestSuccess) {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            self.loginModel = [YMLoginModel mj_objectWithKeyValues:data];
            
            NSUInteger roleId = self.loginModel.user.roleId;
            //判断用户是否为超级管理员/城市运营/区域管理员，否则显示权限不足
            if (roleId == 1  || roleId == 21 || roleId == 1001)
            {
                [[YMUserManager shareinstance] saveUserAccount:self.userNameTextField.text password:self.passwordTextField.text];
                [[YMUserManager shareinstance] saveUser:self.loginModel];
                
                //zcm add: 友盟账号管理
                [MobClick profileSignInWithPUID:[YMUserManager shareinstance].account];
                
                //登录成功时，发出消息，首页及其他需要处理消息
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ym_login_event" object:nil];
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
            } else {
                [YMCAlertView showMessage:@"该账号没有权限使用云分销app"];
            }
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [YMCAlertView showMessage:message];
        }
    }];
}

- (void)forgotPswAction {
    YMFindPswViewController *findPswVC = [[YMFindPswViewController alloc] init];
    [self.navigationController pushViewController:findPswVC animated:YES];
}

@end
