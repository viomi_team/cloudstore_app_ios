//
//  YMResetPswViewController.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/3.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMChangePswViewController.h"
#import "YMButton.h"
//#import "YMTextField.h"
#import <YMCommon/YMCTextField.h>
#import <MBProgressHUD.h>
#import "YMUserAPI.h"
#import "YMUserManager.h"
#import "YMLoginModel.h"
#import <UMMobClick/MobClick.h>

@interface YMChangePswViewController () ///<YMTextFieldDelegate>

//
@property (nonatomic, strong) YMCTextField *oldPswTextField;
//
@property (nonatomic, strong) YMCTextField *confirmPswTextField1;

@property (nonatomic, strong) YMCTextField *confirmPswTextField2;
//
@property (nonatomic, strong) YMButton *confirmBtn;
//
@property (nonatomic, strong) YMUserAPI *changePwdAPI;
//
@property (nonatomic, strong) YMLoginModel *loginModel;

@end

@implementation YMChangePswViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick event:@"Mine_Change_Password"];
}

#pragma mark life
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"修改密码";
    
    [self drawView];
}

- (void)dealloc
{
    NSLog(@"%s",__func__);
    if (self.changePwdAPI) {
        [self.changePwdAPI cancle];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark draw
- (void)drawView
{
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel *accountLabel = [[UILabel alloc] init];
    accountLabel.font = YM_FONT([YMCScreenAdapter sizeBy750:36]);
    [self.parentScrollView addSubview:accountLabel];
    [accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.parentScrollView).with.offset([YMCScreenAdapter sizeBy750:70]);
        make.centerX.equalTo(self.view);
    }];
    NSMutableAttributedString *accountstring = [[NSMutableAttributedString alloc] initWithString:@"当前帐号：" attributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexString:YMCOLOR_LIGHT_GRAY]}];
    [accountstring appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[YMUserManager shareinstance].account attributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexString:YMCOLOR_THEME]}]];
    accountLabel.attributedText = accountstring;
    
    //输入原密码框
//    self.oldPswTextField = [[YMCTextField alloc] initWithStyle:YMTextFieldStyleNormal];
    self.oldPswTextField = [[YMCTextField alloc] initWithType:  YMCTextFieldTypeActionPassword
 icon:nil action:^{}];
    self.oldPswTextField.tag = 1;
    //self.oldPswTextField.delegate = self;
    self.oldPswTextField.returnKeyType = UIReturnKeyNext;
    self.oldPswTextField.placeholder = @"请输入旧密码";
    self.oldPswTextField.secureTextEntry = YES;
    [self.parentScrollView addSubview:self.oldPswTextField];
    [self.oldPswTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(accountLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:70]);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@([YMCScreenAdapter sizeBy750:640]));
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
    }];

    //输入新密码框
//    self.confirmPswTextField1 = [[YMTextField alloc] initWithStyle:YMTextFieldStyleNormal];
    self.confirmPswTextField1 = [[YMCTextField alloc] initWithType:YMCTextFieldTypeActionPassword icon:nil action:^{}];
    self.confirmPswTextField1.tag = 2;
    self.confirmPswTextField1.returnKeyType = UIReturnKeyNext;
    self.confirmPswTextField1.placeholder = @"请输入新密码";
    self.confirmPswTextField1.secureTextEntry = YES;
    [self.parentScrollView addSubview:self.confirmPswTextField1];
    [self.confirmPswTextField1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.oldPswTextField.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@([YMCScreenAdapter sizeBy750:640]));
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
    }];
    
    //输入新密码框
//    self.confirmPswTextField2 = [[YMTextField alloc] initWithStyle:YMTextFieldStyleNormal];
    self.confirmPswTextField2 = [[YMCTextField alloc] initWithType:YMCTextFieldTypeActionPassword icon:nil action:^{
        [self changePasswordAction];
    }];
    self.confirmPswTextField2.tag = 3;
    self.confirmPswTextField2.returnKeyType = UIReturnKeyDone;
    self.confirmPswTextField2.placeholder = @"请再次输入新密码";
    self.confirmPswTextField2.secureTextEntry = YES;
//    self.confirmPswTextField2.myDelegate = self;
    [self.parentScrollView addSubview:self.confirmPswTextField2];
    [self.confirmPswTextField2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.confirmPswTextField1.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@([YMCScreenAdapter sizeBy750:640]));
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
    }];

    //确认按钮
    self.confirmBtn = [[YMButton alloc] initWithStyle:YMButtonStyleNormal];
    [self.confirmBtn setTitle:@"确定" forState:UIControlStateNormal];
    [self.confirmBtn addTarget:self action:@selector(changePasswordAction) forControlEvents:UIControlEventTouchUpInside];
    [self.parentScrollView addSubview:self.confirmBtn];
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.confirmPswTextField2.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:40]);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@([YMCScreenAdapter sizeBy750:640]));
        make.height.equalTo(@([YMCScreenAdapter sizeBy750:100]));
        make.bottom.equalTo(self.parentScrollView).with.offset(-10);
    }];
}

#pragma mark - YMTextFieldDelegate

//- (void)YMTextFieldDone:(YMTextField *)textfield
//{
//    [self changePasswordAction];
//}

#pragma mark - Action
- (void)changePasswordAction
{
    NSString *oldpwd = [self.oldPswTextField.text noWhitespaceAndNewlineCharacterString];
    NSString *confirmPsw1 = [self.confirmPswTextField1.text noWhitespaceAndNewlineCharacterString];
    NSString *confirmPsw2 = [self.confirmPswTextField2.text noWhitespaceAndNewlineCharacterString];
    
    if ([NSString isEmptyString:oldpwd]) {
        [YMCAlertView showMessage:@"请输入旧密码"];
        [self.oldPswTextField becomeFirstResponder];
        return;
    }
    if ([NSString isEmptyString:confirmPsw1]) {
        [YMCAlertView showMessage:@"请输入新密码"];
        [self.confirmPswTextField1 becomeFirstResponder];
        return;
    }
    if (![confirmPsw1 isValidPassword]) {
        [YMCAlertView showMessage:@"请输入只包含字母、数字，长度为6~20的密码"];
        [self.confirmPswTextField1 becomeFirstResponder];
        return;
    }
    if ([NSString isEmptyString:confirmPsw2]) {
        [YMCAlertView showMessage:@"请再次输入新密码"];
        [self.confirmPswTextField2 becomeFirstResponder];
        return;
    }
    if (![confirmPsw1 isEqualToString:confirmPsw2]) {
        [YMCAlertView showMessage:@"两次输入密码不一致"];
        [self.confirmPswTextField2 becomeFirstResponder];
        return;
    }
    
    if (!self.changePwdAPI) {
        self.changePwdAPI = [[YMUserAPI alloc] init];
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self.changePwdAPI changePasswordWitholdPassword:oldpwd newPassword:confirmPsw1 completeBlock:^(YMCRequestStatus status, NSString *message, NSDictionary *data) {
        if (status == YMCRequestSuccess) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"修改成功" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [YMCAlertView showMessage:message];
        }
    }];

}

@end
