//
//  YMMyCenterCell.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/2.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *kCellIdentifier_YMMyCenterCell = @"YMMyCenterCell";

@interface YMMyCenterCell : UITableViewCell

//
@property (nonatomic, strong) UIImageView *changePswImageView;
//
@property (nonatomic, strong) UILabel *changePswLabel;
//
@property (nonatomic, strong) UIImageView *verisonImageView;
//
@property (nonatomic, strong) UILabel *verisonLabel;
//
@property (nonatomic, strong) UILabel *logoutLabel;

- (void)configCellWithIndexPath:(NSIndexPath *)indexPath;

@end
