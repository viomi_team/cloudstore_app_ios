//
//  YMMyCenterCell.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/2.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMMyCenterCell.h"

@interface YMMyCenterCell()

@property (nonatomic, strong) UIView *lineView;

@end

@implementation YMMyCenterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)configCellWithIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            UIView *topLineView = [[UIView alloc] init];
            topLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
            [self.contentView addSubview:topLineView];
            [topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.contentView);
                make.left.equalTo(self.contentView);
                make.right.equalTo(self.contentView);
                make.height.equalTo(@0.5);
            }];
            
            if (!self.changePswImageView) {
                self.changePswImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_pw"]];
                [self.contentView addSubview:self.changePswImageView];
                [self.changePswImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(self.contentView);
                    make.left.equalTo(self.contentView).with.offset(14);
                    make.height.equalTo(@([YMCScreenAdapter sizeBy750:45]));
                    make.width.equalTo(@([YMCScreenAdapter sizeBy750:45]));
                }];
            }
            
            if (!self.changePswLabel) {
                self.changePswLabel = [[UILabel alloc] init];
                self.changePswLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:34]);
                self.changePswLabel.text = @"修改密码";
                self.changePswLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
                [self.contentView addSubview:self.changePswLabel];
                [self.changePswLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(self.contentView);
                    make.left.equalTo(self.changePswImageView.mas_right).with.offset(14);
                }];
                
            }
            
            //下划线
            UIView *bottomLineView = [[UIView alloc] init];
            bottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
            [self.contentView addSubview:bottomLineView];
            [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.changePswLabel.mas_left);;
                make.right.equalTo(self.contentView);
                make.height.equalTo(@0.5);
                make.bottom.equalTo(self.contentView);
            }];
        } else {
            if (!self.verisonImageView) {
                self.verisonImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_info"]];
                [self.contentView addSubview:self.verisonImageView];
                [self.verisonImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(self.contentView);
                    make.left.equalTo(self.contentView).with.offset(14);
                    make.height.equalTo(@([YMCScreenAdapter sizeBy750:45]));
                    make.width.equalTo(@([YMCScreenAdapter sizeBy750:45]));
                }];
            }
            
            if (!self.verisonLabel) {
                self.verisonLabel = [[UILabel alloc] init];
                self.verisonLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:34]);
                self.verisonLabel.text = @"版本信息";
                self.verisonLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
                [self.contentView addSubview:self.verisonLabel];
                [self.verisonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(self.contentView);
                    make.left.equalTo(self.verisonImageView.mas_right).with.offset(14);
                }];
            }
            
            UIView *bottomLineView = [[UIView alloc] init];
            bottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
            [self.contentView addSubview:bottomLineView];
            [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.contentView);
                make.left.equalTo(self.contentView);
                make.right.equalTo(self.contentView);
                make.height.equalTo(@0.5);
            }];
        }
        
    } else {
        
        UIView *topLineView = [[UIView alloc] init];
        topLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
        [self.contentView addSubview:topLineView];
        [topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.height.equalTo(@0.5);
        }];
        
        if (!self.logoutLabel) {
            self.logoutLabel = [[UILabel alloc] init];
            self.logoutLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:34]);
            self.logoutLabel.textColor = [UIColor redColor];
            self.logoutLabel.text = @"退出登录";
            self.logoutLabel.textAlignment = NSTextAlignmentCenter;
            [self.contentView addSubview:self.logoutLabel];
            [self.logoutLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(self.contentView);
            }];
            
            UIView *bottomLineView = [[UIView alloc] init];
            bottomLineView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
            [self.contentView addSubview:bottomLineView];
            [bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.contentView);
                make.left.equalTo(self.contentView);
                make.right.equalTo(self.contentView);
                make.height.equalTo(@0.5);
            }];
        }
    }
}

@end
