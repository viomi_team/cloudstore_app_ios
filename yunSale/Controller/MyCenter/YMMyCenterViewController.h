//
//  YMMyCenterViewController.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/2.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMMyCenterViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
