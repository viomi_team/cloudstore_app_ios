//
//  YMMyCenterViewController.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/2.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMMyCenterViewController.h"
#import "YMMyCenterCell.h"
#import "YMChangePswViewController.h"
#import "YMLoginViewController.h"
//#import "YMNavigationController.h"
#import <YMCommon/YMCBaseNavigationController.h>
#import "YMMyInfoTableViewCell.h"
#import "YMTwoItemTableHeaderView.h"
#import "YMIconNameTableViewCell.h"
#import "YMOneItemTableViewCell.h"
//#import "YMAppUpdateAPI.h"
#import <YMCommon/YMCAppUpdateAPI.h>

#import <UMMobClick/MobClick.h>

@interface YMMyCenterViewController ()

//【设置】表视图
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic,assign) BOOL hasNewVersion;

@end

@implementation YMMyCenterViewController

#pragma mark - Life
- (void)viewDidLoad {
    [super viewDidLoad];

    [self drawView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [MobClick event:@"Mine"];
    
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    NSLog(@"%s",__func__);
}

#pragma mark - Draw
- (void)drawView {
    //tableView
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_TABLE_BACKGROUND];
    self.tableView.separatorColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    self.tableView.separatorInset = UIEdgeInsetsMake(0,[YMCScreenAdapter sizeBy750:90], 0, 0);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[YMIconNameTableViewCell class] forCellReuseIdentifier:kYMIconNameTableViewCell];
    [self.tableView registerClass:[YMMyInfoTableViewCell class] forCellReuseIdentifier:kYMMyInfoTableViewCell];
    [self.tableView registerClass:[YMOneItemTableViewCell class] forCellReuseIdentifier:kYMOneItemTableViewCell];
    [self.view addSubview:_tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

#pragma mark - UITableViewDelegate & UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    switch (section) {
//        case 0:
//            return 1;
//            break;
//        case 1:
//            return 2;
//            break;
//        case 2:
//            return 1;
//            break;
//        default:
//            return 0;
//            break;
//    }
    return section == 1? 2 : 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
//        return [YMScreenAdapter sizeBy750:170];
//    }else{
//        return [YMScreenAdapter sizeBy750:100];
//    }
    return indexPath.section == 0? [YMCScreenAdapter sizeBy750:170] : [YMCScreenAdapter sizeBy750:100];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return [YMCScreenAdapter sizeBy750:24];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        YMMyInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kYMMyInfoTableViewCell forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.iconUrl = nil;
        cell.name = [YMUserManager shareinstance].name;
        
        if ([YMUserManager shareinstance].roleId.integerValue != 1001) {
            cell.details = [NSString stringWithFormat:@"%@ | %@",[YMUserManager shareinstance].channelName,[YMUserManager shareinstance].roleName];
        }else{
            cell.details = [NSString stringWithFormat:@"区域管理 | %@",[YMUserManager shareinstance].roleName];
        }
        
        return cell;
    }else if(indexPath.section == 1){
        YMIconNameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kYMIconNameTableViewCell forIndexPath:indexPath];
        
        if (indexPath.row == 0) {
            cell.iconImageView.image = [UIImage imageNamed:@"icon_pw"];
            cell.nameLabel.text = @"修改密码";
        } else {
            cell.iconImageView.image = [UIImage imageNamed:@"icon_info"];
            cell.nameLabel.text = @"版本信息";
            
            NSString *latestVersion = [YMCAppUpdateAPI latestAppversion];
            NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
            
            NSMutableAttributedString *versionInfo = [[NSMutableAttributedString alloc] initWithString:currentVersion];
            
            if ([latestVersion compare:currentVersion options:NSNumericSearch] == NSOrderedDescending){
                [versionInfo appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" (检测到新版本:%@)",latestVersion] attributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexString:YMCOLOR_RED]}]];
                self.hasNewVersion = YES;
                cell.selectionStyle = UITableViewCellSelectionStyleDefault;
            } else {
                self.hasNewVersion = NO;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.detailLabel.attributedText = versionInfo;
        }
        return cell;
    } else {
        YMOneItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kYMOneItemTableViewCell forIndexPath:indexPath];
        cell.titleLabel.text = @"退出登录";
        cell.titleLabel.textColor = [UIColor colorWithHexString:YMCOLOR_RED];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    if (indexPath.section == 1) {
//        if (indexPath.row == 0) {
//            [self p_goToChangePswVC];
//        }else{
//            [self p_goToUpdVerVC];
//        }
        indexPath.row == 0? [self p_goToChangePswVC] : [self p_goToUpdVerVC];
    }else if (indexPath.section == 2) {
        [self p_logoutAction];
    }
}

//退出登陆操作
#pragma mark - Private
- (void)p_logoutAction {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"确定退出当前帐号吗？" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *confirmExitAction = [UIAlertAction actionWithTitle:@"确定退出" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [[YMUserManager shareinstance] logout];
        
        //zcm add: 关闭该账号的统计
        [MobClick profileSignOff];
        
        [self p_goToLoginVC];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertController addAction:confirmExitAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)p_goToChangePswVC {
    YMChangePswViewController *resetPwdVC = [[YMChangePswViewController alloc] init];
    resetPwdVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:resetPwdVC animated:YES];
}

- (void)p_goToUpdVerVC {
    if (self.hasNewVersion) {
        NSString *updateUrl = [YMCAppUpdateAPI appUpdateUrl];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:updateUrl]];
    }
}

- (void)p_goToLoginVC {
    YMLoginViewController *loginVC = [[YMLoginViewController alloc] init];
    YMCBaseNavigationController *naviVC = [[YMCBaseNavigationController alloc] initWithRootViewController:loginVC];
    [self presentViewController:naviVC animated:YES completion:nil];
}

@end
