//
//  YMMyInfoTableViewCell.h
//  yunSale
//
//  Created by liushilou on 16/11/11.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const kYMMyInfoTableViewCell = @"YMMyInfoTableViewCell";


@interface YMMyInfoTableViewCell : UITableViewCell

//头像url
@property (nonatomic,copy) NSString *iconUrl;
//
@property (nonatomic,copy) NSString *name;
//
@property (nonatomic,strong) NSString *details;



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;


@end
