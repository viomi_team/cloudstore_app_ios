//
//  YMMyInfoTableViewCell.m
//  yunSale
//
//  Created by liushilou on 16/11/11.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMMyInfoTableViewCell.h"
#import <UIImageView+WebCache.h>

@interface YMMyInfoTableViewCell()

@property (nonatomic,strong) UIImageView *iconImageView;
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *detailsLabel;

@end

@implementation YMMyInfoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.contentMode = UIViewContentModeScaleToFill;
        [self.contentView addSubview:_iconImageView];
        _iconImageView.backgroundColor = [UIColor whiteColor];
        [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.top.equalTo(self.contentView).with.offset([YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.bottom.equalTo(self.contentView).with.offset(-[YMCScreenAdapter sizeBy750:YMSPACE_AROUND]);
            make.width.mas_equalTo([YMCScreenAdapter sizeBy750:122]);
        }];
        //设置圆角
        _iconImageView.layer.cornerRadius = 5;
        _iconImageView.layer.masksToBounds = YES;
        _iconImageView.layer.borderWidth = 0.5;
        _iconImageView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_TITLE]);
        _nameLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
        [self.contentView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_iconImageView.mas_right).with.offset(YMSPACE_AROUND);
            make.bottom.equalTo(_iconImageView.mas_centerY).with.offset(-[YMCScreenAdapter sizeBy750:15]);
        }];
        
        _detailsLabel = [[UILabel alloc] init];
        _detailsLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:YMFONTSIZE_DETAILS]);
        _detailsLabel.textColor = [UIColor colorWithHexString:YMCOLOR_GRAY];
        [self.contentView addSubview:_detailsLabel];
        [_detailsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_iconImageView.mas_right).with.offset(YMSPACE_AROUND);
            make.top.equalTo(_iconImageView.mas_centerY).with.offset([YMCScreenAdapter sizeBy750:15]);
        }];
    }
    return self;
}

- (void)setIconUrl:(NSString *)iconUrl
{
    if (!iconUrl) {
        self.iconImageView.image = [UIImage imageNamed:@"ym_index_viomicloud"];
    }else{
        _iconUrl = [iconUrl copy];
        [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:_iconUrl] placeholderImage:[UIImage imageNamed:@"userIcon5"]];
    }
}

- (void)setName:(NSString *)name
{
    _name = [name copy];
    
    self.nameLabel.text = _name;
}

- (void)setDetails:(NSString *)details
{
    _details = [details copy];
    self.detailsLabel.text = _details;
}


@end
