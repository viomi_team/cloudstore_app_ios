//
//  YMBaseNavigationController.m
//  WaterPurifier
//
//  Created by liushilou on 16/6/11.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "YMNavigationController.h"

@interface YMNavigationController ()<UINavigationControllerDelegate>

@end

@implementation YMNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  self.navigationBar.tintColor = [UIColor whiteColor];
  [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
  self.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    viewController.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

@end
