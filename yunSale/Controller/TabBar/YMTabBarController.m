//
//  YMTabBarController.m
//  yunSale
//
//  Created by liushilou on 16/5/16.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "YMTabBarController.h"
#import <YMCommon/UIImage+ymc.h>
//#import "YMNavigationController.h"
#import <YMCommon/YMCBaseNavigationController.h>
#import "YMHomeViewController.h"
#import "YMManageViewController.h"
#import "YMMyCenterViewController.h"

@interface YMTabBarController ()

@end

@implementation YMTabBarController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //修改UITabBar 背景、线条样式
    UIImage *shadowimage = [UIImage imageWithColor:[UIColor colorWithHexString:YMCOLOR_LINE] size:CGSizeMake(1, 0.5)];
    UIImage *backimage = [UIImage imageWithColor:[UIColor whiteColor] size:CGSizeMake(1, 1)];
    [[UITabBar appearance] setShadowImage:shadowimage];
    [[UITabBar appearance] setBackgroundImage:backimage];
    
    //去掉黑线
    [[UINavigationBar appearance]  setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:YMCOLOR_THEME] size:CGSizeMake(10, 10)] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    UIImage *backButtonImage = [[UIImage imageNamed:@"ic_back"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(NSIntegerMin, NSIntegerMin) forBarMetrics:UIBarMetricsDefault];
    
    //【首页】根视图
    YMHomeViewController *homeVC = [[YMHomeViewController alloc] init];
    [self p_setupChildViewController:homeVC navTitle:@"首页" tabTitle:@"首页" imageName:@"icon_home_nor" selectedImageName:@"icon_home_press"];
    
    //【管理】根视图
    YMManageViewController *manageVC = [[YMManageViewController alloc] init];
    [self p_setupChildViewController:manageVC navTitle:@"管理" tabTitle:@"管理" imageName:@"icon_manage_nor" selectedImageName:@"icon_manage_press"];
    
    //【我的】根视图
    YMMyCenterViewController *myCenterVC = [[YMMyCenterViewController alloc] init];
    [self p_setupChildViewController:myCenterVC navTitle:@"我的" tabTitle:@"我的" imageName:@"icon_profile_nor" selectedImageName:@"icon_profile_press"];
    
    //是否透明
    [self.tabBar setTranslucent:NO];
    self.tabBar.barTintColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1];

    //文字选中与未选中的颜色
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11],NSForegroundColorAttributeName:[UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1]} forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11],NSForegroundColorAttributeName:[UIColor colorWithHexString:YMCOLOR_THEME]} forState:UIControlStateSelected];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Private
- (void)p_setupChildViewController:(UIViewController *)childVC
                          navTitle:(NSString *)navTitle
                          tabTitle:(NSString *)tabTitle
                         imageName:(NSString *)imageName
                 selectedImageName:(NSString *)selectedImageName {
    
    //navTitle 要后于 tabTitle 设置，否则导航栏的标题会被 tabTitle 覆盖
    NSDictionary *fontDic = @{NSFontAttributeName:YM_FONT([YMCScreenAdapter fontsizeBy750:38])};
    [self.tabBarItem setTitleTextAttributes:fontDic forState:UIControlStateNormal];
    childVC.title = tabTitle;
    childVC.navigationItem.title = navTitle;

    //防止图片被渲染
    childVC.tabBarItem.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childVC.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    YMCBaseNavigationController *navVC = [[YMCBaseNavigationController alloc] initWithRootViewController:childVC];

    [self addChildViewController:navVC];
}

@end
