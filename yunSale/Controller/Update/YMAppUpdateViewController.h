//
//  YMAppUpdateViewController.h
//  yunSale
//
//  Created by liushilou on 16/11/30.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMAppUpdateViewController : UIViewController

- (instancetype)initWithVersion:(NSString *)version notes:(NSString *)notes url:(NSString *)url;

@end
