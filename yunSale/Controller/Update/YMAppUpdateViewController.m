//
//  YMAppUpdateViewController.m
//  yunSale
//
//  Created by liushilou on 16/11/30.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMAppUpdateViewController.h"

@interface YMAppUpdateViewController ()

@property (nonatomic,copy) NSString *version;

@property (nonatomic,copy) NSString *notes;

@property (nonatomic,copy) NSString *updateUrl;

@property (nonatomic,strong) UILabel *versionLabel;
@property (nonatomic,strong) UILabel *notesLabel;

@end

@implementation YMAppUpdateViewController

- (instancetype)initWithVersion:(NSString *)version notes:(NSString *)notes url:(NSString *)url
{
    self = [self init];
    if (self) {
        _version = [version copy];
        _notes = [notes copy];
        _updateUrl = [url copy];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.modalPresentationStyle=UIModalPresentationOverCurrentContext;
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self drawView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)drawView {
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    UIView *parentView = [[UIView alloc] init];
    parentView.layer.cornerRadius = [YMCScreenAdapter sizeBy750:16];
    parentView.layer.masksToBounds = YES;
    parentView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:parentView];
    [parentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
        make.width.mas_equalTo([YMCScreenAdapter sizeBy750:580]);
    }];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"发现新版本";
    titleLabel.backgroundColor = [UIColor colorWithHexString:YMCOLOR_THEME];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:32]);
    titleLabel.textColor = [UIColor whiteColor];
    [parentView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(parentView).with.offset(0);
        make.top.equalTo(parentView).with.offset(0);
        make.right.equalTo(parentView).with.offset(0);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:100]);
    }];
    
    UIView *btnsView = [[UIView alloc] init];
    btnsView.backgroundColor = [UIColor colorWithHexString:YMCOLOR_LINE];
    [parentView addSubview:btnsView];
    [btnsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(parentView).with.offset(0);
        make.bottom.equalTo(parentView).with.offset(0);
        make.right.equalTo(parentView).with.offset(0);
        make.height.mas_equalTo([YMCScreenAdapter sizeBy750:90]);
    }];
    
//    UIImageView *backimageView = [[UIImageView alloc] init];
//    backimageView.image = [UIImage imageNamed:@"ym_appupdate_viomi"];
//    [parentView addSubview:backimageView];
//    [parentView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(titleLabel.mas_bottom).with.offset([YMScreenAdapter sizeBy750:40]);
//        make.bottom.equalTo(btnsView.mas_top).with.offset([YMScreenAdapter sizeBy750:-40]);
//    }];
    
    self.versionLabel = [[UILabel alloc] init];
    //self.versionLabel.numberOfLines = 2;
    self.versionLabel.text = [NSString stringWithFormat:@"云米V%@版本发布啦,马上下载体验吧!",self.version];
    self.versionLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:26]);
    self.versionLabel.textColor = [UIColor colorWithHexString:YMCOLOR_BLACK];
    [parentView addSubview:self.versionLabel];
    [self.versionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:30]);
        make.left.equalTo(parentView).with.offset([YMCScreenAdapter sizeBy750:30]);
        make.right.equalTo(parentView).with.offset([YMCScreenAdapter sizeBy750:-30]);
    }];
    
    self.notesLabel = [[UILabel alloc] init];
    self.notesLabel.numberOfLines = 10;
    self.notesLabel.text = self.notes;
    self.notesLabel.font = YM_FONT([YMCScreenAdapter fontsizeBy750:24]);
    self.notesLabel.textColor = [UIColor colorWithHexString:YMCOLOR_DARK_GRAY];
    [parentView addSubview:self.notesLabel];
    [self.notesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.versionLabel.mas_bottom).with.offset([YMCScreenAdapter sizeBy750:24]);
        make.left.equalTo(parentView).with.offset([YMCScreenAdapter sizeBy750:30]);
        make.right.equalTo(parentView).with.offset([YMCScreenAdapter sizeBy750:-30]);
        make.bottom.equalTo(btnsView.mas_top).with.offset([YMCScreenAdapter sizeBy750:-30]);
    }];
    
    UIButton *cancleBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    cancleBtn.backgroundColor = [UIColor whiteColor];
    [cancleBtn setTitle:@"暂时不用" forState:UIControlStateNormal];
    [cancleBtn.titleLabel setFont:YM_FONT([YMCScreenAdapter fontsizeBy750:26])];
    [cancleBtn setTitleColor:[UIColor colorWithHexString:YMCOLOR_DARK_GRAY] forState:UIControlStateNormal];
    [cancleBtn addTarget:self action:@selector(cancleAction) forControlEvents:UIControlEventTouchUpInside];
    [btnsView addSubview:cancleBtn];
    
    UIButton *updateBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    updateBtn.backgroundColor = [UIColor whiteColor];
    [updateBtn setTitle:@"马上更新" forState:UIControlStateNormal];
    [updateBtn.titleLabel setFont:YM_FONT([YMCScreenAdapter fontsizeBy750:26])];
    [updateBtn setTitleColor:[UIColor colorWithHexString:@"#1d8acb"] forState:UIControlStateNormal];
    [updateBtn addTarget:self action:@selector(updateAction) forControlEvents:UIControlEventTouchUpInside];
    [btnsView addSubview:updateBtn];
    
    [cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(btnsView).with.offset(0);
        make.top.equalTo(btnsView).with.offset(0.5);
        make.right.equalTo(updateBtn.mas_left).with.offset(-0.5);
        make.bottom.equalTo(btnsView).with.offset(0);
        make.width.equalTo(updateBtn);
    }];
    [updateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(btnsView).with.offset(0.5);
        make.right.equalTo(btnsView).with.offset(0);
        make.bottom.equalTo(btnsView).with.offset(0);
    }];
}


#pragma action
- (void)cancleAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)updateAction {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.updateUrl]];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
