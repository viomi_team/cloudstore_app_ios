//
//  YMCommodityData.h
//  yunSale
//
//  Created by ChengMinZhang on 2017/9/28.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YMCommodityData : NSObject

@property (nonatomic, strong) NSString *commodityName;
@property (nonatomic, strong) NSString *reportDate;
@property (nonatomic, assign) NSInteger quantity;
@property (nonatomic, assign) CGFloat amount;

@end
