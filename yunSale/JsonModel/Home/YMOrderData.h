//
//  YMOrderData.h
//  yunSale
//
//  Created by ChengMinZhang on 2017/8/30.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YMSkuInfo : NSObject

@property (nonatomic, assign) NSInteger skuId; //13

@property (nonatomic, assign) NSInteger quantity; //1

@property (nonatomic, assign) float paymentPrice; //299

@property (nonatomic, assign) float originalPrice; //299

@property (nonatomic, assign) NSInteger sumPrice; //299

@property (nonatomic, strong) NSString *name; //云米超能滤水壶L1（UV杀菌版）

@property (nonatomic, strong) NSString *imgUrl;

@property (nonatomic, assign) NSInteger type; //1

@property (nonatomic, strong) NSString *url;

@end

@interface YMOrderData : NSObject

@property (nonatomic, strong) NSString *orderCode; //398526746

@property (nonatomic, strong) NSNumber *createdTime; //1503892500000

@property (nonatomic, strong) NSString *dealPhaseDesc; //已支付

@property (nonatomic, strong) NSString *linkmanName; //l**

@property (nonatomic, strong) NSString *linkmanPhone; //138*****755

@property (nonatomic, strong) NSString *fullDivisionName; //河北省 秦皇岛市 山海关区

@property (nonatomic, assign) float price; //15783

@property (nonatomic, assign) float paymentPrice; //15783

@property (nonatomic, strong) NSString *payStatusDesc; //已支付

@property (nonatomic, strong) NSString *payTypeDesc; //app支付宝支付

@property (nonatomic, strong) NSString *sourceChannelDesc; //商城app ANDROID版

@property (nonatomic, copy) NSArray<YMSkuInfo *> *skuInfoList;

@property (nonatomic, strong) NSString *settleStatesDesc; //待结算

@property (nonatomic, strong) NSString *invoiceTypeDesc; //普通发票

/*城市运营、区域管理员等专有字段*/
@property (nonatomic, strong) NSArray<YMSkuInfo *> *skuImgs;

@end
