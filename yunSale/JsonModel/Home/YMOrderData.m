//
//  YMOrderData.m
//  yunSale
//
//  Created by ChengMinZhang on 2017/8/30.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import "YMOrderData.h"

@implementation YMSkuInfo

@end

@implementation YMOrderData

// 这个方法对比上面的2个方法更加没有侵入性和污染，因为不需要导入头文件
+ (NSDictionary *)objectClassInArray{
    return @{@"skuInfoList" : @"YMSkuInfo",
             @"skuImgs":@"YMSkuInfo"};
}

@end
