//
//  YMStore.h
//  yunSale
//
//  Created by liushilou on 16/11/17.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YMStore : NSObject

//名称
@property (nonatomic,strong) NSString *name;

//订单数量
@property (nonatomic,assign) NSInteger orderCount;

//销售额-float类型（服务器返回0.38，精度会丢失成为0.50）
@property (nonatomic,assign) CGFloat salesMoney;

//销售额-string类型（直接使用string，保证精度）
@property (nonatomic, strong) NSString *saleMoneyString;

@end
