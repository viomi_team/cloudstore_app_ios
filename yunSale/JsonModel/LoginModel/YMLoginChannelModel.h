//
//  YMLoginChannelModel.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/8.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YMLoginChannelModel : NSObject

//
@property (nonatomic, assign) NSInteger channelId;
//
@property (nonatomic, assign) NSInteger parentId;
//
@property (nonatomic, strong) NSString *parentName;
//
@property (nonatomic, assign) NSInteger code;
//
@property (nonatomic, strong) NSString *name;
//
@property (nonatomic, assign) NSInteger type;
//
@property (nonatomic, assign) NSInteger category;
//分级代号
@property (nonatomic, strong) NSString *divisionCode;
//分级名称
@property (nonatomic, strong) NSString *divisionName;
//
@property (nonatomic, strong) NSString *address;
//
@property (nonatomic, strong) NSString *phone;
//
@property (nonatomic, assign) NSInteger channelLevel;
//
@property (nonatomic, assign) NSInteger templateId;
//
@property (nonatomic, strong) NSString *templateName;
//
@property (nonatomic, strong) NSString *contactName;
//
@property (nonatomic, strong) NSString *contactMobile;
//
@property (nonatomic, assign) NSInteger status;
//
@property (nonatomic, assign) NSInteger approveStatus;
//
@property (nonatomic, strong) NSString *statusDesc;
//
@property (nonatomic, strong) NSString *approveStatusDesc;
//
@property (nonatomic, assign) NSInteger staffNum;

@end
