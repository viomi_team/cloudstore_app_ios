//
//  YMLoginChannelModel.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/8.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMLoginChannelModel.h"

@implementation YMLoginChannelModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    
    return @{
             @"channelId" : @"id"
             };
}

@end
