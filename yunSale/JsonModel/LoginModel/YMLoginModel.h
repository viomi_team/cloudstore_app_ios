//
//  YMLoginModel.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/8.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YMLoginUserModel.h"
#import "YMLoginChannelModel.h"

@interface YMLoginModel : NSObject

//状态码
@property (nonatomic, assign) NSInteger code;
//描述
@property (nonatomic, strong) NSString *desc;
//
@property (nonatomic, strong) NSString *requestUri;
//
@property (nonatomic, strong) NSString *token;
//当前时间
@property (nonatomic, assign) NSInteger now;
//用户信息
@property (nonatomic, strong) YMLoginUserModel *user;
//角色
@property (nonatomic, strong) NSArray *roles;
//渠道
@property (nonatomic, strong) YMLoginChannelModel *channel;
//
@property (nonatomic, strong) NSString *special;

@end
