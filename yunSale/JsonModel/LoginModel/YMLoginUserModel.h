//
//  YMLoginUserModel.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/8.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YMLoginUserModel : NSObject

//
@property (nonatomic, assign) NSInteger userid;
//
@property (nonatomic, strong) NSString *name;
//
@property (nonatomic, strong) NSString *account;
//
@property (nonatomic, assign) NSInteger status;
//
@property (nonatomic, strong) NSString *pwd;
//
@property (nonatomic, strong) NSString *phone;
//
@property (nonatomic, strong) NSString *email;
//
@property (nonatomic, assign) NSInteger createdTime;
//
@property (nonatomic, assign) NSInteger updatedTime;
//
@property (nonatomic, strong) NSString *creator;
//
@property (nonatomic, assign) NSInteger roleId;

@end
