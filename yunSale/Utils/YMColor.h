//
//  YMColor.h
//  yunSale
//
//  Created by liushilou on 16/11/1.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#ifndef YMColor_h
#define YMColor_h

//线条颜色
#define YMCOLOR_LINE @"#E5E5E5"

//线条颜色 暗
#define YMCOLOR_LINE_DARK @"#E1E1E1"

//主题色
#define YMCOLOR_THEME @"#34495e"

//表格背景色
#define YMCOLOR_TABLE_BACKGROUND @"#f5f5f5"

//黑色字体
#define YMCOLOR_BLACK @"#333333"

//深灰色字体
#define YMCOLOR_DARK_GRAY @"#666666"

//浅色字体
#define YMCOLOR_GRAY @"#999999"

//浅色字体
#define YMCOLOR_LIGHT_GRAY @"#b4b4b4"

//红色
#define YMCOLOR_RED @"#ff4d1c"

//绿色
#define YMCOLOR_GREEN @"#72c156"

//蓝色
#define YMCOLOR_BLUE @"#1d8acb"


#endif /* YMColor_h */
