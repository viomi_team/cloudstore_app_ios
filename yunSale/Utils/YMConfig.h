//
//  YMConfig.h
//  yunSale
//
//  Created by liushilou on 16/11/1.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#ifndef YMConfig_h
#define YMConfig_h


//app 测试环境和正式环境切换:1正式环境 2测试环境
#define YM_ENVIRONMENT 1

//1080182980
#define YM_APPID @"1195746463"


#ifdef DEBUG
#define NSLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define NSLog(...);
#endif


#define YM_FONT(s) [UIFont systemFontOfSize:s]
//[UIFont fontWithName:@"DINCond-Medium" size:s]




#endif /* YMConfig_h */
