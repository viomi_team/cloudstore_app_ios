//
//  YMDate.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/17.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, YMDateStringFormat) {
    YMDateStringFormat_YMD,
    YMDateStringFormat_YMDHM
};

//（by 谢立颖 2017，1，13）
typedef NS_ENUM(NSInteger, YMDateType) {
    YMDateType_Integer,     //返回阿拉伯数字类型
    YMDateType_String       //返回中文类型
};

@interface YMDate : NSObject

//获取当前时区的时间
+ (NSDate *)dateNow;

//传入的timeInteger是UTC时间，需要转换成中国的+8时区再传回去
+ (NSString *)stringForDisplayFromInteger:(NSInteger)timeInteger withFormat:(YMDateStringFormat)format;

//传入date，没有加入时区转换，若需要时区转换，则先用下面的dateToZoneEightWithDate:方法转换
+ (NSString *)stringForDislayFromDate:(NSDate *)date withFormat:(YMDateStringFormat)format;

//把标准时转换成中国的+8时区
+ (NSDate *)dateToZoneEightWithDate:(NSDate *)dateUTC;

+ (NSDate *)dateWithFormatString:(NSString *)dateString;

+ (NSString *)stringwithType:(NSInteger)type beginDate:(NSNumber *)beginDate endDate:(NSNumber *)endDate formatter:(NSDateFormatter *)formatter index:(NSInteger)index yearType:(NSInteger)yearType;

//返回
+ (NSString *)yearOfDate:(NSDate *)date;
+ (NSString *)monthOfDate:(NSDate *)date withType:(YMDateType)type;
+ (NSString *)dayOfDate:(NSDate *)date;

@end
