//
//  YMDate.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/17.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMDate.h"

@implementation YMDate

+ (NSDate *)dateNow
{
    return [YMDate dateToZoneEightWithDate:[NSDate date]];
}

//传入的timeInteger是UTC时间，需要转换成中国的+8时区再传出去
+ (NSString *)stringForDisplayFromInteger:(NSInteger)timeInteger withFormat:(YMDateStringFormat)format
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(long)timeInteger/1000];
    
    NSTimeZone *sourchTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSTimeZone *destinationTimeZone = [NSTimeZone localTimeZone];
    NSInteger sourceGMTOffset = [sourchTimeZone secondsFromGMTForDate:date];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:date];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *destinationDateNow = [[NSDate alloc] initWithTimeInterval:interval sinceDate:date];
    
    return [YMDate stringForDislayFromDate:destinationDateNow withFormat:format];
}

+ (NSString *)stringForDislayFromDate:(NSDate *)date withFormat:(YMDateStringFormat)format
{
    NSString *originalDateString = [NSString stringWithFormat:@"%@", date];
    switch (format) {
            case YMDateStringFormat_YMD:
        {
            return [originalDateString substringWithRange:NSMakeRange(0, 10)];
        }
            break;
            
            case YMDateStringFormat_YMDHM:
        {
            return [originalDateString substringWithRange:NSMakeRange(0, 16)];
        }
            break;
    }
}


//标准时转为中国的+8时区
+ (NSDate *)dateToZoneEightWithDate:(NSDate *)dateUTC
{
    NSTimeZone *sourchTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSTimeZone *destinationTimeZone = [NSTimeZone localTimeZone];
    NSInteger sourceGMTOffset = [sourchTimeZone secondsFromGMTForDate:dateUTC];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:dateUTC];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *destinationDateNow = [[NSDate alloc] initWithTimeInterval:interval sinceDate:dateUTC];
    return destinationDateNow;
}


+ (NSDate *)dateWithFormatString:(NSString *)dateString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"]];
    return [formatter dateFromString:dateString];
}

+ (NSString *)stringwithType:(NSInteger)type beginDate:(NSNumber *)beginDate endDate:(NSNumber *)endDate formatter:(NSDateFormatter *)formatter index:(NSInteger)index yearType:(NSInteger)yearType
{
    NSString *beginDateString = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:beginDate.longLongValue/1000]];
    NSString *endDateString = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:endDate.longLongValue/1000]];
    
    NSString *name = @"";
    if (type == 1) {
//        NSString *yesterdayString = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[[NSDate date] timeIntervalSince1970] - 24 * 60 *60]];
//        if ([yesterdayString isEqualToString:beginDateString]) {
        if (index == 0) {
            name = @"昨天";
        }else{
            name = beginDateString;
        }
    }else{
        if (type == 3) {
            //月
            NSString *month = [YMDate monthOfDate:[NSDate dateWithTimeIntervalSince1970:endDate.longLongValue/1000] withType:YMDateType_String];
            NSString *year = [YMDate yearOfDate:[NSDate dateWithTimeIntervalSince1970:endDate.longLongValue/1000]];
            
//            NSString *cMonth = [YMDate yearOfDate:[NSDate date]];
//            NSString *cYear = [YMDate yearOfDate:[NSDate date]];
//            
//            if (([month isEqualToString:cMonth] && [year isEqualToString:cYear]) || [month isEqualToString:@"十二月"]) {
            if ((index == 0 || [month isEqualToString:@"十二月"]) && yearType == 2) {
                name = [NSString stringWithFormat:@"%@\n%@",month,year];
            }else{
                
                //若不是首尾月，则当前月份显示成“本月”（by 谢立颖 2017,1,13） （暂时不修改这个问题了）
//                NSString *presentMonth = [YMDate monthOfDate:[NSDate date] withType:YMDateType_String];
//                if (![presentMonth isEqualToString:month]) {
//                    month = @"本月";
//                }
                
                if (yearType == 2) {
                    name = [NSString stringWithFormat:@"%@\n ",month];
                }else{
                    name = month;
                }
                
            }
        }else{
            //周
            if (index == 0) {
                name = @"本周";
            }else{
                name = [NSString stringWithFormat:@"%@-\n%@",beginDateString,endDateString];
            }
        }
    }
    return name;
}


+ (NSString *)yearOfDate:(NSDate *)date {
    NSCalendar *calendar = [YMDate currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
    NSInteger year = [components year];

    return @(year).stringValue;
}

+ (NSString *)monthOfDate:(NSDate *)date withType:(YMDateType)type {
    NSCalendar *calendar = [YMDate currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
    NSInteger month = [components month];
    
    if (type == YMDateType_Integer) {
        return @(month).stringValue;
    }
    
    NSString *monthString = @"";
    switch (month) {
        case 1:
            monthString = @"一月";
            break;
        case 2:
            monthString = @"二月";
            break;
        case 3:
            monthString = @"三月";
            break;
        case 4:
            monthString = @"四月";
            break;
        case 5:
            monthString = @"五月";
            break;
        case 6:
            monthString = @"六月";
            break;
        case 7:
            monthString = @"七月";
            break;
        case 8:
            monthString = @"八月";
            break;
        case 9:
            monthString = @"九月";
            break;
        case 10:
            monthString = @"十月";
            break;
        case 11:
            monthString = @"十一月";
            break;
        case 12:
            monthString = @"十二月";
            break;
        default:
            break;
    }
    return monthString;
}

+ (NSString *)dayOfDate:(NSDate *)date{
    NSCalendar *calendar = [YMDate currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
    NSInteger day = [components day];
//    
//    if (type == YMDateType_String) {
//        NSString *dayString = @"";
//        switch (day) {
//            case 1:
//                dayString = @"一";
//                break;
//            case 2:
//                dayString = @"二";
//                break;
//            case 3:
//                dayString = @"三";
//                break;
//            case 4:
//                dayString = @"四";
//                break;
//            case 5:
//                dayString = @"五";
//                break;
//            case 6:
//                dayString = @"六";
//                break;
//            case 7:
//                dayString = @"七";
//                break;
//            case 8:
//                dayString = @"八";
//                break;
//            case 9:
//                dayString = @"九";
//                break;
//            case 10:
//                dayString = @"十";
//                break;
//
//    }
    return @(day).stringValue;
}


#pragma mark - 日历获取在9.x之后的系统使用currentCalendar会出异常。在8.0之后使用系统新API。
+ (NSCalendar *)currentCalendar {
    if ([NSCalendar respondsToSelector:@selector(calendarWithIdentifier:)]) {
        return [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    }
    return [NSCalendar currentCalendar];
}


@end
