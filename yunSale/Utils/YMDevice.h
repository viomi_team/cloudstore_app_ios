//
//  YMDevice.h
//  yunSale
//
//  Created by 谢立颖 on 2017/1/16.
//  Copyright © 2017年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, DeviceScreenSizeType) {
    iPhone4,
    iPhone5,
    iPhone6,
    iPhone6p
};

@interface YMDevice : NSObject

/**
 获取设备型号
 
 @return 设备名称
 */
+ (NSString *)deviceModelName;


/**
 获取设备屏幕尺寸类型
 
 @return 设备屏幕尺寸类型
 */
+ (DeviceScreenSizeType)deviceScreenSizeType;


@end
