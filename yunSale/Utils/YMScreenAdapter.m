//
//  YMScreenAdapter.m
//  WaterPurifier
//
//  Created by liushilou on 16/5/19.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "YMScreenAdapter.h"


@implementation YMScreenAdapter



//在640的基础上计算当前机型（6、plus）的尺寸
+ (CGFloat)sizeBy640:(CGFloat)size
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    return (width * size)/640;
}

//在1080的基础上计算当前机型的尺寸 (界面尺寸、字体尺寸都OK)
+ (CGFloat)sizeBy1080:(CGFloat)size
{
  CGFloat width = [UIScreen mainScreen].bounds.size.width;
  return (width * size)/1080;
}

//在750的基础上计算当前机型的尺寸 (界面尺寸、字体尺寸都OK)
+ (CGFloat)sizeBy750:(CGFloat)size
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat value = (width * size)/750;
//    if (width == 320) {
//        value = value * 1.1;
//    }
    return round(value);
}

//由于tableview存在小数问题，返回整数
+ (CGFloat)intergerSizeBy750:(CGFloat)size
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat value = round((width * size)/750);
    return value;
}

+ (CGFloat)fontsizeBy750:(CGFloat)size
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat value = (width * size)/750;
    if (width == 320) {
        value = value * 1.2;
    }
    
    return round(value);
}




+ (CGFloat)fontsizeBy1080:(CGFloat)size
{
  CGFloat width = [UIScreen mainScreen].bounds.size.width;
  if (width == 320) {
    return ((width * size)/1080)*1.2;
  }else{
    return (width * size)/1080;
  }
}


+ (CGFloat)contentHeight:(CGFloat)navheight
{
    
    CGFloat statusheight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    CGFloat windowheight = [UIScreen mainScreen].bounds.size.height;
    
    CGFloat height = windowheight - navheight - statusheight;
    return height;
}

@end
