//
//  YMSecurity.m
//  WaterPurifier
//
//  Created by liushilou on 16/7/26.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "YMSecurity.h"
#import <CommonCrypto/CommonDigest.h>

@implementation YMSecurity

+ (NSString *)md5:(NSString *)str
{
  const char *cStr = [str UTF8String];
  unsigned char digest[CC_MD5_DIGEST_LENGTH];
  CC_MD5( cStr, str.length, digest );
  NSMutableString *result = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
  for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
    [result appendFormat:@"%02x", digest[i]];
  return result;
}

@end
