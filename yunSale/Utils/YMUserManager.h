//
//  YMUserManager.h
//  yunSale
//
//  Created by 谢立颖 on 2016/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YMLoginModel.h"


@interface YMUserManager : NSObject

@property (nonatomic,copy) NSString *account;
@property (nonatomic,copy) NSString *password;

@property (nonatomic,copy) NSString *token;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,strong) NSNumber *userId;
@property (nonatomic,strong) NSNumber *roleId;
@property (nonatomic,copy) NSString *phone;
@property (nonatomic,copy) NSString *email;

@property (nonatomic,strong) NSNumber *channelId;
@property (nonatomic,copy) NSString *channelName;
@property (nonatomic,copy) NSString *channelAdress;

+ (YMUserManager*)shareinstance;

- (BOOL)islogin;

//保存用户名和密码
- (void)saveUserAccount:(NSString *)account password:(NSString *)password;

//保存用户数据
- (void)saveUser:(YMLoginModel *)user;

//退出登录
- (void)logout;

//获取角色name
- (NSString *)roleName;

@end
