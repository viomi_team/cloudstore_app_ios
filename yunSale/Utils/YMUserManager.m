//
//  YMUserManager.m
//  yunSale
//
//  Created by 谢立颖 on 2016/11/9.
//  Copyright © 2016年 yunmi. All rights reserved.
//

#import "YMUserManager.h"

static NSString * const kYMUserAccount = @"ym_user_account";
static NSString * const kYMUserPassword = @"ym_user_password";

static NSString * const kYMUserToken = @"ym_user_token";
static NSString * const kYMUserName = @"ym_user_name";
static NSString * const kYMUserId = @"ym_user_id";
static NSString * const kYMUserRoleId = @"ym_user_roleid";
static NSString * const kYMUserPhone = @"ym_user_phone";
static NSString * const kYMUserEmail = @"ym_user_email";

//channel
static NSString * const kYMUserChannelName = @"ym_user_channel_name";
static NSString * const kYMUserChannelId = @"ym_user_channel_id";
static NSString * const kYMUserChannelAdress = @"ym_user_channel_adress";


@implementation YMUserManager


+ (YMUserManager*)shareinstance
{
    static YMUserManager *shareinstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareinstance = [[self alloc] init];
    });
    return shareinstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        _account = [defaults objectForKey:kYMUserAccount];
        _password = [defaults objectForKey:kYMUserPassword];
        
        _token = [defaults objectForKey:kYMUserToken];
        _name = [defaults objectForKey:kYMUserName];
        _userId = [defaults objectForKey:kYMUserId];
        _roleId = [defaults objectForKey:kYMUserRoleId];
        _phone = [defaults objectForKey:kYMUserPhone];
        _email = [defaults objectForKey:kYMUserEmail];
        
        _channelId = [defaults objectForKey:kYMUserChannelId];
        _channelName = [defaults objectForKey:kYMUserChannelName];
        _channelAdress = [defaults objectForKey:kYMUserChannelAdress];
        
        NSLog(@"user account:%@,password:%@,token:%@,name:%@,userId:%@,roleId:%@,phone:%@,email:%@,channelId:%@,channelName:%@,channelAdress:%@",_account,_password,_token,_name,_userId,_roleId,_phone,_email,_channelId,_channelName,_channelAdress);
    }
    return self;
}

- (BOOL)islogin {
    NSLog(@"token:%@",self.token);
    
    if (!self.token || [self.token isEqualToString:@""]) {
        return NO;
    }else{
        return YES;
    }
}

//保存用户名和密码
- (void)saveUserAccount:(NSString *)account password:(NSString *)password
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (account) {
        self.account = account;
        [defaults setObject:account forKey:kYMUserAccount];
    }
    if (password) {
        self.password = password;
        [defaults setObject:password forKey:kYMUserPassword];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)saveUser:(YMLoginModel *)user {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.token = user.token;
    if (user.token) {
        [defaults setObject:user.token forKey:kYMUserToken];
    }
    self.name = user.user.name;
    if (user.user.name) {
        
        [defaults setObject:user.user.name forKey:kYMUserName];
    }
    self.userId = @(user.user.userid);
    if (user.user.userid) {
        
        [defaults setObject:@(user.user.userid) forKey:kYMUserId];
    }
    self.roleId = @(user.user.roleId);
    if (user.user.roleId) {
        
        [defaults setObject:@(user.user.roleId) forKey:kYMUserRoleId];
    }
    self.phone = user.user.phone;
    if (user.user.phone) {
        
        [defaults setObject:user.user.phone forKey:kYMUserPhone];
    }
    self.email = user.user.email;
    if (user.user.email) {
        
        [defaults setObject:user.user.email forKey:kYMUserEmail];
    }
    self.channelId = @(user.channel.channelId);
    if (user.channel.channelId) {
        
        [defaults setObject:@(user.channel.channelId) forKey:kYMUserChannelId];
    }
    self.channelName = user.channel.name;
    if (user.channel.name) {
        
        [defaults setObject:user.channel.name forKey:kYMUserChannelName];
    }
    self.channelAdress = user.channel.address;
    if (user.channel.address) {
        
        [defaults setObject:user.channel.address forKey:kYMUserChannelAdress];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//退出登录
- (void)logout {
    
    NSLog(@"logout");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:kYMUserAccount];
    [defaults removeObjectForKey:kYMUserPassword];
    [defaults removeObjectForKey:kYMUserToken];
    [defaults removeObjectForKey:kYMUserName];
    [defaults removeObjectForKey:kYMUserId];
    [defaults removeObjectForKey:kYMUserRoleId];
    [defaults removeObjectForKey:kYMUserPhone];
    [defaults removeObjectForKey:kYMUserEmail];
    
    [defaults removeObjectForKey:kYMUserChannelId];
    [defaults removeObjectForKey:kYMUserChannelName];
    [defaults removeObjectForKey:kYMUserChannelAdress];
    
    [defaults synchronize];
}

//获取角色name
- (NSString *)roleName {
    
    NSString *name = nil;
    switch (self.roleId.integerValue) {
        case 1:
            name = @"超级管理员";
            break;
        case 1001:
            name = @"区域管理员";
            break;
        case 21:
            //name = @"店长";
            name = @"城市运营";
            break;
        case 22:
            name = @"收银员";
            break;
        case 23:
            name = @"促销员";
            break;
        case 24:
            name = @"虚拟店长";
            break;
        case 25:
            name = @"兼职促销员";
            break;
        case 30:
            name = @"客服人员";
            break;
        case 40:
            name = @"库存管理员";
            break;
        case 41:
            name = @"库存审核员";
            break;
        case 50:
            name = @"财务结算人员";
            break;
        case 1000:
            name = @"销售数据报表角色";
            break;
        case 2001:
//            name = @"城市运营";
            name = @"店长";
            break;
        case 2002:
            name = @"经销商";
            break;
        case 2003:
            name = @"门店";
            break;
        case 2004:
            name = @"KA";
            break;
        case 2005:
            name = @"区域KA";
            break;
        case 2006:
            name = @"KA门店";
            break;
        default:
            name = @"未知";
            break;
    }

    return name;
}

@end
